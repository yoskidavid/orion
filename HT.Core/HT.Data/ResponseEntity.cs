﻿using System;
using System.Runtime.Serialization;

namespace HT.Data
{
    [DataContract]
    public class ResponseEntity : Response, IResponseEntity
    {
        public ResponseEntity(object body) :
            base(null)
        {
            Body = body;
        }

        public ResponseEntity(Exception exception) :
            base(exception)
        { }

        [DataMember]
        public object Body { get; private set; }
    }
}
