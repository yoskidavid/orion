﻿using HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model;
using HT.Shared.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Logic
{
    public interface IOrderReadyToDispatchRepository
    {
        Task<ParameterResponseModel> GetAsync(string code);
       
        Task<ExistsOrderResponseModel> ExistsOrder(string order);

        Task<IEnumerable<UbigeoResponseModel>> GetUbigeos();

        Task<IEnumerable<TablaGeneralResponseModel>> GetDataFromTablaGeneral(int pFLAG, string pCOD_TABLA);

        Task<ParameterResponseModel> GetParameterValue(string parametro);
        Task<Int32> SaveInOrderQueue(OrderQueueModel orderQueue);
        Task<int> SaveInLog(LogApiModel logApiModel);
        
    }
}
