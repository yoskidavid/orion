﻿using System;
using System.Runtime.Serialization;

namespace HT.Data
{
    [DataContract]
    public class Request : IRequest
    {
        public Request(Type type)
        {
            InnerType = type;
        }

        [DataMember]
        public object Body
        {
            get
            {
                EnsureInnerBody();

                return InnerBody;
            }
        }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }
        
        private object InnerBody { get; set; }

        private Type InnerType { get; set; }

        private void EnsureInnerBody()
        {
            if (Equals(InnerBody, null))
            {
                InnerBody = Activator.CreateInstance(InnerType);
            }
        }
    }
}
