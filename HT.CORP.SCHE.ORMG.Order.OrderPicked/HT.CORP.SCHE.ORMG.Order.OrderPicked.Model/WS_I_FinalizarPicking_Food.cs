﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[WS_I_FINALIZARPICKING_FOOD]")]
    public class WS_I_FinalizarPicking_Food : ModelChild<OrderRequestModel>
    {
        public WS_I_FinalizarPicking_Food()
            : this(null)
        { }

        public WS_I_FinalizarPicking_Food(OrderRequestModel parent)
            : base(parent)
        { }

        [IgnoreDataMember]
        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [IgnoreDataMember]
        [DbParameter("@TIENDA", DbType = DbType.Int32)]
        public int StoreID => Parent.Client.StoreID;

        [IgnoreDataMember]
        [DbParameter("@ID_USUARIO", DbType = DbType.Int32)]
        public int UserID => int.Parse(Parent.UserID);

        [IgnoreDataMember]
        [DbParameter("@COD_EQUIPO", DbType = DbType.String)]
        public string TeamCode => string.Empty;

        [IgnoreDataMember]
        [DbParameter("@IDACTIVIDAD", DbType = DbType.Int32)]
        public int ActivityID => 0;

        [DataMember]
        [DbParameter("@FLAG", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int Flag { get; set; }
    }
}
