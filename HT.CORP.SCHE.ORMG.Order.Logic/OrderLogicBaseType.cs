﻿using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;

namespace HT.Order.Logic
{
    public abstract class OrderLogicBaseType : BaseType
    {
        protected OrderLogicBaseType(ILogger logger, IDatabase<DbDispatch> dbDispatch) :
            this(null, logger, dbDispatch)
        { }

        protected OrderLogicBaseType(IServiceProvider services, ILogger logger, IDatabase<DbDispatch> dbDispatch) :
            base(services, logger)
        {
            this.DbDispatch = dbDispatch;
        }

        public IDatabase DbDispatch
        {
            get;
            private set;
        }
    }
}