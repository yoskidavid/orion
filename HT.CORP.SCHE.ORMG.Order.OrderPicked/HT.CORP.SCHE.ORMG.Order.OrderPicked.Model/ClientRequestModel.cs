﻿using HT.Data.Db;
using HT.Data.Generic;
using System;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
	[DataContract(Name = "Client")]
	[DbCommand("[ORD_INSERTCLIENTE_F]")]
	public class ClientRequestModel : ModelChild<OrderRequestModel>
	{
		public ClientRequestModel()
			: this(null)
		{ }

		public ClientRequestModel(OrderRequestModel parent)
			: base(parent)
		{ }

		[IgnoreDataMember]
		[DbParameter("@PIV_TIPOCLIENTE", DbType = DbType.String, Size = 25)]
		public string CustomerType { get => "NORMAL"; }
		
		[DataMember]
		[DbParameter("@PIV_TIPODOCUMENTO", DbType = DbType.String, Size = 15)]
		public string CustomerDocumentType { get; set; }

		[DataMember]
		[DbParameter("@PIV_RUTCLIENTE", DbType = DbType.String, Size = 15)]
		public string CustomerDocumentNumber { get; set; }

		[DataMember]
		[DbParameter("@PIV_NOMBRES", DbType = DbType.String, Size = 50)]
		public string CustomerFullName { get; set; }

        [IgnoreDataMember] 
        [DbParameter("@PIV_TELEFONO", DbType = DbType.String, Size = 15)]
		public string CustomerPhone2 { get { return CustomerPhone ?? ""; }  }

        [DataMember(Name = "CustomerPhone")]
        public string CustomerPhone { get; set; }

        [DataMember]
		[DbParameter("@PIV_CELULAR", DbType = DbType.String, Size = 15)]
		public string CustomerMobile { get; set; }

		[DataMember(Name = "customerEmail")]
		[DbParameter("@PIV_CORREO", DbType = DbType.String, Size = 100)]
		public string Email { get; set; }

		// CONSULTAR EN LA BASE DE DATOS
		[IgnoreDataMember]
		[DbParameter("@PII_CODIGOPAIS", DbType = DbType.Int32)]
		public int CountryID { get; set; }

		[DataMember]
		[DbParameter("@PIV_DESC_PAIS", DbType = DbType.String, Size = 30)]
		public string CountryName { get; set; }

		// CONSULTAR EN LA BASE DE DATOS
		[IgnoreDataMember]
		[DbParameter("@PII_CODIGODEPARTAMENTO", DbType = DbType.Int32)]
		public string DepartmentID { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PIV_DESC_DEPARTAMENTO", DbType = DbType.String, Size = 30)]
		public string DepartmentName { get; set; }

		// CONSULTAR EN LA BASE DE DATOS
		[IgnoreDataMember]
		[DbParameter("@PII_CODIGOPROVINCIA", DbType = DbType.Int32)]
		public string ProvinceID { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PIV_DESC_PROVINCIA", DbType = DbType.String, Size = 30)]
		public string ProvinceName { get; set; }

        [DataMember(Name = "comunaId")]
        [DbParameter("@PII_CODIGODISTRITO", DbType = DbType.Int32)]
		public string ComunaID { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PIV_DESC_DISTRITO", DbType = DbType.String, Size = 30)]
		public string ComunaName { get; set; }

		[IgnoreDataMember]
		[DbParameter("@PIV_CODIGOPOSTAL", DbType = DbType.String, Size = 25)]
		public string PostalCode { get => "00000"; }
		
		[DataMember]
		[DbParameter("@PIV_DIRECCION", DbType = DbType.String, Size = 75)]
		public string DispatchAddress { get; set; }

		[IgnoreDataMember]
		public string DispatchObservation => DeliveryObservation;

		[DataMember]
		public string DeliveryObservation { get; set; }

		[DataMember]
		public string DispatchReference { get; set; }

		[DataMember]
		public string ReceptorFullName { get; set; }

		[DataMember]
		public string ReceptorDocumentNumber { get; set; }

		[DataMember]
		public string SellerCode { get; set; }
		
		[DataMember]
		public string HourDispatch { get; set; }

		[DataMember]
		public DateTime DateDispatch { get; set; }

		//[DataMember]
		//public string SaleType { get; set; }

		[IgnoreDataMember]
		public string DiscountCoupon { get => string.Empty; }

		// Default: 114
		[DataMember(Name = "storeId")]
		public int StoreID { get; set; }

		[DataMember(Name = "ruc")]
		public string CompanyIdentity { get; set; }

		[DataMember]
		public string CompanyName { get; set; }

		// 1: Despacho a domicilio
		// 2: Retiro en Tienda
		// 3: Despacho Express
		// 4: Despacho a Punto de Retiro
		[DataMember]
		public string ServiceType { get; set; }
		
		[DataMember]
		public string PickUpPoint { get; set; }

		[IgnoreDataMember]
		[DbParameter("@PIV_COMUNA", DbType = DbType.String, Size = 25)]
		public string ComunaName2 { get => ComunaName; }

		[IgnoreDataMember]
		[DbParameter("@PIV_CIUDAD", DbType = DbType.String, Size = 25)]
		public string ProvinceName2 { get => ProvinceName; }

		[DataMember]
		public double Latitude { get; set; }

		[DataMember]
		public double Longitude { get; set; }

        [DataMember]       
        public bool Audit_flag { get; set; }

        [DataMember] 
        public bool Reset_flag { get; set; }


    }
}
