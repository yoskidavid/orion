﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DbCommand("[ORD_INSERTORDENCOLAMASIVO]")]
    public class InsertOrderQueueRequestModel : ModelChild<OrderRequestModel>
    {
        public InsertOrderQueueRequestModel()
            : this(null)
        { }

        public InsertOrderQueueRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@PIV_NUMEROORDEN", DbType = DbType.String)]
        public string OrderNumber => Parent.OrderNumber;

        [DbParameter("@PII_ID_USUARIO", DbType = DbType.Int32)]
        public int? UserID
        {
            get
            {
                int? userId;
                string parentUserId = Parent.UserID.Trim();

                if (string.IsNullOrEmpty(parentUserId))
                {
                    userId = null;
                }
                else
                {
                    userId = int.Parse(parentUserId);
                }

                return userId;
            }
        }

        [DbParameter("@PII_IDTG_TIPO", DbType = DbType.Int32)]
        public int OrderType => Parent.OrderTypeID;

        [DbParameter("@VAL", DbType = DbType.Int32)]
        public int Value => 1;
    }
}
