﻿using HT.CORP.SCHE.ORMG.Order.OrderPicked.Model;
using HT.Shared.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Logic
{
    public interface IOrderPickedRepository
    {
        Task<ParameterResponseModel> GetAsync(string code);

        Task<IEnumerable<HomologaLocalResponseModel>> GetLocalsAsync();

        Task<ExistsOrderResponseModel> ExistsOrder(string order);
        //Task<GetFlagPostVirtualResponseModel> GetFlagPostVirtual(int order);

        Task<OrderFlagPOSModelResponse> GetFlagPostVirtual(string numOrden);

        Task<WS_SOL_ValidarSolicitudOC_PICKING> ValidaTablaSolicitudOC(string order);

        Task<OrderDeleteModelRequest> DeleteOrder(string order);
         
        Task<IEnumerable<UbigeoResponseModel>> GetUbigeos();
         
        Task<IEnumerable<TablaGeneralResponseModel>> GetDataFromTablaGeneral(int pFLAG, string pCOD_TABLA);

        Task<ParameterResponseModel> GetParameterValue(string parametro);
        Task<Int32> SaveInOrderQueue(OrderQueueModel orderQueue);
        Task<Int32> SaveInLog(LogApiModel logApiModel);
    }
}
