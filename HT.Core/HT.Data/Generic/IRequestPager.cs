﻿namespace HT.Data.Generic
{
    public interface IRequestPager<T> : IRequestPager, IRequest<T>
    {

    }
}
