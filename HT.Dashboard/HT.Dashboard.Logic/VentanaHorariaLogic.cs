﻿using HT.Dashboard.Model.Shared;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace HT.Dashboard.Logic
{
    public class VentanaHorariaLogic : SDLogicBaseType
    {
        public VentanaHorariaLogic(IServiceProvider services, ILogger<VentanaHorariaLogic> logger, IDatabase<DbDispatch> dbDispatch)
           : base(logger, dbDispatch)
        {
        }

        public IEnumerable<VentanaHorariaResponseModel> GetVentanaHoraria()
        {
            VentanaHorariaRequestModel request = new VentanaHorariaRequestModel();
            IEnumerable<VentanaHorariaResponseModel> response = null;
            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de Extracción de Ventana Horaria ********");
                response = DbDispatch.ExecuteList<VentanaHorariaRequestModel, VentanaHorariaResponseModel>(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "ADM_GLVentanaHoraria", response);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de Extracción de Ventana Horaria ********");
                DbDispatch.EnsureDispose();
            }            
            return response;
        }
    }
}