﻿using HT.Collections;
using HT.Collections.Generic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace HT.Data.Db
{
    public class DataReaderPager<T> : DataReaderBase<T>, IPagedEnumerable<T>
    {
        private bool AllowPaging = false;

        public DataReaderPager(IDataReader reader, IRequestPager pagerSettings) :
            base(reader)
        {
            this.AllowPaging = pagerSettings.AllowPaging;
            this.PagerSettings = pagerSettings.PagerSettings;
        }

        private PagerSettings PagerSettings { get; set; }

        private Pager Pager { get; set; }

        Pager IPagedEnumerable.Pager
        {
            get => this.Pager;
        }

        public IEnumerator<T> GetEnumerator()
        {
            PagerSettings pagerSettings = this.PagerSettings;

            int pageSize = pagerSettings.PageSize;

            if (pageSize <= 0)
            {
                throw new System.Exception("The pagesize is must be greater than 0.");
            }

            var reader = this.Reader as DbDataReader;
            var pageSettings = this.PagerSettings;

            int pageIndex = pagerSettings.PageIndex,
                startRow = pageIndex * pageSize,
                endRow = startRow + pageSize - 1,
                recordsTotal = 0,
                recordsCount = 0,
                pageCount = 0;

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (object.Equals(AllowPaging, false) || recordsTotal >= startRow && recordsTotal <= endRow)
                    {
                        recordsCount++;

                        yield return reader.GetEntity<T>();
                    }

                    recordsTotal++;
                }

                decimal pageCountOriginal = recordsTotal / pageSize;

                pageCount = (int)Math.Ceiling(pageCountOriginal);
            }

            reader.Close();
            
            this.Pager = new Pager(pageIndex, pageCount, pageSize, recordsCount, recordsTotal);
        }

        IEnumerator IEnumerable.GetEnumerator() =>
            this.GetEnumerator();
    }
}
