﻿using System.Runtime.Serialization;

namespace HT
{
    [DataContract]
    public class HTException : System.Exception
    {
        public HTException() { }

        public HTException(string code, string message)
        {
            Code = code;
            Message = message;
        }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public new string Message { get; set; }
    }
}
