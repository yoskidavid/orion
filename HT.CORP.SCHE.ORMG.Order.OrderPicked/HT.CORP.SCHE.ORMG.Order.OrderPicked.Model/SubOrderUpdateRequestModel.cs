﻿using HT.Data.Db;
using HT.Data.Generic;
using Newtonsoft.Json;
using System.Data;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DbCommand("[PICK_ACTUALIZAR_SUB_ORDEN]")]
    public class SubOrderUpdateRequestModel : ModelChild<OrderRequestModel>
    {
        public SubOrderUpdateRequestModel()
            : this(null)
        { }

        public SubOrderUpdateRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@pFLAG", DbType = DbType.Int16)]
        public short Flag { get; set; }

        [DbParameter("@pIDSUB_ORDEN", DbType = DbType.Int32)]
        public int SubOrderID => 0;

        [DbParameter("@pID_ESTADO", DbType = DbType.Int32)]
        public int Status => 3;
        
        [DbParameter("@pCREADOPOR", DbType = DbType.String, Size = 25)]
        public string UserID => Parent.UserID;

        [DbParameter("@pXml", DbType = DbType.String)]
        public string Xml
        {
            get
            {
                string jsonString = JsonConvert.SerializeObject(Parent);

                return $"<order>{jsonString}</order>";
            }
        }
    }
}
