﻿using HT.Data.Db;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    public class PickRecordManualFindDetOC_PorIdOC_NewResponseModel
    {
        [DataMember(Name = "detailId")]
        [DbColumn(Name = "ID_Detalle_OC")]
        public int DetailID { get; set; }

        [DataMember]
        [DbColumn(Name = "SKU")]
        public string Sku { get; set; }

        [DataMember]
        [DbColumn(Name = "EAN")]
        public string Ean { get; set; }
    }
}
