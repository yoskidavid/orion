﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[WS_API_OC_Upd_Georeferencia]")]
    public class WS_API_updOCGeoreferenciaRequestModel : ModelChild<OrderRequestModel>
    {
        public WS_API_updOCGeoreferenciaRequestModel()
            : this(null)
        { }

        public WS_API_updOCGeoreferenciaRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [IgnoreDataMember]
        [DbParameter("@PNUMERO_ORDEN", DbType = DbType.String, Size = 50)]
        public string OrderNumber => Parent.OrderNumber;

        [IgnoreDataMember]
        [DbParameter("@PLATITUD", DbType = DbType.Double)]
        public double Latitude => Parent.Client.Latitude;

        [IgnoreDataMember]
        [DbParameter("@PLONGITUD", DbType = DbType.Double)]
        public double Longitude => Parent.Client.Longitude;

        [IgnoreDataMember]
        [DbParameter("@PUSERID", DbType = DbType.String, Size = 25)]
        public string UserID => Parent.UserID;

    }
}
