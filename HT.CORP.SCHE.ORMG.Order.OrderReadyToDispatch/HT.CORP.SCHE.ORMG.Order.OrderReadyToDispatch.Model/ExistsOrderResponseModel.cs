﻿using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    public class ExistsOrderResponseModel
    {
        [DbColumn(Order = 0, Name = "ID")]
        public int OrderID { get; set; }

        [DbColumn(Order = 1, Name = "NUMERO_ORDEN")]
        public string NumeroOrden { get; set; }
    }
}
