﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[PICK_REGISTROMANUAL_RESULTADO_IU]")]
    public class PickRecordManualResultUIRequestModel : ModelChild<ProductRequestModel>
    {
        public PickRecordManualResultUIRequestModel()
            : this(null)
        { }

        public PickRecordManualResultUIRequestModel(ProductRequestModel parent)
            : base(parent)
        { }


        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [DbParameter("@ID_Detalle_boleta", DbType = DbType.Int32)]
        public int? ReceiptDetailID => null;

        [DbParameter("@ID_Detalle_OC", DbType = DbType.Int32)]
        public int? DetailID { get; set; }
        
        [DbParameter("@ID_Criterio", DbType = DbType.Int32)]
        public int Criterian => 1;// Parent.Parent.;

        [DbParameter("@Cantidad_faltante", DbType = DbType.Decimal)]
        public decimal Missing => 0;// Parent.Parent.;

        [DbParameter("@Cantidad", DbType = DbType.Decimal)]
        public decimal Quantity => Parent.Quantity;

        [DbParameter("@Mensaje", DbType = DbType.String, Size = 200)]
        public string Message => "Sin Problemas";

        [DbParameter("@Warning", DbType = DbType.Int32)]
        public int Warning => 1;

        [DbParameter("@ID_Detalle_Picking", DbType = DbType.Int32)]
        public int DetailPickingID { get; set; }
        

        [DbParameter("@ID", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int? ID { get; set; }
    }
}
