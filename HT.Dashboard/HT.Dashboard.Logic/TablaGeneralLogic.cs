﻿using HT.Dashboard.Model.Shared;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace HT.Dashboard.Logic
{
    public class TablaGeneralLogic : SDLogicBaseType
    {
        public TablaGeneralLogic(IServiceProvider services, ILogger<TablaGeneralLogic> logger, IDatabase<DbDispatch> dbDispatch)
           : base(services, logger, dbDispatch)
        {
        }

        public IEnumerable<TablaGeneralResponse> GetList(TablaGeneralRequest request)
        {
            IEnumerable<TablaGeneralResponse> response = null;
            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de Extracción de datos de tabla general ********");
                Logger.Log(LogLevel.Information, "pFlag = {0}, pCodTabla = {1}", request.pFlag, request.pCodTabla);
                response = DbDispatch.ExecuteList<TablaGeneralRequest, TablaGeneralResponse>(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "ADM_lisTABLAGENERAL", response);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de Extracción de datos de tabla general ********");
                DbDispatch.EnsureDispose();
            }
            return response;
        }
    }
}