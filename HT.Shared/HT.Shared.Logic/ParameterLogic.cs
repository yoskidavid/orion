﻿using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using HT.Shared.Model;
using Microsoft.Extensions.Logging;
using System;

namespace HT.Shared.Logic
{
    public class ParameterLogic : BaseType
    {
        public ParameterLogic(IServiceProvider services, ILogger<ParameterLogic> logger, IDatabase<DbDispatch> dbDispatch)
            : base(services, logger)
        {
            DbDispatch = dbDispatch;
        }

        protected IDatabase DbDispatch{ get; set; }

        public IResponseEntity<ParameterResponseModel> Get(string code)
        {
            var request = new ParameterRequestModel()
            {
                Code = code
            };

            return Get(request);
        }

        public IResponseEntity<ParameterResponseModel> Get(ParameterRequestModel request)
        {
            return DbDispatch.ExecuteEntity<ParameterRequestModel, ParameterResponseModel>(request);
        }

        public IResponseEntity<ParameterResponseModel> GetCountry()
        {
            return Get("CODIGOPAIS");
        }

        public IResponseEntity<ParameterResponseModel> GetCreateFolioATG()
        {
            return Get("FOLCREAATG");
        }
    }
}
