﻿using HT.Data.Db;
using System;
using System.Data;
using System.Runtime.Serialization;

namespace HT.Order.Model
{
    [DataContract]
    [DbCommand("WS_SOL_insCOMPROBANTE")]
    public class ComprobanteInsertarRequestModel
    {
        [DataMember(Order = 0)]
        [DbParameter("@pID_COMPROBANTE", DbType = DbType.Int32)]
        public int IdComprobante { get; set; }

        [DataMember(Order = 1)]
        [DbParameter("@pID_ORDEN_COMPRA_DS", DbType = DbType.Int32)]
        public int IdOrdenComprobanteDS { get; set; }

        [DataMember(Order = 2)]
        [DbParameter("@pNUMERO_ORDEN_COMPRA", DbType = DbType.String)]
        public string NumeroOrdenCompra { get; set; }

        [DataMember(Order = 3)]
        [DbParameter("@pESTADO_COMPROBANTE", DbType = DbType.String)]
        public string EstadoComprobante { get; set; }

        [DataMember(Order = 4)]
        [DbParameter("@pTIPO_COMPROBANTE", DbType = DbType.String)]
        public string TipoComprobante { get; set; }

        [DataMember(Order = 5)]
        [DbParameter("@pSERIE", DbType = DbType.String)]
        public string Serie { get; set; }

        [DataMember(Order = 6)]
        [DbParameter("@pCORRELATIVO", DbType = DbType.Int32)]
        public int Correlativo { get; set; }

        [DataMember(Order = 7)]
        [DbParameter("@pFECHA_IMPRESION_ORDEN", DbType = DbType.DateTime)]
        public DateTime FechaImpresionOrden { get; set; }

        [DataMember(Order = 8)]
        [DbParameter("@pCALC_MONTO_TOTAL", DbType = DbType.Decimal)]
        public decimal CalcMontoTotal { get; set; }

        [DataMember(Order = 9)]
        [DbParameter("@pUSUARIOCREACION", DbType = DbType.String)]
        public string UsuarioCreacion { get; set; }
    }
}