﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using HT.Data.Generic;
using HT.Dashboard.Model;
using HT.Dashboard.Logic;

namespace HT.Dashboard.Api.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [Route("[controller]/{branchId}")]
    [ApiController]
    public class TableController : ApiControllerBaseType<TableLogic>
    {
        public IConfiguration _configuration;

        public TableController(IServiceProvider services, ILogger<TableController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public IEnumerable<ProductTableResponseModel> GetList(int branchId)
        {
            IEnumerable<ProductTableResponseModel> response = null;
            try
            {
                response = GetLogic().GetTable(branchId).ToList();
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Debug, ex, "Obtener dashboard productividad", response);
            }
            
            return response;
        }

        [HttpPost]
        public IResponseEntity<SucursalResponseModel> GetSucursal(int branchId)
        {
            IResponseEntity<SucursalResponseModel> response = null;
            try
            {
                response = GetLogic().GetSucursal(branchId);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Debug, ex, "Obtener Sucursal", response);
            }
            
            return response;
        }
    }
}