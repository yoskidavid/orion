﻿using System;
using System.Data;

namespace HT.Data.Db
{
    [AttributeUsage((AttributeTargets)(AttributeTargets.Field | AttributeTargets.Property), Inherited = true, AllowMultiple = false)]
    public class DbParameterAttribute : Attribute
    {
        public DbParameterAttribute(string parameterName) :
            this(parameterName, DbType.Object)
        {
            ParameterName = parameterName;
        }

        public DbParameterAttribute(string parameterName, DbType dbType) :
            this(parameterName, dbType, 0)
        {

        }

        public DbParameterAttribute(string parameterName, DbType dbType, int size) :
            this(parameterName, dbType, size, ParameterDirection.Input)
        {

        }

        public DbParameterAttribute(string parameterName, DbType dbType, int size, ParameterDirection direction)
        {
            this.ParameterName = parameterName;
            this.DbType = dbType;
            this.Size = size;
            this.Direction = direction;
        }

        public string ParameterName { get; set; }


        public DbType DbType { get; set; }

        public int Size { get; set; }

        public ParameterDirection Direction { get; set; }

        public object[] AllowedValues
        {
            get;
            set;
        }
    }
}
