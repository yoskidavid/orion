﻿using HT.Data.Db;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    public class WS_API_updOCGeoreferenciaResponseModel
    {
        [DataMember]
        [DbColumn(Name = "CODIGO")]
        public int Code { get; set; }

        [DataMember]
        [DbColumn(Name = "MENSAJE")]
        public string Message { get; set; }
    }
}
