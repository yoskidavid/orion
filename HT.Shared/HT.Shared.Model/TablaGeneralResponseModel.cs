﻿using HT.Data.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace HT.Shared.Model
{
    public class TablaGeneralResponseModel
    {

        [DbColumn(Order = 0, Name = "IDTABLAGENERAL")]
        public int IDTABLAGENERAL { get; set; }

        [DbColumn(Order = 1, Name = "COD_TABLA")]
        public string COD_TABLA { get; set; }

        [DbColumn(Order = 2, Name = "COD_CAMPO")]
        public string COD_CAMPO { get; set; }

        [DbColumn(Order = 3, Name = "COD_INTERNO")]
        public string COD_INTERNO { get; set; }

        [DbColumn(Order = 4, Name = "DESCRIPCION")]
        public string DESCRIPCION { get; set; }

        [DbColumn(Order = 5, Name = "VALOR1")]
        public string VALOR1 { get; set; }

        [DbColumn(Order = 6, Name = "VALOR2")]
        public string VALOR2 { get; set; }

        [DbColumn(Order = 7, Name = "VALOR3")]
        public string VALOR3 { get; set; }

        [DbColumn(Order = 8, Name = "ORDEN")]
        public int ORDEN { get; set; }

        [DbColumn(Order = 9, Name = "FLG_SISTEMA")]
        public bool FLG_SISTEMA { get; set; }

        [DbColumn(Order = 10, Name = "HABILITADO")]
        public bool HABILITADO { get; set; }

        [DbColumn(Order = 11, Name = "CREADOPOR")]
        public string CREADOPOR { get; set; }

        [DbColumn(Order = 12, Name = "FECHACREACION")]
        public DateTime FECHACREACION { get; set; }

        [DbColumn(Order = 13, Name = "MODIFICADOPOR")]
        public string MODIFICADOPOR { get; set; }

        [DbColumn(Order = 14, Name = "FECHAMODIFICACION")]
        public DateTime? FECHAMODIFICACION { get; set; }
    }
}
