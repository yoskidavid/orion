﻿using HT.Data.Db;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace HT.Dashboard.Model.Shared
{
    [DataContract]
    public class TablaGeneralResponse
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "IDTABLAGENERAL")]
        public Int32 IdTablaGeneral { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "COD_TABLA")]
        public String CodTabla { get; set; }

        [DataMember]
        [DbColumn(Order = 2, Name = "COD_CAMPO")]
        public String CodCampo { get; set; }

        [DataMember]
        [DbColumn(Order = 3, Name = "COD_INTERNO")]
        public String CodInterno { get; set; }

        [DataMember]
        [DbColumn(Order = 4, Name = "DESCRIPCION")]
        public String Descripcion { get; set; }

        [DataMember]
        [DbColumn(Order = 5, Name = "VALOR1")]
        public String Valor1 { get; set; }

        [DataMember]
        [DbColumn(Order = 6, Name = "VALOR2")]
        public String Valor2 { get; set; }

        [DataMember]
        [DbColumn(Order = 7, Name = "VALOR3")]
        public String Valor3 { get; set; }

        [DataMember]
        [DbColumn(Order = 8, Name = "ORDEN")]
        public Int32 Orden { get; set; }

        [DataMember]
        [DbColumn(Order = 9, Name = "FLG_SISTEMA")]
        public Boolean FlgSistema { get; set; }

        [DataMember]
        [DbColumn(Order = 10, Name = "HABILITADO")]
        public Boolean Habilitado { get; set; }

        [DataMember]
        [DbColumn(Order = 11, Name = "CREADOPOR")]
        public String CreadoPor { get; set; }

        [DataMember]
        [DbColumn(Order = 12, Name = "FECHACREACION")]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        [DbColumn(Order = 13, Name = "MODIFICADOPOR")]
        public String ModificadoPor { get; set; }

        [DataMember]
        [DbColumn(Order = 14, Name = "FECHAMODIFICACION")]
        public DateTime FechaModificacion { get; set; }
    }
}