﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace HT.Logging.RollingFile
{
    public class BatchingLogger : ILogger
    {
        private readonly BatchingLoggerProvider _provider;
        private readonly string _category;
        private static readonly IDictionary<LogLevel, string> LogLevelString = new Dictionary<LogLevel, string>
        {
            { LogLevel.Trace, "TRACE" },
            { LogLevel.Debug, "DEBUG" },
            { LogLevel.Information, "INFO" },
            { LogLevel.Warning, "WARN" },
            { LogLevel.Error, "ERROR" },
            { LogLevel.Critical, "FATAL" }
        };

        public BatchingLogger(BatchingLoggerProvider loggerProvider, string categoryName)
        {
            _provider = loggerProvider;
            _category = categoryName;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            if (logLevel == LogLevel.None)
            {
                return false;
            }
            return true;
        }

        public void Log<TState>(DateTimeOffset timestamp, LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            StringBuilder builder = new StringBuilder();

            builder.Append($"{timestamp:yyyy-MM-dd HH:mm:ss.fff}");
            builder.Append(" ");
            builder.Append($"{LogLevelString[logLevel],-5}");
            builder.Append(" ");
            builder.Append(_category);

            StackFrame frame = new StackTrace(6, false).GetFrame(0);
            MethodBase method = frame.GetMethod();

            builder.Append(" ");
            builder.Append(method.Name);

            builder.Append(" ");
            builder.AppendLine(formatter(state, exception));

            if (exception != null)
            {
                builder.AppendLine(exception.ToString());
            }

            _provider.AddMessage(timestamp, builder.ToString());
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            Log(DateTimeOffset.Now, logLevel, eventId, state, exception, formatter);
        }
    }
}
