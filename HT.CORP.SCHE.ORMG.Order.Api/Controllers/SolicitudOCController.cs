﻿using System;
using HT.Database;
using HT.Database.Generic;
using HT.Order.Logic;
using HT.Order.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace HT.Order.Api.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    [ProducesResponseType(500)]
    [EnableCors("AllowSpecificOrigin")]
    [Route("/rtl/tot/corp/SCHE/ORMG/[controller]")]
    [ApiController]
    public class SolicitudOCController : ApiControllerBaseType
    {
        public IConfiguration _configuration;

        public SolicitudOCController(IServiceProvider services, ILogger<SolicitudOCController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        [HttpPost("ActualizarEstadoSolicitudOC")]
        public SolicitudOCActualizarEstadoResponseModel ActualizarEstadoSolicitudOC([FromBody] SolicitudOCActualizarEstadoRequestModel request)
        {
            SolicitudOCActualizarEstadoResponseModel objResponse = new SolicitudOCActualizarEstadoResponseModel();
            try
            {
                DbDispatch.EnsureOpen(true);
                objResponse = Services.GetService<SolicitudOCLogic>().ActualizarEstadoSolicitudOC(request);
                DbDispatch.Commit();
            }
            catch (Exception ex)
            {
                this.Logger.Log(LogLevel.Debug, ex, "ActualizarEstadoSolicitudOC", request);
                DbDispatch.Rollback();

                objResponse.NumeroPedido = request.NumeroPedido;
                objResponse.CodigoActEstado = "99";
                objResponse.Observacion = "Ocurrió un error interno";
            }
            finally
            {
                DbDispatch.EnsureDispose();
            }
            return objResponse;
        }
    }
}