﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
 
namespace HT.WebServices
{
    public class PosVirtual
    {

        string urlXPicking;

        public PosVirtual(string _urlXPicking) {
            urlXPicking = _urlXPicking;
        }

        public string ValidateStateReceipt(string orderNumber) {

            var result = ""; 
            try
            {
                BasicHttpBinding basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.None);
                EndpointAddress endpointAddress = new EndpointAddress(urlXPicking);
                WebServiceXpickingSoapClient webServiceXpickingSoapClient = new WebServiceXpickingSoapClient(basicHttpBinding, endpointAddress);

                result = webServiceXpickingSoapClient.getEstado_Orden_PosVirtual(orderNumber);
            }
            catch (Exception ex)
            {
                result = string.Format("99 - Mensaje : {0}", ex.Message);
            }
         
            return result;
        }
        public string SendOrderToPostVistual(int OrderId,int storeId,int EmployeeId)
        {

            var status = 0;
            var result ="";
            try
            {
                BasicHttpBinding basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.None);
                EndpointAddress endpointAddress = new EndpointAddress(urlXPicking);
                WebServiceXpickingSoapClient webServiceXpickingSoapClient = new WebServiceXpickingSoapClient(basicHttpBinding, endpointAddress);

                status = webServiceXpickingSoapClient.Enviar_Orden_PosVirtual(OrderId, storeId, EmployeeId);
                if (status != 1)
                {
                    result = "Error al enviar a POS Virtual";
                }
            }
            catch (Exception ex)
            {
                result = string.Format("Error al enviar a POS Virtual - Mensaje : {0}", ex.Message);
            }

            return result;
        }
    }
}
