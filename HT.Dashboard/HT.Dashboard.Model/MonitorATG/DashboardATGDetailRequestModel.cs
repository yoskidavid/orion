﻿using System.Data;
using System.Runtime.Serialization;
using HT.Data.Db;

namespace HT.Dashboard.Model.MonitorATG
{
    [DataContract]
    [DbCommand("WS_API_SEL_MONITOR_ORDEN_ATG")]
    public class DashboardATGDetailRequestModel
    {
        public DashboardATGDetailRequestModel() {
            BranchID = 0;
            Service = "";          
            StartDate = "";
            EndDate = "";
            RangeDispacht = "";
            OrderType = "";
        }

        [DataMember(Name = "branchId")]
        [DbParameter("@ID_SUCURSAL", DbType = DbType.Int32)]
        public int? BranchID { get; set; }

        [DataMember(Name = "serviceId")]
        [DbParameter("@SERVICIO", DbType = DbType.String)]
        public string Service { get; set; }  

        [DataMember(Name = "startDate")]
        [DbParameter("@FECHA_INICIO", DbType = DbType.String)]
        public string StartDate { get; set; }

        [DataMember(Name = "endDate")]
        [DbParameter("@FECHA_FIN", DbType = DbType.String)]
        public string EndDate { get; set; }

        [DataMember(Name = "rangeDispacht")]
        [DbParameter("@VENTANA_HORARIA", DbType = DbType.String)]
        public string RangeDispacht { get; set; }

        [DataMember(Name = "orderType")]
        [DbParameter("@TIPO_OC", DbType = DbType.String)]
        public string OrderType { get; set; }
    }
}