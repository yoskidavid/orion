﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace HT.Data.Generic
{
    [DataContract]
    public class Response : IResponse
    {
        public Response(Exception exception)
        {
            Exception = exception;
        }

        [IgnoreDataMember]
        public IDictionary<string, object> Values
        {
            get
            {
                EnsureInnerValues();

                return InnerValues;
            }
        }

        [IgnoreDataMember]
        public System.Exception Exception { get; private set; }

        [DataMember]
        public int Code => Exception != null ? Exception.HResult : 0;

        [DataMember]
        public string Message => Exception != null ? Exception.Message : "Éxito";

        private IDictionary<string, object> InnerValues { get; set; }

        private void EnsureInnerValues()
        {
            if (InnerValues == null)
            {
                InnerValues = new Dictionary<string, object>();
            }
        }

        IDictionary Data.IResponse.Values =>
            (IDictionary)Values;
    }
}
