﻿using HT.Shared.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HT.CORP.SCHE.ORMG.Order.OrderPicked.Model;
using HT.Configuration;
using Microsoft.Extensions.Options;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Logic
{
    public class OrderPickedRepository : IOrderPickedRepository
    {
        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(Options.Value.Connections["Dispatch"].ConnectionString);
            }
        }

        public SqlConnection SqlCon
        {
            get
            {
                return new SqlConnection(Options.Value.Connections["Dispatch"].ConnectionString);
            }
        }

        public IOptions<DbSettings> Options { get; }

        public OrderPickedRepository(IOptions<DbSettings> options)
        {
            Options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public async Task<ParameterResponseModel> GetAsync(string code)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var result = 
                    await conn.QueryFirstOrDefaultAsync<ParameterResponseModel>("ADM_GETPARAMETRO", new { PIV_CODIGO = code }, commandType: CommandType.StoredProcedure);

                return result;
            }
        }

        public async Task<IEnumerable<HomologaLocalResponseModel>> GetLocalsAsync()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var result = 
                        await conn.QueryAsync<HomologaLocalResponseModel>("WS_SOL_HOLOGACION_LOCAL",commandType:CommandType.StoredProcedure) ;

                return result;
            }
        }

        public async Task<ExistsOrderResponseModel> ExistsOrder(string order)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open(); 

                var result =
                        await conn.QueryFirstOrDefaultAsync<ExistsOrderResponseModel>("ATG_GETEXISTEORDEN_F",new { piv_NumeroOrden = order },commandType:CommandType.StoredProcedure);
                
                return result;
            }
        }

        public async Task<OrderFlagPOSModelResponse> GetFlagPostVirtual(string numOrden)
        { 
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                String selectQuery = $"SELECT ISNULL(FLAG_POSVIRTUAL,0) AS 'FLAG_POSVIRTUAL' from dbo.OC (NOLOCK) WHERE numero_orden = '{numOrden}'";
                var result =
                        await conn.QueryFirstOrDefaultAsync<OrderFlagPOSModelResponse>(selectQuery, null, commandType: CommandType.Text);

                return result;
            } 
        }

        public async Task<WS_SOL_ValidarSolicitudOC_PICKING> ValidaTablaSolicitudOC(string order)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var _params = new DynamicParameters();
                _params.Add("@NUMERO_ORDEN", order, DbType.String, direction: ParameterDirection.Input);
                _params.Add("@ERROR_CODIGO",0, DbType.Int32, direction: ParameterDirection.Output);
                _params.Add("@ERROR_MENSAJE","", DbType.String, direction: ParameterDirection.Output);

                var response = new WS_SOL_ValidarSolicitudOC_PICKING() { NUMERO_ORDEN = order };
                var result =
                     await conn.ExecuteAsync("WS_SOL_ValidarSolicitudOC_PICKING", _params, commandType: CommandType.StoredProcedure);

                response.COD_INTERNO = _params.Get<int>("ERROR_CODIGO");
                response.VALOR1 = _params.Get<string>("ERROR_MENSAJE");

                return response;
            }
        }

        public async Task<OrderDeleteModelRequest> DeleteOrder(string order)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var _params = new DynamicParameters();
                _params.Add("@NRO_OC", order, DbType.String, direction: ParameterDirection.Input);
                _params.Add("@ERROR_CODIGO",0, DbType.Int32, direction: ParameterDirection.Output);
                _params.Add("@ERROR_MENSAJE","", DbType.String, direction: ParameterDirection.Output);

                var response = new OrderDeleteModelRequest() { NRO_OC = order };

                var result =
                        await conn.ExecuteAsync("WS_SOL_ELIMINAR_OC", _params, commandType: CommandType.StoredProcedure);

                response.ERROR_CODIGO  = _params.Get<int>("@ERROR_CODIGO");
                response.ERROR_MENSAJE = _params.Get<string>("@ERROR_MENSAJE");


              
                return response;
            }
        }

        public async Task<IEnumerable<UbigeoResponseModel>> GetUbigeos()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var result =
                        await conn.QueryAsync<UbigeoResponseModel>("SP_SEL_UBIGEO", commandType: CommandType.StoredProcedure); 
                return result;
            }
        }

        public async Task<IEnumerable<TablaGeneralResponseModel>> GetDataFromTablaGeneral(int pFLAG,string pCOD_TABLA)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                
                var request = new TablaGeneralRequestModel(pFLAG, pCOD_TABLA);

                var result =
                        await conn.QueryAsync<TablaGeneralResponseModel>("ADM_lisTABLAGENERAL", request , commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<ParameterResponseModel> GetParameterValue(string parametro)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var result =
                        await conn.QueryFirstOrDefaultAsync<ParameterResponseModel>("ADM_GETPARAMETRO", new { PIV_CODIGO = parametro }, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<Int32> SaveInOrderQueue(OrderQueueModel orderQueue)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                 
                var rowsAffected =
                        await conn.ExecuteAsync("InsertOrderQueue", orderQueue, commandType: CommandType.StoredProcedure);
                 
                return rowsAffected;
            }
        }

        public async Task<int> SaveInLog(LogApiModel logApiModel)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var rowsAffected =
                        await conn.ExecuteAsync("WS_SOL_LOG_API", logApiModel, commandType: CommandType.StoredProcedure);

                return rowsAffected;
            }
        }
    }
}
