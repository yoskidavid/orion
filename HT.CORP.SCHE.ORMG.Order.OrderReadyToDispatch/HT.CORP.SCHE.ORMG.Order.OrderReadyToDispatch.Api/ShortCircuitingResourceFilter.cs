﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Api
{
    public class ShortCircuitingResourceFilterAttribute : Attribute,
            IResourceFilter
    {
        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            //context.Result = new ContentResult()
            //{
            //    Content = "Resource unavailable - header should not be set"
            //};
        }

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }
    }
}
