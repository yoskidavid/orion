﻿using HT.Data.Db;
using HT.Data.Generic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract(Name = "Orden")]
    [DbCommand("WS_SOL_InsertCabeceraOrden_F")]
    public class OrderRequestModel 
    {
        [DataMember]
        [DbParameter("@PIV_NUMEROORDEN", DbType = DbType.String, Size = 10)]
        public string OrderNumber { get; set; }

        [DataMember]
        public string Pais { get; set; }

        [DataMember(Name = "usrId")]
        public string UserID { get; set; }
        [DataMember]
        public string UserSystem { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PIV_RUTCLIENTE", DbType = DbType.String, Size = 15)]
        public string CustomerDocumentNumber { get => Client.CustomerDocumentNumber; }
        
        [IgnoreDataMember]
        [DbParameter("@PIV_OBSDESPACHO", DbType = DbType.String, Size = 500)]
        public string DispatchObservation { get => Client.DispatchObservation; }

        [IgnoreDataMember]
        [DbParameter("@PIV_OBSENTREGA", DbType = DbType.String, Size = 500)]
        public string DeliveryObservation { get => Client.DeliveryObservation; }

        [IgnoreDataMember]
        [DbParameter("@PIV_RUC", DbType = DbType.String, Size = 15)]
        public string CompanyIdentity { get => Client.CompanyIdentity; }

        [IgnoreDataMember]
        [DbParameter("@PIV_RAZONSOCIAL", DbType = DbType.String, Size = 250)]
        public string CompanyName { get => Client.CompanyName; }

        [IgnoreDataMember]
        [DbParameter("@PIV_SERVICIO", DbType = DbType.String, Size = 100)]
        public string ServiceType { get => Client.ServiceType; }

        [IgnoreDataMember]
        [DbParameter("@PII_NUMEROLOCAL", DbType = DbType.Int32)]
        public int LocalCode { get => Client.StoreID; }

        [IgnoreDataMember]
        [DbParameter("@PIV_REFERENCIA", DbType = DbType.String, Size = 200)]
        public string DispatchReference { get => Client.DispatchReference; }

        [IgnoreDataMember]
        [DbParameter("@PIV_TIENDADERECOJO", DbType = DbType.String, Size = 50)]
        public string PickUpPoint { get => Client.PickUpPoint; }

        [IgnoreDataMember]
        [DbParameter("@PIV_NOMRECEPTOR", DbType = DbType.String, Size = 50)]
        public string ReceptorFullName { get => Client.ReceptorFullName; }
        
        [IgnoreDataMember]
        [DbParameter("@PII_CUOTAS", DbType = DbType.Int32)]
        public int Installments { get => Payment.Installments; }

        [IgnoreDataMember]
        [DbParameter("@PIV_MEDIOPAGO", DbType = DbType.String, Size = 50)]
        public string PaymentType { get => Payment.Payment; }

        [IgnoreDataMember]
        [DbParameter("@PID_FECHACOMPRA", DbType = DbType.DateTime)]
        public DateTime TransactionDate { get => Payment.TransactionDate; }

        [IgnoreDataMember]
        [DbParameter("@PIV_CUPON", DbType = DbType.String, Size = 250)]
        public string DiscountCoupon { get => Client.DiscountCoupon; }

        [IgnoreDataMember]
        [DbParameter("@PIN_FLETE", DbType = DbType.Decimal)]
        public decimal TotalPriceDispatch { get => Payment.TotalPriceDispatch; }

        [IgnoreDataMember]
        [DbParameter("@PIF_MONTOTOTAL", DbType = DbType.Decimal)]
        public decimal TotalAmount { get => Payment.OriginalAmount; }

        [IgnoreDataMember]
        [DbParameter("@PIF_MONTODESPACHO", DbType = DbType.Decimal)]
        public decimal DispatchAmount { get => 0; }

        [IgnoreDataMember]
        [DbParameter("@PID_FECHAHORAINIDESP", DbType = DbType.DateTime)]
        public DateTime DeliveryDate => DateTime.Parse($"{Client.DateDispatch.ToString("yyyy-MM-dd")} {Client.HourDispatch.Replace("Hrs", "", StringComparison.InvariantCulture).Split("-")[0]}:00", CultureInfo.InvariantCulture);

        [IgnoreDataMember]
        [DbParameter("@PID_FECHAHORAFINDESP", DbType = DbType.DateTime)]
        public DateTime DispatchDate => DateTime.Parse($"{Client.DateDispatch.ToString("yyyy-MM-dd")} {Client.HourDispatch.Replace("Hrs", "", StringComparison.InvariantCulture).Split("-")[1]}:00", CultureInfo.InvariantCulture);

        [IgnoreDataMember]
        [DbParameter("@PIV_LUGARDESPACHO", DbType = DbType.String, Size = 75)]
        public string DispatchPlace { get => Client.DispatchAddress; }

        [IgnoreDataMember]
        [DbParameter("@PIV_COMUNADESPACHO", DbType = DbType.String, Size = 25)]
        public string DispatchCommunityName { get => Client.ComunaName; }

        [IgnoreDataMember]
        [DbParameter("@PIV_CIUDADESPACHO", DbType = DbType.String, Size = 25)]
        public string DispatchCityName { get => Client.ProvinceName; }

        [IgnoreDataMember]
        [DbParameter("@PIV_CODVENDEDOR", DbType = DbType.String, Size = 20)]
        public string SellerCode { get => Client.SellerCode; }

        #region Products

        [DataMember]
        public IEnumerable<ProductRequestModel> Products
        {
            get
            {
                ProductsEnsure();

                return ProductsInternal;
            }
        }

        #endregion

        private ModelChildren<OrderRequestModel, ProductRequestModel> ProductsInternal { get; set; }



        [DataMember(Name = "orderId")]
        [DbParameter("@POI_IDOC", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int? OrderID { get; set; }

        [IgnoreDataMember]
        [DbParameter("@piv_DocReceptor", DbType = DbType.String, Size = 14)]
        public string DocumentReceiver { get => ""; }


        [IgnoreDataMember]
        [DbParameter("@piv_FlagAudit", DbType = DbType.Boolean)]
        public bool Audit_flag { get => Client.Audit_flag; }


        //[DataMember]
        //[DbParameter("@piv_Token", DbType = DbType.String, Size = 500)]
        //public string Capture_token { get; set; }


        [IgnoreDataMember]
        [DbParameter("@piv_Reset", DbType = DbType.Boolean)]
        public bool Reset_flag { get => Client.Reset_flag; }

        [DataMember]
        public int OrderTypeID { get; set; }

        #region ProductsEnsure

        protected void ProductsEnsure()
        {
            if (object.Equals(ProductsInternal, null))
            {
                ProductsInternal = new ModelChildren<OrderRequestModel, ProductRequestModel>(this);
            }
        }

        #endregion
        

        private ClientRequestModel m_clientRequestModel;
        private PaymentRequestModel m_paymentInternal;
        private InsertValidateUpdateRequestModel m_insertaValida;
        private InsertOrderXMLRequestModel m_nsertOrdenXML;
        private SubOrderCreaRequestModel m_subOrden;
        private WS_API_updOCGeoreferenciaRequestModel m_WS_API_updOCGeoreferencia;
        private DeriveCDRequestModel m_derivaCD;
        private PickRecordManualResultDeleteRequestModel m_pickRecordManualResultDeleteNew;
        private PickRecordManualDetailPickingDeleteRequestModel m_pickRecordManualDetailPickingDeleteNew;
        private PickRecordManualUpdateOCNewRequestModel m_pickRecordManualUpdateOCNew;
        private WS_I_FinalizarPicking_Food m_WS_I_FinalizarPicking_Food;
        private PickChangeControlUpdateStatusPosATGModel m_PickChangeControlUpdateStatusPosATG;
        private SubOrderUpdateRequestModel m_subOrdenUpdate;
        private PickRecordManualFindDetOC_PorIdOC_NewRequestModel m_PICK_RegistroManual_BuscarDetOC_PorIdOC_New;

        [DataMember]
        public ClientRequestModel Client => this.EnsureInstance(ref m_clientRequestModel);

        [DataMember]
        public PaymentRequestModel Payment => this.EnsureInstance(ref m_paymentInternal);

        [IgnoreDataMember]
        public InsertValidateUpdateRequestModel InsertaValida => this.EnsureInstance(ref m_insertaValida);

        [IgnoreDataMember]
        public InsertOrderXMLRequestModel InsertOrdenXML => this.EnsureInstance(ref m_nsertOrdenXML);

        [IgnoreDataMember]
        public SubOrderCreaRequestModel SubOrden => this.EnsureInstance(ref m_subOrden);

        [IgnoreDataMember]
        public WS_API_updOCGeoreferenciaRequestModel WS_API_updOCGeoreferencia => this.EnsureInstance(ref m_WS_API_updOCGeoreferencia);

        [IgnoreDataMember]
        public DeriveCDRequestModel DerivaCD => this.EnsureInstance(ref m_derivaCD);

        [IgnoreDataMember]
        public PickRecordManualResultDeleteRequestModel PickRecordManualResultDeleteNew => this.EnsureInstance(ref m_pickRecordManualResultDeleteNew);

        [IgnoreDataMember]
        public PickRecordManualDetailPickingDeleteRequestModel PickRecordManualDetailPickingDeleteNew => this.EnsureInstance(ref m_pickRecordManualDetailPickingDeleteNew);

        [IgnoreDataMember]
        public PickRecordManualFindDetOC_PorIdOC_NewRequestModel PICK_RegistroManual_BuscarDetOC_PorIdOC_New => this.EnsureInstance(ref m_PICK_RegistroManual_BuscarDetOC_PorIdOC_New);

        [IgnoreDataMember]
        public PickRecordManualUpdateOCNewRequestModel PickRecordManualUpdateOCNew => this.EnsureInstance(ref m_pickRecordManualUpdateOCNew);

        [IgnoreDataMember]
        public WS_I_FinalizarPicking_Food WS_I_FinalizarPicking_Food => this.EnsureInstance(ref m_WS_I_FinalizarPicking_Food);

        [IgnoreDataMember]
        public SubOrderUpdateRequestModel SubOrdenUpdate => this.EnsureInstance(ref m_subOrdenUpdate);

        [IgnoreDataMember]
        public PickChangeControlUpdateStatusPosATGModel PickChangeControlUpdateStatusPosATG => this.EnsureInstance(ref m_PickChangeControlUpdateStatusPosATG);
    }
}
