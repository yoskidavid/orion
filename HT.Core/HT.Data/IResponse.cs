﻿using System.Collections;

namespace HT.Data
{
    public interface IResponse
    {
        System.Exception Exception { get; }

        IDictionary Values { get; }

        int Code { get; }

        string Message { get; }
    }
}
