﻿using HT.Data.Db;
using HT.Data.Generic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
	[DataContract]
	[DbCommand("FOL_INSERTARFOLIO")]
	public class FolioGeneraRequestModel : ModelChild<OrderRequestModel>
	{
		public FolioGeneraRequestModel()
			: this(null)
		{ }

		public FolioGeneraRequestModel(OrderRequestModel parent)
			: base(parent)
		{ }

		[IgnoreDataMember]
		[DbParameter("@PIV_NUMEROORDEN", DbType = DbType.String)]
		public string OrderNumber { get => Parent.OrderNumber; }

		[DataMember(Name = "folioNum")]
		[DbParameter("@PIV_NUMEROFOLIO", DbType = DbType.String)]
		public string FolioNumber { get; set; }

		[IgnoreDataMember]
		[DbParameter("@PIV_ESTADO", DbType = DbType.String)]
		public string Status { get => "RESERVADO"; }

		// 1: Despacho a domicilio
		// 2: Retiro en Tienda
		// 3: Despacho Express
		// 4: Despacho a Punto de Retiro
		[DataMember]
		[DbParameter("@PIV_TIPOSERVICIO", DbType = DbType.String)]
		public string folioServiceType { get; set; }

		[DataMember(Name = "dispatchWh")]
		[DbParameter("@PII_CODIGOSUCURSAL", DbType = DbType.Int32)]
		public int dispatchWh { get; set; }

		[DataMember]
		[DbParameter("@PIV_CODIGOLOCALRECOJO", DbType = DbType.String)]
		public string FolioPickupPoint { get; set; }

		[DataMember]
		protected string FolioHourDispatch { get; set; }

		[IgnoreDataMember]
		[DbParameter("@PIV_VENTANAHORARIA", DbType = DbType.String)]
		public string BdFolioHourDispatch => FolioHourDispatch.Replace("Hrs", string.Empty);
		
		[DataMember]
		[DbParameter("@FECHADESPACHO", DbType = DbType.Date)]
		public DateTime FolioDateDispatch { get; set; }
		
		[DataMember]
		public decimal folioPriceDispatch { get; set; }

		[IgnoreDataMember]
		[DbParameter("@PIV_CIUDAD_DESPACHO", DbType = DbType.String)]
		public string ReceiverProvinceName { get; set; }

		[DataMember(Name = "receiverComuna")]
		[DbParameter("@PIV_COMUNA_DESPACHO", DbType = DbType.String)]
		public string ReceiverComunaName { get; set; }

		[DataMember(Name = "receiverAddress")]
		[DbParameter("@PIV_LUGAR_DESPACHO", DbType = DbType.String)]
		public string ReceiverAddress { get; set; }

		[DataMember]
		[DbParameter("@PIV_OBSERVACION", DbType = DbType.String)]
		public string Observation { get; set; }

		#region SKUs

		[DataMember(Name = "skus")]
		public IEnumerable<FolioSkuGenerateRequestModel> SKUs
		{
			get
			{
				SkusEnsure();

				return SkusInternal;
			}
		}

		#endregion

		#region LogEstado

		[IgnoreDataMember]
		public FolioStateLogGenerateRequestModel LogEstado
		{
			get
			{
				LogEstadoEnsure();

				return LogEstadoInternal;
			}
		}

		#endregion

		private ModelChildren<FolioGeneraRequestModel, FolioSkuGenerateRequestModel> SkusInternal { get; set; }

		private FolioStateLogGenerateRequestModel LogEstadoInternal { get; set; }

		#region SkusEnsure

		protected void SkusEnsure()
		{
			if (object.Equals(SkusInternal, null))
			{
				SkusInternal = new ModelChildren<FolioGeneraRequestModel, FolioSkuGenerateRequestModel>(this);
			}
		}

		#endregion

		#region LogEstadoEnsure

		protected void LogEstadoEnsure()
		{
			if (object.Equals(LogEstadoInternal, null))
			{
				LogEstadoInternal = new FolioStateLogGenerateRequestModel(this);
			}
		}

		#endregion
	}
}
