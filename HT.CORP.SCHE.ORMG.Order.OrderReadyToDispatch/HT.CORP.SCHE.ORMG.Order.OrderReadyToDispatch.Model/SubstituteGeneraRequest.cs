﻿using HT.Data.Db;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DataContract]
    [DbCommand("SI_SUSTITUTO")]
    public class SubstituteGeneraRequest
    {
        [IgnoreDataMember]
        [DbParameter("@ID_DETALLE_OC", DbType = DbType.Int32)]
        public int ProductID { get; set; }


        [DataMember(Name = "skuCode")]
        [DbParameter("@SKU", DbType = DbType.String, Size = 25)]
        public string Sku { get; set; }

        [DataMember(Name = "skuDescription")]
        [DbParameter("@DESCRIPCION", DbType = DbType.String, Size = 200)]
        public string Description { get; set; }

        [IgnoreDataMember]
        [DbParameter("@HABILITADO", DbType = DbType.Boolean)]
        public bool Status { get; set; }

        [IgnoreDataMember]
        [DbParameter("@CREADORPOR", DbType = DbType.String, Size = 25)]
        public string UserAudit { get; set; }

        [IgnoreDataMember]
        [DbParameter("@IDIDSUSTITUTO", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int SubstiteID { get; set; }
    }
}
