﻿using HT.Logging.RollingFile;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            System.Data.Common.DbProviderFactories.RegisterFactory("System.Data.SqlClient", "System.Data.SqlClient.SqlClientFactory,System.Data.SqlClient");

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                 .ConfigureLogging((context, builder) =>
                 {
                     builder.AddFile(opts =>
                     {
                         context.Configuration.GetSection("FileLoggingOptions").Bind(opts);
                     });
                 })
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.SetBasePath(Directory.GetCurrentDirectory());
                    config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);
                    config.AddJsonFile("dbsettings.json", optional: false, reloadOnChange: false);
                    config.AddCommandLine(args);
                })
                .UseStartup<Startup>();
    }
}
