﻿using HT.Database.Generic;
using Microsoft.Extensions.DependencyInjection;

namespace HT.Database
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddDatabases(this IServiceCollection services)
        {
            services.AddScoped<IDatabase<DbSecurity>, DbSecurity>();
            services.AddScoped<IDatabase<DbDispatch>, DbDispatch>();

            return services;
        }
    }
}
