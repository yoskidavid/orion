﻿using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    public class ExistsOrderResponseModel
    {
        [DbColumn(Order = 0, Name = "ID")]
        public int ID { get; set; }

        [DbColumn(Order = 1, Name = "NUMERO_ORDEN")]
        public string NUMERO_ORDEN { get; set; }
    }
}
