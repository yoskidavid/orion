﻿using HT.Dashboard.Logic;
using HT.Dashboard.Model.Shared;
using HT.Database;
using HT.Database.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HT.Dashboard.Api.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [Route("[controller]")]
    [ApiController]
    public class VentanaHorariaController : ApiControllerBaseType<VentanaHorariaLogic>
    {
        public IConfiguration _configuration;

        public VentanaHorariaController(IServiceProvider services, ILogger<VentanaHorariaController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        [HttpGet("GetList")]
        public IEnumerable<VentanaHorariaResponseModel> GetVentanaHoraria()
        {
            IEnumerable<VentanaHorariaResponseModel> response = null;
            try
            {
                response = GetLogic().GetVentanaHoraria().ToList();
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Debug, ex, "Obtener ventana horaria", response);
            }

            return response;
        }
    }
}