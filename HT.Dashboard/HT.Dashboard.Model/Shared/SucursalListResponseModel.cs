﻿using HT.Data.Db;
using System.Runtime.Serialization;

namespace HT.Dashboard.Model.Shared
{
    [DataContract]
    public class SucursalListResponseModel
    {
        [DataMember(Name = "branchId")]
        [DbColumn(Name = "ID")]
        public int BranchID { get; set; }

        [DataMember]
        [DbColumn(Name = "nombre")]
        public string Name { get; set; }
    }
}