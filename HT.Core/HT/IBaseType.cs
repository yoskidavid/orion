﻿using Microsoft.Extensions.Logging;

namespace HT
{
    public interface IBaseType
    {
        ILogger Logger { get; }
    }
}
