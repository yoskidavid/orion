﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[PICK_REGISTROMANUAL_RESULTADO_DEL_NEW]")]
    public class PickRecordManualResultDeleteRequestModel : ModelChild<OrderRequestModel>
    {
        public PickRecordManualResultDeleteRequestModel()
            : this(null)
        { }

        public PickRecordManualResultDeleteRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [DbParameter("@ID_RESULTADOS", DbType = DbType.String)]
        public string ResultIDs => string.Empty;
    }
}
