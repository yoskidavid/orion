﻿using HT.Data.Db;
using System.Runtime.Serialization;

namespace HT.Shared.Model
{
    [DataContract]
    public class UbigeoResponseModel
    {
        [DataMember(Name = "departmentId")]
        [DbColumn(Name = "CODDEPARTAMENTO")]
        public string CODDEPARTAMENTO { get; set; }

        [DataMember]
        [DbColumn(Name = "NOMBREDEPARTAMENTO")]
        public string NOMBREDEPARTAMENTO { get; set; }

        [DataMember(Name = "provinceId")]
        [DbColumn(Name = "CODPROVINCIA")]
        public string CODPROVINCIA { get; set; }

        [DataMember]
        [DbColumn(Name = "NOMBREPROVINCIA")]
        public string NOMBREPROVINCIA { get; set; }

        [DataMember(Name = "comunaId")]
        [DbColumn(Name = "CODDISTRITO")]
        public string CODDISTRITO { get; set; }

        [DataMember]
        [DbColumn(Name = "NOMBREDISTRITO")]
        public string NOMBREDISTRITO { get; set; }
    }
}
