﻿using HT.Data.Db;
using System;
using System.Data;

namespace HT.Dashboard.Model.Shared
{
    [DbCommand("ADM_lisTABLAGENERAL")]
    public class TablaGeneralRequest
    {
        public TablaGeneralRequest()
        {
            pFlag = 2;
            pIDTablaGeneral = 1;
            pCodCampo = "";
            pCodInterno = "";
            pDescripcion = "";
            pValor1 = "";
            pValor2 = "";
            pValor3 = "";
            pOrden = 1;
            pFlagSistema = false;
            pHabilitado = false;
            pCreadoPor = "";
            pFechaCreacion = DateTime.Now;
            pXml = "";
        }

        [DbParameter("@pFLAG", DbType = DbType.Int16)]
        public Int16 pFlag { get; set; }

        [DbParameter("@pIDTABLAGENERAL", DbType = DbType.Int16)]
        public Int32 pIDTablaGeneral { get; set; }

        [DbParameter("@pCOD_TABLA", DbType = DbType.String)]
        public String pCodTabla { get; set; }

        [DbParameter("@pCOD_CAMPO", DbType = DbType.String)]
        public String pCodCampo { get; set; }

        [DbParameter("@pCOD_INTERNO", DbType = DbType.String)]
        public String pCodInterno { get; set; }

        [DbParameter("@pDESCRIPCION", DbType = DbType.String)]
        public String pDescripcion { get; set; }

        [DbParameter("@pVALOR1", DbType = DbType.String)]
        public String pValor1 { get; set; }

        [DbParameter("@pVALOR2", DbType = DbType.String)]
        public String pValor2 { get; set; }

        [DbParameter("@pVALOR3", DbType = DbType.String)]
        public String pValor3 { get; set; }

        [DbParameter("@pORDEN", DbType = DbType.Int32)]
        public Int32 pOrden { get; set; }

        [DbParameter("@pFLG_SISTEMA", DbType = DbType.Boolean)]
        public Boolean pFlagSistema { get; set; }

        [DbParameter("@pHABILITADO", DbType = DbType.Boolean)]
        public Boolean pHabilitado { get; set; }

        [DbParameter("@pCREADOPOR", DbType = DbType.String)]
        public String pCreadoPor { get; set; }

        [DbParameter("@pFECHACREACION", DbType = DbType.DateTime)]
        public DateTime pFechaCreacion { get; set; }

        [DbParameter("@pXml", DbType = DbType.String)]
        public String pXml { get; set; }
    }
}