﻿using HT.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace HT.Database
{
    public class DbDispatch : Database, Generic.IDatabase<DbDispatch>, IDatabase
    {
        public DbDispatch(ILogger<DbDispatch> logger, IOptions<DbSettings> dbSettings) :
            base(logger, dbSettings)
        { }

        internal override string DbSettingName
        {
            get
            {
                return "Dispatch";
            }
        }
    }
}
