﻿namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    public interface IRequestModel<T>
    {
        T Parent { get; set; }
    }
}
