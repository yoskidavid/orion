﻿using Microsoft.Extensions.DependencyInjection;

namespace HT.Order.Logic
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddOrderLogic(this IServiceCollection services)
        {
            services.AddTransient<ComprobanteLogic>();
            services.AddTransient<SolicitudOCLogic>();

            return services;
        }
    }
}
