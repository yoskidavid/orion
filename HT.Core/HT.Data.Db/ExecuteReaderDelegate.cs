﻿using System.Data;

namespace HT.Data.Db
{
    public delegate T ExecuteReaderDelegate<T>(IDataReader reader, T entity);
}
