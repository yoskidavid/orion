﻿using HT.Data;
using HT.Data.Generic;
using System.Collections.Generic;
using System.Data;

namespace HT.Database
{
    public interface IDatabase
    {
        IDbConnection DbConnection { get; }

        IDbTransaction DbTransaction { get; }

        void EnsureOpen();

        void EnsureOpen(bool beginTransaction);

        void EnsureClose();

        void EnsureDispose();

        void Commit();

        void Rollback();



        IDataReader ExecuteReader<TRequest>(TRequest request)
            where TRequest : new();

        IDataReader ExecuteReader<TRequest>(TRequest request, CommandBehavior commandBehavior)
            where TRequest : new();



        IResponseNonQuery ExecuteNonQuery<TRequest>(TRequest request)
            where TRequest : new();

        IResponseNonQuery ExecuteNonQuery<TRequest>(IRequest<TRequest> request)
            where TRequest : new();


        IResponseEntity<TResponse> ExecuteEntity<TRequest, TResponse>(TRequest request)
            where TRequest : new()
            where TResponse : new();

        IResponseEntity<TResponse> ExecuteEntity<TRequest, TResponse>(IRequest<TRequest> request)
            where TRequest : new()
            where TResponse : new();

        IResponseEntity<TResponse> ExecuteEntity<TRequest, TResponse>(TRequest request, CommandBehavior commandBehavior)
            where TRequest : new()
            where TResponse : new();



        IResponsePager<TResponse> ExecutePager<TRequest, TResponse>(IRequestPager<TRequest> request)
            where TRequest : new()
            where TResponse : new();

        IResponsePager<TResponse> ExecutePager<TRequest, TResponse>(IRequestPager<TRequest> request, CommandBehavior commandBehavior)
            where TRequest : new()
            where TResponse : new();

        IEnumerable<TResponse> ExecuteList<TRequest, TResponse>(TRequest request)
            where TRequest : new()
            where TResponse : new();

        IEnumerable<TResponse> ExecuteList<TRequest, TResponse>(IRequest<TRequest> request)
            where TRequest : new()
            where TResponse : new();

        IEnumerable<TResponse> ExecuteList<TRequest, TResponse>(TRequest request, CommandBehavior commandBehavior)
            where TRequest : new()
            where TResponse : new();
    }
}
