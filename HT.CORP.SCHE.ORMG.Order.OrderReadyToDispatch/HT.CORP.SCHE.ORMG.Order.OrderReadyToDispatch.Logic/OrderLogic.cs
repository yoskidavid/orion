﻿using HT.Shared.Model;
using HT.Shared.Logic;
using HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model;
using HT.Data;
using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Logic
{
    public class OrderLogic : OrderReadyToDispatchLogicBaseType
    {
        public IConfiguration configuration;
        IOrderReadyToDispatchRepository orderRepository;
        public OrderLogic(IServiceProvider services, ILogger<OrderLogic> logger, IDatabase<DbDispatch> dbDispatch, IOrderReadyToDispatchRepository orderRepository, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            this.orderRepository = orderRepository;
        }

        #region ValidateInput

        private async Task<OrderRequestModel> ValidateInput(OrderRequestModel request)
        {
            var ubigeoRequest = new UbigeoRequestModel();
            var ubigeoResponse = await orderRepository.GetUbigeos();

            var client = request.Client;

            var tipoDocumentos = await orderRepository.GetDataFromTablaGeneral(2, "TIP_DOC");

            if (!tipoDocumentos.ToList().Exists(x => x.COD_INTERNO.ToLower().Trim() == client.CustomerDocumentType.ToLower().Trim()))
            {
                throw new System.Exception("Cliente con tipo de documento no registrado.");
            }


            if (string.IsNullOrEmpty(client.CustomerDocumentNumber))
            {
                throw new System.Exception("Cliente no tiene Número de Documento.");
            }

            if (string.IsNullOrEmpty(client.CustomerFullName))
            {
                throw new System.Exception("Cliente no tiene Nombre.");
            }

            if (string.IsNullOrEmpty(client.DispatchAddress))
            {
                throw new System.Exception("Cliente no tiene Dirección de Despacho.");
            }

            var ubigeo = ubigeoResponse.ToList().Find(x => x.CODDISTRITO.Trim() == client.ComunaID.ToString());

            if (ubigeo != null)
            {
            }
            else
            {
                throw new System.Exception("El código de comunaId no está registrado.");
            }


            var itemsQuantity = request.Products.Count();
            var uniqueItemsQuantity = (from x in request.Products
                                       select x.Sku).Distinct().Count();

            if (itemsQuantity == 0)
            {
                throw new System.Exception("La orden no con cuenta con productos.");
            }
            else
            {


                if (itemsQuantity != uniqueItemsQuantity)
                {
                    throw new System.Exception("La orden tiene productos duplicados");
                }



                var existsProductListPriceZero = (from x in request.Products
                                                  where x.ListPrice <= 0
                                                  select x).Count();

                if (existsProductListPriceZero > 0)
                {
                    throw new System.Exception("La orden tiene productos con precio de lista con valor cero");
                }
            }



            return request;
        }


        #endregion

        public async Task<IResponseNonQuery> CreateNewOrder(OrderRequestModel request,string signature)
        {
            IResponseNonQuery response;
            int userId = Convert.ToInt32(request.UserID);
            string orderNumber = request.OrderNumber;
            bool existOrder = await Exists(userId, orderNumber);

            if (request.Products.Count() > 0)
            {
                if (!existOrder)
                {
                    Logger.LogInformation($"Se inicia validación de Datos de la orden {request.OrderNumber}");
                    request = await ValidateInput(request);
                    Logger.LogInformation($"Validación exitosa");

                         
                    var ListaTipoOrden = await orderRepository.GetDataFromTablaGeneral(2, "TIPOORDEN");
                    var TipoOrden = ListaTipoOrden.ToList().Find(x => x.IDTABLAGENERAL == request.OrderTypeID);
            

                    string jsonString = JsonConvert.SerializeObject(request);
                    OrderQueueModel orderQueue = new OrderQueueModel(jsonString, TipoOrden.DESCRIPCION.Replace(" ", ""), request.OrderNumber, signature);

                    var rowAffected = await orderRepository.SaveInOrderQueue(orderQueue);
                    response = new ResponseNonQuery(rowAffected);

                }
                else
                {
                    throw new System.Exception("La orden de compra ya existe");
                    // TO-DO Enviar Mensaje de Correo (confirmar)
                }
            }
            else
            {
                throw new System.Exception("La orden de compra no contiene productos");
            }
            return response;
        }

        public async Task<Int32> WriteLog(LogApiModel logModel)
        {
            var rowsAfeccted = await orderRepository.SaveInLog(logModel);
            return rowsAfeccted;
        }

        public async Task<ParameterResponseModel> GetFlagOrionSucursal()
        {
            string suc_orion = "SUC_ORION";
            var parameterResponse = await orderRepository.GetParameterValue(suc_orion);
            return parameterResponse;
        }

        public IResponseNonQuery Create(OrderRequestModel model)
        {
            return DbDispatch.ExecuteNonQuery(model);
        }

        #region Existe

        public async Task<bool> Exists(int userAuditId, string numeroOrden)
        {
            var OrderResponse = await orderRepository.ExistsOrder(numeroOrden);
            return OrderResponse != null;
        }

     

        #endregion

        
    }
}
