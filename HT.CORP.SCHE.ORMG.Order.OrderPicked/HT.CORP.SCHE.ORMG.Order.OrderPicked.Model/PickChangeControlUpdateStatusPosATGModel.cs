﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[PICK_CONTROLCAMBIOS_ACTUALIZARESTADO_POS_ATG]")]
    public class PickChangeControlUpdateStatusPosATGModel : ModelChild<OrderRequestModel>
    {
        public PickChangeControlUpdateStatusPosATGModel()
            : this(null)
        { }

        public PickChangeControlUpdateStatusPosATGModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [DbParameter("@ID_Estado", DbType = DbType.Int32)]
        public int Status => 1;

        [DbParameter("@ValorEstado", DbType = DbType.Int32)]
        public bool StatusValue => true;
    }
}
