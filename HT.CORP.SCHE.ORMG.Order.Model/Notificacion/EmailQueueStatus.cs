﻿namespace HT.Order.Model
{
    public enum EmailQueueStatus
    {
        New = 1,
        Processed = 2,
        Error = 3
    }
}