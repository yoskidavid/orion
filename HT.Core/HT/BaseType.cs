﻿using Microsoft.Extensions.Logging;
using System;

namespace HT
{
    public abstract class BaseType : IBaseType
    {
        protected BaseType(ILogger logger) :
            this(null, logger)
        {
        }

        protected BaseType(IServiceProvider services, ILogger logger)
        {
            Services = services;
            Logger = logger;
        }

        public ILogger Logger { get; }

        public IServiceProvider Services { get; }
    }
}
