﻿using HT.Data.Db;
using HT.Data.Generic;
using Newtonsoft.Json;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[ATG_INSERTORDENXML_F]")]
    public class InsertOrderXMLRequestModel : ModelChild<OrderRequestModel>
    {
        public InsertOrderXMLRequestModel()
            : this(null)
        { }

        public InsertOrderXMLRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@piv_NumeroOrden", DbType = DbType.String, Size = 10)]
        public string OrderNumber => Parent.OrderNumber;

        [DbParameter("@pixml_Documento", DbType = DbType.Xml)]
        public string Content
        {
            get
            {
                string jsonString = JsonConvert.SerializeObject(Parent);

                return $"<order>{jsonString}</order>";
            }
        }
    }
}
