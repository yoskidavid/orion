﻿using HT.Configuration;
using HT.Order.Model;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace HT.Order.Logic.Data
{
    public class ComprobanteRepository : RepositoryBase, IComprobanteRepository
    {
        public ComprobanteRepository(IOptions<DbSettings> options) 
            : base(options)
        {
        }

        public async Task<int> InsertarComprobanteAsync(ComprobanteInsertarRequestModel comprobanteInsertarRequestModel)
        { 
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var idComprobante = await conn.QueryFirstOrDefaultAsync<int>("WS_SOL_insCOMPROBANTE", new
                {
                    pID_COMPROBANTE = comprobanteInsertarRequestModel.IdComprobante,
                    pID_ORDEN_COMPRA_DS = comprobanteInsertarRequestModel.IdOrdenComprobanteDS,
                    pNUMERO_ORDEN_COMPRA = comprobanteInsertarRequestModel.NumeroOrdenCompra,
                    pESTADO_COMPROBANTE = comprobanteInsertarRequestModel.EstadoComprobante,
                    pTIPO_COMPROBANTE = comprobanteInsertarRequestModel.TipoComprobante,
                    pSERIE = comprobanteInsertarRequestModel.Serie,
                    pCORRELATIVO = comprobanteInsertarRequestModel.Correlativo,
                    pFECHA_IMPRESION_ORDEN = comprobanteInsertarRequestModel.FechaImpresionOrden,
                    pCALC_MONTO_TOTAL = comprobanteInsertarRequestModel.CalcMontoTotal,
                    pUSUARIOCREACION = comprobanteInsertarRequestModel.UsuarioCreacion
                }, commandType: CommandType.StoredProcedure);


                return idComprobante;
            }
           
        }
    }
}
