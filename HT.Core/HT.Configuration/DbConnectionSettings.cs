﻿using System.Text;

namespace HT.Configuration
{
    public class DbConnectionSettings
    {
        public DbConnectionSettings()
        {
            this.IntegratedSecurity = false;
            this.ConnectionTimeout = 120;
        }

        public string Server { get; set; }

        public string Database { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int ConnectionTimeout { get; set; }

        public bool IntegratedSecurity { get; set; }

        public string Provider { get; set; }

        public string ConnectionString
        {
            get
            {
                bool integratedSecurity = this.IntegratedSecurity;

                StringBuilder connectionString = new StringBuilder();

                connectionString.Append($"server={this.Server};");
                connectionString.AppendFormat($"database={this.Database};");
                connectionString.AppendFormat($"user={this.Username};");

                if (integratedSecurity)
                {
                    connectionString.AppendFormat($"Integrated Security={integratedSecurity};");
                }
                else
                {
                    connectionString.AppendFormat($"password={this.Password};");
                }

                connectionString.AppendFormat($"Connection TimeOut={this.ConnectionTimeout};");

                return connectionString.ToString();
            }
        }
    }
}
