﻿using System.Runtime.Serialization;

namespace HT.Collections
{
    [DataContract]
    public class PagerSettings
    {
        public PagerSettings() :
            this(0, 10)
        { }

        public PagerSettings(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }

        [DataMember]
        public int PageIndex { get; set; }

        [DataMember]
        public int PageSize { get; set; }
    }
}
