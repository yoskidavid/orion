﻿using HT.Data.Db;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace HT.Order.Model
{
    [DataContract]
    public class SolicitudOCActualizarEstadoResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "NumeroPedido")]
        public string NumeroPedido { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "CodigoActEstado")]
        public string CodigoActEstado { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "Observacion")]
        public string Observacion { get; set; }
    }
}
