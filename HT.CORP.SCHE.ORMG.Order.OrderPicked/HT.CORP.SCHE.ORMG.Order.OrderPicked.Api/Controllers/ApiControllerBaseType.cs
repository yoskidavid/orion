﻿using HT.Api;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Api.Controllers
{
    public abstract class ApiControllerBaseType : BaseController
    {
        protected ApiControllerBaseType(IServiceProvider services, ILogger logger, IDatabase<DbDispatch> dbDispatch)
            : base(services, logger)
        {
            DbDispatch = dbDispatch;
        }

        protected IDatabase DbDispatch { get; set; }

        protected string GetLogic()
        {
            string logic = (string)this.Services.GetService(typeof(string));

            return logic;
        }
    }
}
