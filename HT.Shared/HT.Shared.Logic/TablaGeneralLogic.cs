﻿using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using HT.Shared.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
namespace HT.Shared.Logic
{
    public class TablaGeneralLogic : BaseType
    {
        public TablaGeneralLogic(IServiceProvider services, ILogger<TablaGeneralLogic> logger, IDatabase<DbDispatch> dbDispatch)
          : base(services, logger)
        {
            DbDispatch = dbDispatch;
        }
        protected IDatabase DbDispatch { get; set; }

        public IEnumerable<TablaGeneralResponseModel> GetTableGeneralList(TablaGeneralRequestModel request)
        {
            return null;// DbDispatch.ExecuteList<TablaGeneralRequestModel, TablaGeneralResponseModel>(request);
        }
    }
}
