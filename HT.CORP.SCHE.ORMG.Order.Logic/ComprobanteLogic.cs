﻿using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using HT.Order.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace HT.Order.Logic
{
    public class ComprobanteLogic : OrderLogicBaseType
    {
        IComprobanteRepository comprobanteRepository;
        IEmailQueueRepository emailQueueRepository;
        IOrderRepository orderRepository;

        public ComprobanteLogic(IServiceProvider services, 
                                ILogger<ComprobanteLogic> logger, 
                                IDatabase<DbDispatch> dbDispatch, 
                                IComprobanteRepository comprobanteRepository,
                                IEmailQueueRepository emailQueueRepository,
                                IOrderRepository orderRepository)
           : base(services, logger, dbDispatch)
        {
            this.comprobanteRepository = comprobanteRepository;
            this.emailQueueRepository = emailQueueRepository;
            this.orderRepository = orderRepository;
        }

        public async Task<ComprobanteInsertarResponseModel> InsertarComprobanteAsync(ComprobanteInsertarRequestModel request)
        {
            Logger.Log(LogLevel.Information, "******** Inicio de Inserción de Comprobante ********");

            int comrobanteId =
                await comprobanteRepository.InsertarComprobanteAsync(request);

            Logger.Log(LogLevel.Information, "ID Comprobante: {0}, ID Orden Compr", comrobanteId);

            if (comrobanteId == 0)
            {
                return new ComprobanteInsertarResponseModel()
                {
                    code = "99",
                    message = string.Format("El ID de Comprobante {0} ya se encuentra registrado", comrobanteId)
                };
            }

            var tipoOrdenDto =
                await orderRepository.GetOrderTypeAsync(request.IdOrdenComprobanteDS);

            if (tipoOrdenDto.TipoOrden == 0)
            {
                return new ComprobanteInsertarResponseModel()
                {
                    code = "99",
                    message = string.Format("La CO con ID = {0} no existe en el Sistema de Despacho", request.IdOrdenComprobanteDS)
                };
            }

            var emailTemplateCode = EmailQueueCode.EmailTemplateCode_SD;
            if (tipoOrdenDto.TipoOrden == (int)OrderTypeEnum.TOTTUSYA)
            {
                emailTemplateCode = EmailQueueCode.EmailTemplateCode;
            }

            await emailQueueRepository.AddEmailQueueAsync(new EmailQueue()
            {
                EntityID = comrobanteId,
                Message = "{ \"ComprobanteId\":" + comrobanteId + " }",
                EmailTemplateCode = emailTemplateCode,
                EmailTemplateFamilyName = EmailQueueCode.EmailTemplateFamilyName,
                EmailSubject = string.Empty,
                EmailBody = string.Empty,
                Status = (int)EmailQueueStatus.New
            });

            Logger.Log(LogLevel.Information, "******** Fin de Inserción de Comprobante ********");

            return new ComprobanteInsertarResponseModel()
            {
                code = "0",
                message = "Éxito"
            };
        }

        //public ComprobanteInsertarResponseModel InsertarComprobante(ComprobanteInsertarRequestModel request)
        //{
        //    ComprobanteInsertarResponseModel objResponse = new ComprobanteInsertarResponseModel(); 
        //    IResponseEntity<ComprobanteInsertarInnerResponseModel> res = insertarComprobante(request);

        //    if (res.Body != null)
        //    {
        //        objResponse.code = "0";
        //        objResponse.message = "Éxito";
        //        AddEmailQueue(request.IdComprobante);
        //    }
        //    else
        //    {
        //        objResponse.code = "99";
        //        objResponse.message = string.Format("El ID de Comprobante {0} ya se encuentra registrado", request.IdComprobante);
        //    }
        //    return objResponse;
        //}

        //private IResponseEntity<ComprobanteInsertarInnerResponseModel> insertarComprobante(ComprobanteInsertarRequestModel request)
        //{
        //    IResponseEntity<ComprobanteInsertarInnerResponseModel> res = null;
        //    Logger.Log(LogLevel.Information, "******** Inicio de Inserción de Comprobante ********");
        //    Logger.Log(LogLevel.Information, "ID Comprobante: {0}, ID Orden Comprobante DS: {1}, N° Orden: {2}, Estado: {3}, Tipo: {4}, Serie: {5}, Correlativo: {6}, Fecha Impresion Orden: {7}, Monto Total: {8}, Usuario Creación: {9}",
        //        request.IdComprobante, request.IdOrdenComprobanteDS, request.NumeroOrdenCompra, request.EstadoComprobante, request.TipoComprobante, request.Serie,
        //        request.Correlativo, request.FechaImpresionOrden, request.CalcMontoTotal, request.UsuarioCreacion);

        //    try
        //    {
        //        res = DbDispatch.ExecuteEntity<ComprobanteInsertarRequestModel, ComprobanteInsertarInnerResponseModel>(request);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Log(LogLevel.Error, ex, "Inserción de Comprobante", request);
        //    }
        //    finally
        //    {
        //        if (res.Body == null)
        //        {
        //            Logger.Log(LogLevel.Information, "ERROR_CODIGO = {0}, ERROR_MENSAJE = {1}","99", "El ID de Comprobante ya se encuentra registrado");
        //        }
        //        Logger.Log(LogLevel.Information, "******** Fin de Inserción de Comprobante ********");
        //    }

        //    return res;
        //}

        //private void AddEmailQueue(int ComprobanteId)
        //{
        //    EmailQueue objEmail = new EmailQueue();
        //    objEmail.EntityID = ComprobanteId;
        //    objEmail.Message = "{ \"ComprobanteId\":" + ComprobanteId + " }";
        //    objEmail.EmailTemplateCode = EmailQueueCode.EmailTemplateCode;
        //    objEmail.EmailTemplateFamilyName = EmailQueueCode.EmailTemplateFamilyName;
        //    objEmail.EmailSubject = string.Empty;
        //    objEmail.EmailBody = string.Empty;
        //    objEmail.Status = (int)EmailQueueStatus.New;

        //    Logger.Log(LogLevel.Information, "******** Inicio de Inserción en Cola de Mensaje ********");
        //    Logger.Log(LogLevel.Information, "EntityID: {0}, Message: {1}, EmailTemplateCode: {2}, EmailTemplateFamilyName: {3}, Status: {4}, EmailSubject: {5}, EmailBody: {6}",
        //        objEmail.EntityID, objEmail.Message, objEmail.EmailTemplateCode, objEmail.EmailTemplateFamilyName, objEmail.EmailSubject, objEmail.EmailBody, objEmail.Status);

        //    try
        //    {
        //        DbDispatch.ExecuteNonQuery(objEmail);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Log(LogLevel.Error, ex, "Inserción en Cola de Mensaje", objEmail);
        //    }
        //    finally
        //    {
        //        Logger.Log(LogLevel.Information, "******** Fin de Inserción en Cola de Mensaje ********");
        //    }
        //}
    }
}