﻿using HT.Data.Db;
using System.Runtime.Serialization;

namespace HT.Dashboard.Model.MonitorATG
{
    [DataContract]
    public class DashboardATGSDResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "TIPO")]
        public string Tipo { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "FECHA_PROCESO")]
        public string FechaProceso { get; set; }

        [DataMember]
        [DbColumn(Order = 2, Name = "CANTIDAD")]
        public int Cantidad { get; set; }

        [DataMember]
        [DbColumn(Order = 3, Name = "COLOR")]
        public string Color { get; set; }
    }
}