﻿using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace HT.Dashboard.Logic
{
    public class TableLogic : SDLogicBaseType
    {
        public TableLogic(IServiceProvider services, ILogger<TableLogic> logger, IDatabase<DbDispatch> dbDispatch)
           : base(services, logger, dbDispatch)
        {
        }

        public IEnumerable<Model.ProductTableResponseModel> GetTable(int branchId)
        {
            var request = new RequestPager<Model.ProductTableRequestModel>();
            request.Body.BranchID = branchId;
            IEnumerable<Model.ProductTableResponseModel> response = null;
            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de Extracción de Dashboard Productividad ********");
                Logger.Log(LogLevel.Information, "ID Sucursal : {0}",branchId);
                response = DbDispatch.ExecuteList<Model.ProductTableRequestModel, Model.ProductTableResponseModel>(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "WS_productividad_tabla", response);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de Extracción de Dashboard Productividad ********");
                DbDispatch.EnsureDispose();
            }
            return response;
        }
        public IResponseEntity<Model.SucursalResponseModel> GetSucursal(int branchId)
        {
            var request = new RequestPager<Model.SucursalRequestModel>();
            request.Body.ID = branchId;
            IResponseEntity<Model.SucursalResponseModel> response = null;
            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de Obtención de Sucursal ********");
                Logger.Log(LogLevel.Information, "ID Sucursal : {0}", branchId);
                response = DbDispatch.ExecuteEntity<Model.SucursalRequestModel, Model.SucursalResponseModel>(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "ADM_getSucursalesNombre", response);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de Obtención de Sucursal ********");
                DbDispatch.EnsureDispose();
            }
            return response;
        }
    }
}