﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HT.Data.Generic
{
    public class ModelChildren<TParent, TChild> : ModelChild<TParent>, IModelChildren<TParent, TChild>
        where TChild : IModelChild<TParent>
    {
        private IEnumerable<TChild> InternalChildren = new List<TChild>();

        public ModelChildren(TParent parent)
            : base(parent)
        { }

        public ModelChildren(TParent parent, IEnumerable<TChild> initial)
            : base(parent)
        {
            //InternalChildren = initial;
            foreach (TChild child in initial)
            {
                Add(child);
            }
        }

        public int Count => InternalChildren.Count();

        public bool IsReadOnly => false;
        
        public void Add(TChild item)
        {
            item.Parent = Parent;

            (InternalChildren as IList).Add(item);
        }

        void ICollection<TChild>.Clear() => throw new NotImplementedException();

        public bool Contains(TChild item) => InternalChildren.Contains(item);

        void ICollection<TChild>.CopyTo(TChild[] array, int arrayIndex) => throw new NotImplementedException();

        public IEnumerator<TChild> GetEnumerator() => InternalChildren.GetEnumerator();

        bool ICollection<TChild>.Remove(TChild item) => throw new NotImplementedException();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
