﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DbCommand("FOL_INSERTFOLIOESTADOORDEN")]
    public class FolioStateOrdenCrearRequestModel : ModelChild<OrderRequestModel>
    {
        public FolioStateOrdenCrearRequestModel()
            : this(null)
        { }

        public FolioStateOrdenCrearRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@PII_IDOC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [DbParameter("@PIV_CODIGOCREAFOLIO", DbType = DbType.String, Size = 10)]
        public string CodigoCreaFolio => "ATG";

    }
}
