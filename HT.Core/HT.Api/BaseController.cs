﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace HT.Api
{
    public abstract class BaseController : ControllerBase, IBaseType
    {
        protected BaseController(IServiceProvider services, ILogger logger)
        {
            this.Services = services;
            this.Logger = logger;
        }

        public IServiceProvider Services { get; private set; }

        public ILogger Logger { get; private set; }
    }
}
