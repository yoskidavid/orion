﻿using HT.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace HT.Order.Logic.Data
{
    public abstract class RepositoryBase
    {
        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(Options.Value.Connections["Dispatch"].ConnectionString);
            }
        }

        public IOptions<DbSettings> Options { get; }

        public RepositoryBase(IOptions<DbSettings> options)
        {
            Options = options ?? throw new ArgumentNullException(nameof(options));
        }
    }
}
