﻿using HT.Data.Db;
using HT.Data.Generic;
using System;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[PICK_REGISTROMANUAL_ACTUALIZAROC_NEW]")]
    public class PickRecordManualUpdateOCNewRequestModel : ModelChild<OrderRequestModel>
    {
        public PickRecordManualUpdateOCNewRequestModel()
            : this(null)
        { }

        public PickRecordManualUpdateOCNewRequestModel(OrderRequestModel parent)
            : base(parent)
        { }
        
        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [DbParameter("@hora_salida_picking", DbType = DbType.DateTime)]
        public DateTime Departure => DateTime.Now;

        [DbParameter("@hora_llegada_picking", DbType = DbType.DateTime)]
        public DateTime Arrival => DateTime.Now;

        [DbParameter("@ID_Operario", DbType = DbType.Int32)]
        public int OP => 10;

        [DbParameter("@ID_Sucursal", DbType = DbType.Int32)]
        public int BranchID => 0;

        [DbParameter("@Observacion_despacho", DbType = DbType.String, Size = 500)]
        public string DispatchObservation => Parent.Client.DispatchObservation;
        
        [DbParameter("@ID_Usuario", DbType = DbType.Int32)]
        public string UserID => Parent.UserID;
    }
}
