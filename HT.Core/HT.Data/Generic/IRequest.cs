﻿namespace HT.Data.Generic
{
    public interface IRequest<T> : IRequest
    {
        new T Body { get; }
    }
}
