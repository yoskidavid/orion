﻿using System.Data;
using System.Reflection;

namespace HT.Data.Db
{
    public static class IDbConectionExtension
    {
        public static IDbCommand CreateCommand(this IDbConnection dbConnection, string commandText) =>
            dbConnection.CreateCommand(null, commandText);

        public static IDbCommand CreateCommand(this IDbConnection dbConnection, IDbTransaction dbTransaction, string commandText) =>
            dbConnection.CreateCommand(dbTransaction, commandText, CommandType.StoredProcedure);

        public static IDbCommand CreateCommand(this IDbConnection dbConnection, IDbTransaction dbTransaction, string commandText, CommandType commandType)
        {
            IDbCommand dbCommand = dbConnection.CreateCommand();

            dbCommand.CommandText = commandText;
            dbCommand.CommandType = commandType;
            dbCommand.Transaction = dbTransaction;

            return dbCommand;
        }

        public static IDbCommand CreateCommand<T>(this IDbConnection dbConnection, T instance) where T : new()
            => CreateCommand<T>(dbConnection, null, instance);

        public static IDbCommand CreateCommand<T>(this IDbConnection dbConnection, IDbTransaction dbTransaction, T instance) // where T : new()
        {
            var type = typeof(T);

            var commandAttribute = type.GetCustomAttribute<DbCommandAttribute>();

            if (object.Equals(commandAttribute, null))
            {
                throw new System.Exception($"{type.FullName} Command attributes is not yet configured.");
            }

            var commandText = commandAttribute.CommandText;
            var commandType = commandAttribute.CommandType;

             IDbCommand dbCommand = dbConnection.CreateCommand(dbTransaction, commandText, commandType);

            dbCommand.CreateParameters(instance);            

            return dbCommand;
        }
    }
}
