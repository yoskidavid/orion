﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    public class LogApiModel
    {
        public LogApiModel(string USR_NOMBRE)
        {
            this.USR_NOMBRE = string.IsNullOrEmpty(USR_NOMBRE) ? "API_USR" : USR_NOMBRE;
        }
        public string NUMERO_ORDEN { get; set; } 
        public string ESTADO_HTTP { get; set; }
        public string SISTEMA => "API_TOTTUS_YA";
        public string MENSAJE { get; set; }
        public string TIEMPO { get; set; }
        public string DATA { get; set; }
        public string USR_NOMBRE   { get; set; }
        public string SIGNATURE { get; set; }
    }
}
