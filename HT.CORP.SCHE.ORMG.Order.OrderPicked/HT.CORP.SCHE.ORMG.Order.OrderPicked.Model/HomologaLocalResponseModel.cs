﻿using System;
using System.Collections.Generic;
using System.Text;
using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    public class HomologaLocalResponseModel
    {
        [DbColumn(Order = 0, Name = "local_origen")]
        public int local_origen { get; set; }

        [DbColumn(Order = 1, Name = "local_destino")]
        public int local_destino { get; set; }
    }
}
