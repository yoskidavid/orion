﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;

namespace HT.Data.Db
{
    public static class IDataReaderEntityExtension
    {
        public static T GetEntity<T>(this IDataReader reader)
        {
            T instance = Activator.CreateInstance<T>();

            Type type = typeof(T);

            IEnumerable<PropertyInfo> properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                DbColumnAttribute member = property.GetCustomAttribute<DbColumnAttribute>();

                if (member != null)
                {
                    int ordinal = member.Order;

                    if (ordinal < 0)
                    {
                        string name = member.Name;

                        if (string.IsNullOrEmpty(name))
                        {
                            name = property.Name;
                        }

                        ordinal = reader.GetOrdinal(name);
                    }

                    if (reader.IsDBNull(ordinal))
                    {
                        property.SetValue(instance, null);
                    }
                    else
                    {
                        property.SetValue(instance, reader.GetValue(ordinal));
                    }
                }
            }

            return instance;
        }

        public static T ReadEntity<T>(this IDataReader reader) =>
            ((DbDataReader)reader).ReadEntity<T>();

        public static T ReadEntity<T>(this DbDataReader reader)
        {
            T instance;

            if (reader.HasRows)
            {
                reader.Read();

                instance = reader.GetEntity<T>();
            }
            else
            {
                instance = default(T);
            }

            reader.Close();

            return instance;
        }        
    }
}
