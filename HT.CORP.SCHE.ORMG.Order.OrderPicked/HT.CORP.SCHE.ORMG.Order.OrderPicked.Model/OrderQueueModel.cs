﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    public class OrderQueueModel
    {
        public OrderQueueModel() { }

        public OrderQueueModel(string message, string messageType, string NUMERO_ORDEN,string SIGNATURE)
        {
            this.Message = message;
            this.MessageType = messageType;
            this.NUMERO_ORDEN = NUMERO_ORDEN;
            this.SIGNATURE = SIGNATURE;
        }

        public string Message { get; set; }
        public string MessageType { get; set; }
        public string NUMERO_ORDEN { get; set; }
        public string SIGNATURE { get; set; }

    }

    public enum QueueStatus

    {

        New = 1,

        Processed = 2,

        Error = 3,

        ErrorNoRetry = 4

    }
}
