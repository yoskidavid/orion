﻿namespace HT.Data.Generic
{
    public interface IModelChild<TParent>
    {
        TParent Parent { get; set; }
    }
}
