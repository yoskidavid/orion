﻿using Microsoft.Extensions.DependencyInjection;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Logic
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddOrderPickedLogic(this IServiceCollection services)
        { 
            services.AddTransient<OrderLogic>(); 

            return services;
        }
    }
}
