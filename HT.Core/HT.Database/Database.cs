﻿using HT.Collections.Generic;
using HT.Configuration;
using HT.Data;
using HT.Data.Db;
using HT.Data.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace HT.Database
{
    public abstract class Database : BaseType, IDatabase
    {
        #region [ Constructor's ]

        protected Database(ILogger logger, IOptions<DbSettings> dbSettings)
            : base(logger)
        {
            DbSettings = dbSettings;
        }

        #endregion

        #region DbSettingName

        internal abstract string DbSettingName { get; }

        #endregion
        
        public IOptions<DbSettings> DbSettings { get; private set; }
        
        public DbConnectionSettings DbConnectionSettings => DbSettings.Value.Connections[DbSettingName];

        #region DbConnection

        public IDbConnection DbConnection
        {
            get
            {
                EnsureDbConnection();

                return DbConnectionInternal;
            }
        }

        #endregion

        private IDbConnection DbConnectionInternal { get; set; }

        public IDbTransaction DbTransaction { get; protected set; }

        #region EnsureDbConnection

        private void EnsureDbConnection()
        {
            if (object.Equals(DbConnectionInternal, null))
            {
                DbConnectionSettings dbConnectionSettings = DbConnectionSettings;

                DbConnectionInternal = DbProviderFactory.CreateConnection(dbConnectionSettings);
            }
        }

        #endregion

        #region EnsureClose

        public void EnsureClose()
        {
            string dbName = DbConnectionSettings.Database;

            IDbConnection dbConnection = DbConnection;

            if (dbConnection.State == ConnectionState.Open)
            {
                Logger.LogDebug($"Cerrando conexión con la base de datos \"{dbName}\".");

                try
                {
                    dbConnection.Close();
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message);
                    Logger.LogDebug(ex, "CATCH");

                    throw;
                }

                Logger.LogDebug($"Conexión con la base de datos \"{dbName}\" cerrada.");
            }
            else
            {
                Logger.LogDebug($"La conexión base de datos \"{dbName}\" ya esta cerrada.");
            }
        }

        #endregion

        #region EnsureDispose

        public void EnsureDispose()
        {
            string dbName = DbConnectionSettings.Database;

            IDbConnection dbConnection = DbConnection;

            Logger.LogDebug($"Desechando conexión con la base de datos \"{dbName}\".");

            if (dbConnection != null)
            {
                EnsureClose();

                dbConnection.Dispose();

                Logger.LogDebug($"Conexión con la base de datos \"{dbName}\" desechada.");
            }
            else
            {
                Logger.LogDebug($"No se puede desechar una conexión con la base de datos \"{dbName}\" no instanciada.");
            }
        }

        #endregion

        #region EnsureOpen

        public void EnsureOpen() => EnsureOpen(false);

        public void EnsureOpen(bool beginTransaction)
        {
            string dbName = DbConnectionSettings.Database;

            IDbConnection dbConnection = DbConnection;

            if (dbConnection.State != ConnectionState.Open)
            {
                Logger.LogDebug($"Abriendo conexión con la base de datos \"{dbName}\".");

                try
                {
                    dbConnection.Open();
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message);
                    Logger.LogDebug(ex, "CATCH");

                    throw;
                }
                finally
                {
                    DbTransaction = (beginTransaction ? dbConnection.BeginTransaction() : null);
                }

                Logger.LogDebug($"Conexión con la base de datos \"{dbName}\" abierta.");
            }
            else
            {
                Logger.LogDebug($"La conexión base de datos \"{dbName}\" ya esta abierta.");
            }
        }

        #endregion

        public void Commit()
        {
            DbTransaction.Commit();
        }

        public void Rollback()
        {
            if (DbTransaction != null)
            {
                DbTransaction.Rollback();
            }
        }

        #region ExecuteReader

        public IDataReader ExecuteReader<TRequest>(TRequest request)
            where TRequest : new() =>
            ExecuteReader(request, CommandBehavior.Default);

        public IDataReader ExecuteReader<TRequest>(TRequest request, CommandBehavior commandBehavior)
            where TRequest : new()
        {
            IDbConnection dbConnection = DbConnection;

            IDbCommand dbCommand = dbConnection.CreateCommand(request);

            dbCommand.Transaction = DbTransaction;

            Logger.LogDebug($"Ejecutando el stored procedure: \"{dbCommand.CommandText}\" => parametros:{GetParameters(dbCommand)}");

            IDataReader reader = dbCommand.ExecuteReader(commandBehavior);

            return reader;
        }

        #endregion

        #region ExecuteNonQuery

        public IResponseNonQuery ExecuteNonQuery<TRequest>(IRequest<TRequest> request)
            where TRequest : new()
        {
            TRequest body = request.Body;

            return ExecuteNonQuery(body);
        }

        public IResponseNonQuery ExecuteNonQuery<TRequest>(TRequest request)
            where TRequest : new()
        {
            IDbConnection dbConnection = DbConnection;

            IDbCommand dbCommand = dbConnection.CreateCommand(request);

            dbCommand.Transaction = DbTransaction;

            Logger.LogDebug($"Ejecutando el stored procedure: \"{dbCommand.CommandText}\" => parametros:{GetParameters(dbCommand)}" );

            var response = new ResponseNonQuery(dbCommand.ExecuteNonQuery());

            var parameters = dbCommand.Parameters;

            foreach (IDbDataParameter parameter in parameters)
            {
                if (parameter.Direction == ParameterDirection.Output || parameter.Direction == ParameterDirection.InputOutput)
                {
                    PropertyInfo[] properties = typeof(TRequest).GetProperties();

                    foreach (var property in properties)
                    {
                        var attribute = property.GetCustomAttribute<DbParameterAttribute>();

                        if (!object.Equals(attribute, null) && attribute.ParameterName == parameter.ParameterName)
                        {
                            var dataMemberAttribute = property.GetCustomAttribute<System.Runtime.Serialization.DataMemberAttribute>();

                            string name = (dataMemberAttribute != null && !string.IsNullOrEmpty(dataMemberAttribute.Name) ? dataMemberAttribute.Name : property.Name.ToCamelCase());

                            response.Values.Add(name, parameter.Value);
                            property.SetValue(request, parameter.Value);
                        }
                    }
                }
            }

            return response;
        }

        private string GetParameters(IDbCommand dbCommand)
        {
            System.Text.StringBuilder result = new System.Text.StringBuilder();
            var parameters = dbCommand.Parameters;
            if (parameters.Count > 0)
            {
                foreach (IDbDataParameter parameter in parameters)
                {
                    result.Append($",{parameter.Value}");// parameter.
                }
                return result.ToString().Substring(1);
            }
            else
            {
                return "";
            } 
            
        }

        #endregion

        #region ExecuteEntity

        public IResponseEntity<TResponse> ExecuteEntity<TRequest, TResponse>(TRequest request)
            where TRequest : new()
            where TResponse : new() =>
            ExecuteEntity<TRequest, TResponse>(request, CommandBehavior.SingleRow);

        public IResponseEntity<TResponse> ExecuteEntity<TRequest, TResponse>(IRequest<TRequest> request)
            where TRequest : new()
            where TResponse : new() =>
            ExecuteEntity<TRequest, TResponse>(request.Body, CommandBehavior.SingleRow);

        public IResponseEntity<TResponse> ExecuteEntity<TRequest, TResponse>(TRequest request, CommandBehavior commandBehavior)
            where TRequest : new()
            where TResponse : new()
        {
            IDataReader reader = ExecuteReader<TRequest>(request, commandBehavior);

            TResponse entity = reader.ReadEntity<TResponse>();

            IResponseEntity<TResponse> response = new ResponseEntity<TResponse>(entity);

            return response;
        }

        #endregion

        #region ExecutePager

        public IResponsePager<TResponse> ExecutePager<TRequest, TResponse>(IRequestPager<TRequest> request)
            where TRequest : new()
            where TResponse : new() =>
            ExecutePager<TRequest, TResponse>(request, CommandBehavior.SequentialAccess);

        public IResponsePager<TResponse> ExecutePager<TRequest, TResponse>(IRequestPager<TRequest> request, CommandBehavior commandBehavior)
            where TRequest : new()
            where TResponse : new()
        {
            IDataReader reader = ExecuteReader<TRequest>(request.Body, commandBehavior);

            IPagedEnumerable<TResponse> pageEnumerable = reader.ReadPager<TResponse>(request);

            IResponsePager<TResponse> response = new ResponsePager<TResponse>(pageEnumerable);

            return response;
        }

        #endregion

        public IEnumerable<TResponse> ExecuteList<TRequest, TResponse>(TRequest request)
            where TRequest : new()
            where TResponse : new() =>
            ExecuteList<TRequest, TResponse>(request, CommandBehavior.SequentialAccess);

        public IEnumerable<TResponse> ExecuteList<TRequest, TResponse>(IRequest<TRequest> request)
            where TRequest : new()
            where TResponse : new() =>
            ExecuteList<TRequest, TResponse>(request.Body, CommandBehavior.SingleResult);

        public IEnumerable<TResponse> ExecuteList<TRequest, TResponse>(TRequest request, CommandBehavior commandBehavior)
            where TRequest : new()
            where TResponse : new()
        {
            IDataReader reader = ExecuteReader<TRequest>(request, commandBehavior);

            IEnumerable<TResponse> enumerable = reader.ReadEnumerable<TResponse>().ToList();

            reader.Close();

            return enumerable;
        }
    }
}
