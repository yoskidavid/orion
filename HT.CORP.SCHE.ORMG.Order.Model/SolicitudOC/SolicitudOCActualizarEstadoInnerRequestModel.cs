﻿using HT.Data.Db;
using System.Data;
using System.Runtime.Serialization;

namespace HT.Order.Model
{
    [DataContract]
    [DbCommand("WS_SOL_UpdateSolicitudOC")]
    public class SolicitudOCActualizarEstadoInnerRequestModel
    {
        [DataMember(Order = 0)]
        [DbParameter("@NUMERO_ORDEN", DbType = DbType.String)]
        public string NumeroOrden { get; set; }

        [DataMember(Order = 1)]
        [DbParameter("@NUMERO_POS", DbType = DbType.String)]
        public string NumeroPos { get; set; }

        [DataMember(Order = 2)]
        [DbParameter("@NUMERO_TIENDA", DbType = DbType.String)]
        public string NumeroTienda { get; set; }

        [DataMember(Order = 3)]
        [DbParameter("@IDCAJERO", DbType = DbType.String)]
        public string IdCajero { get; set; }

        [DataMember(Order = 4)]
        [DbParameter("@IDTG_ESTADO", DbType = DbType.Int32)]
        public int IdTGEstado { get; set; }

        [DataMember(Order = 5)]
        [DbParameter("@IDTG_OBSERVACION", DbType = DbType.String)]
        public string IdTGObservacion { get; set; }

        [DataMember(Order = 6)]
        [DbParameter("@IDTG_REQUEST", DbType = DbType.Xml)]
        public string IdTGRequest { get; set; }

        [DataMember(Order = 7)]
        [DbParameter("@MODIFICADOPOR", DbType = DbType.String)]
        public string ModificadoPor { get; set; }

        [DataMember(Order = 8)]
        [DbParameter("@FLG_PICKING_POS", DbType = DbType.Boolean)]
        public bool FlgPickingPos { get; set; }

        [DataMember(Order = 9)]
        [DbParameter("@ERROR_CODIGO", DbType = DbType.String, Direction = ParameterDirection.Output)]
        public string ErrorCodigo { get; set; }

        [DataMember(Order = 10)]
        [DbParameter("@ERROR_MENSAJE", DbType = DbType.String, Direction = ParameterDirection.Output)]
        public string ErrorMensaje { get; set; }
    }
}
