﻿using HT.Data.Db;
using System.Data;
using System.Runtime.Serialization;

namespace HT.Order.Model
{
    [DataContract]
    [DbCommand("WS_API_instEmailQueue")]
    public class EmailQueue
    {
        [DataMember]
        [DbParameter("@EntityID", DbType = DbType.Int64)]
        public long EntityID { get; set; }
        [DataMember]
        [DbParameter("@Message", DbType = DbType.String)]
        public string Message { get; set; }
        [DataMember]
        [DbParameter("@Status", DbType = DbType.Int32)]
        public int Status { get; set; }
        [DataMember]
        [DbParameter("@EmailTemplateFamilyName", DbType = DbType.String)]
        public string EmailTemplateFamilyName { get; set; }
        [DataMember]
        [DbParameter("@EmailTemplateCode", DbType = DbType.String)]
        public string EmailTemplateCode { get; set; }
        [DataMember]
        [DbParameter("@EmailBody", DbType = DbType.String)]
        public string EmailBody { get; set; }
        [DataMember]
        [DbParameter("@EmailSubject", DbType = DbType.String)]
        public string EmailSubject { get; set; }
    }
}
