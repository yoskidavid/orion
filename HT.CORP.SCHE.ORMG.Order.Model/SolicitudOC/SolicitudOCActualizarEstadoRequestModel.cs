﻿using HT.Data.Db;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace HT.Order.Model
{
    public class SolicitudOCActualizarEstadoRequestModel
    {
        [DataMember(Order = 0)]
        public string NumeroPedido { get; set; }

        [DataMember(Order = 0)]
        public int EstadoPedido { get; set; }

        [DataMember(Order = 0)]
        public string Observacion { get; set; }
    }
}
