﻿using System.Collections;

namespace HT.Collections
{
    public interface IPagedEnumerable : IEnumerable
    {
        Pager Pager { get; }
    }
}
