﻿using HT.Database;
using HT.Dashboard.Logic;
using HT.Shared.Logic;
using HT.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;

namespace HT.Dashboard.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddOptions();
            services
                .AddCors(options =>
                {
                    options.AddPolicy("AllowSpecificOrigin",
                        builder =>
                            builder.WithOrigins("*")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowAnyOrigin()
                            .AllowCredentials());
                })
                .AddSingleton<ILoggerFactory, LoggerFactory>()
                .AddSingleton(typeof(ILogger<>), typeof(Logger<>))
                .AddDatabases()
                .AddSharedLogic()
                .AddDashboardLogic();

            services.Configure<DbSettings>(Configuration);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Dashboard API",
                    Description = "Contiene los servicios para la consulta de los Dashboards de SD",
                    Contact = new Contact
                    {
                        Name = "Sebastian Sancho",
                        Email = "ssancho@neksysse.com",
                        Url = "http://www.neksysse.com"
                    },
                    License = new License
                    {
                        Name = "Tottus"
                    }
                });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowSpecificOrigin");
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("swagger/v1/swagger.json", "API Order V1");
                c.RoutePrefix = string.Empty;
            });
            app.UseMvc();
        }
    }
}