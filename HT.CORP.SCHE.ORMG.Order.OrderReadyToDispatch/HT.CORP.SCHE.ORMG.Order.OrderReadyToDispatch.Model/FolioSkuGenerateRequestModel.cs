﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DataContract]
    [DbCommand("FOL_INSERTFOLIOPRODUCTO")]
    public class FolioSkuGenerateRequestModel :  ModelChild<FolioGeneraRequestModel>
    {
        private FolioInjectToProductRequestModel m_clientRequestModel;

        public FolioSkuGenerateRequestModel()
            : this(null)
        { }

        public FolioSkuGenerateRequestModel(FolioGeneraRequestModel parent)
            : base(parent)
        { }

        [IgnoreDataMember]
        [DbParameter("@PIV_NUMEROFOLIO", DbType = DbType.String)]
        public string FolioNumber { get => Parent.FolioNumber; }

        [DataMember(Name = "skuCode")]
        [DbParameter("@PIV_SKU", DbType = DbType.String)]
        public string Sku { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PIV_EAN", DbType = DbType.String)]
        public string Ean { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PIN_FLETE", DbType = DbType.Decimal)]
        public decimal PriceDispatch => Parent.folioPriceDispatch;

        [IgnoreDataMember]
        public FolioInjectToProductRequestModel Product => this.EnsureInstance(ref m_clientRequestModel);
    }
}
