﻿using System;
using System.Runtime.Serialization;
using HT.Data.Db;

namespace HT.Dashboard.Model.MonitorATG
{
    [DataContract]
    public class ExportToExcelDashBoardResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "TIPO")]
        public string tipo { get; set; }
        [DataMember]
        [DbColumn(Order = 1, Name = "NRO_OC")]
        public string nroOC { get; set; }
        [DataMember]
        [DbColumn(Order = 2, Name = "FOLIO")]
        public string folio { get; set; }
        [DataMember]
        [DbColumn(Order = 3, Name = "ESTADO")]
        public string estado { get; set; }
        [DataMember]
        [DbColumn(Order = 4, Name = "FECHA_PROCESO")]
        public DateTime fechaProceso { get; set; }
    }
}