﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DbCommand("ORD_PROCESO_DERIVA_CD")]
    public class DeriveCDRequestModel : ModelChild<OrderRequestModel>
    {
        public DeriveCDRequestModel()
            : this(null)
        { }

        public DeriveCDRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@PID_OC", DbType = DbType.Int32)]
        public int? OrderID { get => Parent.OrderID; }

        [DbParameter("@PCREADOPOR", DbType = DbType.String, Size = 25)]
        public string CreadoPor { get => "ATG"; }
    }
}
