﻿using HT.Collections;
using System.Runtime.Serialization;

namespace HT.Data.Generic
{
    [DataContract]
    public class RequestPager<T> : Request<T>, IRequestPager<T>
    {
        public RequestPager()
        {
            AllowPaging = false;
        }

        [DataMember]
        public bool AllowPaging { get; set; }
        
        [DataMember]
        public PagerSettings PagerSettings
        {
            get
            {
                EnsureInnerPagerSettings();

                return InnerPagerSettings;
            }
        }
        
        private PagerSettings InnerPagerSettings { get; set; }

        private void EnsureInnerPagerSettings()
        {
            if (Equals(InnerPagerSettings, null))
            {
                InnerPagerSettings = new PagerSettings();
            }
        }
    }
}
