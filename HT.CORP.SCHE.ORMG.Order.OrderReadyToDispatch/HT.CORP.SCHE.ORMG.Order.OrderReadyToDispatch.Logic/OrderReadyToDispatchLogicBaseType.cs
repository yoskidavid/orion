﻿using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Logic
{
    public abstract class OrderReadyToDispatchLogicBaseType : BaseType
    {
        protected OrderReadyToDispatchLogicBaseType(ILogger logger, IDatabase<DbDispatch> dbDispatch) :
            this(null, logger, dbDispatch)
        { }

        protected OrderReadyToDispatchLogicBaseType(IServiceProvider services, ILogger logger, IDatabase<DbDispatch> dbDispatch) :
            base(services, logger)
        {
            this.DbDispatch = dbDispatch;
        }

        public IDatabase DbDispatch
        {
            get;
            private set;
        }
    }
}
