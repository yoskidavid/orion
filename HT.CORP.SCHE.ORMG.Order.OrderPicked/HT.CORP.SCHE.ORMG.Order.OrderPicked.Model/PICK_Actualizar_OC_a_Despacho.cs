﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Text;
using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("PICK_Actualizar_OC")]
    public class PICK_Actualizar_OC
    {
     
        [DbParameter("@Etapa", DbType = DbType.String)]
        public string Stage { get; set; }

        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public Int32 IdOC { get; set; }

        [DbParameter("@empaque_bins", DbType = DbType.String,Size = 250)]
        public string BinsPacking { get => ""; }

        [DbParameter("@empaque_coolers", DbType = DbType.String, Size = 250)]
        public string CoolersPacking { get => ""; }

        [DbParameter("@empaque_sueltos", DbType = DbType.String, Size = 500)]
        public string FreePacking { get => ""; }

        [DbParameter("@numero_bins", DbType = DbType.Int32)]
        public Int32 BinsNumbers { get => 0; }

        [DbParameter("@numero_coolers", DbType = DbType.Int32)]
        public Int32 CoolersNumbers { get => 0; }

        [DbParameter("@numero_sueltos", DbType = DbType.Int32)]
        public Int32 FreeNumbers { get => 0; }
    }
}
