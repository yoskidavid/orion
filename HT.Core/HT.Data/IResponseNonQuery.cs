﻿namespace HT.Data
{
    public interface IResponseNonQuery : IResponse
    {
        int Duration { get; }

        int RecordsAffected { get; }
    }
}
