﻿using System.Collections.Generic;

namespace HT.Data.Generic
{
    public interface IResponse : HT.Data.IResponse
    {
        new IDictionary<string, object> Values { get; }
    }
}
