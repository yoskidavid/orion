﻿using HT.Shared.Model;
using HT.CORP.SCHE.ORMG.Order.OrderPicked.Model;
using HT.Data;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.IO;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Logic
{
    public class OrderLogic : OrderPickerLogicBaseType
    {
        IOrderPickedRepository orderPickedRepository;
        public OrderLogic(IServiceProvider services, ILogger<OrderLogic> logger, IDatabase<DbDispatch> dbDispatch, IOrderPickedRepository orderPickedRepository)
            : base(services, logger, dbDispatch)
        {
            this.orderPickedRepository = orderPickedRepository;   
        }

        #region ValidateInput

        private async Task<RequestModel> ValidateInput(RequestModel request)
        {
            var ubigeoRequest = new UbigeoRequestModel();
            var ubigeoResponse = await orderPickedRepository.GetUbigeos();
             
            var client = request.Body.Client;
 
            var tipoDocumentos = await orderPickedRepository.GetDataFromTablaGeneral(2, "TIP_DOC");

             

            if (!tipoDocumentos.ToList().Exists(x => x.COD_INTERNO.ToLower().Trim() == client.CustomerDocumentType.ToLower().Trim()))
            {
                throw new System.Exception("Cliente con tipo de documento no registrado.");
            }

            if (string.IsNullOrEmpty(client.CustomerDocumentNumber))
            {
                throw new System.Exception("Cliente no tiene Número de Documento.");
            }
             
            if (string.IsNullOrEmpty(client.CustomerFullName))
            {
                throw new System.Exception("Cliente no tiene Nombre.");
            }

            if (string.IsNullOrEmpty(client.DispatchAddress))
            {
                throw new System.Exception("Cliente no tiene Dirección de Despacho.");
            }

            var ubigeo = ubigeoResponse.ToList().Find(x => x.CODDISTRITO.Trim() == client.ComunaID.ToString());

            if (ubigeo != null) { }
            else
            {
                throw new System.Exception("El código de comunaId no está registrado.");
            }

            var itemsQuantity = request.Body.Products.Count();
            var uniqueItemsQuantity = (from x in request.Body.Products
                                       select x.Sku).Distinct().Count();

            if (itemsQuantity == 0)
            {
                throw new System.Exception("La orden no con cuenta con productos.");
            }
            else
            {
        

                if (itemsQuantity != uniqueItemsQuantity)
                {
                    throw new System.Exception("La orden tiene productos duplicados");
                }



                var existsProductListPriceZero = (from x in request.Body.Products
                                                  where x.ListPrice <= 0
                                                  select x).Count(); 

                if (existsProductListPriceZero > 0)
                {
                    throw new System.Exception("La orden tiene productos con precio de lista con valor cero");
                }

                var existsDiscount = (from x in request.Body.Products
                                      where x.Discount < 0
                                      select x).Count();

                if (existsDiscount > 0)
                {
                    throw new System.Exception("La orden tiene productos con descuentos negativos");
                }
            }

          

            return request;

        }

        #endregion

        public async Task<IResponseNonQuery> Create(RequestModel request,string Signature)
        {
            IResponseNonQuery response;
             
            string orderValidateMessage = await ValidateOrder(request);
          
            if(String.IsNullOrEmpty(orderValidateMessage))
            { 
                request = await ValidateInput(request); 
                var responseParameter = await orderPickedRepository.GetParameterValue("CODIGOPAIS"); 
                request.Body.Client.CountryID = Convert.ToInt32(responseParameter.XPARAM_VALOR); 
                var ListaTipoOrden =  await orderPickedRepository.GetDataFromTablaGeneral(2, "TIPOORDEN");
                var TipoOrden = ListaTipoOrden.ToList().Find(x => x.IDTABLAGENERAL == request.Body.OrderTypeID);
                string jsonString = JsonConvert.SerializeObject(request.Body);
                OrderQueueModel orderQueue = new OrderQueueModel(jsonString, TipoOrden.DESCRIPCION.Replace(" ",""), request.Body.OrderNumber, Signature);
                var rowAffected = await orderPickedRepository.SaveInOrderQueue(orderQueue);
                response = new ResponseNonQuery(rowAffected); 
            }
            else
            {
                throw new System.Exception(orderValidateMessage);
            }

            return response;
        }

        public async Task<Int32> WriteLog(LogApiModel logModel) {
            var rowsAfeccted = await orderPickedRepository.SaveInLog(logModel);
            return rowsAfeccted;
        }
        #region "Validaciones de la Orden de Compra"
        private async Task<string> ValidateOrder(RequestModel request)
        {
            string orderNumber = request.Body.OrderNumber.Trim();
            int orderID = Convert.ToInt32(request.Body.OrderID);
            string country = request.Body.Pais.ToLower() ;
            string mensaje = "";
            if (!string.IsNullOrEmpty(orderNumber))
            {
                var OrderResponse = await orderPickedRepository.ExistsOrder(orderNumber);

                if (OrderResponse != null)
                {

                    var success = true;
                    if (request.Body.Reset_flag)
                    {
                        switch (country)
                        {
                            case "pe":
                                
                                 
                                var orderFlagPOS = await orderPickedRepository.GetFlagPostVirtual(orderNumber);
                                
                                if (orderFlagPOS.FLAG_POSVIRTUAL == 1)
                                {
                                    mensaje = "La orden de compra está en el PosVirtual";
                                    success = false;
                                }
                                break;
                        }


                        if (success)
                        { 
                            var response = await orderPickedRepository.ValidaTablaSolicitudOC(orderNumber);
                             
                            if (response != null && response.COD_INTERNO == 3)
                            {
                                mensaje = string.Format("{0} - {1}", response.COD_INTERNO, response.VALOR1);

                                success = false;
                            } 
                        }
                    }
                    else
                    {
                        mensaje = "La orden de compra ya existe";
                    }
                }
            }
            else
            {
                mensaje = "Ingresar el número de la orden";
            } 
            
            return mensaje;
        }
      
        #endregion

        public IResponseNonQuery Create(OrderRequestModel model)
        {
            return DbDispatch.ExecuteNonQuery(model);
        }

        #region Existe

        public bool Exists(ExistsOrderRequestModel model)
        {
            var  response = DbDispatch.ExecuteEntity<ExistsOrderRequestModel, ExistsOrderResponseModel>(model);

            return (response.Body != null);
        }

        #endregion

        public class EnviarNotificacionRequest
        {
            public string JsonMessage { get; set; }
            public string EmailTemplateCode { get; set; }
        }

        public async Task<string> EnviarCorreoAPINotificacion(string OrderId, string Error, string EmailTemplateCode, string url)
        {

            //Logger.LogInformation($"[NUM={objPMM.numero_folio}] Status = {objPMM.status}");
            var response = string.Empty;

            //---------------------------------------------------------------------------------------            
            EnviarNotificacionRequest objRequest = new EnviarNotificacionRequest();

            objRequest.JsonMessage = "{ \"OrderId\":" + OrderId + ", \"ErrorMessage\": \"" + Error + "\" }";            
            objRequest.EmailTemplateCode = EmailTemplateCode;//ERROR_TOTTUS_YA

            response = await POST(url + "EnviarNotificacion", JsonConvert.SerializeObject(objRequest));

            return response;
            //---------------------------------------------------------------------------------
        }

        public async Task<string> POST(string url, string jsonContent)
        {
            string result = String.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    length = response.ContentLength;
                }
            }
            catch (WebException ex)
            {
                result = ex.Message;
            }
            return result;
        }


    }
}
