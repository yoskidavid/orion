﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DataContract]
    [DbCommand("ORD_INSERTDETALLEORDEN_F")]
    public class ProductRequestModel : ModelChild<OrderRequestModel>
    {
        public ProductRequestModel() : this(null)
        { }

        public ProductRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        private string skuCode;
        private string eanCode;
        private string subClass;

        [IgnoreDataMember]
        [DbParameter("@pii_ID_DETALLE_OC", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int? ProductID { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PII_ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [DataMember(Name = "skuCode")]
        [DbParameter("@PIV_CODIGOSKU", DbType = DbType.String)]
        public string Sku { get { return skuCode.Trim(); } set { skuCode = value.Trim(); } }

        [DataMember(Name = "ean")]
        [DbParameter("@PIV_CODIGOEAN", DbType = DbType.String)]
        public string Ean { get { return eanCode.Trim(); } set { eanCode = value.Trim(); } }

        // VALIDAR
        [IgnoreDataMember]
        [DbParameter("@PIV_TIPOMEDIDA", DbType = DbType.String)]
        public string ProductType => "P";

        [DataMember]
        [DbParameter("@PIV_NOMBREPRODUCTO", DbType = DbType.String)]
        public string SkuDescription { get; set; }

        [DataMember]
        [DbParameter("@PIV_MARCA", DbType = DbType.String)]
        public string Brand { get; set; }

        [DataMember]
        [DbParameter("@PIV_MODELO", DbType = DbType.String)]
        public string Format { get; set; }

        [DataMember]
        [DbParameter("@PIV_CODIGOLINEA", DbType = DbType.String)]
        public string SubClass { get { return subClass.Trim(); } set { subClass = value.Trim(); } }


        [DataMember]
        [DbParameter("@PIV_DESCLINEA", DbType = DbType.String)]
        public string SubClassDescription { get; set; }

        [DataMember]
        [DbParameter("@PIF_PRECIOLISTA", DbType = DbType.Decimal)]
        public decimal ListPrice { get; set; }

        [DataMember]
        [DbParameter("@PIF_DESCUENTO", DbType = DbType.Decimal)]
        public decimal Discount { get; set; }

        [DataMember]
        [DbParameter("@PIF_PRECIOUNITARIO", DbType = DbType.Decimal)]
        public decimal UnitPrice { get; set; }

        [DataMember]
        [DbParameter("@PIF_CANTIDAD", DbType = DbType.Decimal)]
        public decimal Quantity { get; set; }

        // UN | KG
        [DataMember(Name = "unit")]
        [DbParameter("@PIV_UNIDAD", DbType = DbType.String)]
        public string UnitMeasure { get; set; }

        [DataMember]
        [DbParameter("@PIF_PRECIOTOTAL", DbType = DbType.Decimal)]
        public decimal TotalPrice { get; set; }

        [DataMember(Name = "promotionId")]
        [DbParameter("@PIV_IDPROMOCION", DbType = DbType.String)]
        public string PromotionID { get; set; }

        [DataMember(Name = "promotionDesc")]
        [DbParameter("@PIV_PROMOCIONDESC", DbType = DbType.String)]
        public string PromotionDescription { get; set; }

        [DataMember]
        [DbParameter("@PIV_COMENTARIO", DbType = DbType.String)]
        public string SubstitutionCriteria { get; set; }

        [DataMember]
        [DbParameter("@PIV_NOTAS_PICKING", DbType = DbType.String)]
        public string PickingNote { get; set; }

        [DataMember]
        public IList<SubstituteGeneraRequest> Substitutes { get; set; }
    }
}
