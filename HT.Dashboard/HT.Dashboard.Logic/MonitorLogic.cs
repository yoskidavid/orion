﻿using HT.Dashboard.Model.MonitorATG;
using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace HT.Dashboard.Logic
{
    public class MonitorLogic : SDLogicBaseType
    {
        public MonitorLogic(IServiceProvider services, ILogger<MonitorLogic> logger, IDatabase<DbDispatch> dbDispatch)
           : base(services, logger, dbDispatch)
        {
        }

        public IEnumerable<DashboardATGSDResponseModel> GetDataMonitorATG_SD()
        {
            var request = new RequestPager<DashBoardATGSDRequestModel>();
            IEnumerable<DashboardATGSDResponseModel> response = null;
            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de Extracción de GetDataMonitorATG_SD ********");
                response = DbDispatch.ExecuteList<DashBoardATGSDRequestModel, DashboardATGSDResponseModel>(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "WS_API_SEL_MONITOR_ATG_SD", response);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de Extracción de GetDataMonitorATG_SD ********");
                DbDispatch.EnsureDispose();
            }
            return response;
        }

        public DataTable GetDataMonitorOC(DashboardATGDetailRequestModel request)
        {
            var dt = new DataTable();
            var requestSemaforo = new SemaforoRequestModel();

            ConfiguraDataTable(ref dt);
            IEnumerable<DashboardATGDetailResponseModel> lisDetalleMonitor = null;
            IEnumerable<SemaforoResponseModel> lisSemaforo = null;

            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de WS_API_SEL_MONITOR_ORDEN_ATG ********");
                Logger.Log(LogLevel.Information, "BranchID = {0}, Service = {1}, StartDate = {2}, EndDate = {3}, RangeDispacht = {4}, OrderType = {5}", request.BranchID, request.Service, request.StartDate, request.EndDate, request.RangeDispacht, request.OrderType);

                lisDetalleMonitor = DbDispatch.ExecuteList<DashboardATGDetailRequestModel, DashboardATGDetailResponseModel>(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "WS_API_SEL_MONITOR_ATG_SD", lisDetalleMonitor);
                lisDetalleMonitor = null;
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de WS_API_SEL_MONITOR_ORDEN_ATG ********");
                DbDispatch.EnsureDispose();
            }

            if (lisDetalleMonitor != null)
            {
                try
                {
                    DbDispatch.EnsureOpen();
                    Logger.Log(LogLevel.Information, "******** Inicio de SP_SEL_SEMAFORO_MONITOR ********");
                    lisSemaforo = DbDispatch.ExecuteList<SemaforoRequestModel, SemaforoResponseModel>(requestSemaforo);
                }
                catch (Exception ex)
                {
                    Logger.Log(LogLevel.Error, ex, "SP_SEL_SEMAFORO_MONITOR", lisSemaforo);
                    lisSemaforo = null;
                }
                finally
                {
                    Logger.Log(LogLevel.Information, "******** Fin de SP_SEL_SEMAFORO_MONITOR ********");
                    DbDispatch.EnsureDispose();
                }
            }

            if (lisSemaforo != null)
            {
                var estados = (from x in lisSemaforo.ToList()
                               orderby x.idEstado ascending
                               select new { IDEstado = x.idEstado, NombreEstado = x.nombreEstado.ToUpper() }
                           ).Distinct();
                foreach (var item in estados)
                {
                    var dataColumn = new DataColumn(item.NombreEstado, typeof(string));
                    dataColumn.DefaultValue = string.Empty;
                    dt.Columns.Add(dataColumn);
                }

                var ordenes = (from x in lisDetalleMonitor
                               orderby x.idEstado ascending
                               select new
                               {
                                   NroOC = x.nroOC
                               }
                               ).Distinct();

                DataRow dr = null;
                var EstadosOC = (from x in lisSemaforo
                                 select new { Estado = x.idEstado, Servicio = x.idServicio }).Distinct();

                foreach (var item in ordenes)
                {
                    dr = dt.NewRow();
                    dr["NRO_OC"] = "Nro OC";
                    dr["NOMBRES"] = "Cliente";
                    dr["DISTRITO"] = "Distrito";
                    dr["SERVICIO"] = "Servicio";
                    dr["HORARIO_DESPACHO"] = "Horario Despacho";

                    foreach (var estado in estados)
                    {
                        dr[estado.NombreEstado] = estado.NombreEstado;
                    }
                    dt.Rows.Add(dr);
                    break;
                }

                foreach (var item in ordenes)
                {
                    var lineDetalle = lisDetalleMonitor.ToList().Find(x => x.nroOC == item.NroOC);
                    dr = dt.NewRow();
                    dr["NRO_OC"] = lineDetalle.nroOC;
                    dr["NOMBRES"] = lineDetalle.nombres;
                    dr["DISTRITO"] = lineDetalle.distrito;
                    dr["SERVICIO"] = lineDetalle.Servicio;
                    dr["HORARIO_DESPACHO"] = lineDetalle.horarioDespacho;

                    var detalles = lisDetalleMonitor.ToList().FindAll(x => x.nroOC == item.NroOC);
                    DateTime fechaActual = detalles[0].fechaActual;
                    double diferenciaHoras = 0;
                    TimeSpan ts;
                    foreach (var estado in EstadosOC)
                    {
                        var detalle = detalles.Find(x => x.idEstado == estado.Estado && x.idServicio == estado.Servicio);
                        var semaforo = new SemaforoResponseModel();

                        if (detalle != null)
                        {
                            ts = detalle.fechaEstado - lineDetalle.fechaInicioDespacho;

                            diferenciaHoras = Math.Ceiling(ts.TotalHours);

                            semaforo = lisSemaforo.ToList().Find(x => x.idServicio == detalle.idServicio && x.idEstado == detalle.idEstado && x.limiteInferior < diferenciaHoras && diferenciaHoras <= x.limiteSuperior);
                        }
                        else
                        {
                            ts = fechaActual - lineDetalle.fechaInicioDespacho;

                            diferenciaHoras = Math.Ceiling(ts.TotalHours);

                            semaforo = lisSemaforo.ToList().Find(x => x.idServicio == estado.Servicio && x.idEstado == estado.Estado && x.limiteInferior < diferenciaHoras && diferenciaHoras <= x.limiteSuperior);
                        }

                        dr[semaforo.nombreEstado.ToUpper()] = string.Format("{0}|{1}", detalle != null ? detalle.fechaEstado.ToString("dd/MM/yyyy HH:mm:ss") : "", semaforo != null ? semaforo.semaforo : "");
                    }
                    dt.Rows.Add(dr);
                }
            }            

            return dt;
        }

        public IEnumerable<ExportToExcelDashBoardResponseModel> GetExcelReportFromData()
        {
            var request = new ExportToExcelDashBoardRequestModel();
            IEnumerable<ExportToExcelDashBoardResponseModel> response = null;

            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de WS_API_SEL_MONITOR_ATG_SD_DETAIL ********");
                response = DbDispatch.ExecuteList<ExportToExcelDashBoardRequestModel, ExportToExcelDashBoardResponseModel>(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "WS_API_SEL_MONITOR_ATG_SD_DETAIL", response);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de WS_API_SEL_MONITOR_ATG_SD_DETAIL ********");
                DbDispatch.EnsureDispose();
            }

            return response;
        }

        public DataTable GetOrdenATGPending(OrdenATGPendingRequestModel request)
        {
            DataTable dt = null;

            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de WS_API_SEL_MONITOR_PENDIENTES ********");
                var pendientes = DbDispatch.ExecuteList<OrdenATGPendingRequestModel, OrdenATGPendingResponseModel>(request);

                dt = ConvertToDataTable<OrdenATGPendingResponseModel>(pendientes);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "WS_API_SEL_MONITOR_ATG_SD_DETAIL");
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de WS_API_SEL_MONITOR_ATG_SD_DETAIL ********");
                DbDispatch.EnsureDispose();
            }

            return dt;
        }

        DataTable ConvertToDataTable<OrdenATGPendingResponseModel>(IEnumerable<OrdenATGPendingResponseModel> source)
        {
            var props = typeof(OrdenATGPendingResponseModel).GetProperties();

            var dt = new DataTable();
            dt.Columns.AddRange(
              props.Select(p => new DataColumn(p.Name, p.PropertyType)).ToArray()
            );

            source.ToList().ForEach(
              i => dt.Rows.Add(props.Select(p => p.GetValue(i, null)).ToArray())
            );

            return dt;
        }

        private void ConfiguraDataTable(ref DataTable dt)
        {
            dt.Columns.Add("NRO_OC", typeof(string));
            dt.Columns.Add("NOMBRES", typeof(string));
            dt.Columns.Add("DISTRITO", typeof(string));
            dt.Columns.Add("SERVICIO", typeof(string));
            dt.Columns.Add("HORARIO_DESPACHO", typeof(string));
        }
    }
}