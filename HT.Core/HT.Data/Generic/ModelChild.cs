﻿using System.Runtime.Serialization;

namespace HT.Data.Generic
{
    [DataContract]
    public class ModelChild<TParent> : IModelChild<TParent>
    {
        public ModelChild(TParent parent)
        {
            Parent = parent;
        }

        [IgnoreDataMember]
        public TParent Parent { get; set; }
    }
}
