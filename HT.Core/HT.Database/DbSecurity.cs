﻿using HT.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace HT.Database
{
    public class DbSecurity : Database, Generic.IDatabase<DbSecurity>, IDatabase
    {
        private const string SECURITY_DbSettingName = "Security";

        public DbSecurity(ILogger<DbSecurity> logger, IOptions<DbSettings> dbSettings) :
            base(logger, dbSettings)
        {

        }

        internal override string DbSettingName
        {
            get
            {
                return SECURITY_DbSettingName;
            }
        }
    }
}
