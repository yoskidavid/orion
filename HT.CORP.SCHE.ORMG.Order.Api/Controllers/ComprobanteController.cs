﻿using System;
using HT.Database;
using HT.Database.Generic;
using HT.Order.Logic;
using HT.Order.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace HT.Order.Api.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    [ProducesResponseType(500)]
    [EnableCors("AllowSpecificOrigin")]
    [Route("/rtl/tot/corp/SCHE/ORMG/[controller]")]
    [ApiController]
    public class ComprobanteController : ApiControllerBaseType
    {
        public IConfiguration _configuration;

        public ComprobanteController(IServiceProvider services, ILogger<ComprobanteController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        [HttpPost("InsertarComprobante")]
        public async Task<IActionResult> InsertarComprobante([FromBody] ComprobanteInsertarRequestModel request)
        {
            ComprobanteInsertarResponseModel objResponse = new ComprobanteInsertarResponseModel();
            try
            {
                
                objResponse = await Services.GetService<ComprobanteLogic>().InsertarComprobanteAsync(request);
                
            }
            catch (Exception ex)
            {
                this.Logger.Log(LogLevel.Debug, ex, "InsertarComprobante", request);
                objResponse.code = "99";
                objResponse.message = "Ocurrió un error interno";
            }
            
            return Ok(objResponse);

            //ComprobanteInsertarResponseModel objResponse = new ComprobanteInsertarResponseModel();
            //try
            //{
            //    DbDispatch.EnsureOpen(true);
            //    objResponse = Services.GetService<ComprobanteLogic>().InsertarComprobante(request);
            //    DbDispatch.Commit();
            //}
            //catch (Exception ex)
            //{
            //    this.Logger.Log(LogLevel.Debug, ex, "InsertarComprobante", request);
            //    DbDispatch.Rollback();
            //    objResponse.code = "99";
            //    objResponse.message = "Ocurrió un error interno";
            //}
            //finally
            //{
            //    DbDispatch.EnsureDispose();
            //}
            //return objResponse;
        }
    }
}