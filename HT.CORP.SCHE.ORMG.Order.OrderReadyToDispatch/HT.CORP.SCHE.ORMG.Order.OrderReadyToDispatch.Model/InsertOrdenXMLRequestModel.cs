﻿using HT.Data.Db;
using HT.Data.Generic;
using Newtonsoft.Json;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DataContract]
    [DbCommand("[DBO].[ATG_INSERTORDENXML_F]")]
    public class InsertOrdenXMLRequestModel : ModelChild<OrderRequestModel>
    {
        public InsertOrdenXMLRequestModel()
            : this(null)
        { }

        public InsertOrdenXMLRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@PIV_NUMEROORDEN", DbType = DbType.String, Size = 10)]
        public string OrderNumber => Parent.OrderNumber;

        [DbParameter("@PIXML_DOCUMENTO", DbType = DbType.Xml)]
        public string Content
        {
            get
            {
                string jsonString = JsonConvert.SerializeObject(Parent);

                return $"<order>{jsonString}</order>";
            }
        }
    }
}
