﻿using HT.Data.Db;
using System.Data;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DbCommand("ATG_GETEXISTEORDEN_F")]
    public class ExistsOrderRequestModel
    {
        [DbParameter("@PIV_NUMEROORDEN", DbType = DbType.String, Size = 25)]
        public string NumeroOrden { get; set; }
    }
}
