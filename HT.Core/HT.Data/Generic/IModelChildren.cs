﻿using System.Collections.Generic;

namespace HT.Data.Generic
{
    public interface IModelChildren<TParent, TChild> : ICollection<TChild>, IModelChild<TParent>
    {
    }
}
