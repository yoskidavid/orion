﻿using HT.Api;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;

namespace HT.Dashboard.Api.Controllers
{
    public abstract class ApiControllerBaseType<TLogic> : BaseController
    {
        protected ApiControllerBaseType(IServiceProvider services, ILogger logger, IDatabase<DbDispatch> dbDispatch)
            : base(services, logger)
        {
            DbDispatch = dbDispatch;
        }

        protected IDatabase DbDispatch { get; set; }

        protected TLogic GetLogic()
        {
            TLogic logic = (TLogic)this.Services.GetService(typeof(TLogic));

            return logic;
        }

        protected T GetLogic<T>()
        {
            return (T)this.Services.GetService(typeof(T));
        }
    }
}