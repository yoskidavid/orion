﻿using HT.Dashboard.Logic;
using HT.Dashboard.Model.Shared;
using HT.Database;
using HT.Database.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HT.Dashboard.Api.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [Route("shared/[controller]")]
    [ApiController]
    public class BranchController : ApiControllerBaseType<SucursalLogic>
    {
        public IConfiguration _configuration;

        public BranchController(IServiceProvider services, ILogger<BranchController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        [HttpGet("list")]
        public IActionResult GetSucursales()
        {
            IEnumerable<SucursalListResponseModel> response = null;
            try
            {
                response = GetLogic().GetSucursales().ToList();
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Debug, ex, "Obtener lista de sucursales", response);
            }

            return Ok(response);
        }
    }
}