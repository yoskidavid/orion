﻿using HT.Dashboard.Model.Shared;
using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace HT.Dashboard.Logic
{
    public class SucursalLogic : SDLogicBaseType
    {
        public SucursalLogic(IServiceProvider services, ILogger<SucursalLogic> logger, IDatabase<DbDispatch> dbDispatch)
           : base(services, logger, dbDispatch)
        {
        }

        public IEnumerable<SucursalListResponseModel> GetSucursales()
        {
            var request = new RequestPager<SucursalListRequestModel>();
            IEnumerable<SucursalListResponseModel> response = null;

            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de Extracción de Sucursales ********");                
                response = DbDispatch.ExecuteList<SucursalListRequestModel, SucursalListResponseModel>(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "ADM_getSucursales", response);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de Extracción de Sucursales ********");
                DbDispatch.EnsureDispose();
            }

            return response;
        }
    }
}