﻿namespace HT.Data.Generic
{
    public interface IModelNested : IModelChild<IModelNested>
    {
        IModelChildren<IModelNested, IModelNested> Children { get; set; }
    }
}
