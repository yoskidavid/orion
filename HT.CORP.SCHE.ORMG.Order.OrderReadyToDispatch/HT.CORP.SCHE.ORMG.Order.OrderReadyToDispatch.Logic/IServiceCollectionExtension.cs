﻿using Microsoft.Extensions.DependencyInjection;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Logic
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddOrderReadyToDispatchGenerarLogic(this IServiceCollection services)
        {
            services.AddTransient<OrderLogic>();
            return services;
        }
    }
}
