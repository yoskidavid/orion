﻿using HT.Configuration;
using HT.Order.Model;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace HT.Order.Logic.Data
{
    public class EmailQueueRepository : RepositoryBase, IEmailQueueRepository
    {
        public EmailQueueRepository(IOptions<DbSettings> options)
            : base(options)
        {
        }

        public async Task AddEmailQueueAsync(EmailQueue emailQueue)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                await conn.ExecuteAsync("WS_API_instEmailQueue", emailQueue, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
