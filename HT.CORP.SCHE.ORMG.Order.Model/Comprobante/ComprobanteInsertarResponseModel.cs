﻿using HT.Data.Db;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace HT.Order.Model
{
    [DataContract]
    public class ComprobanteInsertarResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "code")]
        public string code { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "message")]
        public string message { get; set; }
    }
}