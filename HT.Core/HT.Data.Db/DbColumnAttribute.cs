﻿using System;

namespace HT.Data.Db
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class DbColumnAttribute : Attribute
    {
        public DbColumnAttribute() :
            this(string.Empty, -1)
        {

        }

        public DbColumnAttribute(string name, int order)
        {
            Name = name;
            Order = order;
        }

        public string Name { get; set; }

        public int Order { get; set; }
    }
}
