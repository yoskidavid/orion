﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[PICK_REGISTROMANUAL_DETALLE_PICKING_IU]")]
    public class PickRecordManualDetailPickingIURequestModel : ModelChild<ProductRequestModel>
    {
        public PickRecordManualDetailPickingIURequestModel()
            : this(null)
        { }

        public PickRecordManualDetailPickingIURequestModel(ProductRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [DbParameter("@SKU", DbType = DbType.String, Size = 10)]
        public string Sku => Parent.Sku;

        [DbParameter("@EAN", DbType = DbType.String, Size = 20)]
        public string Ean => Parent.Ean;

        [DbParameter("@Cantidad", DbType = DbType.Decimal)]
        public decimal Quantity => Parent.Quantity;

        [DbParameter("@Unidad", DbType = DbType.String, Size = 5)]
        public string UnitMeasure => Parent.UnitMeasure;

        [DbParameter("@Precio", DbType = DbType.Decimal)]
        public decimal Price => Parent.UnitPrice;

        [DbParameter("@Sustituto", DbType = DbType.Boolean)]
        public bool Substitute => false;

        [DbParameter("@ID", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int? ID { get; set; }
    }
}
