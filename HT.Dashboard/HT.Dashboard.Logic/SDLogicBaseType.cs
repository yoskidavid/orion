﻿using System;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;

namespace HT.Dashboard.Logic
{
    public abstract class SDLogicBaseType : BaseType
    {
        protected SDLogicBaseType(ILogger logger, IDatabase<DbDispatch> dbDispatch) :
            this(null, logger, dbDispatch)
        { }

        protected SDLogicBaseType(IServiceProvider services, ILogger logger, IDatabase<DbDispatch> dbDispatch) :
            base(services, logger)
        {
            this.DbDispatch = dbDispatch;
        }

        public IDatabase DbDispatch
        {
            get;
            private set;
        }
    }
}