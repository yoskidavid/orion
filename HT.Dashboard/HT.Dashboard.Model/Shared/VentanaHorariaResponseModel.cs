﻿using System;
using System.Runtime.Serialization;
using HT.Data.Db;

namespace HT.Dashboard.Model.Shared
{
    [DataContract]
    public class VentanaHorariaResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "ID")]
        public Int32 Id { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "Ventana")]
        public String Ventana { get; set; }
    }
}