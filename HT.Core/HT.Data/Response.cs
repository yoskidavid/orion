﻿using System.Collections;
using System.Runtime.Serialization;

namespace HT.Data
{
    [DataContract]
    public class Response : IResponse
    {
        public Response(System.Exception exception)
        {
            Exception = exception;
        }

        [IgnoreDataMember]
        public IDictionary Values
        {
            get
            {
                EnsureInnerValues();

                return InnerValues;
            }
        }

        [IgnoreDataMember]
        public System.Exception Exception { get; private set; }

        [DataMember]
        public int Code => Exception != null ? Exception.HResult : 0;

        [DataMember]
        public string Message => Exception != null ? Exception.Message : "Éxito";

        private Hashtable InnerValues { get; set; }

        private void EnsureInnerValues()
        {
            if (InnerValues == null)
            {
                InnerValues = new Hashtable();
            }
        }
    }
}
