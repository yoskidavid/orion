﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace HT.Data.Db
{
    public class DataReaderList<T> : DataReaderBase<T>, IEnumerable<T>, IEnumerable
    {
        public DataReaderList(IDataReader reader) :
            base(reader)
        { }
        
        public IEnumerator<T> GetEnumerator()
        {
            var reader = this.Reader as DbDataReader;

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    yield return reader.GetEntity<T>();
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator() =>
            this.GetEnumerator();
    }
}
