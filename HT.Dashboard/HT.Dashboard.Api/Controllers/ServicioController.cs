﻿using HT.Dashboard.Logic;
using HT.Dashboard.Model.Shared;
using HT.Database;
using HT.Database.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HT.Dashboard.Api.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [Route("[controller]/{branchId}")]
    [ApiController]
    public class ServicioController : ApiControllerBaseType<ServicioLogic>
    {
        public IConfiguration _configuration;

        public ServicioController(IServiceProvider services, ILogger<ServicioController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        [HttpGet("GetList")]
        public IEnumerable<ServicioListResponseModel> GetTipoServicios()
        {
            IEnumerable<ServicioListResponseModel> response = null;
            try
            {
                response = GetLogic().GetTipoServicios().ToList();
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Debug, ex, "Obtener Tipos de Servicio", response);
            }

            return response;
        }
    }
}