﻿using HT.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace HT.Data.Db
{
    public static class IDataReaderIPagedEnumerable
    {
        public static IPagedEnumerable<T> ReadPager<T>(this IDataReader reader, IRequestPager settings) =>
            ReadPager<T>((DbDataReader)reader, settings);

        public static IPagedEnumerable<T> ReadPager<T>(this DbDataReader reader, IRequestPager settings) =>
            new DataReaderPager<T>(reader, settings);
    }
}
