﻿using HT.Dashboard.Logic;
using HT.Dashboard.Model.Shared;
using HT.Database;
using HT.Database.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace HT.Dashboard.Api.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [Route("[controller]")]
    [ApiController]
    public class TablaGeneralController : ApiControllerBaseType<TablaGeneralLogic>
    {
        public IConfiguration _configuration;

        public TablaGeneralController(IServiceProvider services, ILogger<TablaGeneralController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        [HttpGet("tablageneral-list/{CodigoTabla}")]
        public IEnumerable<TablaGeneralResponse> GetList(string CodigoTabla)
        {
            IEnumerable<TablaGeneralResponse> response = null;
            TablaGeneralRequest request = new TablaGeneralRequest();
            request.pFlag = 2;
            request.pCodTabla = CodigoTabla;

            try
            {
                response = GetLogic().GetList(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Debug, ex, "Obtener datos tabla general", response);
            }

            return response;
        }
    }
}