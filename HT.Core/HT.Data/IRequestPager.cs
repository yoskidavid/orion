﻿using HT.Collections;

namespace HT.Data
{
    public interface IRequestPager: IRequest
    {
        bool AllowPaging { get; set; }

        PagerSettings PagerSettings { get; }
    }
}
