﻿using HT.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace HT.Order.Logic.Data
{
    public class OrderRepository : RepositoryBase, IOrderRepository
    {
        public OrderRepository(IOptions<DbSettings> options)
            : base(options)
        {
        }

        public async Task<TipoOrdenDTO> GetOrderTypeAsync(long idOc)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var result =
                        await conn.QueryFirstOrDefaultAsync<TipoOrdenDTO>("GET_ORDEN_TIPOID", new { ID_OC = idOc }, commandType: CommandType.StoredProcedure);

                return result;
            }
        }
    }
}
