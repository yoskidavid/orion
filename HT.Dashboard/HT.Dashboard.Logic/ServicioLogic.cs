﻿using HT.Dashboard.Model.Shared;
using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace HT.Dashboard.Logic
{
    public class ServicioLogic : SDLogicBaseType
    {
        public ServicioLogic(IServiceProvider services, ILogger<ServicioLogic> logger, IDatabase<DbDispatch> dbDispatch)
           : base(services, logger, dbDispatch)
        {
        }

        public IEnumerable<ServicioListResponseModel> GetTipoServicios()
        {
            var request = new RequestPager<ServicioListRequestModel>();
            IEnumerable<ServicioListResponseModel> response = null;
            try
            {
                DbDispatch.EnsureOpen();
                Logger.Log(LogLevel.Information, "******** Inicio de Extracción de Tipos de Servicio ********");
                response = DbDispatch.ExecuteList<ServicioListRequestModel, ServicioListResponseModel>(request);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "ADM_GLTipoServicio", response);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de Extracción de Tipos de Servicio ********");
                DbDispatch.EnsureDispose();
            }
            return response;
        }
    }
}