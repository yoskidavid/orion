﻿namespace HT.Data
{
    public interface IResponseEntity : IResponse
    {
        object Body { get; }
    }
}
