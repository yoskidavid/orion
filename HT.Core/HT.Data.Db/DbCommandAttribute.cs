﻿using System;
using System.Data;

namespace HT.Data.Db
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class DbCommandAttribute : Attribute
    {
        public DbCommandAttribute(string commandText) :
            this(commandText, CommandType.StoredProcedure)
        {
            
        }

        public DbCommandAttribute(string commandText, CommandType commandType)
        {
            this.CommandText = commandText;
            this.CommandType = commandType;
        }

        public string CommandText { get; set; }

        public CommandType CommandType { get; set; }
    }
}
