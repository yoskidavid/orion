﻿using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    public class GetFlagPostVirtualResponseModel
    {
        [DbColumn(Order = 0, Name = "FLAG_POSVIRTUAL")]
        public int FLAG_POSVIRTUAL { get; set; }

    }
}
