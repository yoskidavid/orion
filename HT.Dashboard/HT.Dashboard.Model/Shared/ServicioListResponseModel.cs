﻿using HT.Data.Db;
using System.Runtime.Serialization;

namespace HT.Dashboard.Model.Shared
{
    [DataContract]
    public class ServicioListResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "Descripcion")]
        public string Descripcion { get; set; }
    }
}