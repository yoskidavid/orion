﻿using Microsoft.Extensions.DependencyInjection;

namespace HT.Dashboard.Logic
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddDashboardLogic(this IServiceCollection services)
        {
            services.AddTransient<TableLogic>();
            services.AddTransient<SucursalLogic>();
            services.AddTransient<VentanaHorariaLogic>();
            services.AddTransient<ServicioLogic>();
            services.AddTransient<TablaGeneralLogic>();
            services.AddTransient<MonitorLogic>();

            return services;
        }
    }
}
