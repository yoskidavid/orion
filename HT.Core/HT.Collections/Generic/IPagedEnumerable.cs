﻿using System.Collections.Generic;

namespace HT.Collections.Generic
{
    public interface IPagedEnumerable<T> : IEnumerable<T>, IPagedEnumerable
    {
        
    }
}
