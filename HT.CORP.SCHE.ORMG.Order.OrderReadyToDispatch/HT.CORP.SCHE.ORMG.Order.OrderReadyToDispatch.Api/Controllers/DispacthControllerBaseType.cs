﻿using HT.Api;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Api.Api.Controllers
{
    public abstract class DispacthControllerBaseType : BaseController
    {
        protected DispacthControllerBaseType(IServiceProvider services, ILogger logger, IDatabase<DbDispatch> dbDispatch)
            : base(services, logger)
        {
            DbDispatch = dbDispatch;
        }

        protected IDatabase DbDispatch { get; set; }
    }
}
