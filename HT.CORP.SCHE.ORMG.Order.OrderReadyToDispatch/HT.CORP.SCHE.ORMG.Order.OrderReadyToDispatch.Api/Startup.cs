﻿using HT.Configuration;
using HT.Shared.Logic;
using HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Logic;
using HT.Database;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Api.Api
{
    public class ApiHeaderJsonNamingStrategyOptions
    {
        public string HeaderName { get; set; }
        public Dictionary<string, NamingStrategy> NamingStrategies { get; set; }
        public NamingStrategy DefaultStrategy { get; set; }
        public Func<IHttpContextAccessor> HttpContextAccessorProvider { get; set; }
    }

    public class ApiHeaderJsonNamingStrategy : NamingStrategy
    {
        private readonly SnakeCaseNamingStrategy _snakeCaseNamingStrategy = new SnakeCaseNamingStrategy();
        private readonly CamelCaseNamingStrategy _camelCaseNamingStrategy = new CamelCaseNamingStrategy();
        private readonly ApiHeaderJsonNamingStrategyOptions _options;

        public ApiHeaderJsonNamingStrategy(ApiHeaderJsonNamingStrategyOptions options)
        {
            _options = options;
        }

        public string GetValidNamingStrategyHeader()
        {
            if (_options.HttpContextAccessorProvider() == null)
            {
                return string.Empty;
            }

            var httpContext = _options.HttpContextAccessorProvider().HttpContext;
            var namingStrategyHeader = httpContext.Request.Headers[_options.HeaderName].FirstOrDefault()?.ToLower();

            if (string.IsNullOrEmpty(namingStrategyHeader) || !_options.NamingStrategies.ContainsKey(namingStrategyHeader))
            {
                return string.Empty;
            }

            return namingStrategyHeader;
        }

        protected override string ResolvePropertyName(string name)
        {
            NamingStrategy strategy;

            var namingStrategyHeader = GetValidNamingStrategyHeader();

            //If the header is empty or it's a value we don't really know about. Use our default strategy. 
            if (string.IsNullOrEmpty(namingStrategyHeader))
            {
                strategy = _options.DefaultStrategy;
            }
            else
            {
                strategy = _options.NamingStrategies[namingStrategyHeader];
            }

            //This is actually bit of a bastardization because we shouldn't really be calling this method here. 
            //We want to actually just call the "ResolvePropertyName" method, but it's protected. This essentially ends up doing the same thing. 
            return strategy.GetPropertyName(name, false);
        }
    }
    
    public class ApiHeaderJsonContractResolver : DefaultContractResolver
    {
        private readonly ApiHeaderJsonNamingStrategy _apiHeaderJsonNamingStrategy;
        private readonly Func<IMemoryCache> _memoryCacheProvider;

        public ApiHeaderJsonContractResolver(ApiHeaderJsonNamingStrategyOptions namingStrategyOptions, Func<IMemoryCache> memoryCacheProvider)
        {
            _apiHeaderJsonNamingStrategy = new ApiHeaderJsonNamingStrategy(namingStrategyOptions);
            NamingStrategy = _apiHeaderJsonNamingStrategy;
            _memoryCacheProvider = memoryCacheProvider;
        }

        protected override JsonConverter ResolveContractConverter(Type objectType)
        {
            return base.ResolveContractConverter(objectType);
        }

        public override JsonContract ResolveContract(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            string cacheKey = _apiHeaderJsonNamingStrategy.GetValidNamingStrategyHeader() + type.ToString();
            JsonContract contract = _memoryCacheProvider().GetOrCreate(cacheKey, (entry) =>
            {
                entry.AbsoluteExpiration = DateTimeOffset.MaxValue;
                return CreateContract(type);
            });

            return contract;
        }
    }

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddOptions();

            services
                .AddCors(options =>
                {
                    options.AddPolicy("AllowSpecificOrigin",
                        builder =>
                            builder.WithOrigins("*")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowAnyOrigin()
                            .AllowCredentials());
                })
                .AddSingleton<ILoggerFactory, LoggerFactory>()
                .AddSingleton(typeof(ILogger<>), typeof(Logger<>))
                .AddTransient<IOrderReadyToDispatchRepository, OrderReadyToDispatchRepository>()
                .AddDatabases()
                .AddSharedLogic()
                .AddOrderReadyToDispatchGenerarLogic();

            services.Configure<DbSettings>(Configuration);

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "API Purcharse Order",
                    Description = "Business object that represents the Purchase Order",
                    Contact = new Contact
                    {
                        Name = "NEKSYS",
                        Email = "mreyna@neksysse.com",
                        Url = "http://www.neksysse.com"
                    },
                    License = new License
                    {
                        Name = "Tottus",
                        //Url = "https://--/license"
                    }
                });

                // Configure Swagger to use the xml documentation file
                var xmlFile = Path.ChangeExtension(typeof(Startup).Assembly.Location, ".xml");
                c.IncludeXmlComments(xmlFile);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowSpecificOrigin");

            app.UseStaticFiles();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Orden de Compra");
                c.RoutePrefix = string.Empty;
            });

            app.UseMvc();
        }
    }
}
