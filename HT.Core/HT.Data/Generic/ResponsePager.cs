﻿using HT.Collections;
using HT.Collections.Generic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace HT.Data.Generic
{
    [DataContract]
    public class ResponsePager<T> : Response, IResponsePager<T>
    {
        public ResponsePager(IPagedEnumerable<T> paged) :
            base(null)
        {
            Paged = paged;
            IsPaged = false;
        }

        public ResponsePager(Exception exception) :
            base(exception)
        { }

        [DataMember]
        public IEnumerable<T> Body
        {
            get
            {
                if (IsPaged.Equals(true))
                {
                    return InnerPaged;
                }
                else if (Paged == null)
                {
                    return null;
                }
                else
                {
                    return Paged;
                }
            }
        }

        [DataMember]
        public Pager Pager
        {
            get
            {
                if (Paged == null)
                {
                    return null;
                }
                else
                {
                    return Paged.Pager;
                }
            }
        }

        public IResponsePager<T> ToPager()
        {
            InnerPaged = (Paged == null ? null : Paged.ToList());
            IsPaged = true;

            return this;
        }

        IResponsePager IResponsePager.ToPager() => ToPager();

        private bool IsPaged { get; set; }

        private IList<T> InnerPaged { get; set; }

        private IPagedEnumerable<T> Paged { get; set; }

        IEnumerable IResponsePager.Body => 
            Body;
    }
}
