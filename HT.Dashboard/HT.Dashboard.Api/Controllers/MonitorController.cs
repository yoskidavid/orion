﻿using HT.Dashboard.Logic;
using HT.Dashboard.Model.MonitorATG;
using HT.Database;
using HT.Database.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HT.Dashboard.Api.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [Route("[controller]")]
    [ApiController]
    public class MonitorController : ApiControllerBaseType<MonitorLogic>
    {
        public IConfiguration _configuration;

        public MonitorController(IServiceProvider services, ILogger<MonitorController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        [HttpGet("order-resume")]
        public IEnumerable<DashboardATGSDResponseModel> GetResume()
        {
            IEnumerable<DashboardATGSDResponseModel> response = null;
            try
            {
                response = GetLogic().GetDataMonitorATG_SD().ToList();
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Debug, ex, "Obtener Ordenes Resumen", response);
            }

            return response;
        }

        [HttpGet("order-details/{request}")]
        public System.Data.DataTable GetDataDetailsFilter(string request)
        {
            System.Data.DataTable response = null;
            try
            {
                var data = request.Split('|');

                DashboardATGDetailRequestModel req = new DashboardATGDetailRequestModel();
                req.BranchID = Convert.ToInt32(data[0]);
                req.Service = data[1];
                req.StartDate = data[2];
                req.EndDate = data[3];
                req.RangeDispacht = data[4];
                req.OrderType = data[5];

                response = GetLogic().GetDataMonitorOC(req);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Debug, ex, "Obtener Ordenes Resumen", response);
            }

            return response;
        }

        [HttpGet("order-excel-report")]
        public IActionResult GetExcelReportFromData()
        {
            try
            {
                var request = new OrdenATGPendingRequestModel();
                var data = GetLogic().GetExcelReportFromData();
                var ocPendiente = GetLogic().GetOrdenATGPending(request);
                byte[] fileContents;

                using (var package = new ExcelPackage())
                {
                    var worksheetATG = package.Workbook.Worksheets.Add("ATG");
                    var worksheetSD = package.Workbook.Worksheets.Add("SD");
                    var worksheetOF = package.Workbook.Worksheets.Add("ORDER FOLIO");
                    var worksheetFE = package.Workbook.Worksheets.Add("FOLIO ERROR");
                    var worksheetOP = package.Workbook.Worksheets.Add("ORDENES PENDIENTES");
                    var atgData = (from x in data
                                   where x.tipo == "ATG"
                                   select new { NroOC = x.nroOC, Estado = x.estado, FechaProceso = x.fechaProceso.ToString("dd/MM/yyyy HH:mm:ss") });

                    worksheetATG.Cells["A1"].LoadFromCollection(atgData, true);

                    var sdData = (from x in data
                                  where x.tipo == "SD"
                                  select new { NroOC = x.nroOC, Estado = x.estado, FechaProceso = x.fechaProceso.ToString("dd/MM/yyyy HH:mm:ss") });

                    worksheetSD.Cells["A1"].LoadFromCollection(sdData, true);

                    var orderFolioData = (from x in data
                                          where x.tipo == "ORDER_FOLIO"
                                          select new { NroOC = x.nroOC, Folio = x.folio, Estado = x.estado, FechaProceso = x.fechaProceso.ToString("dd/MM/yyyy HH:mm:ss") });

                    worksheetOF.Cells["A1"].LoadFromCollection(orderFolioData, true);

                    var orderFolioErrorData = (from x in data
                                               where x.tipo == "FOLIO_ERROR"
                                               select new { NroOC = x.nroOC, Error = x.folio, Estado = x.estado, FechaProceso = x.fechaProceso.ToString("dd/MM/yyyy HH:mm:ss") });

                    worksheetFE.Cells["A1"].LoadFromCollection(orderFolioErrorData, true);

                    worksheetOP.Cells["A1"].LoadFromDataTable(ocPendiente, true);

                    fileContents = package.GetAsByteArray();
                }
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: string.Format("ReporteDetalleOC_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy_HHmmss"))
                );
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
    }
}