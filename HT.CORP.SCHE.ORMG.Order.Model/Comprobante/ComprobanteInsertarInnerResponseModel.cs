﻿using HT.Data.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace HT.Order.Model
{
    public class ComprobanteInsertarInnerResponseModel
    {
        [DbColumn(Order = 0, Name = "code")]
        public int ID_COMPROBANTE { get; set; }
    }
}