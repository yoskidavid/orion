﻿using System.Runtime.Serialization;

namespace HT.Collections
{
    [DataContract]
    public class Pager
    {
        public Pager(int pageIndex, int pageCount, int pageSize, int recordsCount, int recordsTotal)
        {
            PageIndex = pageIndex;
            PageCount = pageCount;
            PageSize = pageSize;
            RecordsCount = recordsCount;
            RecordsTotal = recordsTotal;
        }

        [DataMember]
        public int PageIndex { get; private set; }

        [DataMember]
        public int PageCount { get; private set; }

        [DataMember]
        public int PageSize { get; private set; }

        [DataMember]
        public int RecordsCount { get; private set; }

        [DataMember]
        public int RecordsTotal { get; private set; }
    }
}
