﻿using HT.Data.Db;
using System.Data;

namespace HT.Shared.Model
{
    [DbCommand("ADM_GETPARAMETRO")]
    public class ParameterRequestModel
    {
        [DbParameter("@PIV_CODIGO", DbType = DbType.String, Size = 10)]
        public string Code { get; set; }
    }
}
