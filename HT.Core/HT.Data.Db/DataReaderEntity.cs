﻿using System.Data;

namespace HT.Data.Db
{
    public class DataReaderEntity<T> : DataReaderBase<T>
    {

        public DataReaderEntity(IDataReader reader) :
            base(reader)
        { }
    }
}
