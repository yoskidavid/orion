﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Linq;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DataContract]
    [DbCommand("FOL_UPDATEFOLIODETALLEOC")]
    public class FolioInjectToProductRequestModel : ModelChild<FolioSkuGenerateRequestModel>
    {
        public FolioInjectToProductRequestModel()
            : this(null)
        { }

        public FolioInjectToProductRequestModel(FolioSkuGenerateRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@PIV_NUMEROORDEN", DbType = DbType.String)]
        public string OrderNumber { get => Parent.Parent.OrderNumber; }

        [IgnoreDataMember]
        [DbParameter("@PIV_SKU", DbType = DbType.String)]
        public string Sku
        {
            get
            {
                string sku;

                ProductRequestModel product = Parent.Parent.Parent.Products.FirstOrDefault(x => x.Sku.Equals(Parent.Sku));

                if (product is null)
                {
                    sku = string.Empty;
                }
                else
                {
                    sku = product.Sku;
                }

                return sku;
            }
        }

        [DbParameter("@PIV_NUMEROFOLIO", DbType = DbType.String)]
        public string FolioNumber { get => Parent.Parent.FolioNumber; }

    }
}
