﻿namespace HT.Data.Generic
{
    public interface IResponseEntity<T> : IResponseEntity, IResponse
    {
        new T Body { get; }
    }
}
