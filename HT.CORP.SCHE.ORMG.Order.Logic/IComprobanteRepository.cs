﻿using HT.Order.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HT.Order.Logic
{
    public interface IComprobanteRepository
    {
        Task<int> InsertarComprobanteAsync(ComprobanteInsertarRequestModel comprobanteInsertarRequestModel);
    }
}
