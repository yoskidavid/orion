﻿namespace HT.Data
{
    public interface IRequest
    {
        int UserID { get; set; }

        object Body { get; }
    }
}
