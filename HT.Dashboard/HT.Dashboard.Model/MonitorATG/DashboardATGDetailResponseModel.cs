﻿using System;
using System.Runtime.Serialization;
using HT.Data.Db;

namespace HT.Dashboard.Model.MonitorATG
{
    [DataContract]
    public class DashboardATGDetailResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "NRO_OC")]
        public string nroOC { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "NOMBRES")]
        public string nombres { get; set; }

        [DataMember]
        [DbColumn(Order = 2, Name = "DISTRITO")]
        public string distrito { get; set; }

        [DataMember]
        [DbColumn(Order = 3, Name = "HORARIO_DESPACHO")]
        public string horarioDespacho { get; set; }

        [DataMember]
        [DbColumn(Order = 4, Name = "NOMBRE_ESTADO")]
        public string nombreEstado { get; set; }

        [DataMember]
        [DbColumn(Order = 5, Name = "FECHA_ESTADO")]
        public DateTime fechaEstado { get; set; }

        [DataMember]
        [DbColumn(Order = 6, Name = "ID_ESTADO")]
        public int idEstado { get; set; }

        [DataMember]
        [DbColumn(Order = 7, Name = "ID_SERVICIO")]
        public int idServicio { get; set; }

        [DataMember]
        [DbColumn(Order = 8, Name = "SERVICIO")]
        public string Servicio { get; set; }

        [DataMember]
        [DbColumn(Order = 9, Name = "FECHA_INICIO_DESPACHO")]
        public DateTime fechaInicioDespacho { get; set; }

        [DataMember]
        [DbColumn(Order = 10, Name = "FECHA_FIN_DESPACHO")]
        public DateTime fechaFinDespacho { get; set; }


        [DataMember]
        [DbColumn(Order = 11, Name = "FECHA_ACTUAL")]
        public DateTime fechaActual { get; set; }

    }
}
