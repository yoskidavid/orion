﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//     //
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace ServiceReference1
{
    using System.Runtime.Serialization;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eUsuario", Namespace="http://tempuri.org/")]
    public partial class eUsuario : object
    {
        
        private int EstadoValidacionPropertyField;
        
        private int IdPropertyField;
        
        private string RutPropertyField;
        
        private int Id_sucursalPropertyField;
        
        private string NombrePropertyField;
        
        private string ContraseniaPropertyField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int EstadoValidacionProperty
        {
            get
            {
                return this.EstadoValidacionPropertyField;
            }
            set
            {
                this.EstadoValidacionPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IdProperty
        {
            get
            {
                return this.IdPropertyField;
            }
            set
            {
                this.IdPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string RutProperty
        {
            get
            {
                return this.RutPropertyField;
            }
            set
            {
                this.RutPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public int Id_sucursalProperty
        {
            get
            {
                return this.Id_sucursalPropertyField;
            }
            set
            {
                this.Id_sucursalPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string NombreProperty
        {
            get
            {
                return this.NombrePropertyField;
            }
            set
            {
                this.NombrePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string ContraseniaProperty
        {
            get
            {
                return this.ContraseniaPropertyField;
            }
            set
            {
                this.ContraseniaPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEOrdenCompra", Namespace="http://tempuri.org/", ItemName="eOrdenCompra")]
    public class ArrayOfEOrdenCompra : System.Collections.Generic.List<ServiceReference1.eOrdenCompra>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eOrdenCompra", Namespace="http://tempuri.org/")]
    public partial class eOrdenCompra : object
    {
        
        private int IdPropertyField;
        
        private string NumeroOrdenPropertyField;
        
        private int CantidadArticulosPropertyField;
        
        private int EstadoPropertyField;
        
        private int TiendaPropertyField;
        
        private string NomTipoClientePropertyField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IdProperty
        {
            get
            {
                return this.IdPropertyField;
            }
            set
            {
                this.IdPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string NumeroOrdenProperty
        {
            get
            {
                return this.NumeroOrdenPropertyField;
            }
            set
            {
                this.NumeroOrdenPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int CantidadArticulosProperty
        {
            get
            {
                return this.CantidadArticulosPropertyField;
            }
            set
            {
                this.CantidadArticulosPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public int EstadoProperty
        {
            get
            {
                return this.EstadoPropertyField;
            }
            set
            {
                this.EstadoPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public int TiendaProperty
        {
            get
            {
                return this.TiendaPropertyField;
            }
            set
            {
                this.TiendaPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string NomTipoClienteProperty
        {
            get
            {
                return this.NomTipoClientePropertyField;
            }
            set
            {
                this.NomTipoClientePropertyField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEArticulo", Namespace="http://tempuri.org/", ItemName="eArticulo")]
    public class ArrayOfEArticulo : System.Collections.Generic.List<ServiceReference1.eArticulo>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eArticulo", Namespace="http://tempuri.org/")]
    public partial class eArticulo : object
    {
        
        private int IdPropertyField;
        
        private string ProductoPropertyField;
        
        private string EtiquetaPropertyField;
        
        private decimal Cantidad_detallePropertyField;
        
        private decimal Cantidad_faltantePropertyField;
        
        private string UnidadPropertyField;
        
        private int PesablePropertyField;
        
        private string SkuPropertyField;
        
        private string EanPropertyField;
        
        private string SkuSustitutoPropertyField;
        
        private decimal Cantidad_sustituidaPropertyField;
        
        private int EstadoPropertyField;
        
        private decimal CantidadPickingPropertyField;
        
        private decimal cantidad_dec_FaltantePropertyField;
        
        private string notas_pickingPropertyField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IdProperty
        {
            get
            {
                return this.IdPropertyField;
            }
            set
            {
                this.IdPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string ProductoProperty
        {
            get
            {
                return this.ProductoPropertyField;
            }
            set
            {
                this.ProductoPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string EtiquetaProperty
        {
            get
            {
                return this.EtiquetaPropertyField;
            }
            set
            {
                this.EtiquetaPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public decimal Cantidad_detalleProperty
        {
            get
            {
                return this.Cantidad_detallePropertyField;
            }
            set
            {
                this.Cantidad_detallePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public decimal Cantidad_faltanteProperty
        {
            get
            {
                return this.Cantidad_faltantePropertyField;
            }
            set
            {
                this.Cantidad_faltantePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string UnidadProperty
        {
            get
            {
                return this.UnidadPropertyField;
            }
            set
            {
                this.UnidadPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public int PesableProperty
        {
            get
            {
                return this.PesablePropertyField;
            }
            set
            {
                this.PesablePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string SkuProperty
        {
            get
            {
                return this.SkuPropertyField;
            }
            set
            {
                this.SkuPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=8)]
        public string EanProperty
        {
            get
            {
                return this.EanPropertyField;
            }
            set
            {
                this.EanPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=9)]
        public string SkuSustitutoProperty
        {
            get
            {
                return this.SkuSustitutoPropertyField;
            }
            set
            {
                this.SkuSustitutoPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=10)]
        public decimal Cantidad_sustituidaProperty
        {
            get
            {
                return this.Cantidad_sustituidaPropertyField;
            }
            set
            {
                this.Cantidad_sustituidaPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=11)]
        public int EstadoProperty
        {
            get
            {
                return this.EstadoPropertyField;
            }
            set
            {
                this.EstadoPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=12)]
        public decimal CantidadPickingProperty
        {
            get
            {
                return this.CantidadPickingPropertyField;
            }
            set
            {
                this.CantidadPickingPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=13)]
        public decimal cantidad_dec_FaltanteProperty
        {
            get
            {
                return this.cantidad_dec_FaltantePropertyField;
            }
            set
            {
                this.cantidad_dec_FaltantePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=14)]
        public string notas_pickingProperty
        {
            get
            {
                return this.notas_pickingPropertyField;
            }
            set
            {
                this.notas_pickingPropertyField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eDetalleArticulo", Namespace="http://tempuri.org/")]
    public partial class eDetalleArticulo : object
    {
        
        private int Id_docPropertyField;
        
        private string ProductoPropertyField;
        
        private string MarcaPropertyField;
        
        private double PrecioReferencialPropertyField;
        
        private string CodigoClasePropertyField;
        
        private string NombreClasePropertyField;
        
        private string ModeloPropertyField;
        
        private string EtiquetaPropertyField;
        
        private decimal CantidadFaltantePropertyField;
        
        private string UnidadPropertyField;
        
        private string NotasPickingPropertyField;
        
        private int PesablePropertyField;
        
        private string ComentarioPropertyField;
        
        private string SkuSustitutoPropertyField;
        
        private decimal Cantidad_sustituidaPropertyField;
        
        private decimal CantidadPickingPropertyField;
        
        private decimal cantidad_dec_FaltantePropertyField;
        
        private int StockPropertyField;
        
        private int CriterioSustitucionPropertyField;
        
        private int EstadoPropertyField;
        
        private string EANPropertyField;
        
        private int tiendaPropertyField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int Id_docProperty
        {
            get
            {
                return this.Id_docPropertyField;
            }
            set
            {
                this.Id_docPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string ProductoProperty
        {
            get
            {
                return this.ProductoPropertyField;
            }
            set
            {
                this.ProductoPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string MarcaProperty
        {
            get
            {
                return this.MarcaPropertyField;
            }
            set
            {
                this.MarcaPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public double PrecioReferencialProperty
        {
            get
            {
                return this.PrecioReferencialPropertyField;
            }
            set
            {
                this.PrecioReferencialPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string CodigoClaseProperty
        {
            get
            {
                return this.CodigoClasePropertyField;
            }
            set
            {
                this.CodigoClasePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string NombreClaseProperty
        {
            get
            {
                return this.NombreClasePropertyField;
            }
            set
            {
                this.NombreClasePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string ModeloProperty
        {
            get
            {
                return this.ModeloPropertyField;
            }
            set
            {
                this.ModeloPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string EtiquetaProperty
        {
            get
            {
                return this.EtiquetaPropertyField;
            }
            set
            {
                this.EtiquetaPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=8)]
        public decimal CantidadFaltanteProperty
        {
            get
            {
                return this.CantidadFaltantePropertyField;
            }
            set
            {
                this.CantidadFaltantePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=9)]
        public string UnidadProperty
        {
            get
            {
                return this.UnidadPropertyField;
            }
            set
            {
                this.UnidadPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=10)]
        public string NotasPickingProperty
        {
            get
            {
                return this.NotasPickingPropertyField;
            }
            set
            {
                this.NotasPickingPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=11)]
        public int PesableProperty
        {
            get
            {
                return this.PesablePropertyField;
            }
            set
            {
                this.PesablePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=12)]
        public string ComentarioProperty
        {
            get
            {
                return this.ComentarioPropertyField;
            }
            set
            {
                this.ComentarioPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=13)]
        public string SkuSustitutoProperty
        {
            get
            {
                return this.SkuSustitutoPropertyField;
            }
            set
            {
                this.SkuSustitutoPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=14)]
        public decimal Cantidad_sustituidaProperty
        {
            get
            {
                return this.Cantidad_sustituidaPropertyField;
            }
            set
            {
                this.Cantidad_sustituidaPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=15)]
        public decimal CantidadPickingProperty
        {
            get
            {
                return this.CantidadPickingPropertyField;
            }
            set
            {
                this.CantidadPickingPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=16)]
        public decimal cantidad_dec_FaltanteProperty
        {
            get
            {
                return this.cantidad_dec_FaltantePropertyField;
            }
            set
            {
                this.cantidad_dec_FaltantePropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=17)]
        public int StockProperty
        {
            get
            {
                return this.StockPropertyField;
            }
            set
            {
                this.StockPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=18)]
        public int CriterioSustitucionProperty
        {
            get
            {
                return this.CriterioSustitucionPropertyField;
            }
            set
            {
                this.CriterioSustitucionPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=19)]
        public int EstadoProperty
        {
            get
            {
                return this.EstadoPropertyField;
            }
            set
            {
                this.EstadoPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=20)]
        public string EANProperty
        {
            get
            {
                return this.EANPropertyField;
            }
            set
            {
                this.EANPropertyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=21)]
        public int tiendaProperty
        {
            get
            {
                return this.tiendaPropertyField;
            }
            set
            {
                this.tiendaPropertyField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfString", Namespace="http://tempuri.org/", ItemName="string")]
    public class ArrayOfString : System.Collections.Generic.List<string>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEColaImpresion", Namespace="http://tempuri.org/", ItemName="eColaImpresion")]
    public class ArrayOfEColaImpresion : System.Collections.Generic.List<ServiceReference1.eColaImpresion>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eColaImpresion", Namespace="http://tempuri.org/")]
    public partial class eColaImpresion : object
    {
        
        private int IDCOLAIMPRESIONField;
        
        private int ID_SUCURSALField;
        
        private string COLAField;
        
        private string IPField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IDCOLAIMPRESION
        {
            get
            {
                return this.IDCOLAIMPRESIONField;
            }
            set
            {
                this.IDCOLAIMPRESIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ID_SUCURSAL
        {
            get
            {
                return this.ID_SUCURSALField;
            }
            set
            {
                this.ID_SUCURSALField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string COLA
        {
            get
            {
                return this.COLAField;
            }
            set
            {
                this.COLAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string IP
        {
            get
            {
                return this.IPField;
            }
            set
            {
                this.IPField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eEtiquetaOC", Namespace="http://tempuri.org/")]
    public partial class eEtiquetaOC : object
    {
        
        private int ID_OCField;
        
        private string NUMERO_ORDENField;
        
        private string CLIENTEField;
        
        private string VENTANAField;
        
        private string TIPOENTREGAField;
        
        private string LUGARENTREGAField;
        
        private int NUMERO_BINSField;
        
        private int NRO_SUELTOSField;
        
        private string OPERARIOField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ID_OC
        {
            get
            {
                return this.ID_OCField;
            }
            set
            {
                this.ID_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string NUMERO_ORDEN
        {
            get
            {
                return this.NUMERO_ORDENField;
            }
            set
            {
                this.NUMERO_ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string CLIENTE
        {
            get
            {
                return this.CLIENTEField;
            }
            set
            {
                this.CLIENTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string VENTANA
        {
            get
            {
                return this.VENTANAField;
            }
            set
            {
                this.VENTANAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string TIPOENTREGA
        {
            get
            {
                return this.TIPOENTREGAField;
            }
            set
            {
                this.TIPOENTREGAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string LUGARENTREGA
        {
            get
            {
                return this.LUGARENTREGAField;
            }
            set
            {
                this.LUGARENTREGAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public int NUMERO_BINS
        {
            get
            {
                return this.NUMERO_BINSField;
            }
            set
            {
                this.NUMERO_BINSField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=7)]
        public int NRO_SUELTOS
        {
            get
            {
                return this.NRO_SUELTOSField;
            }
            set
            {
                this.NRO_SUELTOSField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=8)]
        public string OPERARIO
        {
            get
            {
                return this.OPERARIOField;
            }
            set
            {
                this.OPERARIOField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eErrorMP", Namespace="http://tempuri.org/")]
    public partial class eErrorMP : object
    {
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eLote", Namespace="http://tempuri.org/")]
    public partial class eLote : object
    {
        
        private string OPERARIOField;
        
        private int IDLOTEField;
        
        private string CODIGO_LOTEField;
        
        private string MUNDOField;
        
        private int CANT_OCField;
        
        private int CANT_SKUField;
        
        private int CANT_PENDIENTEField;
        
        private int FLG_IMPRESIONField;
        
        private int TIENDAField;
        
        private int CANT_JABAField;
        
        private int CANT_BULTOField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string OPERARIO
        {
            get
            {
                return this.OPERARIOField;
            }
            set
            {
                this.OPERARIOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public int IDLOTE
        {
            get
            {
                return this.IDLOTEField;
            }
            set
            {
                this.IDLOTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string CODIGO_LOTE
        {
            get
            {
                return this.CODIGO_LOTEField;
            }
            set
            {
                this.CODIGO_LOTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string MUNDO
        {
            get
            {
                return this.MUNDOField;
            }
            set
            {
                this.MUNDOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public int CANT_OC
        {
            get
            {
                return this.CANT_OCField;
            }
            set
            {
                this.CANT_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public int CANT_SKU
        {
            get
            {
                return this.CANT_SKUField;
            }
            set
            {
                this.CANT_SKUField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public int CANT_PENDIENTE
        {
            get
            {
                return this.CANT_PENDIENTEField;
            }
            set
            {
                this.CANT_PENDIENTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=7)]
        public int FLG_IMPRESION
        {
            get
            {
                return this.FLG_IMPRESIONField;
            }
            set
            {
                this.FLG_IMPRESIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=8)]
        public int TIENDA
        {
            get
            {
                return this.TIENDAField;
            }
            set
            {
                this.TIENDAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=9)]
        public int CANT_JABA
        {
            get
            {
                return this.CANT_JABAField;
            }
            set
            {
                this.CANT_JABAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=10)]
        public int CANT_BULTO
        {
            get
            {
                return this.CANT_BULTOField;
            }
            set
            {
                this.CANT_BULTOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=11)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=12)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eCantPedido", Namespace="http://tempuri.org/")]
    public partial class eCantPedido : object
    {
        
        private int CANT_MONOPICKINGField;
        
        private int CANT_MULTIPICKINGField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int CANT_MONOPICKING
        {
            get
            {
                return this.CANT_MONOPICKINGField;
            }
            set
            {
                this.CANT_MONOPICKINGField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int CANT_MULTIPICKING
        {
            get
            {
                return this.CANT_MULTIPICKINGField;
            }
            set
            {
                this.CANT_MULTIPICKINGField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eOTContenedores", Namespace="http://tempuri.org/")]
    public partial class eOTContenedores : object
    {
        
        private int ID_OTField;
        
        private string OT_CLIENTEField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private ServiceReference1.ArrayOfEContenedorCargaItem CONTENEDOR_CARGAField;
        
        private ServiceReference1.ArrayOfEContenedorCargadoItem CONTENEDOR_CARGADOField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ID_OT
        {
            get
            {
                return this.ID_OTField;
            }
            set
            {
                this.ID_OTField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string OT_CLIENTE
        {
            get
            {
                return this.OT_CLIENTEField;
            }
            set
            {
                this.OT_CLIENTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public ServiceReference1.ArrayOfEContenedorCargaItem CONTENEDOR_CARGA
        {
            get
            {
                return this.CONTENEDOR_CARGAField;
            }
            set
            {
                this.CONTENEDOR_CARGAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public ServiceReference1.ArrayOfEContenedorCargadoItem CONTENEDOR_CARGADO
        {
            get
            {
                return this.CONTENEDOR_CARGADOField;
            }
            set
            {
                this.CONTENEDOR_CARGADOField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEContenedorCargaItem", Namespace="http://tempuri.org/", ItemName="eContenedorCargaItem")]
    public class ArrayOfEContenedorCargaItem : System.Collections.Generic.List<ServiceReference1.eContenedorCargaItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEContenedorCargadoItem", Namespace="http://tempuri.org/", ItemName="eContenedorCargadoItem")]
    public class ArrayOfEContenedorCargadoItem : System.Collections.Generic.List<ServiceReference1.eContenedorCargadoItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eContenedorCargaItem", Namespace="http://tempuri.org/")]
    public partial class eContenedorCargaItem : object
    {
        
        private int ID_OTField;
        
        private int ID_CONTENEDORField;
        
        private string CODIGO_CONTENEDORField;
        
        private int ESTADOField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ID_OT
        {
            get
            {
                return this.ID_OTField;
            }
            set
            {
                this.ID_OTField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public int ID_CONTENEDOR
        {
            get
            {
                return this.ID_CONTENEDORField;
            }
            set
            {
                this.ID_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string CODIGO_CONTENEDOR
        {
            get
            {
                return this.CODIGO_CONTENEDORField;
            }
            set
            {
                this.CODIGO_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public int ESTADO
        {
            get
            {
                return this.ESTADOField;
            }
            set
            {
                this.ESTADOField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eContenedorCargadoItem", Namespace="http://tempuri.org/")]
    public partial class eContenedorCargadoItem : object
    {
        
        private int ID_OTField;
        
        private int ID_CONTENEDORField;
        
        private string CODIGO_CONTENEDORField;
        
        private int ESTADOField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ID_OT
        {
            get
            {
                return this.ID_OTField;
            }
            set
            {
                this.ID_OTField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public int ID_CONTENEDOR
        {
            get
            {
                return this.ID_CONTENEDORField;
            }
            set
            {
                this.ID_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string CODIGO_CONTENEDOR
        {
            get
            {
                return this.CODIGO_CONTENEDORField;
            }
            set
            {
                this.CODIGO_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public int ESTADO
        {
            get
            {
                return this.ESTADOField;
            }
            set
            {
                this.ESTADOField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePickingLote", Namespace="http://tempuri.org/")]
    public partial class ePickingLote : object
    {
        
        private int IDLOTEField;
        
        private int IDMUNDOField;
        
        private string MUNDOField;
        
        private string CODIGO_LOTEField;
        
        private bool FLG_REPICKINGField;
        
        private int CANT_POSICIONField;
        
        private int FLG_GRUORDPRIField;
        
        private int IDACTIVIDADField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private ServiceReference1.ArrayOfEPickingPendienteItem PICKING_PENDIENTEField;
        
        private ServiceReference1.ArrayOfEPickingEncontradoItem PICKING_ENCONTRADOField;
        
        private ServiceReference1.ArrayOfEPickingSustitutoItem PICKING_SUSTITUTOField;
        
        private ServiceReference1.ArrayOfEPickingFaltanteItem PICKING_FALTANTEField;
        
        private ServiceReference1.ArrayOfEPickingDetalleLoteItem PICKING_DETALLELOTEField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IDLOTE
        {
            get
            {
                return this.IDLOTEField;
            }
            set
            {
                this.IDLOTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IDMUNDO
        {
            get
            {
                return this.IDMUNDOField;
            }
            set
            {
                this.IDMUNDOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MUNDO
        {
            get
            {
                return this.MUNDOField;
            }
            set
            {
                this.MUNDOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string CODIGO_LOTE
        {
            get
            {
                return this.CODIGO_LOTEField;
            }
            set
            {
                this.CODIGO_LOTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public bool FLG_REPICKING
        {
            get
            {
                return this.FLG_REPICKINGField;
            }
            set
            {
                this.FLG_REPICKINGField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public int CANT_POSICION
        {
            get
            {
                return this.CANT_POSICIONField;
            }
            set
            {
                this.CANT_POSICIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public int FLG_GRUORDPRI
        {
            get
            {
                return this.FLG_GRUORDPRIField;
            }
            set
            {
                this.FLG_GRUORDPRIField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=7)]
        public int IDACTIVIDAD
        {
            get
            {
                return this.IDACTIVIDADField;
            }
            set
            {
                this.IDACTIVIDADField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=8)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=9)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=10)]
        public ServiceReference1.ArrayOfEPickingPendienteItem PICKING_PENDIENTE
        {
            get
            {
                return this.PICKING_PENDIENTEField;
            }
            set
            {
                this.PICKING_PENDIENTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=11)]
        public ServiceReference1.ArrayOfEPickingEncontradoItem PICKING_ENCONTRADO
        {
            get
            {
                return this.PICKING_ENCONTRADOField;
            }
            set
            {
                this.PICKING_ENCONTRADOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=12)]
        public ServiceReference1.ArrayOfEPickingSustitutoItem PICKING_SUSTITUTO
        {
            get
            {
                return this.PICKING_SUSTITUTOField;
            }
            set
            {
                this.PICKING_SUSTITUTOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=13)]
        public ServiceReference1.ArrayOfEPickingFaltanteItem PICKING_FALTANTE
        {
            get
            {
                return this.PICKING_FALTANTEField;
            }
            set
            {
                this.PICKING_FALTANTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=14)]
        public ServiceReference1.ArrayOfEPickingDetalleLoteItem PICKING_DETALLELOTE
        {
            get
            {
                return this.PICKING_DETALLELOTEField;
            }
            set
            {
                this.PICKING_DETALLELOTEField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEPickingPendienteItem", Namespace="http://tempuri.org/", ItemName="ePickingPendienteItem")]
    public class ArrayOfEPickingPendienteItem : System.Collections.Generic.List<ServiceReference1.ePickingPendienteItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEPickingEncontradoItem", Namespace="http://tempuri.org/", ItemName="ePickingEncontradoItem")]
    public class ArrayOfEPickingEncontradoItem : System.Collections.Generic.List<ServiceReference1.ePickingEncontradoItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEPickingSustitutoItem", Namespace="http://tempuri.org/", ItemName="ePickingSustitutoItem")]
    public class ArrayOfEPickingSustitutoItem : System.Collections.Generic.List<ServiceReference1.ePickingSustitutoItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEPickingFaltanteItem", Namespace="http://tempuri.org/", ItemName="ePickingFaltanteItem")]
    public class ArrayOfEPickingFaltanteItem : System.Collections.Generic.List<ServiceReference1.ePickingFaltanteItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEPickingDetalleLoteItem", Namespace="http://tempuri.org/", ItemName="ePickingDetalleLoteItem")]
    public class ArrayOfEPickingDetalleLoteItem : System.Collections.Generic.List<ServiceReference1.ePickingDetalleLoteItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePickingPendienteItem", Namespace="http://tempuri.org/")]
    public partial class ePickingPendienteItem : object
    {
        
        private int ORDENField;
        
        private string CLASEField;
        
        private string SKUField;
        
        private string PRODUCTOField;
        
        private string UNIDADField;
        
        private double CANT_PENDIENTEField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ORDEN
        {
            get
            {
                return this.ORDENField;
            }
            set
            {
                this.ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string CLASE
        {
            get
            {
                return this.CLASEField;
            }
            set
            {
                this.CLASEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string SKU
        {
            get
            {
                return this.SKUField;
            }
            set
            {
                this.SKUField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string PRODUCTO
        {
            get
            {
                return this.PRODUCTOField;
            }
            set
            {
                this.PRODUCTOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string UNIDAD
        {
            get
            {
                return this.UNIDADField;
            }
            set
            {
                this.UNIDADField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public double CANT_PENDIENTE
        {
            get
            {
                return this.CANT_PENDIENTEField;
            }
            set
            {
                this.CANT_PENDIENTEField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePickingEncontradoItem", Namespace="http://tempuri.org/")]
    public partial class ePickingEncontradoItem : object
    {
        
        private int ID_OCField;
        
        private string NUMERO_ORDENField;
        
        private string TIPO_CONTENEDORField;
        
        private string CODIGO_CONTENEDORField;
        
        private double CANT_ENCONTRADAField;
        
        private string UNIDADField;
        
        private string PROD_ENCONTRADOField;
        
        private int ID_DETALLE_OCField;
        
        private int ID_RESULTADOField;
        
        private int IDLOTEDETALLEPICKINGField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ID_OC
        {
            get
            {
                return this.ID_OCField;
            }
            set
            {
                this.ID_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string NUMERO_ORDEN
        {
            get
            {
                return this.NUMERO_ORDENField;
            }
            set
            {
                this.NUMERO_ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string TIPO_CONTENEDOR
        {
            get
            {
                return this.TIPO_CONTENEDORField;
            }
            set
            {
                this.TIPO_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string CODIGO_CONTENEDOR
        {
            get
            {
                return this.CODIGO_CONTENEDORField;
            }
            set
            {
                this.CODIGO_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public double CANT_ENCONTRADA
        {
            get
            {
                return this.CANT_ENCONTRADAField;
            }
            set
            {
                this.CANT_ENCONTRADAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string UNIDAD
        {
            get
            {
                return this.UNIDADField;
            }
            set
            {
                this.UNIDADField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string PROD_ENCONTRADO
        {
            get
            {
                return this.PROD_ENCONTRADOField;
            }
            set
            {
                this.PROD_ENCONTRADOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=7)]
        public int ID_DETALLE_OC
        {
            get
            {
                return this.ID_DETALLE_OCField;
            }
            set
            {
                this.ID_DETALLE_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=8)]
        public int ID_RESULTADO
        {
            get
            {
                return this.ID_RESULTADOField;
            }
            set
            {
                this.ID_RESULTADOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=9)]
        public int IDLOTEDETALLEPICKING
        {
            get
            {
                return this.IDLOTEDETALLEPICKINGField;
            }
            set
            {
                this.IDLOTEDETALLEPICKINGField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePickingSustitutoItem", Namespace="http://tempuri.org/")]
    public partial class ePickingSustitutoItem : object
    {
        
        private int ID_OCField;
        
        private string NUMERO_ORDENField;
        
        private string PROD_SOLICITADOField;
        
        private string PROD_SUSTITUTOField;
        
        private double CANT_SUSTITUIDAField;
        
        private int ID_DETALLE_OCField;
        
        private int ID_RESULTADOField;
        
        private int IDLOTEDETALLEPICKINGField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ID_OC
        {
            get
            {
                return this.ID_OCField;
            }
            set
            {
                this.ID_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string NUMERO_ORDEN
        {
            get
            {
                return this.NUMERO_ORDENField;
            }
            set
            {
                this.NUMERO_ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string PROD_SOLICITADO
        {
            get
            {
                return this.PROD_SOLICITADOField;
            }
            set
            {
                this.PROD_SOLICITADOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string PROD_SUSTITUTO
        {
            get
            {
                return this.PROD_SUSTITUTOField;
            }
            set
            {
                this.PROD_SUSTITUTOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public double CANT_SUSTITUIDA
        {
            get
            {
                return this.CANT_SUSTITUIDAField;
            }
            set
            {
                this.CANT_SUSTITUIDAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public int ID_DETALLE_OC
        {
            get
            {
                return this.ID_DETALLE_OCField;
            }
            set
            {
                this.ID_DETALLE_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public int ID_RESULTADO
        {
            get
            {
                return this.ID_RESULTADOField;
            }
            set
            {
                this.ID_RESULTADOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=7)]
        public int IDLOTEDETALLEPICKING
        {
            get
            {
                return this.IDLOTEDETALLEPICKINGField;
            }
            set
            {
                this.IDLOTEDETALLEPICKINGField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePickingFaltanteItem", Namespace="http://tempuri.org/")]
    public partial class ePickingFaltanteItem : object
    {
        
        private int IDLOTEField;
        
        private string CODIGO_LOTEField;
        
        private int ID_OCField;
        
        private string NUMERO_ORDENField;
        
        private string PROD_FALTANTEField;
        
        private double CANT_FALTANTEField;
        
        private int ID_DETALLE_OCField;
        
        private int ID_RESULTADOField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IDLOTE
        {
            get
            {
                return this.IDLOTEField;
            }
            set
            {
                this.IDLOTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string CODIGO_LOTE
        {
            get
            {
                return this.CODIGO_LOTEField;
            }
            set
            {
                this.CODIGO_LOTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int ID_OC
        {
            get
            {
                return this.ID_OCField;
            }
            set
            {
                this.ID_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string NUMERO_ORDEN
        {
            get
            {
                return this.NUMERO_ORDENField;
            }
            set
            {
                this.NUMERO_ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string PROD_FALTANTE
        {
            get
            {
                return this.PROD_FALTANTEField;
            }
            set
            {
                this.PROD_FALTANTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public double CANT_FALTANTE
        {
            get
            {
                return this.CANT_FALTANTEField;
            }
            set
            {
                this.CANT_FALTANTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public int ID_DETALLE_OC
        {
            get
            {
                return this.ID_DETALLE_OCField;
            }
            set
            {
                this.ID_DETALLE_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=7)]
        public int ID_RESULTADO
        {
            get
            {
                return this.ID_RESULTADOField;
            }
            set
            {
                this.ID_RESULTADOField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePickingDetalleLoteItem", Namespace="http://tempuri.org/")]
    public partial class ePickingDetalleLoteItem : object
    {
        
        private int ORDENField;
        
        private int ID_OCField;
        
        private string NUMERO_ORDENField;
        
        private string SKUField;
        
        private string PRODUCTOField;
        
        private string UNIDADField;
        
        private double CANTIDADField;
        
        private int PRIORIDADField;
        
        private int FLAGField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ORDEN
        {
            get
            {
                return this.ORDENField;
            }
            set
            {
                this.ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public int ID_OC
        {
            get
            {
                return this.ID_OCField;
            }
            set
            {
                this.ID_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string NUMERO_ORDEN
        {
            get
            {
                return this.NUMERO_ORDENField;
            }
            set
            {
                this.NUMERO_ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string SKU
        {
            get
            {
                return this.SKUField;
            }
            set
            {
                this.SKUField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string PRODUCTO
        {
            get
            {
                return this.PRODUCTOField;
            }
            set
            {
                this.PRODUCTOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string UNIDAD
        {
            get
            {
                return this.UNIDADField;
            }
            set
            {
                this.UNIDADField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public double CANTIDAD
        {
            get
            {
                return this.CANTIDADField;
            }
            set
            {
                this.CANTIDADField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=7)]
        public int PRIORIDAD
        {
            get
            {
                return this.PRIORIDADField;
            }
            set
            {
                this.PRIORIDADField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=8)]
        public int FLAG
        {
            get
            {
                return this.FLAGField;
            }
            set
            {
                this.FLAGField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eDetalleArticuloMP", Namespace="http://tempuri.org/")]
    public partial class eDetalleArticuloMP : object
    {
        
        private int IDLOTEField;
        
        private string CODIGO_LOTEField;
        
        private int ID_OCField;
        
        private string NUMERO_ORDENField;
        
        private int ID_DETALLEOCField;
        
        private int IDLOTEDETALLEField;
        
        private string SKUField;
        
        private string EANField;
        
        private int TIENDAField;
        
        private string PRODUCTOField;
        
        private string MARCAField;
        
        private string CATEGORIAField;
        
        private string PRESENTACIONField;
        
        private string UNIDADField;
        
        private int PESABLEField;
        
        private double PRECIOField;
        
        private double STOCKField;
        
        private string NOTAField;
        
        private string NOTA_SUSTITUCIONField;
        
        private double CANT_SOLICITADAField;
        
        private decimal CANT_PENDIENTEField;
        
        private string UBICACIONField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private string NOMTIPOCLIENTEField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IDLOTE
        {
            get
            {
                return this.IDLOTEField;
            }
            set
            {
                this.IDLOTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string CODIGO_LOTE
        {
            get
            {
                return this.CODIGO_LOTEField;
            }
            set
            {
                this.CODIGO_LOTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int ID_OC
        {
            get
            {
                return this.ID_OCField;
            }
            set
            {
                this.ID_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string NUMERO_ORDEN
        {
            get
            {
                return this.NUMERO_ORDENField;
            }
            set
            {
                this.NUMERO_ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public int ID_DETALLEOC
        {
            get
            {
                return this.ID_DETALLEOCField;
            }
            set
            {
                this.ID_DETALLEOCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public int IDLOTEDETALLE
        {
            get
            {
                return this.IDLOTEDETALLEField;
            }
            set
            {
                this.IDLOTEDETALLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string SKU
        {
            get
            {
                return this.SKUField;
            }
            set
            {
                this.SKUField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string EAN
        {
            get
            {
                return this.EANField;
            }
            set
            {
                this.EANField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=8)]
        public int TIENDA
        {
            get
            {
                return this.TIENDAField;
            }
            set
            {
                this.TIENDAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=9)]
        public string PRODUCTO
        {
            get
            {
                return this.PRODUCTOField;
            }
            set
            {
                this.PRODUCTOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=10)]
        public string MARCA
        {
            get
            {
                return this.MARCAField;
            }
            set
            {
                this.MARCAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=11)]
        public string CATEGORIA
        {
            get
            {
                return this.CATEGORIAField;
            }
            set
            {
                this.CATEGORIAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=12)]
        public string PRESENTACION
        {
            get
            {
                return this.PRESENTACIONField;
            }
            set
            {
                this.PRESENTACIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=13)]
        public string UNIDAD
        {
            get
            {
                return this.UNIDADField;
            }
            set
            {
                this.UNIDADField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=14)]
        public int PESABLE
        {
            get
            {
                return this.PESABLEField;
            }
            set
            {
                this.PESABLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=15)]
        public double PRECIO
        {
            get
            {
                return this.PRECIOField;
            }
            set
            {
                this.PRECIOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=16)]
        public double STOCK
        {
            get
            {
                return this.STOCKField;
            }
            set
            {
                this.STOCKField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=17)]
        public string NOTA
        {
            get
            {
                return this.NOTAField;
            }
            set
            {
                this.NOTAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=18)]
        public string NOTA_SUSTITUCION
        {
            get
            {
                return this.NOTA_SUSTITUCIONField;
            }
            set
            {
                this.NOTA_SUSTITUCIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=19)]
        public double CANT_SOLICITADA
        {
            get
            {
                return this.CANT_SOLICITADAField;
            }
            set
            {
                this.CANT_SOLICITADAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=20)]
        public decimal CANT_PENDIENTE
        {
            get
            {
                return this.CANT_PENDIENTEField;
            }
            set
            {
                this.CANT_PENDIENTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=21)]
        public string UBICACION
        {
            get
            {
                return this.UBICACIONField;
            }
            set
            {
                this.UBICACIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=22)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=23)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=24)]
        public string NOMTIPOCLIENTE
        {
            get
            {
                return this.NOMTIPOCLIENTEField;
            }
            set
            {
                this.NOMTIPOCLIENTEField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eCliente", Namespace="http://tempuri.org/")]
    public partial class eCliente : object
    {
        
        private string CLIENTEField;
        
        private string TELEFONOField;
        
        private int CRITERIOField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string CLIENTE
        {
            get
            {
                return this.CLIENTEField;
            }
            set
            {
                this.CLIENTEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string TELEFONO
        {
            get
            {
                return this.TELEFONOField;
            }
            set
            {
                this.TELEFONOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int CRITERIO
        {
            get
            {
                return this.CRITERIOField;
            }
            set
            {
                this.CRITERIOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eValidaSustitucion", Namespace="http://tempuri.org/")]
    public partial class eValidaSustitucion : object
    {
        
        private int ID_DETALLE_OCField;
        
        private string SKUField;
        
        private string EANField;
        
        private decimal CANTIDADField;
        
        private string UNIDADField;
        
        private int PESABLEField;
        
        private string PRODUCTOField;
        
        private string MARCAField;
        
        private string CLASEField;
        
        private int STOCKField;
        
        private double PRECIOREFERENCIALField;
        
        private int TIENDAField;
        
        private string TIPO_PESABLEField;
        
        private double CANT_PESABLEField;
        
        private int FLAGField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ID_DETALLE_OC
        {
            get
            {
                return this.ID_DETALLE_OCField;
            }
            set
            {
                this.ID_DETALLE_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string SKU
        {
            get
            {
                return this.SKUField;
            }
            set
            {
                this.SKUField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string EAN
        {
            get
            {
                return this.EANField;
            }
            set
            {
                this.EANField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public decimal CANTIDAD
        {
            get
            {
                return this.CANTIDADField;
            }
            set
            {
                this.CANTIDADField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string UNIDAD
        {
            get
            {
                return this.UNIDADField;
            }
            set
            {
                this.UNIDADField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public int PESABLE
        {
            get
            {
                return this.PESABLEField;
            }
            set
            {
                this.PESABLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string PRODUCTO
        {
            get
            {
                return this.PRODUCTOField;
            }
            set
            {
                this.PRODUCTOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string MARCA
        {
            get
            {
                return this.MARCAField;
            }
            set
            {
                this.MARCAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=8)]
        public string CLASE
        {
            get
            {
                return this.CLASEField;
            }
            set
            {
                this.CLASEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=9)]
        public int STOCK
        {
            get
            {
                return this.STOCKField;
            }
            set
            {
                this.STOCKField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=10)]
        public double PRECIOREFERENCIAL
        {
            get
            {
                return this.PRECIOREFERENCIALField;
            }
            set
            {
                this.PRECIOREFERENCIALField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=11)]
        public int TIENDA
        {
            get
            {
                return this.TIENDAField;
            }
            set
            {
                this.TIENDAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=12)]
        public string TIPO_PESABLE
        {
            get
            {
                return this.TIPO_PESABLEField;
            }
            set
            {
                this.TIPO_PESABLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=13)]
        public double CANT_PESABLE
        {
            get
            {
                return this.CANT_PESABLEField;
            }
            set
            {
                this.CANT_PESABLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=14)]
        public int FLAG
        {
            get
            {
                return this.FLAGField;
            }
            set
            {
                this.FLAGField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=15)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=16)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eValidaContenedor", Namespace="http://tempuri.org/")]
    public partial class eValidaContenedor : object
    {
        
        private int IDLOTEDETALLEField;
        
        private int IDCONTENEDORField;
        
        private string CODIGO_CONTENEDORField;
        
        private int IDTG_TIPOCONTENEDORField;
        
        private int POSICIONField;
        
        private string TIPOCONTENEDORField;
        
        private int IDTG_ESTADOField;
        
        private string ESTADOField;
        
        private int FLG_BULTOField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IDLOTEDETALLE
        {
            get
            {
                return this.IDLOTEDETALLEField;
            }
            set
            {
                this.IDLOTEDETALLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public int IDCONTENEDOR
        {
            get
            {
                return this.IDCONTENEDORField;
            }
            set
            {
                this.IDCONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string CODIGO_CONTENEDOR
        {
            get
            {
                return this.CODIGO_CONTENEDORField;
            }
            set
            {
                this.CODIGO_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public int IDTG_TIPOCONTENEDOR
        {
            get
            {
                return this.IDTG_TIPOCONTENEDORField;
            }
            set
            {
                this.IDTG_TIPOCONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public int POSICION
        {
            get
            {
                return this.POSICIONField;
            }
            set
            {
                this.POSICIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string TIPOCONTENEDOR
        {
            get
            {
                return this.TIPOCONTENEDORField;
            }
            set
            {
                this.TIPOCONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=6)]
        public int IDTG_ESTADO
        {
            get
            {
                return this.IDTG_ESTADOField;
            }
            set
            {
                this.IDTG_ESTADOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string ESTADO
        {
            get
            {
                return this.ESTADOField;
            }
            set
            {
                this.ESTADOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=8)]
        public int FLG_BULTO
        {
            get
            {
                return this.FLG_BULTOField;
            }
            set
            {
                this.FLG_BULTOField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=9)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=10)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eValidaArticuloMP", Namespace="http://tempuri.org/")]
    public partial class eValidaArticuloMP : object
    {
        
        private string TIPO_PESABLEField;
        
        private double CANT_PESABLEField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string TIPO_PESABLE
        {
            get
            {
                return this.TIPO_PESABLEField;
            }
            set
            {
                this.TIPO_PESABLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public double CANT_PESABLE
        {
            get
            {
                return this.CANT_PESABLEField;
            }
            set
            {
                this.CANT_PESABLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePosicionJaba", Namespace="http://tempuri.org/")]
    public partial class ePosicionJaba : object
    {
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private ServiceReference1.ArrayOfEPosicionJabaItem POSICION_JABAField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public ServiceReference1.ArrayOfEPosicionJabaItem POSICION_JABA
        {
            get
            {
                return this.POSICION_JABAField;
            }
            set
            {
                this.POSICION_JABAField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEPosicionJabaItem", Namespace="http://tempuri.org/", ItemName="ePosicionJabaItem")]
    public class ArrayOfEPosicionJabaItem : System.Collections.Generic.List<ServiceReference1.ePosicionJabaItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePosicionJabaItem", Namespace="http://tempuri.org/")]
    public partial class ePosicionJabaItem : object
    {
        
        private string FECHA_VENTANAField;
        
        private int CANT_CONTENEDORField;
        
        private int POSICIONField;
        
        private string CODIGO_CONTENEDORField;
        
        private string DESCRIPCIONField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string FECHA_VENTANA
        {
            get
            {
                return this.FECHA_VENTANAField;
            }
            set
            {
                this.FECHA_VENTANAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public int CANT_CONTENEDOR
        {
            get
            {
                return this.CANT_CONTENEDORField;
            }
            set
            {
                this.CANT_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int POSICION
        {
            get
            {
                return this.POSICIONField;
            }
            set
            {
                this.POSICIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string CODIGO_CONTENEDOR
        {
            get
            {
                return this.CODIGO_CONTENEDORField;
            }
            set
            {
                this.CODIGO_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string DESCRIPCION
        {
            get
            {
                return this.DESCRIPCIONField;
            }
            set
            {
                this.DESCRIPCIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eCantAlmacenada", Namespace="http://tempuri.org/")]
    public partial class eCantAlmacenada : object
    {
        
        private int CANT_ALMACENADAField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int CANT_ALMACENADA
        {
            get
            {
                return this.CANT_ALMACENADAField;
            }
            set
            {
                this.CANT_ALMACENADAField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eContenedor", Namespace="http://tempuri.org/")]
    public partial class eContenedor : object
    {
        
        private string NUMERO_ORDENField;
        
        private int POSICIONField;
        
        private string ID_OTField;
        
        private int ORDEN_OTField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string NUMERO_ORDEN
        {
            get
            {
                return this.NUMERO_ORDENField;
            }
            set
            {
                this.NUMERO_ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int POSICION
        {
            get
            {
                return this.POSICIONField;
            }
            set
            {
                this.POSICIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string ID_OT
        {
            get
            {
                return this.ID_OTField;
            }
            set
            {
                this.ID_OTField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public int ORDEN_OT
        {
            get
            {
                return this.ORDEN_OTField;
            }
            set
            {
                this.ORDEN_OTField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eUbicacion", Namespace="http://tempuri.org/")]
    public partial class eUbicacion : object
    {
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private int IDUBICACIONField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int IDUBICACION
        {
            get
            {
                return this.IDUBICACIONField;
            }
            set
            {
                this.IDUBICACIONField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePosicionOrdenCompra", Namespace="http://tempuri.org/")]
    public partial class ePosicionOrdenCompra : object
    {
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private ServiceReference1.ArrayOfEPosicionOrdenCompraItem POSICION_ORDENCOMPRAField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public ServiceReference1.ArrayOfEPosicionOrdenCompraItem POSICION_ORDENCOMPRA
        {
            get
            {
                return this.POSICION_ORDENCOMPRAField;
            }
            set
            {
                this.POSICION_ORDENCOMPRAField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEPosicionOrdenCompraItem", Namespace="http://tempuri.org/", ItemName="ePosicionOrdenCompraItem")]
    public class ArrayOfEPosicionOrdenCompraItem : System.Collections.Generic.List<ServiceReference1.ePosicionOrdenCompraItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePosicionOrdenCompraItem", Namespace="http://tempuri.org/")]
    public partial class ePosicionOrdenCompraItem : object
    {
        
        private int ORDENField;
        
        private int IDLOTEDETALLEField;
        
        private int POSICIONField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ORDEN
        {
            get
            {
                return this.ORDENField;
            }
            set
            {
                this.ORDENField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public int IDLOTEDETALLE
        {
            get
            {
                return this.IDLOTEDETALLEField;
            }
            set
            {
                this.IDLOTEDETALLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int POSICION
        {
            get
            {
                return this.POSICIONField;
            }
            set
            {
                this.POSICIONField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePosicionContenedorLote", Namespace="http://tempuri.org/")]
    public partial class ePosicionContenedorLote : object
    {
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private ServiceReference1.ArrayOfEPosicionContenedorLoteItem POSICION_CONTENEDORLOTEField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public ServiceReference1.ArrayOfEPosicionContenedorLoteItem POSICION_CONTENEDORLOTE
        {
            get
            {
                return this.POSICION_CONTENEDORLOTEField;
            }
            set
            {
                this.POSICION_CONTENEDORLOTEField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEPosicionContenedorLoteItem", Namespace="http://tempuri.org/", ItemName="ePosicionContenedorLoteItem")]
    public class ArrayOfEPosicionContenedorLoteItem : System.Collections.Generic.List<ServiceReference1.ePosicionContenedorLoteItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePosicionContenedorLoteItem", Namespace="http://tempuri.org/")]
    public partial class ePosicionContenedorLoteItem : object
    {
        
        private int IDLOTEDETALLEField;
        
        private int ID_OCField;
        
        private int IDCONTENEDORField;
        
        private string CODIGO_CONTENEDORField;
        
        private int POSICIONField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IDLOTEDETALLE
        {
            get
            {
                return this.IDLOTEDETALLEField;
            }
            set
            {
                this.IDLOTEDETALLEField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int ID_OC
        {
            get
            {
                return this.ID_OCField;
            }
            set
            {
                this.ID_OCField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int IDCONTENEDOR
        {
            get
            {
                return this.IDCONTENEDORField;
            }
            set
            {
                this.IDCONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string CODIGO_CONTENEDOR
        {
            get
            {
                return this.CODIGO_CONTENEDORField;
            }
            set
            {
                this.CODIGO_CONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public int POSICION
        {
            get
            {
                return this.POSICIONField;
            }
            set
            {
                this.POSICIONField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ePickingFaltante", Namespace="http://tempuri.org/")]
    public partial class ePickingFaltante : object
    {
        
        private int CANT_IMPRIMIRField;
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private ServiceReference1.ArrayOfEPickingFaltanteItem LISTADOField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int CANT_IMPRIMIR
        {
            get
            {
                return this.CANT_IMPRIMIRField;
            }
            set
            {
                this.CANT_IMPRIMIRField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public ServiceReference1.ArrayOfEPickingFaltanteItem LISTADO
        {
            get
            {
                return this.LISTADOField;
            }
            set
            {
                this.LISTADOField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eUbicacionContenedor", Namespace="http://tempuri.org/")]
    public partial class eUbicacionContenedor : object
    {
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private ServiceReference1.ArrayOfEUbicacionContenedorItem LISTADOField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public ServiceReference1.ArrayOfEUbicacionContenedorItem LISTADO
        {
            get
            {
                return this.LISTADOField;
            }
            set
            {
                this.LISTADOField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEUbicacionContenedorItem", Namespace="http://tempuri.org/", ItemName="eUbicacionContenedorItem")]
    public class ArrayOfEUbicacionContenedorItem : System.Collections.Generic.List<ServiceReference1.eUbicacionContenedorItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eUbicacionContenedorItem", Namespace="http://tempuri.org/")]
    public partial class eUbicacionContenedorItem : object
    {
        
        private int IDUBICACIONField;
        
        private string CODIGO_UBICACIONField;
        
        private int IDCONTENEDORField;
        
        private string CODIGO_CONTENEDORField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IDUBICACION
        {
            get
            {
                return this.IDUBICACIONField;
            }
            set
            {
                this.IDUBICACIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string CODIGO_UBICACION
        {
            get
            {
                return this.CODIGO_UBICACIONField;
            }
            set
            {
                this.CODIGO_UBICACIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int IDCONTENEDOR
        {
            get
            {
                return this.IDCONTENEDORField;
            }
            set
            {
                this.IDCONTENEDORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string CODIGO_CONTENEDOR
        {
            get
            {
                return this.CODIGO_CONTENEDORField;
            }
            set
            {
                this.CODIGO_CONTENEDORField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfEUbicacionOCItem", Namespace="http://tempuri.org/", ItemName="eUbicacionOCItem")]
    public class ArrayOfEUbicacionOCItem : System.Collections.Generic.List<ServiceReference1.eUbicacionOCItem>
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eUbicacionOCItem", Namespace="http://tempuri.org/")]
    public partial class eUbicacionOCItem : object
    {
        
        private int IDUBICACIONField;
        
        private string CODIGO_UBICACIONField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int IDUBICACION
        {
            get
            {
                return this.IDUBICACIONField;
            }
            set
            {
                this.IDUBICACIONField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string CODIGO_UBICACION
        {
            get
            {
                return this.CODIGO_UBICACIONField;
            }
            set
            {
                this.CODIGO_UBICACIONField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="eOperario", Namespace="http://tempuri.org/")]
    public partial class eOperario : object
    {
        
        private int COD_ERRORField;
        
        private string MSJ_ERRORField;
        
        private int CODIGO_LOCALField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int COD_ERROR
        {
            get
            {
                return this.COD_ERRORField;
            }
            set
            {
                this.COD_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string MSJ_ERROR
        {
            get
            {
                return this.MSJ_ERRORField;
            }
            set
            {
                this.MSJ_ERRORField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public int CODIGO_LOCAL
        {
            get
            {
                return this.CODIGO_LOCALField;
            }
            set
            {
                this.CODIGO_LOCALField = value;
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.WebServiceXpickingSoap")]
    public interface WebServiceXpickingSoap
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getValidacion_Usuario", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getValidacion_UsuarioResponse> getValidacion_UsuarioAsync(ServiceReference1.getValidacion_UsuarioRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getListar_Ordenes_Compra", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getListar_Ordenes_CompraResponse> getListar_Ordenes_CompraAsync(ServiceReference1.getListar_Ordenes_CompraRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getListar_Articulos_OC", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getListar_Articulos_OCResponse> getListar_Articulos_OCAsync(ServiceReference1.getListar_Articulos_OCRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getListar_Articulos_OC_Eliminar", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getListar_Articulos_OC_EliminarResponse> getListar_Articulos_OC_EliminarAsync(ServiceReference1.getListar_Articulos_OC_EliminarRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getListar_Detalle_Articulo", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getListar_Detalle_ArticuloResponse> getListar_Detalle_ArticuloAsync(ServiceReference1.getListar_Detalle_ArticuloRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getGrabar_Picking_Articulo", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getGrabar_Picking_ArticuloResponse> getGrabar_Picking_ArticuloAsync(ServiceReference1.getGrabar_Picking_ArticuloRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Finalizar_Picking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.Finalizar_PickingResponse> Finalizar_PickingAsync(ServiceReference1.Finalizar_PickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Finalizar_Picking_Food", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.Finalizar_Picking_FoodResponse> Finalizar_Picking_FoodAsync(ServiceReference1.Finalizar_Picking_FoodRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Enviar_Orden_PosVirtual", ReplyAction="*")]
        System.Threading.Tasks.Task<int> Enviar_Orden_PosVirtualAsync(int id_oc, int id_tienda, int id_operario);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Faltante_Picking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.Faltante_PickingResponse> Faltante_PickingAsync(ServiceReference1.Faltante_PickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Sustituto_Detalle_Articulo", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.Sustituto_Detalle_ArticuloResponse> Sustituto_Detalle_ArticuloAsync(ServiceReference1.Sustituto_Detalle_ArticuloRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Asignar_Jabas", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.Asignar_JabasResponse> Asignar_JabasAsync(ServiceReference1.Asignar_JabasRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Listar_Jabas", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.Listar_JabasResponse> Listar_JabasAsync(ServiceReference1.Listar_JabasRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Eliminar_Picking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.Eliminar_PickingResponse> Eliminar_PickingAsync(ServiceReference1.Eliminar_PickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Eliminar_Picking_Total", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.Eliminar_Picking_TotalResponse> Eliminar_Picking_TotalAsync(ServiceReference1.Eliminar_Picking_TotalRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Iniciar_Cerrar_Actividad_OC", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.Iniciar_Cerrar_Actividad_OCResponse> Iniciar_Cerrar_Actividad_OCAsync(ServiceReference1.Iniciar_Cerrar_Actividad_OCRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getEstado_Orden_PosVirtual", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getEstado_Orden_PosVirtualResponse> getEstado_Orden_PosVirtualAsync(ServiceReference1.getEstado_Orden_PosVirtualRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getNumero_ProductoSuelto", ReplyAction="*")]
        System.Threading.Tasks.Task<int> getNumero_ProductoSueltoAsync(int id_oc);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Actualizar_Numero_ProductoSuelto", ReplyAction="*")]
        System.Threading.Tasks.Task<int> Actualizar_Numero_ProductoSueltoAsync(int id_oc, int cantidad);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getColaImpresion", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getColaImpresionResponse> getColaImpresionAsync(ServiceReference1.getColaImpresionRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getprinterStatus", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getprinterStatusResponse> getprinterStatusAsync(ServiceReference1.getprinterStatusRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getEtiquetaOC", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getEtiquetaOCResponse> getEtiquetaOCAsync(ServiceReference1.getEtiquetaOCRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/printEtiquetaCONTENEDOR", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaCONTENEDORResponse> printEtiquetaCONTENEDORAsync(ServiceReference1.printEtiquetaCONTENEDORRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/printEtiquetaUBICACION", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaUBICACIONResponse> printEtiquetaUBICACIONAsync(ServiceReference1.printEtiquetaUBICACIONRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/printEtiquetaOC", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaOCResponse> printEtiquetaOCAsync(ServiceReference1.printEtiquetaOCRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/printEtiquetaLOTE", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaLOTEResponse> printEtiquetaLOTEAsync(ServiceReference1.printEtiquetaLOTERequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/printEtiquetaOCLOTE", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaOCLOTEResponse> printEtiquetaOCLOTEAsync(ServiceReference1.printEtiquetaOCLOTERequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/validaVersion", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.validaVersionResponse> validaVersionAsync(ServiceReference1.validaVersionRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/validaEquipo", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.validaEquipoResponse> validaEquipoAsync(ServiceReference1.validaEquipoRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/actulizarIdEquipo", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.actulizarIdEquipoResponse> actulizarIdEquipoAsync(ServiceReference1.actulizarIdEquipoRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getAsignarLote", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getAsignarLoteResponse> getAsignarLoteAsync(ServiceReference1.getAsignarLoteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getContenedorSugeridoText", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getContenedorSugeridoTextResponse> getContenedorSugeridoTextAsync(ServiceReference1.getContenedorSugeridoTextRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getContenedorSugeridoPaso", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getContenedorSugeridoPasoResponse> getContenedorSugeridoPasoAsync(ServiceReference1.getContenedorSugeridoPasoRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getCantPedXOperario", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getCantPedXOperarioResponse> getCantPedXOperarioAsync(ServiceReference1.getCantPedXOperarioRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/inicioMultipicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.inicioMultipickingResponse> inicioMultipickingAsync(ServiceReference1.inicioMultipickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/updateEstadoPreparacionLoteOC", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.updateEstadoPreparacionLoteOCResponse> updateEstadoPreparacionLoteOCAsync(ServiceReference1.updateEstadoPreparacionLoteOCRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/actualizarFLG_SALTO_Detalle_OC", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.actualizarFLG_SALTO_Detalle_OCResponse> actualizarFLG_SALTO_Detalle_OCAsync(ServiceReference1.actualizarFLG_SALTO_Detalle_OCRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getOTContenedores", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getOTContenedoresResponse> getOTContenedoresAsync(ServiceReference1.getOTContenedoresRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getLotePicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getLotePickingResponse> getLotePickingAsync(ServiceReference1.getLotePickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getDetalleArticulo", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getDetalleArticuloResponse> getDetalleArticuloAsync(ServiceReference1.getDetalleArticuloRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getCliente", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getClienteResponse> getClienteAsync(ServiceReference1.getClienteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/validaSustitucion", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.validaSustitucionResponse> validaSustitucionAsync(ServiceReference1.validaSustitucionRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/validaContenedor", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.validaContenedorResponse> validaContenedorAsync(ServiceReference1.validaContenedorRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/validaMultiPicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.validaMultiPickingResponse> validaMultiPickingAsync(ServiceReference1.validaMultiPickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/grabarMultiPicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.grabarMultiPickingResponse> grabarMultiPickingAsync(ServiceReference1.grabarMultiPickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/sustituirMultiPicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.sustituirMultiPickingResponse> sustituirMultiPickingAsync(ServiceReference1.sustituirMultiPickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/faltanteMultiPicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.faltanteMultiPickingResponse> faltanteMultiPickingAsync(ServiceReference1.faltanteMultiPickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getPosicionJaba", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getPosicionJabaResponse> getPosicionJabaAsync(ServiceReference1.getPosicionJabaRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getCantContenedorAlmacenado", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getCantContenedorAlmacenadoResponse> getCantContenedorAlmacenadoAsync(ServiceReference1.getCantContenedorAlmacenadoRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getContenedor", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getContenedorResponse> getContenedorAsync(ServiceReference1.getContenedorRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ObtenerUbicacion", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.ObtenerUbicacionResponse> ObtenerUbicacionAsync(ServiceReference1.ObtenerUbicacionRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/grabarUbicacion", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.grabarUbicacionResponse> grabarUbicacionAsync(ServiceReference1.grabarUbicacionRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/grabarCargaContenedor", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.grabarCargaContenedorResponse> grabarCargaContenedorAsync(ServiceReference1.grabarCargaContenedorRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/finalizarAlmacanamiento", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.finalizarAlmacanamientoResponse> finalizarAlmacanamientoAsync(ServiceReference1.finalizarAlmacanamientoRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/eliminarMultipicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.eliminarMultipickingResponse> eliminarMultipickingAsync(ServiceReference1.eliminarMultipickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/eliminarContenedorCargado", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.eliminarContenedorCargadoResponse> eliminarContenedorCargadoAsync(ServiceReference1.eliminarContenedorCargadoRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/descargarContenedoresOT", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.descargarContenedoresOTResponse> descargarContenedoresOTAsync(ServiceReference1.descargarContenedoresOTRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getPosicionOrdenCompra", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getPosicionOrdenCompraResponse> getPosicionOrdenCompraAsync(ServiceReference1.getPosicionOrdenCompraRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getPosicionContenedorLote", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getPosicionContenedorLoteResponse> getPosicionContenedorLoteAsync(ServiceReference1.getPosicionContenedorLoteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/actulizarImpresion_Lote", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.actulizarImpresion_LoteResponse> actulizarImpresion_LoteAsync(ServiceReference1.actulizarImpresion_LoteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getRePickingFaltante", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getRePickingFaltanteResponse> getRePickingFaltanteAsync(ServiceReference1.getRePickingFaltanteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getDetalleArticuloFaltante", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getDetalleArticuloFaltanteResponse> getDetalleArticuloFaltanteAsync(ServiceReference1.getDetalleArticuloFaltanteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getUbicacionContenedorList", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getUbicacionContenedorListResponse> getUbicacionContenedorListAsync(ServiceReference1.getUbicacionContenedorListRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getUbicacionOCList", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getUbicacionOCListResponse> getUbicacionOCListAsync(ServiceReference1.getUbicacionOCListRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/validaMultiPickingFaltante", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.validaMultiPickingFaltanteResponse> validaMultiPickingFaltanteAsync(ServiceReference1.validaMultiPickingFaltanteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/validaContenedorRePicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.validaContenedorRePickingResponse> validaContenedorRePickingAsync(ServiceReference1.validaContenedorRePickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/grabarRePicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.grabarRePickingResponse> grabarRePickingAsync(ServiceReference1.grabarRePickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/grabarRePickingSustituto", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.grabarRePickingSustitutoResponse> grabarRePickingSustitutoAsync(ServiceReference1.grabarRePickingSustitutoRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/valida_TodoFaltante", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.valida_TodoFaltanteResponse> valida_TodoFaltanteAsync(ServiceReference1.valida_TodoFaltanteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getOperarioCodigoLocal", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getOperarioCodigoLocalResponse> getOperarioCodigoLocalAsync(ServiceReference1.getOperarioCodigoLocalRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/updateCantPosicion", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.updateCantPosicionResponse> updateCantPosicionAsync(ServiceReference1.updateCantPosicionRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/updateDetalleOCSalto", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.updateDetalleOCSaltoResponse> updateDetalleOCSaltoAsync(ServiceReference1.updateDetalleOCSaltoRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ValidaOcContenedor", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.ValidaOcContenedorResponse> ValidaOcContenedorAsync(ServiceReference1.ValidaOcContenedorRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ConsolidarJabaOC", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.ConsolidarJabaOCResponse> ConsolidarJabaOCAsync(ServiceReference1.ConsolidarJabaOCRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/validaContenedorMonoPicking", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.validaContenedorMonoPickingResponse> validaContenedorMonoPickingAsync(ServiceReference1.validaContenedorMonoPickingRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/updateContenedor", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.updateContenedorResponse> updateContenedorAsync(ServiceReference1.updateContenedorRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/updateContenedoresOC", ReplyAction="*")]
        System.Threading.Tasks.Task<int> updateContenedoresOCAsync(int id_oc, int id_estado);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/validaContenedorRePickingWS", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.validaContenedorRePickingWSResponse> validaContenedorRePickingWSAsync(ServiceReference1.validaContenedorRePickingWSRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getPickingPendienteXLote", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getPickingPendienteXLoteResponse> getPickingPendienteXLoteAsync(ServiceReference1.getPickingPendienteXLoteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getDetalleArticuloOC", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getDetalleArticuloOCResponse> getDetalleArticuloOCAsync(ServiceReference1.getDetalleArticuloOCRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getSKUImageByte", ReplyAction="*")]
        System.Threading.Tasks.Task<ServiceReference1.getSKUImageByteResponse> getSKUImageByteAsync(ServiceReference1.getSKUImageByteRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getValidacion_UsuarioRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getValidacion_Usuario", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getValidacion_UsuarioRequestBody Body;
        
        public getValidacion_UsuarioRequest()
        {
        }
        
        public getValidacion_UsuarioRequest(ServiceReference1.getValidacion_UsuarioRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getValidacion_UsuarioRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string codigo_usuario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string clave;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string version;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string id_equipo;
        
        public getValidacion_UsuarioRequestBody()
        {
        }
        
        public getValidacion_UsuarioRequestBody(string codigo_usuario, string clave, string version, string id_equipo)
        {
            this.codigo_usuario = codigo_usuario;
            this.clave = clave;
            this.version = version;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getValidacion_UsuarioResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getValidacion_UsuarioResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getValidacion_UsuarioResponseBody Body;
        
        public getValidacion_UsuarioResponse()
        {
        }
        
        public getValidacion_UsuarioResponse(ServiceReference1.getValidacion_UsuarioResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getValidacion_UsuarioResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eUsuario getValidacion_UsuarioResult;
        
        public getValidacion_UsuarioResponseBody()
        {
        }
        
        public getValidacion_UsuarioResponseBody(ServiceReference1.eUsuario getValidacion_UsuarioResult)
        {
            this.getValidacion_UsuarioResult = getValidacion_UsuarioResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getListar_Ordenes_CompraRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getListar_Ordenes_Compra", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getListar_Ordenes_CompraRequestBody Body;
        
        public getListar_Ordenes_CompraRequest()
        {
        }
        
        public getListar_Ordenes_CompraRequest(ServiceReference1.getListar_Ordenes_CompraRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getListar_Ordenes_CompraRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_usuario;
        
        public getListar_Ordenes_CompraRequestBody()
        {
        }
        
        public getListar_Ordenes_CompraRequestBody(int id_usuario)
        {
            this.id_usuario = id_usuario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getListar_Ordenes_CompraResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getListar_Ordenes_CompraResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getListar_Ordenes_CompraResponseBody Body;
        
        public getListar_Ordenes_CompraResponse()
        {
        }
        
        public getListar_Ordenes_CompraResponse(ServiceReference1.getListar_Ordenes_CompraResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getListar_Ordenes_CompraResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ArrayOfEOrdenCompra getListar_Ordenes_CompraResult;
        
        public getListar_Ordenes_CompraResponseBody()
        {
        }
        
        public getListar_Ordenes_CompraResponseBody(ServiceReference1.ArrayOfEOrdenCompra getListar_Ordenes_CompraResult)
        {
            this.getListar_Ordenes_CompraResult = getListar_Ordenes_CompraResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getListar_Articulos_OCRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getListar_Articulos_OC", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getListar_Articulos_OCRequestBody Body;
        
        public getListar_Articulos_OCRequest()
        {
        }
        
        public getListar_Articulos_OCRequest(ServiceReference1.getListar_Articulos_OCRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getListar_Articulos_OCRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        public getListar_Articulos_OCRequestBody()
        {
        }
        
        public getListar_Articulos_OCRequestBody(int id_oc, int id_operario)
        {
            this.id_oc = id_oc;
            this.id_operario = id_operario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getListar_Articulos_OCResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getListar_Articulos_OCResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getListar_Articulos_OCResponseBody Body;
        
        public getListar_Articulos_OCResponse()
        {
        }
        
        public getListar_Articulos_OCResponse(ServiceReference1.getListar_Articulos_OCResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getListar_Articulos_OCResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ArrayOfEArticulo getListar_Articulos_OCResult;
        
        public getListar_Articulos_OCResponseBody()
        {
        }
        
        public getListar_Articulos_OCResponseBody(ServiceReference1.ArrayOfEArticulo getListar_Articulos_OCResult)
        {
            this.getListar_Articulos_OCResult = getListar_Articulos_OCResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getListar_Articulos_OC_EliminarRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getListar_Articulos_OC_Eliminar", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getListar_Articulos_OC_EliminarRequestBody Body;
        
        public getListar_Articulos_OC_EliminarRequest()
        {
        }
        
        public getListar_Articulos_OC_EliminarRequest(ServiceReference1.getListar_Articulos_OC_EliminarRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getListar_Articulos_OC_EliminarRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        public getListar_Articulos_OC_EliminarRequestBody()
        {
        }
        
        public getListar_Articulos_OC_EliminarRequestBody(int id_oc, int id_operario)
        {
            this.id_oc = id_oc;
            this.id_operario = id_operario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getListar_Articulos_OC_EliminarResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getListar_Articulos_OC_EliminarResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getListar_Articulos_OC_EliminarResponseBody Body;
        
        public getListar_Articulos_OC_EliminarResponse()
        {
        }
        
        public getListar_Articulos_OC_EliminarResponse(ServiceReference1.getListar_Articulos_OC_EliminarResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getListar_Articulos_OC_EliminarResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ArrayOfEArticulo getListar_Articulos_OC_EliminarResult;
        
        public getListar_Articulos_OC_EliminarResponseBody()
        {
        }
        
        public getListar_Articulos_OC_EliminarResponseBody(ServiceReference1.ArrayOfEArticulo getListar_Articulos_OC_EliminarResult)
        {
            this.getListar_Articulos_OC_EliminarResult = getListar_Articulos_OC_EliminarResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getListar_Detalle_ArticuloRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getListar_Detalle_Articulo", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getListar_Detalle_ArticuloRequestBody Body;
        
        public getListar_Detalle_ArticuloRequest()
        {
        }
        
        public getListar_Detalle_ArticuloRequest(ServiceReference1.getListar_Detalle_ArticuloRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getListar_Detalle_ArticuloRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_detalle_oc;
        
        public getListar_Detalle_ArticuloRequestBody()
        {
        }
        
        public getListar_Detalle_ArticuloRequestBody(int id_oc, int id_detalle_oc)
        {
            this.id_oc = id_oc;
            this.id_detalle_oc = id_detalle_oc;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getListar_Detalle_ArticuloResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getListar_Detalle_ArticuloResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getListar_Detalle_ArticuloResponseBody Body;
        
        public getListar_Detalle_ArticuloResponse()
        {
        }
        
        public getListar_Detalle_ArticuloResponse(ServiceReference1.getListar_Detalle_ArticuloResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getListar_Detalle_ArticuloResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eDetalleArticulo getListar_Detalle_ArticuloResult;
        
        public getListar_Detalle_ArticuloResponseBody()
        {
        }
        
        public getListar_Detalle_ArticuloResponseBody(ServiceReference1.eDetalleArticulo getListar_Detalle_ArticuloResult)
        {
            this.getListar_Detalle_ArticuloResult = getListar_Detalle_ArticuloResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getGrabar_Picking_ArticuloRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getGrabar_Picking_Articulo", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getGrabar_Picking_ArticuloRequestBody Body;
        
        public getGrabar_Picking_ArticuloRequest()
        {
        }
        
        public getGrabar_Picking_ArticuloRequest(ServiceReference1.getGrabar_Picking_ArticuloRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getGrabar_Picking_ArticuloRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string ean;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public double cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string id_Pesable;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int ID_OPERARIO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string COD_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string FECHAHORA_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=7)]
        public int ID_ACTIVIDAD;
        
        public getGrabar_Picking_ArticuloRequestBody()
        {
        }
        
        public getGrabar_Picking_ArticuloRequestBody(int id_oc, string ean, double cantidad, string id_Pesable, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            this.id_oc = id_oc;
            this.ean = ean;
            this.cantidad = cantidad;
            this.id_Pesable = id_Pesable;
            this.ID_OPERARIO = ID_OPERARIO;
            this.COD_EQUIPO = COD_EQUIPO;
            this.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            this.ID_ACTIVIDAD = ID_ACTIVIDAD;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getGrabar_Picking_ArticuloResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getGrabar_Picking_ArticuloResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getGrabar_Picking_ArticuloResponseBody Body;
        
        public getGrabar_Picking_ArticuloResponse()
        {
        }
        
        public getGrabar_Picking_ArticuloResponse(ServiceReference1.getGrabar_Picking_ArticuloResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getGrabar_Picking_ArticuloResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eArticulo getGrabar_Picking_ArticuloResult;
        
        public getGrabar_Picking_ArticuloResponseBody()
        {
        }
        
        public getGrabar_Picking_ArticuloResponseBody(ServiceReference1.eArticulo getGrabar_Picking_ArticuloResult)
        {
            this.getGrabar_Picking_ArticuloResult = getGrabar_Picking_ArticuloResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Finalizar_PickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Finalizar_Picking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Finalizar_PickingRequestBody Body;
        
        public Finalizar_PickingRequest()
        {
        }
        
        public Finalizar_PickingRequest(ServiceReference1.Finalizar_PickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Finalizar_PickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_tienda;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string COD_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string FECHAHORA_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public int ID_ACTIVIDAD;
        
        public Finalizar_PickingRequestBody()
        {
        }
        
        public Finalizar_PickingRequestBody(int id_oc, int id_tienda, int id_operario, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            this.id_oc = id_oc;
            this.id_tienda = id_tienda;
            this.id_operario = id_operario;
            this.COD_EQUIPO = COD_EQUIPO;
            this.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            this.ID_ACTIVIDAD = ID_ACTIVIDAD;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Finalizar_PickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Finalizar_PickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Finalizar_PickingResponseBody Body;
        
        public Finalizar_PickingResponse()
        {
        }
        
        public Finalizar_PickingResponse(ServiceReference1.Finalizar_PickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Finalizar_PickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int Finalizar_PickingResult;
        
        public Finalizar_PickingResponseBody()
        {
        }
        
        public Finalizar_PickingResponseBody(int Finalizar_PickingResult)
        {
            this.Finalizar_PickingResult = Finalizar_PickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Finalizar_Picking_FoodRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Finalizar_Picking_Food", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Finalizar_Picking_FoodRequestBody Body;
        
        public Finalizar_Picking_FoodRequest()
        {
        }
        
        public Finalizar_Picking_FoodRequest(ServiceReference1.Finalizar_Picking_FoodRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Finalizar_Picking_FoodRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_tienda;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string COD_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string FECHAHORA_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public int ID_ACTIVIDAD;
        
        public Finalizar_Picking_FoodRequestBody()
        {
        }
        
        public Finalizar_Picking_FoodRequestBody(int id_oc, int id_tienda, int id_operario, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            this.id_oc = id_oc;
            this.id_tienda = id_tienda;
            this.id_operario = id_operario;
            this.COD_EQUIPO = COD_EQUIPO;
            this.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            this.ID_ACTIVIDAD = ID_ACTIVIDAD;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Finalizar_Picking_FoodResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Finalizar_Picking_FoodResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Finalizar_Picking_FoodResponseBody Body;
        
        public Finalizar_Picking_FoodResponse()
        {
        }
        
        public Finalizar_Picking_FoodResponse(ServiceReference1.Finalizar_Picking_FoodResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Finalizar_Picking_FoodResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int Finalizar_Picking_FoodResult;
        
        public Finalizar_Picking_FoodResponseBody()
        {
        }
        
        public Finalizar_Picking_FoodResponseBody(int Finalizar_Picking_FoodResult)
        {
            this.Finalizar_Picking_FoodResult = Finalizar_Picking_FoodResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Faltante_PickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Faltante_Picking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Faltante_PickingRequestBody Body;
        
        public Faltante_PickingRequest()
        {
        }
        
        public Faltante_PickingRequest(ServiceReference1.Faltante_PickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Faltante_PickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_detalle_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public double cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int ID_OPERARIO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string COD_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string FECHAHORA_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public int ID_ACTIVIDAD;
        
        public Faltante_PickingRequestBody()
        {
        }
        
        public Faltante_PickingRequestBody(int id_oc, int id_detalle_oc, double cantidad, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            this.id_oc = id_oc;
            this.id_detalle_oc = id_detalle_oc;
            this.cantidad = cantidad;
            this.ID_OPERARIO = ID_OPERARIO;
            this.COD_EQUIPO = COD_EQUIPO;
            this.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            this.ID_ACTIVIDAD = ID_ACTIVIDAD;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Faltante_PickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Faltante_PickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Faltante_PickingResponseBody Body;
        
        public Faltante_PickingResponse()
        {
        }
        
        public Faltante_PickingResponse(ServiceReference1.Faltante_PickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Faltante_PickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int Faltante_PickingResult;
        
        public Faltante_PickingResponseBody()
        {
        }
        
        public Faltante_PickingResponseBody(int Faltante_PickingResult)
        {
            this.Faltante_PickingResult = Faltante_PickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Sustituto_Detalle_ArticuloRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Sustituto_Detalle_Articulo", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Sustituto_Detalle_ArticuloRequestBody Body;
        
        public Sustituto_Detalle_ArticuloRequest()
        {
        }
        
        public Sustituto_Detalle_ArticuloRequest(ServiceReference1.Sustituto_Detalle_ArticuloRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Sustituto_Detalle_ArticuloRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string sku;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string ean;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public double cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int tienda;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string id_pesable;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public int ID_OPERARIO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string COD_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=8)]
        public string FECHAHORA_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=9)]
        public int ID_ACTIVIDAD;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=10)]
        public string mensajeerror;
        
        public Sustituto_Detalle_ArticuloRequestBody()
        {
        }
        
        public Sustituto_Detalle_ArticuloRequestBody(int id_oc, string sku, string ean, double cantidad, int tienda, string id_pesable, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD, string mensajeerror)
        {
            this.id_oc = id_oc;
            this.sku = sku;
            this.ean = ean;
            this.cantidad = cantidad;
            this.tienda = tienda;
            this.id_pesable = id_pesable;
            this.ID_OPERARIO = ID_OPERARIO;
            this.COD_EQUIPO = COD_EQUIPO;
            this.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            this.ID_ACTIVIDAD = ID_ACTIVIDAD;
            this.mensajeerror = mensajeerror;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Sustituto_Detalle_ArticuloResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Sustituto_Detalle_ArticuloResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Sustituto_Detalle_ArticuloResponseBody Body;
        
        public Sustituto_Detalle_ArticuloResponse()
        {
        }
        
        public Sustituto_Detalle_ArticuloResponse(ServiceReference1.Sustituto_Detalle_ArticuloResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Sustituto_Detalle_ArticuloResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eDetalleArticulo Sustituto_Detalle_ArticuloResult;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string mensajeerror;
        
        public Sustituto_Detalle_ArticuloResponseBody()
        {
        }
        
        public Sustituto_Detalle_ArticuloResponseBody(ServiceReference1.eDetalleArticulo Sustituto_Detalle_ArticuloResult, string mensajeerror)
        {
            this.Sustituto_Detalle_ArticuloResult = Sustituto_Detalle_ArticuloResult;
            this.mensajeerror = mensajeerror;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Asignar_JabasRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Asignar_Jabas", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Asignar_JabasRequestBody Body;
        
        public Asignar_JabasRequest()
        {
        }
        
        public Asignar_JabasRequest(ServiceReference1.Asignar_JabasRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Asignar_JabasRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string id_jaba;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int cantidad;
        
        public Asignar_JabasRequestBody()
        {
        }
        
        public Asignar_JabasRequestBody(int id_oc, string id_jaba, int cantidad)
        {
            this.id_oc = id_oc;
            this.id_jaba = id_jaba;
            this.cantidad = cantidad;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Asignar_JabasResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Asignar_JabasResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Asignar_JabasResponseBody Body;
        
        public Asignar_JabasResponse()
        {
        }
        
        public Asignar_JabasResponse(ServiceReference1.Asignar_JabasResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Asignar_JabasResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int Asignar_JabasResult;
        
        public Asignar_JabasResponseBody()
        {
        }
        
        public Asignar_JabasResponseBody(int Asignar_JabasResult)
        {
            this.Asignar_JabasResult = Asignar_JabasResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Listar_JabasRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Listar_Jabas", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Listar_JabasRequestBody Body;
        
        public Listar_JabasRequest()
        {
        }
        
        public Listar_JabasRequest(ServiceReference1.Listar_JabasRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Listar_JabasRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        public Listar_JabasRequestBody()
        {
        }
        
        public Listar_JabasRequestBody(int id_oc)
        {
            this.id_oc = id_oc;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Listar_JabasResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Listar_JabasResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Listar_JabasResponseBody Body;
        
        public Listar_JabasResponse()
        {
        }
        
        public Listar_JabasResponse(ServiceReference1.Listar_JabasResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Listar_JabasResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ArrayOfString Listar_JabasResult;
        
        public Listar_JabasResponseBody()
        {
        }
        
        public Listar_JabasResponseBody(ServiceReference1.ArrayOfString Listar_JabasResult)
        {
            this.Listar_JabasResult = Listar_JabasResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Eliminar_PickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Eliminar_Picking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Eliminar_PickingRequestBody Body;
        
        public Eliminar_PickingRequest()
        {
        }
        
        public Eliminar_PickingRequest(ServiceReference1.Eliminar_PickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Eliminar_PickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string sku;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public double cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_criterio;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int ID_OPERARIO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string COD_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string FECHAHORA_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=7)]
        public int ID_ACTIVIDAD;
        
        public Eliminar_PickingRequestBody()
        {
        }
        
        public Eliminar_PickingRequestBody(int id_oc, string sku, double cantidad, int id_criterio, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            this.id_oc = id_oc;
            this.sku = sku;
            this.cantidad = cantidad;
            this.id_criterio = id_criterio;
            this.ID_OPERARIO = ID_OPERARIO;
            this.COD_EQUIPO = COD_EQUIPO;
            this.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            this.ID_ACTIVIDAD = ID_ACTIVIDAD;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Eliminar_PickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Eliminar_PickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Eliminar_PickingResponseBody Body;
        
        public Eliminar_PickingResponse()
        {
        }
        
        public Eliminar_PickingResponse(ServiceReference1.Eliminar_PickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Eliminar_PickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int Eliminar_PickingResult;
        
        public Eliminar_PickingResponseBody()
        {
        }
        
        public Eliminar_PickingResponseBody(int Eliminar_PickingResult)
        {
            this.Eliminar_PickingResult = Eliminar_PickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Eliminar_Picking_TotalRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Eliminar_Picking_Total", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Eliminar_Picking_TotalRequestBody Body;
        
        public Eliminar_Picking_TotalRequest()
        {
        }
        
        public Eliminar_Picking_TotalRequest(ServiceReference1.Eliminar_Picking_TotalRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Eliminar_Picking_TotalRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string sku;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_criterio;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int ID_OPERARIO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string COD_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string FECHAHORA_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public int ID_ACTIVIDAD;
        
        public Eliminar_Picking_TotalRequestBody()
        {
        }
        
        public Eliminar_Picking_TotalRequestBody(int id_oc, string sku, int id_criterio, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            this.id_oc = id_oc;
            this.sku = sku;
            this.id_criterio = id_criterio;
            this.ID_OPERARIO = ID_OPERARIO;
            this.COD_EQUIPO = COD_EQUIPO;
            this.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            this.ID_ACTIVIDAD = ID_ACTIVIDAD;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Eliminar_Picking_TotalResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Eliminar_Picking_TotalResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Eliminar_Picking_TotalResponseBody Body;
        
        public Eliminar_Picking_TotalResponse()
        {
        }
        
        public Eliminar_Picking_TotalResponse(ServiceReference1.Eliminar_Picking_TotalResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Eliminar_Picking_TotalResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int Eliminar_Picking_TotalResult;
        
        public Eliminar_Picking_TotalResponseBody()
        {
        }
        
        public Eliminar_Picking_TotalResponseBody(int Eliminar_Picking_TotalResult)
        {
            this.Eliminar_Picking_TotalResult = Eliminar_Picking_TotalResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Iniciar_Cerrar_Actividad_OCRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Iniciar_Cerrar_Actividad_OC", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Iniciar_Cerrar_Actividad_OCRequestBody Body;
        
        public Iniciar_Cerrar_Actividad_OCRequest()
        {
        }
        
        public Iniciar_Cerrar_Actividad_OCRequest(ServiceReference1.Iniciar_Cerrar_Actividad_OCRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Iniciar_Cerrar_Actividad_OCRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string COD_EQUIPO;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string tipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int ID_ACTV;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public int ID_ACTIVIDAD;
        
        public Iniciar_Cerrar_Actividad_OCRequestBody()
        {
        }
        
        public Iniciar_Cerrar_Actividad_OCRequestBody(int id_oc, int id_operario, string COD_EQUIPO, string tipo, int ID_ACTV, int ID_ACTIVIDAD)
        {
            this.id_oc = id_oc;
            this.id_operario = id_operario;
            this.COD_EQUIPO = COD_EQUIPO;
            this.tipo = tipo;
            this.ID_ACTV = ID_ACTV;
            this.ID_ACTIVIDAD = ID_ACTIVIDAD;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Iniciar_Cerrar_Actividad_OCResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Iniciar_Cerrar_Actividad_OCResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.Iniciar_Cerrar_Actividad_OCResponseBody Body;
        
        public Iniciar_Cerrar_Actividad_OCResponse()
        {
        }
        
        public Iniciar_Cerrar_Actividad_OCResponse(ServiceReference1.Iniciar_Cerrar_Actividad_OCResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class Iniciar_Cerrar_Actividad_OCResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int Iniciar_Cerrar_Actividad_OCResult;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int ID_ACTIVIDAD;
        
        public Iniciar_Cerrar_Actividad_OCResponseBody()
        {
        }
        
        public Iniciar_Cerrar_Actividad_OCResponseBody(int Iniciar_Cerrar_Actividad_OCResult, int ID_ACTIVIDAD)
        {
            this.Iniciar_Cerrar_Actividad_OCResult = Iniciar_Cerrar_Actividad_OCResult;
            this.ID_ACTIVIDAD = ID_ACTIVIDAD;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getEstado_Orden_PosVirtualRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getEstado_Orden_PosVirtual", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getEstado_Orden_PosVirtualRequestBody Body;
        
        public getEstado_Orden_PosVirtualRequest()
        {
        }
        
        public getEstado_Orden_PosVirtualRequest(ServiceReference1.getEstado_Orden_PosVirtualRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getEstado_Orden_PosVirtualRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string numeroOrden;
        
        public getEstado_Orden_PosVirtualRequestBody()
        {
        }
        
        public getEstado_Orden_PosVirtualRequestBody(string numeroOrden)
        {
            this.numeroOrden = numeroOrden;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getEstado_Orden_PosVirtualResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getEstado_Orden_PosVirtualResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getEstado_Orden_PosVirtualResponseBody Body;
        
        public getEstado_Orden_PosVirtualResponse()
        {
        }
        
        public getEstado_Orden_PosVirtualResponse(ServiceReference1.getEstado_Orden_PosVirtualResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getEstado_Orden_PosVirtualResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string getEstado_Orden_PosVirtualResult;
        
        public getEstado_Orden_PosVirtualResponseBody()
        {
        }
        
        public getEstado_Orden_PosVirtualResponseBody(string getEstado_Orden_PosVirtualResult)
        {
            this.getEstado_Orden_PosVirtualResult = getEstado_Orden_PosVirtualResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getColaImpresionRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getColaImpresion", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getColaImpresionRequestBody Body;
        
        public getColaImpresionRequest()
        {
        }
        
        public getColaImpresionRequest(ServiceReference1.getColaImpresionRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getColaImpresionRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int flag;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int codigo_local;
        
        public getColaImpresionRequestBody()
        {
        }
        
        public getColaImpresionRequestBody(int flag, int id_oc, int codigo_local)
        {
            this.flag = flag;
            this.id_oc = id_oc;
            this.codigo_local = codigo_local;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getColaImpresionResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getColaImpresionResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getColaImpresionResponseBody Body;
        
        public getColaImpresionResponse()
        {
        }
        
        public getColaImpresionResponse(ServiceReference1.getColaImpresionResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getColaImpresionResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ArrayOfEColaImpresion getColaImpresionResult;
        
        public getColaImpresionResponseBody()
        {
        }
        
        public getColaImpresionResponseBody(ServiceReference1.ArrayOfEColaImpresion getColaImpresionResult)
        {
            this.getColaImpresionResult = getColaImpresionResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getprinterStatusRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getprinterStatus", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getprinterStatusRequestBody Body;
        
        public getprinterStatusRequest()
        {
        }
        
        public getprinterStatusRequest(ServiceReference1.getprinterStatusRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getprinterStatusRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string ip;
        
        public getprinterStatusRequestBody()
        {
        }
        
        public getprinterStatusRequestBody(string ip)
        {
            this.ip = ip;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getprinterStatusResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getprinterStatusResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getprinterStatusResponseBody Body;
        
        public getprinterStatusResponse()
        {
        }
        
        public getprinterStatusResponse(ServiceReference1.getprinterStatusResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getprinterStatusResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string getprinterStatusResult;
        
        public getprinterStatusResponseBody()
        {
        }
        
        public getprinterStatusResponseBody(string getprinterStatusResult)
        {
            this.getprinterStatusResult = getprinterStatusResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getEtiquetaOCRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getEtiquetaOC", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getEtiquetaOCRequestBody Body;
        
        public getEtiquetaOCRequest()
        {
        }
        
        public getEtiquetaOCRequest(ServiceReference1.getEtiquetaOCRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getEtiquetaOCRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        public getEtiquetaOCRequestBody()
        {
        }
        
        public getEtiquetaOCRequestBody(int id_oc)
        {
            this.id_oc = id_oc;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getEtiquetaOCResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getEtiquetaOCResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getEtiquetaOCResponseBody Body;
        
        public getEtiquetaOCResponse()
        {
        }
        
        public getEtiquetaOCResponse(ServiceReference1.getEtiquetaOCResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getEtiquetaOCResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eEtiquetaOC getEtiquetaOCResult;
        
        public getEtiquetaOCResponseBody()
        {
        }
        
        public getEtiquetaOCResponseBody(ServiceReference1.eEtiquetaOC getEtiquetaOCResult)
        {
            this.getEtiquetaOCResult = getEtiquetaOCResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaCONTENEDORRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaCONTENEDOR", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaCONTENEDORRequestBody Body;
        
        public printEtiquetaCONTENEDORRequest()
        {
        }
        
        public printEtiquetaCONTENEDORRequest(ServiceReference1.printEtiquetaCONTENEDORRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaCONTENEDORRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string lstContenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string ip;
        
        public printEtiquetaCONTENEDORRequestBody()
        {
        }
        
        public printEtiquetaCONTENEDORRequestBody(string lstContenedor, string ip)
        {
            this.lstContenedor = lstContenedor;
            this.ip = ip;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaCONTENEDORResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaCONTENEDORResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaCONTENEDORResponseBody Body;
        
        public printEtiquetaCONTENEDORResponse()
        {
        }
        
        public printEtiquetaCONTENEDORResponse(ServiceReference1.printEtiquetaCONTENEDORResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaCONTENEDORResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string printEtiquetaCONTENEDORResult;
        
        public printEtiquetaCONTENEDORResponseBody()
        {
        }
        
        public printEtiquetaCONTENEDORResponseBody(string printEtiquetaCONTENEDORResult)
        {
            this.printEtiquetaCONTENEDORResult = printEtiquetaCONTENEDORResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaUBICACIONRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaUBICACION", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaUBICACIONRequestBody Body;
        
        public printEtiquetaUBICACIONRequest()
        {
        }
        
        public printEtiquetaUBICACIONRequest(ServiceReference1.printEtiquetaUBICACIONRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaUBICACIONRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string ubicacion;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string ip;
        
        public printEtiquetaUBICACIONRequestBody()
        {
        }
        
        public printEtiquetaUBICACIONRequestBody(string ubicacion, string ip)
        {
            this.ubicacion = ubicacion;
            this.ip = ip;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaUBICACIONResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaUBICACIONResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaUBICACIONResponseBody Body;
        
        public printEtiquetaUBICACIONResponse()
        {
        }
        
        public printEtiquetaUBICACIONResponse(ServiceReference1.printEtiquetaUBICACIONResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaUBICACIONResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string printEtiquetaUBICACIONResult;
        
        public printEtiquetaUBICACIONResponseBody()
        {
        }
        
        public printEtiquetaUBICACIONResponseBody(string printEtiquetaUBICACIONResult)
        {
            this.printEtiquetaUBICACIONResult = printEtiquetaUBICACIONResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaOCRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaOC", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaOCRequestBody Body;
        
        public printEtiquetaOCRequest()
        {
        }
        
        public printEtiquetaOCRequest(ServiceReference1.printEtiquetaOCRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaOCRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string ip;
        
        public printEtiquetaOCRequestBody()
        {
        }
        
        public printEtiquetaOCRequestBody(int id_oc, string ip)
        {
            this.id_oc = id_oc;
            this.ip = ip;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaOCResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaOCResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaOCResponseBody Body;
        
        public printEtiquetaOCResponse()
        {
        }
        
        public printEtiquetaOCResponse(ServiceReference1.printEtiquetaOCResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaOCResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string printEtiquetaOCResult;
        
        public printEtiquetaOCResponseBody()
        {
        }
        
        public printEtiquetaOCResponseBody(string printEtiquetaOCResult)
        {
            this.printEtiquetaOCResult = printEtiquetaOCResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaLOTERequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaLOTE", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaLOTERequestBody Body;
        
        public printEtiquetaLOTERequest()
        {
        }
        
        public printEtiquetaLOTERequest(ServiceReference1.printEtiquetaLOTERequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaLOTERequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int flag;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string codigo_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string codigo_repicking;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string ip;
        
        public printEtiquetaLOTERequestBody()
        {
        }
        
        public printEtiquetaLOTERequestBody(int flag, int id_lote, string codigo_contenedor, string codigo_repicking, string ip)
        {
            this.flag = flag;
            this.id_lote = id_lote;
            this.codigo_contenedor = codigo_contenedor;
            this.codigo_repicking = codigo_repicking;
            this.ip = ip;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaLOTEResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaLOTEResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaLOTEResponseBody Body;
        
        public printEtiquetaLOTEResponse()
        {
        }
        
        public printEtiquetaLOTEResponse(ServiceReference1.printEtiquetaLOTEResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaLOTEResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string printEtiquetaLOTEResult;
        
        public printEtiquetaLOTEResponseBody()
        {
        }
        
        public printEtiquetaLOTEResponseBody(string printEtiquetaLOTEResult)
        {
            this.printEtiquetaLOTEResult = printEtiquetaLOTEResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaOCLOTERequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaOCLOTE", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaOCLOTERequestBody Body;
        
        public printEtiquetaOCLOTERequest()
        {
        }
        
        public printEtiquetaOCLOTERequest(ServiceReference1.printEtiquetaOCLOTERequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaOCLOTERequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int flag;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string ip;
        
        public printEtiquetaOCLOTERequestBody()
        {
        }
        
        public printEtiquetaOCLOTERequestBody(int flag, int id_oc, string ip)
        {
            this.flag = flag;
            this.id_oc = id_oc;
            this.ip = ip;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class printEtiquetaOCLOTEResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="printEtiquetaOCLOTEResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.printEtiquetaOCLOTEResponseBody Body;
        
        public printEtiquetaOCLOTEResponse()
        {
        }
        
        public printEtiquetaOCLOTEResponse(ServiceReference1.printEtiquetaOCLOTEResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class printEtiquetaOCLOTEResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string printEtiquetaOCLOTEResult;
        
        public printEtiquetaOCLOTEResponseBody()
        {
        }
        
        public printEtiquetaOCLOTEResponseBody(string printEtiquetaOCLOTEResult)
        {
            this.printEtiquetaOCLOTEResult = printEtiquetaOCLOTEResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaVersionRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaVersion", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaVersionRequestBody Body;
        
        public validaVersionRequest()
        {
        }
        
        public validaVersionRequest(ServiceReference1.validaVersionRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaVersionRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string version;
        
        public validaVersionRequestBody()
        {
        }
        
        public validaVersionRequestBody(string version)
        {
            this.version = version;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaVersionResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaVersionResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaVersionResponseBody Body;
        
        public validaVersionResponse()
        {
        }
        
        public validaVersionResponse(ServiceReference1.validaVersionResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaVersionResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP validaVersionResult;
        
        public validaVersionResponseBody()
        {
        }
        
        public validaVersionResponseBody(ServiceReference1.eErrorMP validaVersionResult)
        {
            this.validaVersionResult = validaVersionResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaEquipoRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaEquipo", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaEquipoRequestBody Body;
        
        public validaEquipoRequest()
        {
        }
        
        public validaEquipoRequest(ServiceReference1.validaEquipoRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaEquipoRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int flag;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public validaEquipoRequestBody()
        {
        }
        
        public validaEquipoRequestBody(int flag, int id_operario, string id_equipo)
        {
            this.flag = flag;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaEquipoResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaEquipoResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaEquipoResponseBody Body;
        
        public validaEquipoResponse()
        {
        }
        
        public validaEquipoResponse(ServiceReference1.validaEquipoResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaEquipoResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP validaEquipoResult;
        
        public validaEquipoResponseBody()
        {
        }
        
        public validaEquipoResponseBody(ServiceReference1.eErrorMP validaEquipoResult)
        {
            this.validaEquipoResult = validaEquipoResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class actulizarIdEquipoRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="actulizarIdEquipo", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.actulizarIdEquipoRequestBody Body;
        
        public actulizarIdEquipoRequest()
        {
        }
        
        public actulizarIdEquipoRequest(ServiceReference1.actulizarIdEquipoRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class actulizarIdEquipoRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string id_equipo;
        
        public actulizarIdEquipoRequestBody()
        {
        }
        
        public actulizarIdEquipoRequestBody(int id_operario, string id_equipo)
        {
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class actulizarIdEquipoResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="actulizarIdEquipoResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.actulizarIdEquipoResponseBody Body;
        
        public actulizarIdEquipoResponse()
        {
        }
        
        public actulizarIdEquipoResponse(ServiceReference1.actulizarIdEquipoResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class actulizarIdEquipoResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP actulizarIdEquipoResult;
        
        public actulizarIdEquipoResponseBody()
        {
        }
        
        public actulizarIdEquipoResponseBody(ServiceReference1.eErrorMP actulizarIdEquipoResult)
        {
            this.actulizarIdEquipoResult = actulizarIdEquipoResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getAsignarLoteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getAsignarLote", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getAsignarLoteRequestBody Body;
        
        public getAsignarLoteRequest()
        {
        }
        
        public getAsignarLoteRequest(ServiceReference1.getAsignarLoteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getAsignarLoteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_sucursal;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string id_equipo;
        
        public getAsignarLoteRequestBody()
        {
        }
        
        public getAsignarLoteRequestBody(int id_lote, int id_operario, int id_sucursal, string id_equipo)
        {
            this.id_lote = id_lote;
            this.id_operario = id_operario;
            this.id_sucursal = id_sucursal;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getAsignarLoteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getAsignarLoteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getAsignarLoteResponseBody Body;
        
        public getAsignarLoteResponse()
        {
        }
        
        public getAsignarLoteResponse(ServiceReference1.getAsignarLoteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getAsignarLoteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eLote getAsignarLoteResult;
        
        public getAsignarLoteResponseBody()
        {
        }
        
        public getAsignarLoteResponseBody(ServiceReference1.eLote getAsignarLoteResult)
        {
            this.getAsignarLoteResult = getAsignarLoteResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getContenedorSugeridoTextRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getContenedorSugeridoText", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getContenedorSugeridoTextRequestBody Body;
        
        public getContenedorSugeridoTextRequest()
        {
        }
        
        public getContenedorSugeridoTextRequest(ServiceReference1.getContenedorSugeridoTextRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getContenedorSugeridoTextRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        public getContenedorSugeridoTextRequestBody()
        {
        }
        
        public getContenedorSugeridoTextRequestBody(int id_lote)
        {
            this.id_lote = id_lote;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getContenedorSugeridoTextResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getContenedorSugeridoTextResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getContenedorSugeridoTextResponseBody Body;
        
        public getContenedorSugeridoTextResponse()
        {
        }
        
        public getContenedorSugeridoTextResponse(ServiceReference1.getContenedorSugeridoTextResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getContenedorSugeridoTextResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string getContenedorSugeridoTextResult;
        
        public getContenedorSugeridoTextResponseBody()
        {
        }
        
        public getContenedorSugeridoTextResponseBody(string getContenedorSugeridoTextResult)
        {
            this.getContenedorSugeridoTextResult = getContenedorSugeridoTextResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getContenedorSugeridoPasoRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getContenedorSugeridoPaso", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getContenedorSugeridoPasoRequestBody Body;
        
        public getContenedorSugeridoPasoRequest()
        {
        }
        
        public getContenedorSugeridoPasoRequest(ServiceReference1.getContenedorSugeridoPasoRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getContenedorSugeridoPasoRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string sku;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string ean;
        
        public getContenedorSugeridoPasoRequestBody()
        {
        }
        
        public getContenedorSugeridoPasoRequestBody(int id_lote, int oc, string sku, string ean)
        {
            this.id_lote = id_lote;
            this.oc = oc;
            this.sku = sku;
            this.ean = ean;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getContenedorSugeridoPasoResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getContenedorSugeridoPasoResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getContenedorSugeridoPasoResponseBody Body;
        
        public getContenedorSugeridoPasoResponse()
        {
        }
        
        public getContenedorSugeridoPasoResponse(ServiceReference1.getContenedorSugeridoPasoResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getContenedorSugeridoPasoResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string getContenedorSugeridoPasoResult;
        
        public getContenedorSugeridoPasoResponseBody()
        {
        }
        
        public getContenedorSugeridoPasoResponseBody(string getContenedorSugeridoPasoResult)
        {
            this.getContenedorSugeridoPasoResult = getContenedorSugeridoPasoResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getCantPedXOperarioRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getCantPedXOperario", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getCantPedXOperarioRequestBody Body;
        
        public getCantPedXOperarioRequest()
        {
        }
        
        public getCantPedXOperarioRequest(ServiceReference1.getCantPedXOperarioRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getCantPedXOperarioRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_operario;
        
        public getCantPedXOperarioRequestBody()
        {
        }
        
        public getCantPedXOperarioRequestBody(int id_operario)
        {
            this.id_operario = id_operario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getCantPedXOperarioResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getCantPedXOperarioResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getCantPedXOperarioResponseBody Body;
        
        public getCantPedXOperarioResponse()
        {
        }
        
        public getCantPedXOperarioResponse(ServiceReference1.getCantPedXOperarioResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getCantPedXOperarioResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eCantPedido getCantPedXOperarioResult;
        
        public getCantPedXOperarioResponseBody()
        {
        }
        
        public getCantPedXOperarioResponseBody(ServiceReference1.eCantPedido getCantPedXOperarioResult)
        {
            this.getCantPedXOperarioResult = getCantPedXOperarioResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class inicioMultipickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="inicioMultipicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.inicioMultipickingRequestBody Body;
        
        public inicioMultipickingRequest()
        {
        }
        
        public inicioMultipickingRequest(ServiceReference1.inicioMultipickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class inicioMultipickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int flag;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int idLote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int idOperario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string idEquipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int criterio;
        
        public inicioMultipickingRequestBody()
        {
        }
        
        public inicioMultipickingRequestBody(int flag, int idLote, int idOperario, string idEquipo, int criterio)
        {
            this.flag = flag;
            this.idLote = idLote;
            this.idOperario = idOperario;
            this.idEquipo = idEquipo;
            this.criterio = criterio;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class inicioMultipickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="inicioMultipickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.inicioMultipickingResponseBody Body;
        
        public inicioMultipickingResponse()
        {
        }
        
        public inicioMultipickingResponse(ServiceReference1.inicioMultipickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class inicioMultipickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP inicioMultipickingResult;
        
        public inicioMultipickingResponseBody()
        {
        }
        
        public inicioMultipickingResponseBody(ServiceReference1.eErrorMP inicioMultipickingResult)
        {
            this.inicioMultipickingResult = inicioMultipickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateEstadoPreparacionLoteOCRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateEstadoPreparacionLoteOC", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.updateEstadoPreparacionLoteOCRequestBody Body;
        
        public updateEstadoPreparacionLoteOCRequest()
        {
        }
        
        public updateEstadoPreparacionLoteOCRequest(ServiceReference1.updateEstadoPreparacionLoteOCRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class updateEstadoPreparacionLoteOCRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idLote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int idOperario;
        
        public updateEstadoPreparacionLoteOCRequestBody()
        {
        }
        
        public updateEstadoPreparacionLoteOCRequestBody(int idLote, int idOperario)
        {
            this.idLote = idLote;
            this.idOperario = idOperario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateEstadoPreparacionLoteOCResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateEstadoPreparacionLoteOCResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.updateEstadoPreparacionLoteOCResponseBody Body;
        
        public updateEstadoPreparacionLoteOCResponse()
        {
        }
        
        public updateEstadoPreparacionLoteOCResponse(ServiceReference1.updateEstadoPreparacionLoteOCResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class updateEstadoPreparacionLoteOCResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP updateEstadoPreparacionLoteOCResult;
        
        public updateEstadoPreparacionLoteOCResponseBody()
        {
        }
        
        public updateEstadoPreparacionLoteOCResponseBody(ServiceReference1.eErrorMP updateEstadoPreparacionLoteOCResult)
        {
            this.updateEstadoPreparacionLoteOCResult = updateEstadoPreparacionLoteOCResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class actualizarFLG_SALTO_Detalle_OCRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="actualizarFLG_SALTO_Detalle_OC", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.actualizarFLG_SALTO_Detalle_OCRequestBody Body;
        
        public actualizarFLG_SALTO_Detalle_OCRequest()
        {
        }
        
        public actualizarFLG_SALTO_Detalle_OCRequest(ServiceReference1.actualizarFLG_SALTO_Detalle_OCRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class actualizarFLG_SALTO_Detalle_OCRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idLote;
        
        public actualizarFLG_SALTO_Detalle_OCRequestBody()
        {
        }
        
        public actualizarFLG_SALTO_Detalle_OCRequestBody(int idLote)
        {
            this.idLote = idLote;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class actualizarFLG_SALTO_Detalle_OCResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="actualizarFLG_SALTO_Detalle_OCResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.actualizarFLG_SALTO_Detalle_OCResponseBody Body;
        
        public actualizarFLG_SALTO_Detalle_OCResponse()
        {
        }
        
        public actualizarFLG_SALTO_Detalle_OCResponse(ServiceReference1.actualizarFLG_SALTO_Detalle_OCResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class actualizarFLG_SALTO_Detalle_OCResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP actualizarFLG_SALTO_Detalle_OCResult;
        
        public actualizarFLG_SALTO_Detalle_OCResponseBody()
        {
        }
        
        public actualizarFLG_SALTO_Detalle_OCResponseBody(ServiceReference1.eErrorMP actualizarFLG_SALTO_Detalle_OCResult)
        {
            this.actualizarFLG_SALTO_Detalle_OCResult = actualizarFLG_SALTO_Detalle_OCResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getOTContenedoresRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getOTContenedores", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getOTContenedoresRequestBody Body;
        
        public getOTContenedoresRequest()
        {
        }
        
        public getOTContenedoresRequest(ServiceReference1.getOTContenedoresRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getOTContenedoresRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string codigo_OT;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public getOTContenedoresRequestBody()
        {
        }
        
        public getOTContenedoresRequestBody(string codigo_OT, int id_operario, string id_equipo)
        {
            this.codigo_OT = codigo_OT;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getOTContenedoresResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getOTContenedoresResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getOTContenedoresResponseBody Body;
        
        public getOTContenedoresResponse()
        {
        }
        
        public getOTContenedoresResponse(ServiceReference1.getOTContenedoresResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getOTContenedoresResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eOTContenedores getOTContenedoresResult;
        
        public getOTContenedoresResponseBody()
        {
        }
        
        public getOTContenedoresResponseBody(ServiceReference1.eOTContenedores getOTContenedoresResult)
        {
            this.getOTContenedoresResult = getOTContenedoresResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getLotePickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getLotePicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getLotePickingRequestBody Body;
        
        public getLotePickingRequest()
        {
        }
        
        public getLotePickingRequest(ServiceReference1.getLotePickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getLotePickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idLote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public getLotePickingRequestBody()
        {
        }
        
        public getLotePickingRequestBody(int idLote, int id_operario, string id_equipo)
        {
            this.idLote = idLote;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getLotePickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getLotePickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getLotePickingResponseBody Body;
        
        public getLotePickingResponse()
        {
        }
        
        public getLotePickingResponse(ServiceReference1.getLotePickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getLotePickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ePickingLote getLotePickingResult;
        
        public getLotePickingResponseBody()
        {
        }
        
        public getLotePickingResponseBody(ServiceReference1.ePickingLote getLotePickingResult)
        {
            this.getLotePickingResult = getLotePickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getDetalleArticuloRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getDetalleArticulo", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getDetalleArticuloRequestBody Body;
        
        public getDetalleArticuloRequest()
        {
        }
        
        public getDetalleArticuloRequest(ServiceReference1.getDetalleArticuloRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getDetalleArticuloRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idLote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_detalle_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string sku;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public getDetalleArticuloRequestBody()
        {
        }
        
        public getDetalleArticuloRequestBody(int idLote, int id_detalle_oc, string sku, int id_operario, string id_equipo)
        {
            this.idLote = idLote;
            this.id_detalle_oc = id_detalle_oc;
            this.sku = sku;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getDetalleArticuloResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getDetalleArticuloResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getDetalleArticuloResponseBody Body;
        
        public getDetalleArticuloResponse()
        {
        }
        
        public getDetalleArticuloResponse(ServiceReference1.getDetalleArticuloResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getDetalleArticuloResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eDetalleArticuloMP getDetalleArticuloResult;
        
        public getDetalleArticuloResponseBody()
        {
        }
        
        public getDetalleArticuloResponseBody(ServiceReference1.eDetalleArticuloMP getDetalleArticuloResult)
        {
            this.getDetalleArticuloResult = getDetalleArticuloResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getClienteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getCliente", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getClienteRequestBody Body;
        
        public getClienteRequest()
        {
        }
        
        public getClienteRequest(ServiceReference1.getClienteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getClienteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public getClienteRequestBody()
        {
        }
        
        public getClienteRequestBody(int id_oc, int id_operario, string id_equipo)
        {
            this.id_oc = id_oc;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getClienteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getClienteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getClienteResponseBody Body;
        
        public getClienteResponse()
        {
        }
        
        public getClienteResponse(ServiceReference1.getClienteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getClienteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eCliente getClienteResult;
        
        public getClienteResponseBody()
        {
        }
        
        public getClienteResponseBody(ServiceReference1.eCliente getClienteResult)
        {
            this.getClienteResult = getClienteResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaSustitucionRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaSustitucion", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaSustitucionRequestBody Body;
        
        public validaSustitucionRequest()
        {
        }
        
        public validaSustitucionRequest(ServiceReference1.validaSustitucionRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaSustitucionRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string sku;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string ean;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public double cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int tienda;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string id_equipo;
        
        public validaSustitucionRequestBody()
        {
        }
        
        public validaSustitucionRequestBody(int id_oc, string sku, string ean, double cantidad, int tienda, int id_operario, string id_equipo)
        {
            this.id_oc = id_oc;
            this.sku = sku;
            this.ean = ean;
            this.cantidad = cantidad;
            this.tienda = tienda;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaSustitucionResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaSustitucionResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaSustitucionResponseBody Body;
        
        public validaSustitucionResponse()
        {
        }
        
        public validaSustitucionResponse(ServiceReference1.validaSustitucionResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaSustitucionResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eValidaSustitucion validaSustitucionResult;
        
        public validaSustitucionResponseBody()
        {
        }
        
        public validaSustitucionResponseBody(ServiceReference1.eValidaSustitucion validaSustitucionResult)
        {
            this.validaSustitucionResult = validaSustitucionResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaContenedorRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaContenedor", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaContenedorRequestBody Body;
        
        public validaContenedorRequest()
        {
        }
        
        public validaContenedorRequest(ServiceReference1.validaContenedorRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaContenedorRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_lotedetalle;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string cod_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_sucursal;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string id_equipo;
        
        public validaContenedorRequestBody()
        {
        }
        
        public validaContenedorRequestBody(int id_lote, int id_lotedetalle, string cod_contenedor, int id_sucursal, int id_operario, string id_equipo)
        {
            this.id_lote = id_lote;
            this.id_lotedetalle = id_lotedetalle;
            this.cod_contenedor = cod_contenedor;
            this.id_sucursal = id_sucursal;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaContenedorResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaContenedorResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaContenedorResponseBody Body;
        
        public validaContenedorResponse()
        {
        }
        
        public validaContenedorResponse(ServiceReference1.validaContenedorResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaContenedorResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eValidaContenedor validaContenedorResult;
        
        public validaContenedorResponseBody()
        {
        }
        
        public validaContenedorResponseBody(ServiceReference1.eValidaContenedor validaContenedorResult)
        {
            this.validaContenedorResult = validaContenedorResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaMultiPickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaMultiPicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaMultiPickingRequestBody Body;
        
        public validaMultiPickingRequest()
        {
        }
        
        public validaMultiPickingRequest(ServiceReference1.validaMultiPickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaMultiPickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string ean;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public decimal cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public validaMultiPickingRequestBody()
        {
        }
        
        public validaMultiPickingRequestBody(int id_oc, string ean, decimal cantidad, int id_operario, string id_equipo)
        {
            this.id_oc = id_oc;
            this.ean = ean;
            this.cantidad = cantidad;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaMultiPickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaMultiPickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaMultiPickingResponseBody Body;
        
        public validaMultiPickingResponse()
        {
        }
        
        public validaMultiPickingResponse(ServiceReference1.validaMultiPickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaMultiPickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eValidaArticuloMP validaMultiPickingResult;
        
        public validaMultiPickingResponseBody()
        {
        }
        
        public validaMultiPickingResponseBody(ServiceReference1.eValidaArticuloMP validaMultiPickingResult)
        {
            this.validaMultiPickingResult = validaMultiPickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarMultiPickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarMultiPicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarMultiPickingRequestBody Body;
        
        public grabarMultiPickingRequest()
        {
        }
        
        public grabarMultiPickingRequest(ServiceReference1.grabarMultiPickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarMultiPickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string ean;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public decimal cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string fechahora_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public int id_actividad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=7)]
        public int idlotedetalle;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=8)]
        public int idcontenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=9)]
        public int posicion;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=10)]
        public string nom_operario;
        
        public grabarMultiPickingRequestBody()
        {
        }
        
        public grabarMultiPickingRequestBody(int id_oc, string ean, decimal cantidad, int id_operario, string id_equipo, string fechahora_equipo, int id_actividad, int idlotedetalle, int idcontenedor, int posicion, string nom_operario)
        {
            this.id_oc = id_oc;
            this.ean = ean;
            this.cantidad = cantidad;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
            this.fechahora_equipo = fechahora_equipo;
            this.id_actividad = id_actividad;
            this.idlotedetalle = idlotedetalle;
            this.idcontenedor = idcontenedor;
            this.posicion = posicion;
            this.nom_operario = nom_operario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarMultiPickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarMultiPickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarMultiPickingResponseBody Body;
        
        public grabarMultiPickingResponse()
        {
        }
        
        public grabarMultiPickingResponse(ServiceReference1.grabarMultiPickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarMultiPickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP grabarMultiPickingResult;
        
        public grabarMultiPickingResponseBody()
        {
        }
        
        public grabarMultiPickingResponseBody(ServiceReference1.eErrorMP grabarMultiPickingResult)
        {
            this.grabarMultiPickingResult = grabarMultiPickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class sustituirMultiPickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="sustituirMultiPicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.sustituirMultiPickingRequestBody Body;
        
        public sustituirMultiPickingRequest()
        {
        }
        
        public sustituirMultiPickingRequest(ServiceReference1.sustituirMultiPickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class sustituirMultiPickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string sku_original;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string ean_sustituto;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public decimal cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string marca;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public double precio;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string id_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=8)]
        public string fechahora_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=9)]
        public int id_actividad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=10)]
        public int idlotedetalle;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=11)]
        public int idcontenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=12)]
        public int posicion;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=13)]
        public string nom_operario;
        
        public sustituirMultiPickingRequestBody()
        {
        }
        
        public sustituirMultiPickingRequestBody(int id_oc, string sku_original, string ean_sustituto, decimal cantidad, string marca, double precio, int id_operario, string id_equipo, string fechahora_equipo, int id_actividad, int idlotedetalle, int idcontenedor, int posicion, string nom_operario)
        {
            this.id_oc = id_oc;
            this.sku_original = sku_original;
            this.ean_sustituto = ean_sustituto;
            this.cantidad = cantidad;
            this.marca = marca;
            this.precio = precio;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
            this.fechahora_equipo = fechahora_equipo;
            this.id_actividad = id_actividad;
            this.idlotedetalle = idlotedetalle;
            this.idcontenedor = idcontenedor;
            this.posicion = posicion;
            this.nom_operario = nom_operario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class sustituirMultiPickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="sustituirMultiPickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.sustituirMultiPickingResponseBody Body;
        
        public sustituirMultiPickingResponse()
        {
        }
        
        public sustituirMultiPickingResponse(ServiceReference1.sustituirMultiPickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class sustituirMultiPickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP sustituirMultiPickingResult;
        
        public sustituirMultiPickingResponseBody()
        {
        }
        
        public sustituirMultiPickingResponseBody(ServiceReference1.eErrorMP sustituirMultiPickingResult)
        {
            this.sustituirMultiPickingResult = sustituirMultiPickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class faltanteMultiPickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="faltanteMultiPicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.faltanteMultiPickingRequestBody Body;
        
        public faltanteMultiPickingRequest()
        {
        }
        
        public faltanteMultiPickingRequest(ServiceReference1.faltanteMultiPickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class faltanteMultiPickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_detalleoc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public double cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string fechahora_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public int id_actividad;
        
        public faltanteMultiPickingRequestBody()
        {
        }
        
        public faltanteMultiPickingRequestBody(int id_oc, int id_detalleoc, double cantidad, int id_operario, string id_equipo, string fechahora_equipo, int id_actividad)
        {
            this.id_oc = id_oc;
            this.id_detalleoc = id_detalleoc;
            this.cantidad = cantidad;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
            this.fechahora_equipo = fechahora_equipo;
            this.id_actividad = id_actividad;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class faltanteMultiPickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="faltanteMultiPickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.faltanteMultiPickingResponseBody Body;
        
        public faltanteMultiPickingResponse()
        {
        }
        
        public faltanteMultiPickingResponse(ServiceReference1.faltanteMultiPickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class faltanteMultiPickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP faltanteMultiPickingResult;
        
        public faltanteMultiPickingResponseBody()
        {
        }
        
        public faltanteMultiPickingResponseBody(ServiceReference1.eErrorMP faltanteMultiPickingResult)
        {
            this.faltanteMultiPickingResult = faltanteMultiPickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getPosicionJabaRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getPosicionJaba", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getPosicionJabaRequestBody Body;
        
        public getPosicionJabaRequest()
        {
        }
        
        public getPosicionJabaRequest(ServiceReference1.getPosicionJabaRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getPosicionJabaRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idLote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public getPosicionJabaRequestBody()
        {
        }
        
        public getPosicionJabaRequestBody(int idLote, int id_operario, string id_equipo)
        {
            this.idLote = idLote;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getPosicionJabaResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getPosicionJabaResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getPosicionJabaResponseBody Body;
        
        public getPosicionJabaResponse()
        {
        }
        
        public getPosicionJabaResponse(ServiceReference1.getPosicionJabaResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getPosicionJabaResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ePosicionJaba getPosicionJabaResult;
        
        public getPosicionJabaResponseBody()
        {
        }
        
        public getPosicionJabaResponseBody(ServiceReference1.ePosicionJaba getPosicionJabaResult)
        {
            this.getPosicionJabaResult = getPosicionJabaResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getCantContenedorAlmacenadoRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getCantContenedorAlmacenado", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getCantContenedorAlmacenadoRequestBody Body;
        
        public getCantContenedorAlmacenadoRequest()
        {
        }
        
        public getCantContenedorAlmacenadoRequest(ServiceReference1.getCantContenedorAlmacenadoRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getCantContenedorAlmacenadoRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public getCantContenedorAlmacenadoRequestBody()
        {
        }
        
        public getCantContenedorAlmacenadoRequestBody(int id_lote, int id_operario, string id_equipo)
        {
            this.id_lote = id_lote;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getCantContenedorAlmacenadoResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getCantContenedorAlmacenadoResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getCantContenedorAlmacenadoResponseBody Body;
        
        public getCantContenedorAlmacenadoResponse()
        {
        }
        
        public getCantContenedorAlmacenadoResponse(ServiceReference1.getCantContenedorAlmacenadoResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getCantContenedorAlmacenadoResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eCantAlmacenada getCantContenedorAlmacenadoResult;
        
        public getCantContenedorAlmacenadoResponseBody()
        {
        }
        
        public getCantContenedorAlmacenadoResponseBody(ServiceReference1.eCantAlmacenada getCantContenedorAlmacenadoResult)
        {
            this.getCantContenedorAlmacenadoResult = getCantContenedorAlmacenadoResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getContenedorRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getContenedor", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getContenedorRequestBody Body;
        
        public getContenedorRequest()
        {
        }
        
        public getContenedorRequest(ServiceReference1.getContenedorRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getContenedorRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string cod_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string id_equipo;
        
        public getContenedorRequestBody()
        {
        }
        
        public getContenedorRequestBody(int id_lote, string cod_contenedor, int id_operario, string id_equipo)
        {
            this.id_lote = id_lote;
            this.cod_contenedor = cod_contenedor;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getContenedorResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getContenedorResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getContenedorResponseBody Body;
        
        public getContenedorResponse()
        {
        }
        
        public getContenedorResponse(ServiceReference1.getContenedorResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getContenedorResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eContenedor getContenedorResult;
        
        public getContenedorResponseBody()
        {
        }
        
        public getContenedorResponseBody(ServiceReference1.eContenedor getContenedorResult)
        {
            this.getContenedorResult = getContenedorResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ObtenerUbicacionRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ObtenerUbicacion", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.ObtenerUbicacionRequestBody Body;
        
        public ObtenerUbicacionRequest()
        {
        }
        
        public ObtenerUbicacionRequest(ServiceReference1.ObtenerUbicacionRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class ObtenerUbicacionRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public bool flag;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string codigo_ubicacion;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string nom_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public ObtenerUbicacionRequestBody()
        {
        }
        
        public ObtenerUbicacionRequestBody(bool flag, string codigo_ubicacion, string nom_operario, int id_operario, string id_equipo)
        {
            this.flag = flag;
            this.codigo_ubicacion = codigo_ubicacion;
            this.nom_operario = nom_operario;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ObtenerUbicacionResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ObtenerUbicacionResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.ObtenerUbicacionResponseBody Body;
        
        public ObtenerUbicacionResponse()
        {
        }
        
        public ObtenerUbicacionResponse(ServiceReference1.ObtenerUbicacionResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class ObtenerUbicacionResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eUbicacion ObtenerUbicacionResult;
        
        public ObtenerUbicacionResponseBody()
        {
        }
        
        public ObtenerUbicacionResponseBody(ServiceReference1.eUbicacion ObtenerUbicacionResult)
        {
            this.ObtenerUbicacionResult = ObtenerUbicacionResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarUbicacionRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarUbicacion", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarUbicacionRequestBody Body;
        
        public grabarUbicacionRequest()
        {
        }
        
        public grabarUbicacionRequest(ServiceReference1.grabarUbicacionRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarUbicacionRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public bool flag;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string codigo_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string codigo_ubicacion;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string nom_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string id_equipo;
        
        public grabarUbicacionRequestBody()
        {
        }
        
        public grabarUbicacionRequestBody(bool flag, int id_lote, string codigo_contenedor, string codigo_ubicacion, string nom_operario, int id_operario, string id_equipo)
        {
            this.flag = flag;
            this.id_lote = id_lote;
            this.codigo_contenedor = codigo_contenedor;
            this.codigo_ubicacion = codigo_ubicacion;
            this.nom_operario = nom_operario;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarUbicacionResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarUbicacionResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarUbicacionResponseBody Body;
        
        public grabarUbicacionResponse()
        {
        }
        
        public grabarUbicacionResponse(ServiceReference1.grabarUbicacionResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarUbicacionResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP grabarUbicacionResult;
        
        public grabarUbicacionResponseBody()
        {
        }
        
        public grabarUbicacionResponseBody(ServiceReference1.eErrorMP grabarUbicacionResult)
        {
            this.grabarUbicacionResult = grabarUbicacionResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarCargaContenedorRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarCargaContenedor", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarCargaContenedorRequestBody Body;
        
        public grabarCargaContenedorRequest()
        {
        }
        
        public grabarCargaContenedorRequest(ServiceReference1.grabarCargaContenedorRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarCargaContenedorRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string id_ot;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string id_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string nom_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public grabarCargaContenedorRequestBody()
        {
        }
        
        public grabarCargaContenedorRequestBody(string id_ot, string id_contenedor, string nom_operario, int id_operario, string id_equipo)
        {
            this.id_ot = id_ot;
            this.id_contenedor = id_contenedor;
            this.nom_operario = nom_operario;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarCargaContenedorResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarCargaContenedorResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarCargaContenedorResponseBody Body;
        
        public grabarCargaContenedorResponse()
        {
        }
        
        public grabarCargaContenedorResponse(ServiceReference1.grabarCargaContenedorResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarCargaContenedorResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP grabarCargaContenedorResult;
        
        public grabarCargaContenedorResponseBody()
        {
        }
        
        public grabarCargaContenedorResponseBody(ServiceReference1.eErrorMP grabarCargaContenedorResult)
        {
            this.grabarCargaContenedorResult = grabarCargaContenedorResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class finalizarAlmacanamientoRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="finalizarAlmacanamiento", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.finalizarAlmacanamientoRequestBody Body;
        
        public finalizarAlmacanamientoRequest()
        {
        }
        
        public finalizarAlmacanamientoRequest(ServiceReference1.finalizarAlmacanamientoRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class finalizarAlmacanamientoRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public finalizarAlmacanamientoRequestBody()
        {
        }
        
        public finalizarAlmacanamientoRequestBody(int id_lote, int id_operario, string id_equipo)
        {
            this.id_lote = id_lote;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class finalizarAlmacanamientoResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="finalizarAlmacanamientoResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.finalizarAlmacanamientoResponseBody Body;
        
        public finalizarAlmacanamientoResponse()
        {
        }
        
        public finalizarAlmacanamientoResponse(ServiceReference1.finalizarAlmacanamientoResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class finalizarAlmacanamientoResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP finalizarAlmacanamientoResult;
        
        public finalizarAlmacanamientoResponseBody()
        {
        }
        
        public finalizarAlmacanamientoResponseBody(ServiceReference1.eErrorMP finalizarAlmacanamientoResult)
        {
            this.finalizarAlmacanamientoResult = finalizarAlmacanamientoResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class eliminarMultipickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="eliminarMultipicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.eliminarMultipickingRequestBody Body;
        
        public eliminarMultipickingRequest()
        {
        }
        
        public eliminarMultipickingRequest(ServiceReference1.eliminarMultipickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class eliminarMultipickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int flag;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_detalle_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_lotedetallepicking;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_resultado;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string id_equipo;
        
        public eliminarMultipickingRequestBody()
        {
        }
        
        public eliminarMultipickingRequestBody(int flag, int id_detalle_oc, int id_lotedetallepicking, int id_resultado, int id_operario, string id_equipo)
        {
            this.flag = flag;
            this.id_detalle_oc = id_detalle_oc;
            this.id_lotedetallepicking = id_lotedetallepicking;
            this.id_resultado = id_resultado;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class eliminarMultipickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="eliminarMultipickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.eliminarMultipickingResponseBody Body;
        
        public eliminarMultipickingResponse()
        {
        }
        
        public eliminarMultipickingResponse(ServiceReference1.eliminarMultipickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class eliminarMultipickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP eliminarMultipickingResult;
        
        public eliminarMultipickingResponseBody()
        {
        }
        
        public eliminarMultipickingResponseBody(ServiceReference1.eErrorMP eliminarMultipickingResult)
        {
            this.eliminarMultipickingResult = eliminarMultipickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class eliminarContenedorCargadoRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="eliminarContenedorCargado", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.eliminarContenedorCargadoRequestBody Body;
        
        public eliminarContenedorCargadoRequest()
        {
        }
        
        public eliminarContenedorCargadoRequest(ServiceReference1.eliminarContenedorCargadoRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class eliminarContenedorCargadoRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int flag;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int idContenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int idOT;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public eliminarContenedorCargadoRequestBody()
        {
        }
        
        public eliminarContenedorCargadoRequestBody(int flag, int idContenedor, int idOT, int id_operario, string id_equipo)
        {
            this.flag = flag;
            this.idContenedor = idContenedor;
            this.idOT = idOT;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class eliminarContenedorCargadoResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="eliminarContenedorCargadoResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.eliminarContenedorCargadoResponseBody Body;
        
        public eliminarContenedorCargadoResponse()
        {
        }
        
        public eliminarContenedorCargadoResponse(ServiceReference1.eliminarContenedorCargadoResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class eliminarContenedorCargadoResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP eliminarContenedorCargadoResult;
        
        public eliminarContenedorCargadoResponseBody()
        {
        }
        
        public eliminarContenedorCargadoResponseBody(ServiceReference1.eErrorMP eliminarContenedorCargadoResult)
        {
            this.eliminarContenedorCargadoResult = eliminarContenedorCargadoResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class descargarContenedoresOTRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="descargarContenedoresOT", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.descargarContenedoresOTRequestBody Body;
        
        public descargarContenedoresOTRequest()
        {
        }
        
        public descargarContenedoresOTRequest(ServiceReference1.descargarContenedoresOTRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class descargarContenedoresOTRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string codigo_OT;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int numContenedores;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string nom_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public descargarContenedoresOTRequestBody()
        {
        }
        
        public descargarContenedoresOTRequestBody(string codigo_OT, int numContenedores, string nom_operario, int id_operario, string id_equipo)
        {
            this.codigo_OT = codigo_OT;
            this.numContenedores = numContenedores;
            this.nom_operario = nom_operario;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class descargarContenedoresOTResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="descargarContenedoresOTResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.descargarContenedoresOTResponseBody Body;
        
        public descargarContenedoresOTResponse()
        {
        }
        
        public descargarContenedoresOTResponse(ServiceReference1.descargarContenedoresOTResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class descargarContenedoresOTResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP descargarContenedoresOTResult;
        
        public descargarContenedoresOTResponseBody()
        {
        }
        
        public descargarContenedoresOTResponseBody(ServiceReference1.eErrorMP descargarContenedoresOTResult)
        {
            this.descargarContenedoresOTResult = descargarContenedoresOTResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getPosicionOrdenCompraRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getPosicionOrdenCompra", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getPosicionOrdenCompraRequestBody Body;
        
        public getPosicionOrdenCompraRequest()
        {
        }
        
        public getPosicionOrdenCompraRequest(ServiceReference1.getPosicionOrdenCompraRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getPosicionOrdenCompraRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int idlotedetalle;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public getPosicionOrdenCompraRequestBody()
        {
        }
        
        public getPosicionOrdenCompraRequestBody(int id_lote, int id_oc, int idlotedetalle, int id_operario, string id_equipo)
        {
            this.id_lote = id_lote;
            this.id_oc = id_oc;
            this.idlotedetalle = idlotedetalle;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getPosicionOrdenCompraResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getPosicionOrdenCompraResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getPosicionOrdenCompraResponseBody Body;
        
        public getPosicionOrdenCompraResponse()
        {
        }
        
        public getPosicionOrdenCompraResponse(ServiceReference1.getPosicionOrdenCompraResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getPosicionOrdenCompraResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ePosicionOrdenCompra getPosicionOrdenCompraResult;
        
        public getPosicionOrdenCompraResponseBody()
        {
        }
        
        public getPosicionOrdenCompraResponseBody(ServiceReference1.ePosicionOrdenCompra getPosicionOrdenCompraResult)
        {
            this.getPosicionOrdenCompraResult = getPosicionOrdenCompraResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getPosicionContenedorLoteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getPosicionContenedorLote", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getPosicionContenedorLoteRequestBody Body;
        
        public getPosicionContenedorLoteRequest()
        {
        }
        
        public getPosicionContenedorLoteRequest(ServiceReference1.getPosicionContenedorLoteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getPosicionContenedorLoteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public getPosicionContenedorLoteRequestBody()
        {
        }
        
        public getPosicionContenedorLoteRequestBody(int id_lote, int id_operario, string id_equipo)
        {
            this.id_lote = id_lote;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getPosicionContenedorLoteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getPosicionContenedorLoteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getPosicionContenedorLoteResponseBody Body;
        
        public getPosicionContenedorLoteResponse()
        {
        }
        
        public getPosicionContenedorLoteResponse(ServiceReference1.getPosicionContenedorLoteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getPosicionContenedorLoteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ePosicionContenedorLote getPosicionContenedorLoteResult;
        
        public getPosicionContenedorLoteResponseBody()
        {
        }
        
        public getPosicionContenedorLoteResponseBody(ServiceReference1.ePosicionContenedorLote getPosicionContenedorLoteResult)
        {
            this.getPosicionContenedorLoteResult = getPosicionContenedorLoteResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class actulizarImpresion_LoteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="actulizarImpresion_Lote", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.actulizarImpresion_LoteRequestBody Body;
        
        public actulizarImpresion_LoteRequest()
        {
        }
        
        public actulizarImpresion_LoteRequest(ServiceReference1.actulizarImpresion_LoteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class actulizarImpresion_LoteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        public actulizarImpresion_LoteRequestBody()
        {
        }
        
        public actulizarImpresion_LoteRequestBody(int id_lote)
        {
            this.id_lote = id_lote;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class actulizarImpresion_LoteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="actulizarImpresion_LoteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.actulizarImpresion_LoteResponseBody Body;
        
        public actulizarImpresion_LoteResponse()
        {
        }
        
        public actulizarImpresion_LoteResponse(ServiceReference1.actulizarImpresion_LoteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class actulizarImpresion_LoteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP actulizarImpresion_LoteResult;
        
        public actulizarImpresion_LoteResponseBody()
        {
        }
        
        public actulizarImpresion_LoteResponseBody(ServiceReference1.eErrorMP actulizarImpresion_LoteResult)
        {
            this.actulizarImpresion_LoteResult = actulizarImpresion_LoteResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getRePickingFaltanteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getRePickingFaltante", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getRePickingFaltanteRequestBody Body;
        
        public getRePickingFaltanteRequest()
        {
        }
        
        public getRePickingFaltanteRequest(ServiceReference1.getRePickingFaltanteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getRePickingFaltanteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string codigo;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_ola;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string id_equipo;
        
        public getRePickingFaltanteRequestBody()
        {
        }
        
        public getRePickingFaltanteRequestBody(string codigo, int id_ola, int id_operario, string id_equipo)
        {
            this.codigo = codigo;
            this.id_ola = id_ola;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getRePickingFaltanteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getRePickingFaltanteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getRePickingFaltanteResponseBody Body;
        
        public getRePickingFaltanteResponse()
        {
        }
        
        public getRePickingFaltanteResponse(ServiceReference1.getRePickingFaltanteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getRePickingFaltanteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ePickingFaltante getRePickingFaltanteResult;
        
        public getRePickingFaltanteResponseBody()
        {
        }
        
        public getRePickingFaltanteResponseBody(ServiceReference1.ePickingFaltante getRePickingFaltanteResult)
        {
            this.getRePickingFaltanteResult = getRePickingFaltanteResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getDetalleArticuloFaltanteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getDetalleArticuloFaltante", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getDetalleArticuloFaltanteRequestBody Body;
        
        public getDetalleArticuloFaltanteRequest()
        {
        }
        
        public getDetalleArticuloFaltanteRequest(ServiceReference1.getDetalleArticuloFaltanteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getDetalleArticuloFaltanteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_detalle_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public getDetalleArticuloFaltanteRequestBody()
        {
        }
        
        public getDetalleArticuloFaltanteRequestBody(int id_detalle_oc, int id_operario, string id_equipo)
        {
            this.id_detalle_oc = id_detalle_oc;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getDetalleArticuloFaltanteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getDetalleArticuloFaltanteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getDetalleArticuloFaltanteResponseBody Body;
        
        public getDetalleArticuloFaltanteResponse()
        {
        }
        
        public getDetalleArticuloFaltanteResponse(ServiceReference1.getDetalleArticuloFaltanteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getDetalleArticuloFaltanteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eDetalleArticuloMP getDetalleArticuloFaltanteResult;
        
        public getDetalleArticuloFaltanteResponseBody()
        {
        }
        
        public getDetalleArticuloFaltanteResponseBody(ServiceReference1.eDetalleArticuloMP getDetalleArticuloFaltanteResult)
        {
            this.getDetalleArticuloFaltanteResult = getDetalleArticuloFaltanteResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getUbicacionContenedorListRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getUbicacionContenedorList", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getUbicacionContenedorListRequestBody Body;
        
        public getUbicacionContenedorListRequest()
        {
        }
        
        public getUbicacionContenedorListRequest(ServiceReference1.getUbicacionContenedorListRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getUbicacionContenedorListRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_detalleoc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_resultado;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public getUbicacionContenedorListRequestBody()
        {
        }
        
        public getUbicacionContenedorListRequestBody(int id_oc, int id_detalleoc, int id_resultado, int id_operario, string id_equipo)
        {
            this.id_oc = id_oc;
            this.id_detalleoc = id_detalleoc;
            this.id_resultado = id_resultado;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getUbicacionContenedorListResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getUbicacionContenedorListResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getUbicacionContenedorListResponseBody Body;
        
        public getUbicacionContenedorListResponse()
        {
        }
        
        public getUbicacionContenedorListResponse(ServiceReference1.getUbicacionContenedorListResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getUbicacionContenedorListResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eUbicacionContenedor getUbicacionContenedorListResult;
        
        public getUbicacionContenedorListResponseBody()
        {
        }
        
        public getUbicacionContenedorListResponseBody(ServiceReference1.eUbicacionContenedor getUbicacionContenedorListResult)
        {
            this.getUbicacionContenedorListResult = getUbicacionContenedorListResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getUbicacionOCListRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getUbicacionOCList", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getUbicacionOCListRequestBody Body;
        
        public getUbicacionOCListRequest()
        {
        }
        
        public getUbicacionOCListRequest(ServiceReference1.getUbicacionOCListRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getUbicacionOCListRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string numeroOC;
        
        public getUbicacionOCListRequestBody()
        {
        }
        
        public getUbicacionOCListRequestBody(string numeroOC)
        {
            this.numeroOC = numeroOC;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getUbicacionOCListResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getUbicacionOCListResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getUbicacionOCListResponseBody Body;
        
        public getUbicacionOCListResponse()
        {
        }
        
        public getUbicacionOCListResponse(ServiceReference1.getUbicacionOCListResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getUbicacionOCListResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ArrayOfEUbicacionOCItem getUbicacionOCListResult;
        
        public getUbicacionOCListResponseBody()
        {
        }
        
        public getUbicacionOCListResponseBody(ServiceReference1.ArrayOfEUbicacionOCItem getUbicacionOCListResult)
        {
            this.getUbicacionOCListResult = getUbicacionOCListResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaMultiPickingFaltanteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaMultiPickingFaltante", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaMultiPickingFaltanteRequestBody Body;
        
        public validaMultiPickingFaltanteRequest()
        {
        }
        
        public validaMultiPickingFaltanteRequest(ServiceReference1.validaMultiPickingFaltanteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaMultiPickingFaltanteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string ean;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public decimal cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public validaMultiPickingFaltanteRequestBody()
        {
        }
        
        public validaMultiPickingFaltanteRequestBody(int id_oc, string ean, decimal cantidad, int id_operario, string id_equipo)
        {
            this.id_oc = id_oc;
            this.ean = ean;
            this.cantidad = cantidad;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaMultiPickingFaltanteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaMultiPickingFaltanteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaMultiPickingFaltanteResponseBody Body;
        
        public validaMultiPickingFaltanteResponse()
        {
        }
        
        public validaMultiPickingFaltanteResponse(ServiceReference1.validaMultiPickingFaltanteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaMultiPickingFaltanteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eValidaArticuloMP validaMultiPickingFaltanteResult;
        
        public validaMultiPickingFaltanteResponseBody()
        {
        }
        
        public validaMultiPickingFaltanteResponseBody(ServiceReference1.eValidaArticuloMP validaMultiPickingFaltanteResult)
        {
            this.validaMultiPickingFaltanteResult = validaMultiPickingFaltanteResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaContenedorRePickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaContenedorRePicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaContenedorRePickingRequestBody Body;
        
        public validaContenedorRePickingRequest()
        {
        }
        
        public validaContenedorRePickingRequest(ServiceReference1.validaContenedorRePickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaContenedorRePickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_lotedetalle;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string cod_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_sucursal;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string id_equipo;
        
        public validaContenedorRePickingRequestBody()
        {
        }
        
        public validaContenedorRePickingRequestBody(int id_lote, int id_lotedetalle, string cod_contenedor, int id_sucursal, int id_operario, string id_equipo)
        {
            this.id_lote = id_lote;
            this.id_lotedetalle = id_lotedetalle;
            this.cod_contenedor = cod_contenedor;
            this.id_sucursal = id_sucursal;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaContenedorRePickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaContenedorRePickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaContenedorRePickingResponseBody Body;
        
        public validaContenedorRePickingResponse()
        {
        }
        
        public validaContenedorRePickingResponse(ServiceReference1.validaContenedorRePickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaContenedorRePickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eValidaContenedor validaContenedorRePickingResult;
        
        public validaContenedorRePickingResponseBody()
        {
        }
        
        public validaContenedorRePickingResponseBody(ServiceReference1.eValidaContenedor validaContenedorRePickingResult)
        {
            this.validaContenedorRePickingResult = validaContenedorRePickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarRePickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarRePicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarRePickingRequestBody Body;
        
        public grabarRePickingRequest()
        {
        }
        
        public grabarRePickingRequest(ServiceReference1.grabarRePickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarRePickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_lotedetalle;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_detalle_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int id_resultado;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public int id_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public int id_ubicacion;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string ean;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=8)]
        public decimal cant_pickeada;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=9)]
        public decimal cant_pendiente;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=10)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=11)]
        public string id_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=12)]
        public string fechahora_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=13)]
        public string nom_operario;
        
        public grabarRePickingRequestBody()
        {
        }
        
        public grabarRePickingRequestBody(int id_lote, int id_lotedetalle, int id_oc, int id_detalle_oc, int id_resultado, int id_contenedor, int id_ubicacion, string ean, decimal cant_pickeada, decimal cant_pendiente, int id_operario, string id_equipo, string fechahora_equipo, string nom_operario)
        {
            this.id_lote = id_lote;
            this.id_lotedetalle = id_lotedetalle;
            this.id_oc = id_oc;
            this.id_detalle_oc = id_detalle_oc;
            this.id_resultado = id_resultado;
            this.id_contenedor = id_contenedor;
            this.id_ubicacion = id_ubicacion;
            this.ean = ean;
            this.cant_pickeada = cant_pickeada;
            this.cant_pendiente = cant_pendiente;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
            this.fechahora_equipo = fechahora_equipo;
            this.nom_operario = nom_operario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarRePickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarRePickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarRePickingResponseBody Body;
        
        public grabarRePickingResponse()
        {
        }
        
        public grabarRePickingResponse(ServiceReference1.grabarRePickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarRePickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP grabarRePickingResult;
        
        public grabarRePickingResponseBody()
        {
        }
        
        public grabarRePickingResponseBody(ServiceReference1.eErrorMP grabarRePickingResult)
        {
            this.grabarRePickingResult = grabarRePickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarRePickingSustitutoRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarRePickingSustituto", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarRePickingSustitutoRequestBody Body;
        
        public grabarRePickingSustitutoRequest()
        {
        }
        
        public grabarRePickingSustitutoRequest(ServiceReference1.grabarRePickingSustitutoRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarRePickingSustitutoRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_lotedetalle;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_detalle_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int id_resultado;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string sku_original;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string ean_sustituto;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=7)]
        public decimal cant_pickeada;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=8)]
        public decimal cant_pendiente;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=9)]
        public string marca;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=10)]
        public double precio;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=11)]
        public int id_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=12)]
        public int id_ubicacion;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=13)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=14)]
        public string id_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=15)]
        public string fechahora_equipo;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=16)]
        public string nom_operario;
        
        public grabarRePickingSustitutoRequestBody()
        {
        }
        
        public grabarRePickingSustitutoRequestBody(
                    int id_lote, 
                    int id_lotedetalle, 
                    int id_oc, 
                    int id_detalle_oc, 
                    int id_resultado, 
                    string sku_original, 
                    string ean_sustituto, 
                    decimal cant_pickeada, 
                    decimal cant_pendiente, 
                    string marca, 
                    double precio, 
                    int id_contenedor, 
                    int id_ubicacion, 
                    int id_operario, 
                    string id_equipo, 
                    string fechahora_equipo, 
                    string nom_operario)
        {
            this.id_lote = id_lote;
            this.id_lotedetalle = id_lotedetalle;
            this.id_oc = id_oc;
            this.id_detalle_oc = id_detalle_oc;
            this.id_resultado = id_resultado;
            this.sku_original = sku_original;
            this.ean_sustituto = ean_sustituto;
            this.cant_pickeada = cant_pickeada;
            this.cant_pendiente = cant_pendiente;
            this.marca = marca;
            this.precio = precio;
            this.id_contenedor = id_contenedor;
            this.id_ubicacion = id_ubicacion;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
            this.fechahora_equipo = fechahora_equipo;
            this.nom_operario = nom_operario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class grabarRePickingSustitutoResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="grabarRePickingSustitutoResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.grabarRePickingSustitutoResponseBody Body;
        
        public grabarRePickingSustitutoResponse()
        {
        }
        
        public grabarRePickingSustitutoResponse(ServiceReference1.grabarRePickingSustitutoResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class grabarRePickingSustitutoResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP grabarRePickingSustitutoResult;
        
        public grabarRePickingSustitutoResponseBody()
        {
        }
        
        public grabarRePickingSustitutoResponseBody(ServiceReference1.eErrorMP grabarRePickingSustitutoResult)
        {
            this.grabarRePickingSustitutoResult = grabarRePickingSustitutoResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class valida_TodoFaltanteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="valida_TodoFaltante", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.valida_TodoFaltanteRequestBody Body;
        
        public valida_TodoFaltanteRequest()
        {
        }
        
        public valida_TodoFaltanteRequest(ServiceReference1.valida_TodoFaltanteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class valida_TodoFaltanteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idLote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int idOperario;
        
        public valida_TodoFaltanteRequestBody()
        {
        }
        
        public valida_TodoFaltanteRequestBody(int idLote, int idOperario)
        {
            this.idLote = idLote;
            this.idOperario = idOperario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class valida_TodoFaltanteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="valida_TodoFaltanteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.valida_TodoFaltanteResponseBody Body;
        
        public valida_TodoFaltanteResponse()
        {
        }
        
        public valida_TodoFaltanteResponse(ServiceReference1.valida_TodoFaltanteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class valida_TodoFaltanteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP valida_TodoFaltanteResult;
        
        public valida_TodoFaltanteResponseBody()
        {
        }
        
        public valida_TodoFaltanteResponseBody(ServiceReference1.eErrorMP valida_TodoFaltanteResult)
        {
            this.valida_TodoFaltanteResult = valida_TodoFaltanteResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getOperarioCodigoLocalRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getOperarioCodigoLocal", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getOperarioCodigoLocalRequestBody Body;
        
        public getOperarioCodigoLocalRequest()
        {
        }
        
        public getOperarioCodigoLocalRequest(ServiceReference1.getOperarioCodigoLocalRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getOperarioCodigoLocalRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idOperario;
        
        public getOperarioCodigoLocalRequestBody()
        {
        }
        
        public getOperarioCodigoLocalRequestBody(int idOperario)
        {
            this.idOperario = idOperario;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getOperarioCodigoLocalResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getOperarioCodigoLocalResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getOperarioCodigoLocalResponseBody Body;
        
        public getOperarioCodigoLocalResponse()
        {
        }
        
        public getOperarioCodigoLocalResponse(ServiceReference1.getOperarioCodigoLocalResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getOperarioCodigoLocalResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eOperario getOperarioCodigoLocalResult;
        
        public getOperarioCodigoLocalResponseBody()
        {
        }
        
        public getOperarioCodigoLocalResponseBody(ServiceReference1.eOperario getOperarioCodigoLocalResult)
        {
            this.getOperarioCodigoLocalResult = getOperarioCodigoLocalResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateCantPosicionRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateCantPosicion", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.updateCantPosicionRequestBody Body;
        
        public updateCantPosicionRequest()
        {
        }
        
        public updateCantPosicionRequest(ServiceReference1.updateCantPosicionRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class updateCantPosicionRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int cantidad;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string operador;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public updateCantPosicionRequestBody()
        {
        }
        
        public updateCantPosicionRequestBody(int id_lote, int cantidad, string operador, int id_operario, string id_equipo)
        {
            this.id_lote = id_lote;
            this.cantidad = cantidad;
            this.operador = operador;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateCantPosicionResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateCantPosicionResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.updateCantPosicionResponseBody Body;
        
        public updateCantPosicionResponse()
        {
        }
        
        public updateCantPosicionResponse(ServiceReference1.updateCantPosicionResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class updateCantPosicionResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP updateCantPosicionResult;
        
        public updateCantPosicionResponseBody()
        {
        }
        
        public updateCantPosicionResponseBody(ServiceReference1.eErrorMP updateCantPosicionResult)
        {
            this.updateCantPosicionResult = updateCantPosicionResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateDetalleOCSaltoRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateDetalleOCSalto", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.updateDetalleOCSaltoRequestBody Body;
        
        public updateDetalleOCSaltoRequest()
        {
        }
        
        public updateDetalleOCSaltoRequest(ServiceReference1.updateDetalleOCSaltoRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class updateDetalleOCSaltoRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idDetalleOC;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_equipo;
        
        public updateDetalleOCSaltoRequestBody()
        {
        }
        
        public updateDetalleOCSaltoRequestBody(int idDetalleOC, int id_operario, string id_equipo)
        {
            this.idDetalleOC = idDetalleOC;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateDetalleOCSaltoResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateDetalleOCSaltoResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.updateDetalleOCSaltoResponseBody Body;
        
        public updateDetalleOCSaltoResponse()
        {
        }
        
        public updateDetalleOCSaltoResponse(ServiceReference1.updateDetalleOCSaltoResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class updateDetalleOCSaltoResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP updateDetalleOCSaltoResult;
        
        public updateDetalleOCSaltoResponseBody()
        {
        }
        
        public updateDetalleOCSaltoResponseBody(ServiceReference1.eErrorMP updateDetalleOCSaltoResult)
        {
            this.updateDetalleOCSaltoResult = updateDetalleOCSaltoResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ValidaOcContenedorRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ValidaOcContenedor", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.ValidaOcContenedorRequestBody Body;
        
        public ValidaOcContenedorRequest()
        {
        }
        
        public ValidaOcContenedorRequest(ServiceReference1.ValidaOcContenedorRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class ValidaOcContenedorRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string ordenCompra;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string id_equipo;
        
        public ValidaOcContenedorRequestBody()
        {
        }
        
        public ValidaOcContenedorRequestBody(string contenedor, string ordenCompra, int id_operario, string id_equipo)
        {
            this.contenedor = contenedor;
            this.ordenCompra = ordenCompra;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ValidaOcContenedorResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ValidaOcContenedorResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.ValidaOcContenedorResponseBody Body;
        
        public ValidaOcContenedorResponse()
        {
        }
        
        public ValidaOcContenedorResponse(ServiceReference1.ValidaOcContenedorResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class ValidaOcContenedorResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP ValidaOcContenedorResult;
        
        public ValidaOcContenedorResponseBody()
        {
        }
        
        public ValidaOcContenedorResponseBody(ServiceReference1.eErrorMP ValidaOcContenedorResult)
        {
            this.ValidaOcContenedorResult = ValidaOcContenedorResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ConsolidarJabaOCRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ConsolidarJabaOC", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.ConsolidarJabaOCRequestBody Body;
        
        public ConsolidarJabaOCRequest()
        {
        }
        
        public ConsolidarJabaOCRequest(ServiceReference1.ConsolidarJabaOCRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class ConsolidarJabaOCRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string contenedorOrigen;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string contenedorDestino;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string ordenCompra;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public ConsolidarJabaOCRequestBody()
        {
        }
        
        public ConsolidarJabaOCRequestBody(string contenedorOrigen, string contenedorDestino, string ordenCompra, int id_operario, string id_equipo)
        {
            this.contenedorOrigen = contenedorOrigen;
            this.contenedorDestino = contenedorDestino;
            this.ordenCompra = ordenCompra;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ConsolidarJabaOCResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ConsolidarJabaOCResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.ConsolidarJabaOCResponseBody Body;
        
        public ConsolidarJabaOCResponse()
        {
        }
        
        public ConsolidarJabaOCResponse(ServiceReference1.ConsolidarJabaOCResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class ConsolidarJabaOCResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP ConsolidarJabaOCResult;
        
        public ConsolidarJabaOCResponseBody()
        {
        }
        
        public ConsolidarJabaOCResponseBody(ServiceReference1.eErrorMP ConsolidarJabaOCResult)
        {
            this.ConsolidarJabaOCResult = ConsolidarJabaOCResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaContenedorMonoPickingRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaContenedorMonoPicking", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaContenedorMonoPickingRequestBody Body;
        
        public validaContenedorMonoPickingRequest()
        {
        }
        
        public validaContenedorMonoPickingRequest(ServiceReference1.validaContenedorMonoPickingRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaContenedorMonoPickingRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string cod_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_sucursal;
        
        public validaContenedorMonoPickingRequestBody()
        {
        }
        
        public validaContenedorMonoPickingRequestBody(string cod_contenedor, int id_sucursal)
        {
            this.cod_contenedor = cod_contenedor;
            this.id_sucursal = id_sucursal;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaContenedorMonoPickingResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaContenedorMonoPickingResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaContenedorMonoPickingResponseBody Body;
        
        public validaContenedorMonoPickingResponse()
        {
        }
        
        public validaContenedorMonoPickingResponse(ServiceReference1.validaContenedorMonoPickingResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaContenedorMonoPickingResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eValidaContenedor validaContenedorMonoPickingResult;
        
        public validaContenedorMonoPickingResponseBody()
        {
        }
        
        public validaContenedorMonoPickingResponseBody(ServiceReference1.eValidaContenedor validaContenedorMonoPickingResult)
        {
            this.validaContenedorMonoPickingResult = validaContenedorMonoPickingResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateContenedorRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateContenedor", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.updateContenedorRequestBody Body;
        
        public updateContenedorRequest()
        {
        }
        
        public updateContenedorRequest(ServiceReference1.updateContenedorRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class updateContenedorRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_sucursal;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string CodigoContenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public int Estado;
        
        public updateContenedorRequestBody()
        {
        }
        
        public updateContenedorRequestBody(int id_sucursal, string CodigoContenedor, int Estado)
        {
            this.id_sucursal = id_sucursal;
            this.CodigoContenedor = CodigoContenedor;
            this.Estado = Estado;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateContenedorResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateContenedorResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.updateContenedorResponseBody Body;
        
        public updateContenedorResponse()
        {
        }
        
        public updateContenedorResponse(ServiceReference1.updateContenedorResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class updateContenedorResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP updateContenedorResult;
        
        public updateContenedorResponseBody()
        {
        }
        
        public updateContenedorResponseBody(ServiceReference1.eErrorMP updateContenedorResult)
        {
            this.updateContenedorResult = updateContenedorResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaContenedorRePickingWSRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaContenedorRePickingWS", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaContenedorRePickingWSRequestBody Body;
        
        public validaContenedorRePickingWSRequest()
        {
        }
        
        public validaContenedorRePickingWSRequest(ServiceReference1.validaContenedorRePickingWSRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaContenedorRePickingWSRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id_lote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_lotedetalle;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string id_contenedor;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string id_equipo;
        
        public validaContenedorRePickingWSRequestBody()
        {
        }
        
        public validaContenedorRePickingWSRequestBody(int id_lote, int id_lotedetalle, string id_contenedor, int id_operario, string id_equipo)
        {
            this.id_lote = id_lote;
            this.id_lotedetalle = id_lotedetalle;
            this.id_contenedor = id_contenedor;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class validaContenedorRePickingWSResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="validaContenedorRePickingWSResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.validaContenedorRePickingWSResponseBody Body;
        
        public validaContenedorRePickingWSResponse()
        {
        }
        
        public validaContenedorRePickingWSResponse(ServiceReference1.validaContenedorRePickingWSResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class validaContenedorRePickingWSResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eErrorMP validaContenedorRePickingWSResult;
        
        public validaContenedorRePickingWSResponseBody()
        {
        }
        
        public validaContenedorRePickingWSResponseBody(ServiceReference1.eErrorMP validaContenedorRePickingWSResult)
        {
            this.validaContenedorRePickingWSResult = validaContenedorRePickingWSResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getPickingPendienteXLoteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getPickingPendienteXLote", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getPickingPendienteXLoteRequestBody Body;
        
        public getPickingPendienteXLoteRequest()
        {
        }
        
        public getPickingPendienteXLoteRequest(ServiceReference1.getPickingPendienteXLoteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getPickingPendienteXLoteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idLote;
        
        public getPickingPendienteXLoteRequestBody()
        {
        }
        
        public getPickingPendienteXLoteRequestBody(int idLote)
        {
            this.idLote = idLote;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getPickingPendienteXLoteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getPickingPendienteXLoteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getPickingPendienteXLoteResponseBody Body;
        
        public getPickingPendienteXLoteResponse()
        {
        }
        
        public getPickingPendienteXLoteResponse(ServiceReference1.getPickingPendienteXLoteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getPickingPendienteXLoteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.ArrayOfEPickingPendienteItem getPickingPendienteXLoteResult;
        
        public getPickingPendienteXLoteResponseBody()
        {
        }
        
        public getPickingPendienteXLoteResponseBody(ServiceReference1.ArrayOfEPickingPendienteItem getPickingPendienteXLoteResult)
        {
            this.getPickingPendienteXLoteResult = getPickingPendienteXLoteResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getDetalleArticuloOCRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getDetalleArticuloOC", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getDetalleArticuloOCRequestBody Body;
        
        public getDetalleArticuloOCRequest()
        {
        }
        
        public getDetalleArticuloOCRequest(ServiceReference1.getDetalleArticuloOCRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getDetalleArticuloOCRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idLote;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public int id_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string nro_oc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string sku;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=4)]
        public int id_operario;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string id_equipo;
        
        public getDetalleArticuloOCRequestBody()
        {
        }
        
        public getDetalleArticuloOCRequestBody(int idLote, int id_oc, string nro_oc, string sku, int id_operario, string id_equipo)
        {
            this.idLote = idLote;
            this.id_oc = id_oc;
            this.nro_oc = nro_oc;
            this.sku = sku;
            this.id_operario = id_operario;
            this.id_equipo = id_equipo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getDetalleArticuloOCResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getDetalleArticuloOCResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getDetalleArticuloOCResponseBody Body;
        
        public getDetalleArticuloOCResponse()
        {
        }
        
        public getDetalleArticuloOCResponse(ServiceReference1.getDetalleArticuloOCResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getDetalleArticuloOCResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ServiceReference1.eDetalleArticuloMP getDetalleArticuloOCResult;
        
        public getDetalleArticuloOCResponseBody()
        {
        }
        
        public getDetalleArticuloOCResponseBody(ServiceReference1.eDetalleArticuloMP getDetalleArticuloOCResult)
        {
            this.getDetalleArticuloOCResult = getDetalleArticuloOCResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getSKUImageByteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getSKUImageByte", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getSKUImageByteRequestBody Body;
        
        public getSKUImageByteRequest()
        {
        }
        
        public getSKUImageByteRequest(ServiceReference1.getSKUImageByteRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getSKUImageByteRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string SKU;
        
        public getSKUImageByteRequestBody()
        {
        }
        
        public getSKUImageByteRequestBody(string SKU)
        {
            this.SKU = SKU;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getSKUImageByteResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getSKUImageByteResponse", Namespace="http://tempuri.org/", Order=0)]
        public ServiceReference1.getSKUImageByteResponseBody Body;
        
        public getSKUImageByteResponse()
        {
        }
        
        public getSKUImageByteResponse(ServiceReference1.getSKUImageByteResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getSKUImageByteResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public byte[] getSKUImageByteResult;
        
        public getSKUImageByteResponseBody()
        {
        }
        
        public getSKUImageByteResponseBody(byte[] getSKUImageByteResult)
        {
            this.getSKUImageByteResult = getSKUImageByteResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public interface WebServiceXpickingSoapChannel : ServiceReference1.WebServiceXpickingSoap, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public partial class WebServiceXpickingSoapClient : System.ServiceModel.ClientBase<ServiceReference1.WebServiceXpickingSoap>, ServiceReference1.WebServiceXpickingSoap
    {
        
    /// <summary>
    /// Implemente este método parcial para configurar el punto de conexión de servicio.
    /// </summary>
    /// <param name="serviceEndpoint">El punto de conexión para configurar</param>
    /// <param name="clientCredentials">Credenciales de cliente</param>
    static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public WebServiceXpickingSoapClient(EndpointConfiguration endpointConfiguration) : 
                base(WebServiceXpickingSoapClient.GetBindingForEndpoint(endpointConfiguration), WebServiceXpickingSoapClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public WebServiceXpickingSoapClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(WebServiceXpickingSoapClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public WebServiceXpickingSoapClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(WebServiceXpickingSoapClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public WebServiceXpickingSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getValidacion_UsuarioResponse> ServiceReference1.WebServiceXpickingSoap.getValidacion_UsuarioAsync(ServiceReference1.getValidacion_UsuarioRequest request)
        {
            return base.Channel.getValidacion_UsuarioAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getValidacion_UsuarioResponse> getValidacion_UsuarioAsync(string codigo_usuario, string clave, string version, string id_equipo)
        {
            ServiceReference1.getValidacion_UsuarioRequest inValue = new ServiceReference1.getValidacion_UsuarioRequest();
            inValue.Body = new ServiceReference1.getValidacion_UsuarioRequestBody();
            inValue.Body.codigo_usuario = codigo_usuario;
            inValue.Body.clave = clave;
            inValue.Body.version = version;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getValidacion_UsuarioAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getListar_Ordenes_CompraResponse> ServiceReference1.WebServiceXpickingSoap.getListar_Ordenes_CompraAsync(ServiceReference1.getListar_Ordenes_CompraRequest request)
        {
            return base.Channel.getListar_Ordenes_CompraAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getListar_Ordenes_CompraResponse> getListar_Ordenes_CompraAsync(int id_usuario)
        {
            ServiceReference1.getListar_Ordenes_CompraRequest inValue = new ServiceReference1.getListar_Ordenes_CompraRequest();
            inValue.Body = new ServiceReference1.getListar_Ordenes_CompraRequestBody();
            inValue.Body.id_usuario = id_usuario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getListar_Ordenes_CompraAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getListar_Articulos_OCResponse> ServiceReference1.WebServiceXpickingSoap.getListar_Articulos_OCAsync(ServiceReference1.getListar_Articulos_OCRequest request)
        {
            return base.Channel.getListar_Articulos_OCAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getListar_Articulos_OCResponse> getListar_Articulos_OCAsync(int id_oc, int id_operario)
        {
            ServiceReference1.getListar_Articulos_OCRequest inValue = new ServiceReference1.getListar_Articulos_OCRequest();
            inValue.Body = new ServiceReference1.getListar_Articulos_OCRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_operario = id_operario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getListar_Articulos_OCAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getListar_Articulos_OC_EliminarResponse> ServiceReference1.WebServiceXpickingSoap.getListar_Articulos_OC_EliminarAsync(ServiceReference1.getListar_Articulos_OC_EliminarRequest request)
        {
            return base.Channel.getListar_Articulos_OC_EliminarAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getListar_Articulos_OC_EliminarResponse> getListar_Articulos_OC_EliminarAsync(int id_oc, int id_operario)
        {
            ServiceReference1.getListar_Articulos_OC_EliminarRequest inValue = new ServiceReference1.getListar_Articulos_OC_EliminarRequest();
            inValue.Body = new ServiceReference1.getListar_Articulos_OC_EliminarRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_operario = id_operario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getListar_Articulos_OC_EliminarAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getListar_Detalle_ArticuloResponse> ServiceReference1.WebServiceXpickingSoap.getListar_Detalle_ArticuloAsync(ServiceReference1.getListar_Detalle_ArticuloRequest request)
        {
            return base.Channel.getListar_Detalle_ArticuloAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getListar_Detalle_ArticuloResponse> getListar_Detalle_ArticuloAsync(int id_oc, int id_detalle_oc)
        {
            ServiceReference1.getListar_Detalle_ArticuloRequest inValue = new ServiceReference1.getListar_Detalle_ArticuloRequest();
            inValue.Body = new ServiceReference1.getListar_Detalle_ArticuloRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_detalle_oc = id_detalle_oc;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getListar_Detalle_ArticuloAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getGrabar_Picking_ArticuloResponse> ServiceReference1.WebServiceXpickingSoap.getGrabar_Picking_ArticuloAsync(ServiceReference1.getGrabar_Picking_ArticuloRequest request)
        {
            return base.Channel.getGrabar_Picking_ArticuloAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getGrabar_Picking_ArticuloResponse> getGrabar_Picking_ArticuloAsync(int id_oc, string ean, double cantidad, string id_Pesable, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            ServiceReference1.getGrabar_Picking_ArticuloRequest inValue = new ServiceReference1.getGrabar_Picking_ArticuloRequest();
            inValue.Body = new ServiceReference1.getGrabar_Picking_ArticuloRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.ean = ean;
            inValue.Body.cantidad = cantidad;
            inValue.Body.id_Pesable = id_Pesable;
            inValue.Body.ID_OPERARIO = ID_OPERARIO;
            inValue.Body.COD_EQUIPO = COD_EQUIPO;
            inValue.Body.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            inValue.Body.ID_ACTIVIDAD = ID_ACTIVIDAD;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getGrabar_Picking_ArticuloAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.Finalizar_PickingResponse> ServiceReference1.WebServiceXpickingSoap.Finalizar_PickingAsync(ServiceReference1.Finalizar_PickingRequest request)
        {
            return base.Channel.Finalizar_PickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Finalizar_PickingResponse> Finalizar_PickingAsync(int id_oc, int id_tienda, int id_operario, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            ServiceReference1.Finalizar_PickingRequest inValue = new ServiceReference1.Finalizar_PickingRequest();
            inValue.Body = new ServiceReference1.Finalizar_PickingRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_tienda = id_tienda;
            inValue.Body.id_operario = id_operario;
            inValue.Body.COD_EQUIPO = COD_EQUIPO;
            inValue.Body.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            inValue.Body.ID_ACTIVIDAD = ID_ACTIVIDAD;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).Finalizar_PickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.Finalizar_Picking_FoodResponse> ServiceReference1.WebServiceXpickingSoap.Finalizar_Picking_FoodAsync(ServiceReference1.Finalizar_Picking_FoodRequest request)
        {
            return base.Channel.Finalizar_Picking_FoodAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Finalizar_Picking_FoodResponse> Finalizar_Picking_FoodAsync(int id_oc, int id_tienda, int id_operario, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            ServiceReference1.Finalizar_Picking_FoodRequest inValue = new ServiceReference1.Finalizar_Picking_FoodRequest();
            inValue.Body = new ServiceReference1.Finalizar_Picking_FoodRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_tienda = id_tienda;
            inValue.Body.id_operario = id_operario;
            inValue.Body.COD_EQUIPO = COD_EQUIPO;
            inValue.Body.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            inValue.Body.ID_ACTIVIDAD = ID_ACTIVIDAD;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).Finalizar_Picking_FoodAsync(inValue);
        }
        
        public System.Threading.Tasks.Task<int> Enviar_Orden_PosVirtualAsync(int id_oc, int id_tienda, int id_operario)
        {
            return base.Channel.Enviar_Orden_PosVirtualAsync(id_oc, id_tienda, id_operario);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.Faltante_PickingResponse> ServiceReference1.WebServiceXpickingSoap.Faltante_PickingAsync(ServiceReference1.Faltante_PickingRequest request)
        {
            return base.Channel.Faltante_PickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Faltante_PickingResponse> Faltante_PickingAsync(int id_oc, int id_detalle_oc, double cantidad, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            ServiceReference1.Faltante_PickingRequest inValue = new ServiceReference1.Faltante_PickingRequest();
            inValue.Body = new ServiceReference1.Faltante_PickingRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_detalle_oc = id_detalle_oc;
            inValue.Body.cantidad = cantidad;
            inValue.Body.ID_OPERARIO = ID_OPERARIO;
            inValue.Body.COD_EQUIPO = COD_EQUIPO;
            inValue.Body.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            inValue.Body.ID_ACTIVIDAD = ID_ACTIVIDAD;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).Faltante_PickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.Sustituto_Detalle_ArticuloResponse> ServiceReference1.WebServiceXpickingSoap.Sustituto_Detalle_ArticuloAsync(ServiceReference1.Sustituto_Detalle_ArticuloRequest request)
        {
            return base.Channel.Sustituto_Detalle_ArticuloAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Sustituto_Detalle_ArticuloResponse> Sustituto_Detalle_ArticuloAsync(int id_oc, string sku, string ean, double cantidad, int tienda, string id_pesable, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD, string mensajeerror)
        {
            ServiceReference1.Sustituto_Detalle_ArticuloRequest inValue = new ServiceReference1.Sustituto_Detalle_ArticuloRequest();
            inValue.Body = new ServiceReference1.Sustituto_Detalle_ArticuloRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.sku = sku;
            inValue.Body.ean = ean;
            inValue.Body.cantidad = cantidad;
            inValue.Body.tienda = tienda;
            inValue.Body.id_pesable = id_pesable;
            inValue.Body.ID_OPERARIO = ID_OPERARIO;
            inValue.Body.COD_EQUIPO = COD_EQUIPO;
            inValue.Body.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            inValue.Body.ID_ACTIVIDAD = ID_ACTIVIDAD;
            inValue.Body.mensajeerror = mensajeerror;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).Sustituto_Detalle_ArticuloAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.Asignar_JabasResponse> ServiceReference1.WebServiceXpickingSoap.Asignar_JabasAsync(ServiceReference1.Asignar_JabasRequest request)
        {
            return base.Channel.Asignar_JabasAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Asignar_JabasResponse> Asignar_JabasAsync(int id_oc, string id_jaba, int cantidad)
        {
            ServiceReference1.Asignar_JabasRequest inValue = new ServiceReference1.Asignar_JabasRequest();
            inValue.Body = new ServiceReference1.Asignar_JabasRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_jaba = id_jaba;
            inValue.Body.cantidad = cantidad;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).Asignar_JabasAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.Listar_JabasResponse> ServiceReference1.WebServiceXpickingSoap.Listar_JabasAsync(ServiceReference1.Listar_JabasRequest request)
        {
            return base.Channel.Listar_JabasAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Listar_JabasResponse> Listar_JabasAsync(int id_oc)
        {
            ServiceReference1.Listar_JabasRequest inValue = new ServiceReference1.Listar_JabasRequest();
            inValue.Body = new ServiceReference1.Listar_JabasRequestBody();
            inValue.Body.id_oc = id_oc;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).Listar_JabasAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.Eliminar_PickingResponse> ServiceReference1.WebServiceXpickingSoap.Eliminar_PickingAsync(ServiceReference1.Eliminar_PickingRequest request)
        {
            return base.Channel.Eliminar_PickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Eliminar_PickingResponse> Eliminar_PickingAsync(int id_oc, string sku, double cantidad, int id_criterio, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            ServiceReference1.Eliminar_PickingRequest inValue = new ServiceReference1.Eliminar_PickingRequest();
            inValue.Body = new ServiceReference1.Eliminar_PickingRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.sku = sku;
            inValue.Body.cantidad = cantidad;
            inValue.Body.id_criterio = id_criterio;
            inValue.Body.ID_OPERARIO = ID_OPERARIO;
            inValue.Body.COD_EQUIPO = COD_EQUIPO;
            inValue.Body.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            inValue.Body.ID_ACTIVIDAD = ID_ACTIVIDAD;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).Eliminar_PickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.Eliminar_Picking_TotalResponse> ServiceReference1.WebServiceXpickingSoap.Eliminar_Picking_TotalAsync(ServiceReference1.Eliminar_Picking_TotalRequest request)
        {
            return base.Channel.Eliminar_Picking_TotalAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Eliminar_Picking_TotalResponse> Eliminar_Picking_TotalAsync(int id_oc, string sku, int id_criterio, int ID_OPERARIO, string COD_EQUIPO, string FECHAHORA_EQUIPO, int ID_ACTIVIDAD)
        {
            ServiceReference1.Eliminar_Picking_TotalRequest inValue = new ServiceReference1.Eliminar_Picking_TotalRequest();
            inValue.Body = new ServiceReference1.Eliminar_Picking_TotalRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.sku = sku;
            inValue.Body.id_criterio = id_criterio;
            inValue.Body.ID_OPERARIO = ID_OPERARIO;
            inValue.Body.COD_EQUIPO = COD_EQUIPO;
            inValue.Body.FECHAHORA_EQUIPO = FECHAHORA_EQUIPO;
            inValue.Body.ID_ACTIVIDAD = ID_ACTIVIDAD;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).Eliminar_Picking_TotalAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.Iniciar_Cerrar_Actividad_OCResponse> ServiceReference1.WebServiceXpickingSoap.Iniciar_Cerrar_Actividad_OCAsync(ServiceReference1.Iniciar_Cerrar_Actividad_OCRequest request)
        {
            return base.Channel.Iniciar_Cerrar_Actividad_OCAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Iniciar_Cerrar_Actividad_OCResponse> Iniciar_Cerrar_Actividad_OCAsync(int id_oc, int id_operario, string COD_EQUIPO, string tipo, int ID_ACTV, int ID_ACTIVIDAD)
        {
            ServiceReference1.Iniciar_Cerrar_Actividad_OCRequest inValue = new ServiceReference1.Iniciar_Cerrar_Actividad_OCRequest();
            inValue.Body = new ServiceReference1.Iniciar_Cerrar_Actividad_OCRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_operario = id_operario;
            inValue.Body.COD_EQUIPO = COD_EQUIPO;
            inValue.Body.tipo = tipo;
            inValue.Body.ID_ACTV = ID_ACTV;
            inValue.Body.ID_ACTIVIDAD = ID_ACTIVIDAD;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).Iniciar_Cerrar_Actividad_OCAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getEstado_Orden_PosVirtualResponse> ServiceReference1.WebServiceXpickingSoap.getEstado_Orden_PosVirtualAsync(ServiceReference1.getEstado_Orden_PosVirtualRequest request)
        {
            return base.Channel.getEstado_Orden_PosVirtualAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getEstado_Orden_PosVirtualResponse> getEstado_Orden_PosVirtualAsync(string numeroOrden)
        {
            ServiceReference1.getEstado_Orden_PosVirtualRequest inValue = new ServiceReference1.getEstado_Orden_PosVirtualRequest();
            inValue.Body = new ServiceReference1.getEstado_Orden_PosVirtualRequestBody();
            inValue.Body.numeroOrden = numeroOrden;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getEstado_Orden_PosVirtualAsync(inValue);
        }
        
        public System.Threading.Tasks.Task<int> getNumero_ProductoSueltoAsync(int id_oc)
        {
            return base.Channel.getNumero_ProductoSueltoAsync(id_oc);
        }
        
        public System.Threading.Tasks.Task<int> Actualizar_Numero_ProductoSueltoAsync(int id_oc, int cantidad)
        {
            return base.Channel.Actualizar_Numero_ProductoSueltoAsync(id_oc, cantidad);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getColaImpresionResponse> ServiceReference1.WebServiceXpickingSoap.getColaImpresionAsync(ServiceReference1.getColaImpresionRequest request)
        {
            return base.Channel.getColaImpresionAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getColaImpresionResponse> getColaImpresionAsync(int flag, int id_oc, int codigo_local)
        {
            ServiceReference1.getColaImpresionRequest inValue = new ServiceReference1.getColaImpresionRequest();
            inValue.Body = new ServiceReference1.getColaImpresionRequestBody();
            inValue.Body.flag = flag;
            inValue.Body.id_oc = id_oc;
            inValue.Body.codigo_local = codigo_local;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getColaImpresionAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getprinterStatusResponse> ServiceReference1.WebServiceXpickingSoap.getprinterStatusAsync(ServiceReference1.getprinterStatusRequest request)
        {
            return base.Channel.getprinterStatusAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getprinterStatusResponse> getprinterStatusAsync(string ip)
        {
            ServiceReference1.getprinterStatusRequest inValue = new ServiceReference1.getprinterStatusRequest();
            inValue.Body = new ServiceReference1.getprinterStatusRequestBody();
            inValue.Body.ip = ip;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getprinterStatusAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getEtiquetaOCResponse> ServiceReference1.WebServiceXpickingSoap.getEtiquetaOCAsync(ServiceReference1.getEtiquetaOCRequest request)
        {
            return base.Channel.getEtiquetaOCAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getEtiquetaOCResponse> getEtiquetaOCAsync(int id_oc)
        {
            ServiceReference1.getEtiquetaOCRequest inValue = new ServiceReference1.getEtiquetaOCRequest();
            inValue.Body = new ServiceReference1.getEtiquetaOCRequestBody();
            inValue.Body.id_oc = id_oc;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getEtiquetaOCAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaCONTENEDORResponse> ServiceReference1.WebServiceXpickingSoap.printEtiquetaCONTENEDORAsync(ServiceReference1.printEtiquetaCONTENEDORRequest request)
        {
            return base.Channel.printEtiquetaCONTENEDORAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.printEtiquetaCONTENEDORResponse> printEtiquetaCONTENEDORAsync(string lstContenedor, string ip)
        {
            ServiceReference1.printEtiquetaCONTENEDORRequest inValue = new ServiceReference1.printEtiquetaCONTENEDORRequest();
            inValue.Body = new ServiceReference1.printEtiquetaCONTENEDORRequestBody();
            inValue.Body.lstContenedor = lstContenedor;
            inValue.Body.ip = ip;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).printEtiquetaCONTENEDORAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaUBICACIONResponse> ServiceReference1.WebServiceXpickingSoap.printEtiquetaUBICACIONAsync(ServiceReference1.printEtiquetaUBICACIONRequest request)
        {
            return base.Channel.printEtiquetaUBICACIONAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.printEtiquetaUBICACIONResponse> printEtiquetaUBICACIONAsync(string ubicacion, string ip)
        {
            ServiceReference1.printEtiquetaUBICACIONRequest inValue = new ServiceReference1.printEtiquetaUBICACIONRequest();
            inValue.Body = new ServiceReference1.printEtiquetaUBICACIONRequestBody();
            inValue.Body.ubicacion = ubicacion;
            inValue.Body.ip = ip;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).printEtiquetaUBICACIONAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaOCResponse> ServiceReference1.WebServiceXpickingSoap.printEtiquetaOCAsync(ServiceReference1.printEtiquetaOCRequest request)
        {
            return base.Channel.printEtiquetaOCAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.printEtiquetaOCResponse> printEtiquetaOCAsync(int id_oc, string ip)
        {
            ServiceReference1.printEtiquetaOCRequest inValue = new ServiceReference1.printEtiquetaOCRequest();
            inValue.Body = new ServiceReference1.printEtiquetaOCRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.ip = ip;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).printEtiquetaOCAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaLOTEResponse> ServiceReference1.WebServiceXpickingSoap.printEtiquetaLOTEAsync(ServiceReference1.printEtiquetaLOTERequest request)
        {
            return base.Channel.printEtiquetaLOTEAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.printEtiquetaLOTEResponse> printEtiquetaLOTEAsync(int flag, int id_lote, string codigo_contenedor, string codigo_repicking, string ip)
        {
            ServiceReference1.printEtiquetaLOTERequest inValue = new ServiceReference1.printEtiquetaLOTERequest();
            inValue.Body = new ServiceReference1.printEtiquetaLOTERequestBody();
            inValue.Body.flag = flag;
            inValue.Body.id_lote = id_lote;
            inValue.Body.codigo_contenedor = codigo_contenedor;
            inValue.Body.codigo_repicking = codigo_repicking;
            inValue.Body.ip = ip;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).printEtiquetaLOTEAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.printEtiquetaOCLOTEResponse> ServiceReference1.WebServiceXpickingSoap.printEtiquetaOCLOTEAsync(ServiceReference1.printEtiquetaOCLOTERequest request)
        {
            return base.Channel.printEtiquetaOCLOTEAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.printEtiquetaOCLOTEResponse> printEtiquetaOCLOTEAsync(int flag, int id_oc, string ip)
        {
            ServiceReference1.printEtiquetaOCLOTERequest inValue = new ServiceReference1.printEtiquetaOCLOTERequest();
            inValue.Body = new ServiceReference1.printEtiquetaOCLOTERequestBody();
            inValue.Body.flag = flag;
            inValue.Body.id_oc = id_oc;
            inValue.Body.ip = ip;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).printEtiquetaOCLOTEAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.validaVersionResponse> ServiceReference1.WebServiceXpickingSoap.validaVersionAsync(ServiceReference1.validaVersionRequest request)
        {
            return base.Channel.validaVersionAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.validaVersionResponse> validaVersionAsync(string version)
        {
            ServiceReference1.validaVersionRequest inValue = new ServiceReference1.validaVersionRequest();
            inValue.Body = new ServiceReference1.validaVersionRequestBody();
            inValue.Body.version = version;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).validaVersionAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.validaEquipoResponse> ServiceReference1.WebServiceXpickingSoap.validaEquipoAsync(ServiceReference1.validaEquipoRequest request)
        {
            return base.Channel.validaEquipoAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.validaEquipoResponse> validaEquipoAsync(int flag, int id_operario, string id_equipo)
        {
            ServiceReference1.validaEquipoRequest inValue = new ServiceReference1.validaEquipoRequest();
            inValue.Body = new ServiceReference1.validaEquipoRequestBody();
            inValue.Body.flag = flag;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).validaEquipoAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.actulizarIdEquipoResponse> ServiceReference1.WebServiceXpickingSoap.actulizarIdEquipoAsync(ServiceReference1.actulizarIdEquipoRequest request)
        {
            return base.Channel.actulizarIdEquipoAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.actulizarIdEquipoResponse> actulizarIdEquipoAsync(int id_operario, string id_equipo)
        {
            ServiceReference1.actulizarIdEquipoRequest inValue = new ServiceReference1.actulizarIdEquipoRequest();
            inValue.Body = new ServiceReference1.actulizarIdEquipoRequestBody();
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).actulizarIdEquipoAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getAsignarLoteResponse> ServiceReference1.WebServiceXpickingSoap.getAsignarLoteAsync(ServiceReference1.getAsignarLoteRequest request)
        {
            return base.Channel.getAsignarLoteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getAsignarLoteResponse> getAsignarLoteAsync(int id_lote, int id_operario, int id_sucursal, string id_equipo)
        {
            ServiceReference1.getAsignarLoteRequest inValue = new ServiceReference1.getAsignarLoteRequest();
            inValue.Body = new ServiceReference1.getAsignarLoteRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_sucursal = id_sucursal;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getAsignarLoteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getContenedorSugeridoTextResponse> ServiceReference1.WebServiceXpickingSoap.getContenedorSugeridoTextAsync(ServiceReference1.getContenedorSugeridoTextRequest request)
        {
            return base.Channel.getContenedorSugeridoTextAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getContenedorSugeridoTextResponse> getContenedorSugeridoTextAsync(int id_lote)
        {
            ServiceReference1.getContenedorSugeridoTextRequest inValue = new ServiceReference1.getContenedorSugeridoTextRequest();
            inValue.Body = new ServiceReference1.getContenedorSugeridoTextRequestBody();
            inValue.Body.id_lote = id_lote;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getContenedorSugeridoTextAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getContenedorSugeridoPasoResponse> ServiceReference1.WebServiceXpickingSoap.getContenedorSugeridoPasoAsync(ServiceReference1.getContenedorSugeridoPasoRequest request)
        {
            return base.Channel.getContenedorSugeridoPasoAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getContenedorSugeridoPasoResponse> getContenedorSugeridoPasoAsync(int id_lote, int oc, string sku, string ean)
        {
            ServiceReference1.getContenedorSugeridoPasoRequest inValue = new ServiceReference1.getContenedorSugeridoPasoRequest();
            inValue.Body = new ServiceReference1.getContenedorSugeridoPasoRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.oc = oc;
            inValue.Body.sku = sku;
            inValue.Body.ean = ean;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getContenedorSugeridoPasoAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getCantPedXOperarioResponse> ServiceReference1.WebServiceXpickingSoap.getCantPedXOperarioAsync(ServiceReference1.getCantPedXOperarioRequest request)
        {
            return base.Channel.getCantPedXOperarioAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getCantPedXOperarioResponse> getCantPedXOperarioAsync(int id_operario)
        {
            ServiceReference1.getCantPedXOperarioRequest inValue = new ServiceReference1.getCantPedXOperarioRequest();
            inValue.Body = new ServiceReference1.getCantPedXOperarioRequestBody();
            inValue.Body.id_operario = id_operario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getCantPedXOperarioAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.inicioMultipickingResponse> ServiceReference1.WebServiceXpickingSoap.inicioMultipickingAsync(ServiceReference1.inicioMultipickingRequest request)
        {
            return base.Channel.inicioMultipickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.inicioMultipickingResponse> inicioMultipickingAsync(int flag, int idLote, int idOperario, string idEquipo, int criterio)
        {
            ServiceReference1.inicioMultipickingRequest inValue = new ServiceReference1.inicioMultipickingRequest();
            inValue.Body = new ServiceReference1.inicioMultipickingRequestBody();
            inValue.Body.flag = flag;
            inValue.Body.idLote = idLote;
            inValue.Body.idOperario = idOperario;
            inValue.Body.idEquipo = idEquipo;
            inValue.Body.criterio = criterio;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).inicioMultipickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.updateEstadoPreparacionLoteOCResponse> ServiceReference1.WebServiceXpickingSoap.updateEstadoPreparacionLoteOCAsync(ServiceReference1.updateEstadoPreparacionLoteOCRequest request)
        {
            return base.Channel.updateEstadoPreparacionLoteOCAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.updateEstadoPreparacionLoteOCResponse> updateEstadoPreparacionLoteOCAsync(int idLote, int idOperario)
        {
            ServiceReference1.updateEstadoPreparacionLoteOCRequest inValue = new ServiceReference1.updateEstadoPreparacionLoteOCRequest();
            inValue.Body = new ServiceReference1.updateEstadoPreparacionLoteOCRequestBody();
            inValue.Body.idLote = idLote;
            inValue.Body.idOperario = idOperario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).updateEstadoPreparacionLoteOCAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.actualizarFLG_SALTO_Detalle_OCResponse> ServiceReference1.WebServiceXpickingSoap.actualizarFLG_SALTO_Detalle_OCAsync(ServiceReference1.actualizarFLG_SALTO_Detalle_OCRequest request)
        {
            return base.Channel.actualizarFLG_SALTO_Detalle_OCAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.actualizarFLG_SALTO_Detalle_OCResponse> actualizarFLG_SALTO_Detalle_OCAsync(int idLote)
        {
            ServiceReference1.actualizarFLG_SALTO_Detalle_OCRequest inValue = new ServiceReference1.actualizarFLG_SALTO_Detalle_OCRequest();
            inValue.Body = new ServiceReference1.actualizarFLG_SALTO_Detalle_OCRequestBody();
            inValue.Body.idLote = idLote;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).actualizarFLG_SALTO_Detalle_OCAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getOTContenedoresResponse> ServiceReference1.WebServiceXpickingSoap.getOTContenedoresAsync(ServiceReference1.getOTContenedoresRequest request)
        {
            return base.Channel.getOTContenedoresAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getOTContenedoresResponse> getOTContenedoresAsync(string codigo_OT, int id_operario, string id_equipo)
        {
            ServiceReference1.getOTContenedoresRequest inValue = new ServiceReference1.getOTContenedoresRequest();
            inValue.Body = new ServiceReference1.getOTContenedoresRequestBody();
            inValue.Body.codigo_OT = codigo_OT;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getOTContenedoresAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getLotePickingResponse> ServiceReference1.WebServiceXpickingSoap.getLotePickingAsync(ServiceReference1.getLotePickingRequest request)
        {
            return base.Channel.getLotePickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getLotePickingResponse> getLotePickingAsync(int idLote, int id_operario, string id_equipo)
        {
            ServiceReference1.getLotePickingRequest inValue = new ServiceReference1.getLotePickingRequest();
            inValue.Body = new ServiceReference1.getLotePickingRequestBody();
            inValue.Body.idLote = idLote;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getLotePickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getDetalleArticuloResponse> ServiceReference1.WebServiceXpickingSoap.getDetalleArticuloAsync(ServiceReference1.getDetalleArticuloRequest request)
        {
            return base.Channel.getDetalleArticuloAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getDetalleArticuloResponse> getDetalleArticuloAsync(int idLote, int id_detalle_oc, string sku, int id_operario, string id_equipo)
        {
            ServiceReference1.getDetalleArticuloRequest inValue = new ServiceReference1.getDetalleArticuloRequest();
            inValue.Body = new ServiceReference1.getDetalleArticuloRequestBody();
            inValue.Body.idLote = idLote;
            inValue.Body.id_detalle_oc = id_detalle_oc;
            inValue.Body.sku = sku;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getDetalleArticuloAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getClienteResponse> ServiceReference1.WebServiceXpickingSoap.getClienteAsync(ServiceReference1.getClienteRequest request)
        {
            return base.Channel.getClienteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getClienteResponse> getClienteAsync(int id_oc, int id_operario, string id_equipo)
        {
            ServiceReference1.getClienteRequest inValue = new ServiceReference1.getClienteRequest();
            inValue.Body = new ServiceReference1.getClienteRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getClienteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.validaSustitucionResponse> ServiceReference1.WebServiceXpickingSoap.validaSustitucionAsync(ServiceReference1.validaSustitucionRequest request)
        {
            return base.Channel.validaSustitucionAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.validaSustitucionResponse> validaSustitucionAsync(int id_oc, string sku, string ean, double cantidad, int tienda, int id_operario, string id_equipo)
        {
            ServiceReference1.validaSustitucionRequest inValue = new ServiceReference1.validaSustitucionRequest();
            inValue.Body = new ServiceReference1.validaSustitucionRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.sku = sku;
            inValue.Body.ean = ean;
            inValue.Body.cantidad = cantidad;
            inValue.Body.tienda = tienda;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).validaSustitucionAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.validaContenedorResponse> ServiceReference1.WebServiceXpickingSoap.validaContenedorAsync(ServiceReference1.validaContenedorRequest request)
        {
            return base.Channel.validaContenedorAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.validaContenedorResponse> validaContenedorAsync(int id_lote, int id_lotedetalle, string cod_contenedor, int id_sucursal, int id_operario, string id_equipo)
        {
            ServiceReference1.validaContenedorRequest inValue = new ServiceReference1.validaContenedorRequest();
            inValue.Body = new ServiceReference1.validaContenedorRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_lotedetalle = id_lotedetalle;
            inValue.Body.cod_contenedor = cod_contenedor;
            inValue.Body.id_sucursal = id_sucursal;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).validaContenedorAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.validaMultiPickingResponse> ServiceReference1.WebServiceXpickingSoap.validaMultiPickingAsync(ServiceReference1.validaMultiPickingRequest request)
        {
            return base.Channel.validaMultiPickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.validaMultiPickingResponse> validaMultiPickingAsync(int id_oc, string ean, decimal cantidad, int id_operario, string id_equipo)
        {
            ServiceReference1.validaMultiPickingRequest inValue = new ServiceReference1.validaMultiPickingRequest();
            inValue.Body = new ServiceReference1.validaMultiPickingRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.ean = ean;
            inValue.Body.cantidad = cantidad;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).validaMultiPickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.grabarMultiPickingResponse> ServiceReference1.WebServiceXpickingSoap.grabarMultiPickingAsync(ServiceReference1.grabarMultiPickingRequest request)
        {
            return base.Channel.grabarMultiPickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.grabarMultiPickingResponse> grabarMultiPickingAsync(int id_oc, string ean, decimal cantidad, int id_operario, string id_equipo, string fechahora_equipo, int id_actividad, int idlotedetalle, int idcontenedor, int posicion, string nom_operario)
        {
            ServiceReference1.grabarMultiPickingRequest inValue = new ServiceReference1.grabarMultiPickingRequest();
            inValue.Body = new ServiceReference1.grabarMultiPickingRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.ean = ean;
            inValue.Body.cantidad = cantidad;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            inValue.Body.fechahora_equipo = fechahora_equipo;
            inValue.Body.id_actividad = id_actividad;
            inValue.Body.idlotedetalle = idlotedetalle;
            inValue.Body.idcontenedor = idcontenedor;
            inValue.Body.posicion = posicion;
            inValue.Body.nom_operario = nom_operario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).grabarMultiPickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.sustituirMultiPickingResponse> ServiceReference1.WebServiceXpickingSoap.sustituirMultiPickingAsync(ServiceReference1.sustituirMultiPickingRequest request)
        {
            return base.Channel.sustituirMultiPickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.sustituirMultiPickingResponse> sustituirMultiPickingAsync(int id_oc, string sku_original, string ean_sustituto, decimal cantidad, string marca, double precio, int id_operario, string id_equipo, string fechahora_equipo, int id_actividad, int idlotedetalle, int idcontenedor, int posicion, string nom_operario)
        {
            ServiceReference1.sustituirMultiPickingRequest inValue = new ServiceReference1.sustituirMultiPickingRequest();
            inValue.Body = new ServiceReference1.sustituirMultiPickingRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.sku_original = sku_original;
            inValue.Body.ean_sustituto = ean_sustituto;
            inValue.Body.cantidad = cantidad;
            inValue.Body.marca = marca;
            inValue.Body.precio = precio;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            inValue.Body.fechahora_equipo = fechahora_equipo;
            inValue.Body.id_actividad = id_actividad;
            inValue.Body.idlotedetalle = idlotedetalle;
            inValue.Body.idcontenedor = idcontenedor;
            inValue.Body.posicion = posicion;
            inValue.Body.nom_operario = nom_operario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).sustituirMultiPickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.faltanteMultiPickingResponse> ServiceReference1.WebServiceXpickingSoap.faltanteMultiPickingAsync(ServiceReference1.faltanteMultiPickingRequest request)
        {
            return base.Channel.faltanteMultiPickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.faltanteMultiPickingResponse> faltanteMultiPickingAsync(int id_oc, int id_detalleoc, double cantidad, int id_operario, string id_equipo, string fechahora_equipo, int id_actividad)
        {
            ServiceReference1.faltanteMultiPickingRequest inValue = new ServiceReference1.faltanteMultiPickingRequest();
            inValue.Body = new ServiceReference1.faltanteMultiPickingRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_detalleoc = id_detalleoc;
            inValue.Body.cantidad = cantidad;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            inValue.Body.fechahora_equipo = fechahora_equipo;
            inValue.Body.id_actividad = id_actividad;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).faltanteMultiPickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getPosicionJabaResponse> ServiceReference1.WebServiceXpickingSoap.getPosicionJabaAsync(ServiceReference1.getPosicionJabaRequest request)
        {
            return base.Channel.getPosicionJabaAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getPosicionJabaResponse> getPosicionJabaAsync(int idLote, int id_operario, string id_equipo)
        {
            ServiceReference1.getPosicionJabaRequest inValue = new ServiceReference1.getPosicionJabaRequest();
            inValue.Body = new ServiceReference1.getPosicionJabaRequestBody();
            inValue.Body.idLote = idLote;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getPosicionJabaAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getCantContenedorAlmacenadoResponse> ServiceReference1.WebServiceXpickingSoap.getCantContenedorAlmacenadoAsync(ServiceReference1.getCantContenedorAlmacenadoRequest request)
        {
            return base.Channel.getCantContenedorAlmacenadoAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getCantContenedorAlmacenadoResponse> getCantContenedorAlmacenadoAsync(int id_lote, int id_operario, string id_equipo)
        {
            ServiceReference1.getCantContenedorAlmacenadoRequest inValue = new ServiceReference1.getCantContenedorAlmacenadoRequest();
            inValue.Body = new ServiceReference1.getCantContenedorAlmacenadoRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getCantContenedorAlmacenadoAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getContenedorResponse> ServiceReference1.WebServiceXpickingSoap.getContenedorAsync(ServiceReference1.getContenedorRequest request)
        {
            return base.Channel.getContenedorAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getContenedorResponse> getContenedorAsync(int id_lote, string cod_contenedor, int id_operario, string id_equipo)
        {
            ServiceReference1.getContenedorRequest inValue = new ServiceReference1.getContenedorRequest();
            inValue.Body = new ServiceReference1.getContenedorRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.cod_contenedor = cod_contenedor;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getContenedorAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.ObtenerUbicacionResponse> ServiceReference1.WebServiceXpickingSoap.ObtenerUbicacionAsync(ServiceReference1.ObtenerUbicacionRequest request)
        {
            return base.Channel.ObtenerUbicacionAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.ObtenerUbicacionResponse> ObtenerUbicacionAsync(bool flag, string codigo_ubicacion, string nom_operario, int id_operario, string id_equipo)
        {
            ServiceReference1.ObtenerUbicacionRequest inValue = new ServiceReference1.ObtenerUbicacionRequest();
            inValue.Body = new ServiceReference1.ObtenerUbicacionRequestBody();
            inValue.Body.flag = flag;
            inValue.Body.codigo_ubicacion = codigo_ubicacion;
            inValue.Body.nom_operario = nom_operario;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).ObtenerUbicacionAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.grabarUbicacionResponse> ServiceReference1.WebServiceXpickingSoap.grabarUbicacionAsync(ServiceReference1.grabarUbicacionRequest request)
        {
            return base.Channel.grabarUbicacionAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.grabarUbicacionResponse> grabarUbicacionAsync(bool flag, int id_lote, string codigo_contenedor, string codigo_ubicacion, string nom_operario, int id_operario, string id_equipo)
        {
            ServiceReference1.grabarUbicacionRequest inValue = new ServiceReference1.grabarUbicacionRequest();
            inValue.Body = new ServiceReference1.grabarUbicacionRequestBody();
            inValue.Body.flag = flag;
            inValue.Body.id_lote = id_lote;
            inValue.Body.codigo_contenedor = codigo_contenedor;
            inValue.Body.codigo_ubicacion = codigo_ubicacion;
            inValue.Body.nom_operario = nom_operario;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).grabarUbicacionAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.grabarCargaContenedorResponse> ServiceReference1.WebServiceXpickingSoap.grabarCargaContenedorAsync(ServiceReference1.grabarCargaContenedorRequest request)
        {
            return base.Channel.grabarCargaContenedorAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.grabarCargaContenedorResponse> grabarCargaContenedorAsync(string id_ot, string id_contenedor, string nom_operario, int id_operario, string id_equipo)
        {
            ServiceReference1.grabarCargaContenedorRequest inValue = new ServiceReference1.grabarCargaContenedorRequest();
            inValue.Body = new ServiceReference1.grabarCargaContenedorRequestBody();
            inValue.Body.id_ot = id_ot;
            inValue.Body.id_contenedor = id_contenedor;
            inValue.Body.nom_operario = nom_operario;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).grabarCargaContenedorAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.finalizarAlmacanamientoResponse> ServiceReference1.WebServiceXpickingSoap.finalizarAlmacanamientoAsync(ServiceReference1.finalizarAlmacanamientoRequest request)
        {
            return base.Channel.finalizarAlmacanamientoAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.finalizarAlmacanamientoResponse> finalizarAlmacanamientoAsync(int id_lote, int id_operario, string id_equipo)
        {
            ServiceReference1.finalizarAlmacanamientoRequest inValue = new ServiceReference1.finalizarAlmacanamientoRequest();
            inValue.Body = new ServiceReference1.finalizarAlmacanamientoRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).finalizarAlmacanamientoAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.eliminarMultipickingResponse> ServiceReference1.WebServiceXpickingSoap.eliminarMultipickingAsync(ServiceReference1.eliminarMultipickingRequest request)
        {
            return base.Channel.eliminarMultipickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.eliminarMultipickingResponse> eliminarMultipickingAsync(int flag, int id_detalle_oc, int id_lotedetallepicking, int id_resultado, int id_operario, string id_equipo)
        {
            ServiceReference1.eliminarMultipickingRequest inValue = new ServiceReference1.eliminarMultipickingRequest();
            inValue.Body = new ServiceReference1.eliminarMultipickingRequestBody();
            inValue.Body.flag = flag;
            inValue.Body.id_detalle_oc = id_detalle_oc;
            inValue.Body.id_lotedetallepicking = id_lotedetallepicking;
            inValue.Body.id_resultado = id_resultado;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).eliminarMultipickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.eliminarContenedorCargadoResponse> ServiceReference1.WebServiceXpickingSoap.eliminarContenedorCargadoAsync(ServiceReference1.eliminarContenedorCargadoRequest request)
        {
            return base.Channel.eliminarContenedorCargadoAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.eliminarContenedorCargadoResponse> eliminarContenedorCargadoAsync(int flag, int idContenedor, int idOT, int id_operario, string id_equipo)
        {
            ServiceReference1.eliminarContenedorCargadoRequest inValue = new ServiceReference1.eliminarContenedorCargadoRequest();
            inValue.Body = new ServiceReference1.eliminarContenedorCargadoRequestBody();
            inValue.Body.flag = flag;
            inValue.Body.idContenedor = idContenedor;
            inValue.Body.idOT = idOT;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).eliminarContenedorCargadoAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.descargarContenedoresOTResponse> ServiceReference1.WebServiceXpickingSoap.descargarContenedoresOTAsync(ServiceReference1.descargarContenedoresOTRequest request)
        {
            return base.Channel.descargarContenedoresOTAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.descargarContenedoresOTResponse> descargarContenedoresOTAsync(string codigo_OT, int numContenedores, string nom_operario, int id_operario, string id_equipo)
        {
            ServiceReference1.descargarContenedoresOTRequest inValue = new ServiceReference1.descargarContenedoresOTRequest();
            inValue.Body = new ServiceReference1.descargarContenedoresOTRequestBody();
            inValue.Body.codigo_OT = codigo_OT;
            inValue.Body.numContenedores = numContenedores;
            inValue.Body.nom_operario = nom_operario;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).descargarContenedoresOTAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getPosicionOrdenCompraResponse> ServiceReference1.WebServiceXpickingSoap.getPosicionOrdenCompraAsync(ServiceReference1.getPosicionOrdenCompraRequest request)
        {
            return base.Channel.getPosicionOrdenCompraAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getPosicionOrdenCompraResponse> getPosicionOrdenCompraAsync(int id_lote, int id_oc, int idlotedetalle, int id_operario, string id_equipo)
        {
            ServiceReference1.getPosicionOrdenCompraRequest inValue = new ServiceReference1.getPosicionOrdenCompraRequest();
            inValue.Body = new ServiceReference1.getPosicionOrdenCompraRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_oc = id_oc;
            inValue.Body.idlotedetalle = idlotedetalle;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getPosicionOrdenCompraAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getPosicionContenedorLoteResponse> ServiceReference1.WebServiceXpickingSoap.getPosicionContenedorLoteAsync(ServiceReference1.getPosicionContenedorLoteRequest request)
        {
            return base.Channel.getPosicionContenedorLoteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getPosicionContenedorLoteResponse> getPosicionContenedorLoteAsync(int id_lote, int id_operario, string id_equipo)
        {
            ServiceReference1.getPosicionContenedorLoteRequest inValue = new ServiceReference1.getPosicionContenedorLoteRequest();
            inValue.Body = new ServiceReference1.getPosicionContenedorLoteRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getPosicionContenedorLoteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.actulizarImpresion_LoteResponse> ServiceReference1.WebServiceXpickingSoap.actulizarImpresion_LoteAsync(ServiceReference1.actulizarImpresion_LoteRequest request)
        {
            return base.Channel.actulizarImpresion_LoteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.actulizarImpresion_LoteResponse> actulizarImpresion_LoteAsync(int id_lote)
        {
            ServiceReference1.actulizarImpresion_LoteRequest inValue = new ServiceReference1.actulizarImpresion_LoteRequest();
            inValue.Body = new ServiceReference1.actulizarImpresion_LoteRequestBody();
            inValue.Body.id_lote = id_lote;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).actulizarImpresion_LoteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getRePickingFaltanteResponse> ServiceReference1.WebServiceXpickingSoap.getRePickingFaltanteAsync(ServiceReference1.getRePickingFaltanteRequest request)
        {
            return base.Channel.getRePickingFaltanteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getRePickingFaltanteResponse> getRePickingFaltanteAsync(string codigo, int id_ola, int id_operario, string id_equipo)
        {
            ServiceReference1.getRePickingFaltanteRequest inValue = new ServiceReference1.getRePickingFaltanteRequest();
            inValue.Body = new ServiceReference1.getRePickingFaltanteRequestBody();
            inValue.Body.codigo = codigo;
            inValue.Body.id_ola = id_ola;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getRePickingFaltanteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getDetalleArticuloFaltanteResponse> ServiceReference1.WebServiceXpickingSoap.getDetalleArticuloFaltanteAsync(ServiceReference1.getDetalleArticuloFaltanteRequest request)
        {
            return base.Channel.getDetalleArticuloFaltanteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getDetalleArticuloFaltanteResponse> getDetalleArticuloFaltanteAsync(int id_detalle_oc, int id_operario, string id_equipo)
        {
            ServiceReference1.getDetalleArticuloFaltanteRequest inValue = new ServiceReference1.getDetalleArticuloFaltanteRequest();
            inValue.Body = new ServiceReference1.getDetalleArticuloFaltanteRequestBody();
            inValue.Body.id_detalle_oc = id_detalle_oc;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getDetalleArticuloFaltanteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getUbicacionContenedorListResponse> ServiceReference1.WebServiceXpickingSoap.getUbicacionContenedorListAsync(ServiceReference1.getUbicacionContenedorListRequest request)
        {
            return base.Channel.getUbicacionContenedorListAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getUbicacionContenedorListResponse> getUbicacionContenedorListAsync(int id_oc, int id_detalleoc, int id_resultado, int id_operario, string id_equipo)
        {
            ServiceReference1.getUbicacionContenedorListRequest inValue = new ServiceReference1.getUbicacionContenedorListRequest();
            inValue.Body = new ServiceReference1.getUbicacionContenedorListRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_detalleoc = id_detalleoc;
            inValue.Body.id_resultado = id_resultado;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getUbicacionContenedorListAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getUbicacionOCListResponse> ServiceReference1.WebServiceXpickingSoap.getUbicacionOCListAsync(ServiceReference1.getUbicacionOCListRequest request)
        {
            return base.Channel.getUbicacionOCListAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getUbicacionOCListResponse> getUbicacionOCListAsync(string numeroOC)
        {
            ServiceReference1.getUbicacionOCListRequest inValue = new ServiceReference1.getUbicacionOCListRequest();
            inValue.Body = new ServiceReference1.getUbicacionOCListRequestBody();
            inValue.Body.numeroOC = numeroOC;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getUbicacionOCListAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.validaMultiPickingFaltanteResponse> ServiceReference1.WebServiceXpickingSoap.validaMultiPickingFaltanteAsync(ServiceReference1.validaMultiPickingFaltanteRequest request)
        {
            return base.Channel.validaMultiPickingFaltanteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.validaMultiPickingFaltanteResponse> validaMultiPickingFaltanteAsync(int id_oc, string ean, decimal cantidad, int id_operario, string id_equipo)
        {
            ServiceReference1.validaMultiPickingFaltanteRequest inValue = new ServiceReference1.validaMultiPickingFaltanteRequest();
            inValue.Body = new ServiceReference1.validaMultiPickingFaltanteRequestBody();
            inValue.Body.id_oc = id_oc;
            inValue.Body.ean = ean;
            inValue.Body.cantidad = cantidad;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).validaMultiPickingFaltanteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.validaContenedorRePickingResponse> ServiceReference1.WebServiceXpickingSoap.validaContenedorRePickingAsync(ServiceReference1.validaContenedorRePickingRequest request)
        {
            return base.Channel.validaContenedorRePickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.validaContenedorRePickingResponse> validaContenedorRePickingAsync(int id_lote, int id_lotedetalle, string cod_contenedor, int id_sucursal, int id_operario, string id_equipo)
        {
            ServiceReference1.validaContenedorRePickingRequest inValue = new ServiceReference1.validaContenedorRePickingRequest();
            inValue.Body = new ServiceReference1.validaContenedorRePickingRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_lotedetalle = id_lotedetalle;
            inValue.Body.cod_contenedor = cod_contenedor;
            inValue.Body.id_sucursal = id_sucursal;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).validaContenedorRePickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.grabarRePickingResponse> ServiceReference1.WebServiceXpickingSoap.grabarRePickingAsync(ServiceReference1.grabarRePickingRequest request)
        {
            return base.Channel.grabarRePickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.grabarRePickingResponse> grabarRePickingAsync(int id_lote, int id_lotedetalle, int id_oc, int id_detalle_oc, int id_resultado, int id_contenedor, int id_ubicacion, string ean, decimal cant_pickeada, decimal cant_pendiente, int id_operario, string id_equipo, string fechahora_equipo, string nom_operario)
        {
            ServiceReference1.grabarRePickingRequest inValue = new ServiceReference1.grabarRePickingRequest();
            inValue.Body = new ServiceReference1.grabarRePickingRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_lotedetalle = id_lotedetalle;
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_detalle_oc = id_detalle_oc;
            inValue.Body.id_resultado = id_resultado;
            inValue.Body.id_contenedor = id_contenedor;
            inValue.Body.id_ubicacion = id_ubicacion;
            inValue.Body.ean = ean;
            inValue.Body.cant_pickeada = cant_pickeada;
            inValue.Body.cant_pendiente = cant_pendiente;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            inValue.Body.fechahora_equipo = fechahora_equipo;
            inValue.Body.nom_operario = nom_operario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).grabarRePickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.grabarRePickingSustitutoResponse> ServiceReference1.WebServiceXpickingSoap.grabarRePickingSustitutoAsync(ServiceReference1.grabarRePickingSustitutoRequest request)
        {
            return base.Channel.grabarRePickingSustitutoAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.grabarRePickingSustitutoResponse> grabarRePickingSustitutoAsync(
                    int id_lote, 
                    int id_lotedetalle, 
                    int id_oc, 
                    int id_detalle_oc, 
                    int id_resultado, 
                    string sku_original, 
                    string ean_sustituto, 
                    decimal cant_pickeada, 
                    decimal cant_pendiente, 
                    string marca, 
                    double precio, 
                    int id_contenedor, 
                    int id_ubicacion, 
                    int id_operario, 
                    string id_equipo, 
                    string fechahora_equipo, 
                    string nom_operario)
        {
            ServiceReference1.grabarRePickingSustitutoRequest inValue = new ServiceReference1.grabarRePickingSustitutoRequest();
            inValue.Body = new ServiceReference1.grabarRePickingSustitutoRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_lotedetalle = id_lotedetalle;
            inValue.Body.id_oc = id_oc;
            inValue.Body.id_detalle_oc = id_detalle_oc;
            inValue.Body.id_resultado = id_resultado;
            inValue.Body.sku_original = sku_original;
            inValue.Body.ean_sustituto = ean_sustituto;
            inValue.Body.cant_pickeada = cant_pickeada;
            inValue.Body.cant_pendiente = cant_pendiente;
            inValue.Body.marca = marca;
            inValue.Body.precio = precio;
            inValue.Body.id_contenedor = id_contenedor;
            inValue.Body.id_ubicacion = id_ubicacion;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            inValue.Body.fechahora_equipo = fechahora_equipo;
            inValue.Body.nom_operario = nom_operario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).grabarRePickingSustitutoAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.valida_TodoFaltanteResponse> ServiceReference1.WebServiceXpickingSoap.valida_TodoFaltanteAsync(ServiceReference1.valida_TodoFaltanteRequest request)
        {
            return base.Channel.valida_TodoFaltanteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.valida_TodoFaltanteResponse> valida_TodoFaltanteAsync(int idLote, int idOperario)
        {
            ServiceReference1.valida_TodoFaltanteRequest inValue = new ServiceReference1.valida_TodoFaltanteRequest();
            inValue.Body = new ServiceReference1.valida_TodoFaltanteRequestBody();
            inValue.Body.idLote = idLote;
            inValue.Body.idOperario = idOperario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).valida_TodoFaltanteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getOperarioCodigoLocalResponse> ServiceReference1.WebServiceXpickingSoap.getOperarioCodigoLocalAsync(ServiceReference1.getOperarioCodigoLocalRequest request)
        {
            return base.Channel.getOperarioCodigoLocalAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getOperarioCodigoLocalResponse> getOperarioCodigoLocalAsync(int idOperario)
        {
            ServiceReference1.getOperarioCodigoLocalRequest inValue = new ServiceReference1.getOperarioCodigoLocalRequest();
            inValue.Body = new ServiceReference1.getOperarioCodigoLocalRequestBody();
            inValue.Body.idOperario = idOperario;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getOperarioCodigoLocalAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.updateCantPosicionResponse> ServiceReference1.WebServiceXpickingSoap.updateCantPosicionAsync(ServiceReference1.updateCantPosicionRequest request)
        {
            return base.Channel.updateCantPosicionAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.updateCantPosicionResponse> updateCantPosicionAsync(int id_lote, int cantidad, string operador, int id_operario, string id_equipo)
        {
            ServiceReference1.updateCantPosicionRequest inValue = new ServiceReference1.updateCantPosicionRequest();
            inValue.Body = new ServiceReference1.updateCantPosicionRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.cantidad = cantidad;
            inValue.Body.operador = operador;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).updateCantPosicionAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.updateDetalleOCSaltoResponse> ServiceReference1.WebServiceXpickingSoap.updateDetalleOCSaltoAsync(ServiceReference1.updateDetalleOCSaltoRequest request)
        {
            return base.Channel.updateDetalleOCSaltoAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.updateDetalleOCSaltoResponse> updateDetalleOCSaltoAsync(int idDetalleOC, int id_operario, string id_equipo)
        {
            ServiceReference1.updateDetalleOCSaltoRequest inValue = new ServiceReference1.updateDetalleOCSaltoRequest();
            inValue.Body = new ServiceReference1.updateDetalleOCSaltoRequestBody();
            inValue.Body.idDetalleOC = idDetalleOC;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).updateDetalleOCSaltoAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.ValidaOcContenedorResponse> ServiceReference1.WebServiceXpickingSoap.ValidaOcContenedorAsync(ServiceReference1.ValidaOcContenedorRequest request)
        {
            return base.Channel.ValidaOcContenedorAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.ValidaOcContenedorResponse> ValidaOcContenedorAsync(string contenedor, string ordenCompra, int id_operario, string id_equipo)
        {
            ServiceReference1.ValidaOcContenedorRequest inValue = new ServiceReference1.ValidaOcContenedorRequest();
            inValue.Body = new ServiceReference1.ValidaOcContenedorRequestBody();
            inValue.Body.contenedor = contenedor;
            inValue.Body.ordenCompra = ordenCompra;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).ValidaOcContenedorAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.ConsolidarJabaOCResponse> ServiceReference1.WebServiceXpickingSoap.ConsolidarJabaOCAsync(ServiceReference1.ConsolidarJabaOCRequest request)
        {
            return base.Channel.ConsolidarJabaOCAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.ConsolidarJabaOCResponse> ConsolidarJabaOCAsync(string contenedorOrigen, string contenedorDestino, string ordenCompra, int id_operario, string id_equipo)
        {
            ServiceReference1.ConsolidarJabaOCRequest inValue = new ServiceReference1.ConsolidarJabaOCRequest();
            inValue.Body = new ServiceReference1.ConsolidarJabaOCRequestBody();
            inValue.Body.contenedorOrigen = contenedorOrigen;
            inValue.Body.contenedorDestino = contenedorDestino;
            inValue.Body.ordenCompra = ordenCompra;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).ConsolidarJabaOCAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.validaContenedorMonoPickingResponse> ServiceReference1.WebServiceXpickingSoap.validaContenedorMonoPickingAsync(ServiceReference1.validaContenedorMonoPickingRequest request)
        {
            return base.Channel.validaContenedorMonoPickingAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.validaContenedorMonoPickingResponse> validaContenedorMonoPickingAsync(string cod_contenedor, int id_sucursal)
        {
            ServiceReference1.validaContenedorMonoPickingRequest inValue = new ServiceReference1.validaContenedorMonoPickingRequest();
            inValue.Body = new ServiceReference1.validaContenedorMonoPickingRequestBody();
            inValue.Body.cod_contenedor = cod_contenedor;
            inValue.Body.id_sucursal = id_sucursal;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).validaContenedorMonoPickingAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.updateContenedorResponse> ServiceReference1.WebServiceXpickingSoap.updateContenedorAsync(ServiceReference1.updateContenedorRequest request)
        {
            return base.Channel.updateContenedorAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.updateContenedorResponse> updateContenedorAsync(int id_sucursal, string CodigoContenedor, int Estado)
        {
            ServiceReference1.updateContenedorRequest inValue = new ServiceReference1.updateContenedorRequest();
            inValue.Body = new ServiceReference1.updateContenedorRequestBody();
            inValue.Body.id_sucursal = id_sucursal;
            inValue.Body.CodigoContenedor = CodigoContenedor;
            inValue.Body.Estado = Estado;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).updateContenedorAsync(inValue);
        }
        
        public System.Threading.Tasks.Task<int> updateContenedoresOCAsync(int id_oc, int id_estado)
        {
            return base.Channel.updateContenedoresOCAsync(id_oc, id_estado);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.validaContenedorRePickingWSResponse> ServiceReference1.WebServiceXpickingSoap.validaContenedorRePickingWSAsync(ServiceReference1.validaContenedorRePickingWSRequest request)
        {
            return base.Channel.validaContenedorRePickingWSAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.validaContenedorRePickingWSResponse> validaContenedorRePickingWSAsync(int id_lote, int id_lotedetalle, string id_contenedor, int id_operario, string id_equipo)
        {
            ServiceReference1.validaContenedorRePickingWSRequest inValue = new ServiceReference1.validaContenedorRePickingWSRequest();
            inValue.Body = new ServiceReference1.validaContenedorRePickingWSRequestBody();
            inValue.Body.id_lote = id_lote;
            inValue.Body.id_lotedetalle = id_lotedetalle;
            inValue.Body.id_contenedor = id_contenedor;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).validaContenedorRePickingWSAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getPickingPendienteXLoteResponse> ServiceReference1.WebServiceXpickingSoap.getPickingPendienteXLoteAsync(ServiceReference1.getPickingPendienteXLoteRequest request)
        {
            return base.Channel.getPickingPendienteXLoteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getPickingPendienteXLoteResponse> getPickingPendienteXLoteAsync(int idLote)
        {
            ServiceReference1.getPickingPendienteXLoteRequest inValue = new ServiceReference1.getPickingPendienteXLoteRequest();
            inValue.Body = new ServiceReference1.getPickingPendienteXLoteRequestBody();
            inValue.Body.idLote = idLote;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getPickingPendienteXLoteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getDetalleArticuloOCResponse> ServiceReference1.WebServiceXpickingSoap.getDetalleArticuloOCAsync(ServiceReference1.getDetalleArticuloOCRequest request)
        {
            return base.Channel.getDetalleArticuloOCAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getDetalleArticuloOCResponse> getDetalleArticuloOCAsync(int idLote, int id_oc, string nro_oc, string sku, int id_operario, string id_equipo)
        {
            ServiceReference1.getDetalleArticuloOCRequest inValue = new ServiceReference1.getDetalleArticuloOCRequest();
            inValue.Body = new ServiceReference1.getDetalleArticuloOCRequestBody();
            inValue.Body.idLote = idLote;
            inValue.Body.id_oc = id_oc;
            inValue.Body.nro_oc = nro_oc;
            inValue.Body.sku = sku;
            inValue.Body.id_operario = id_operario;
            inValue.Body.id_equipo = id_equipo;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getDetalleArticuloOCAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.getSKUImageByteResponse> ServiceReference1.WebServiceXpickingSoap.getSKUImageByteAsync(ServiceReference1.getSKUImageByteRequest request)
        {
            return base.Channel.getSKUImageByteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.getSKUImageByteResponse> getSKUImageByteAsync(string SKU)
        {
            ServiceReference1.getSKUImageByteRequest inValue = new ServiceReference1.getSKUImageByteRequest();
            inValue.Body = new ServiceReference1.getSKUImageByteRequestBody();
            inValue.Body.SKU = SKU;
            return ((ServiceReference1.WebServiceXpickingSoap)(this)).getSKUImageByteAsync(inValue);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.WebServiceXpickingSoap))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            if ((endpointConfiguration == EndpointConfiguration.WebServiceXpickingSoap12))
            {
                System.ServiceModel.Channels.CustomBinding result = new System.ServiceModel.Channels.CustomBinding();
                System.ServiceModel.Channels.TextMessageEncodingBindingElement textBindingElement = new System.ServiceModel.Channels.TextMessageEncodingBindingElement();
                textBindingElement.MessageVersion = System.ServiceModel.Channels.MessageVersion.CreateVersion(System.ServiceModel.EnvelopeVersion.Soap12, System.ServiceModel.Channels.AddressingVersion.None);
                result.Elements.Add(textBindingElement);
                System.ServiceModel.Channels.HttpTransportBindingElement httpBindingElement = new System.ServiceModel.Channels.HttpTransportBindingElement();
                httpBindingElement.AllowCookies = true;
                httpBindingElement.MaxBufferSize = int.MaxValue;
                httpBindingElement.MaxReceivedMessageSize = int.MaxValue;
                result.Elements.Add(httpBindingElement);
                return result;
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.WebServiceXpickingSoap))
            {
                return new System.ServiceModel.EndpointAddress("http://172.22.2.210/WebServiceXpicking/WebServiceXpicking.asmx");
            }
            if ((endpointConfiguration == EndpointConfiguration.WebServiceXpickingSoap12))
            {
                return new System.ServiceModel.EndpointAddress("http://172.22.2.210/WebServiceXpicking/WebServiceXpicking.asmx");
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }
        
        public enum EndpointConfiguration
        {
            
            WebServiceXpickingSoap,
            
            WebServiceXpickingSoap12,
        }
    }
}
