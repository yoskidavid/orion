﻿using HT.Data.Db;
using HT.Data.Generic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DataContract]
    [DbCommand("WS_SOL_InsertCabeceraOrden_F")]
    public class OrderRequestModel
    {
        [DataMember(Name = "orderId")]
        [DbParameter("@PIV_NUMEROORDEN", DbType = DbType.String)]
        public string OrderNumber { get; set; }

        [DataMember]
        public string Pais { get; set; }

        //   [DataMember(Name = "usrId")]
        public string UserID { get; set; } = "";
        [DataMember]
        public string UserSystem { get; set; }
        [IgnoreDataMember]
        [DbParameter("@PIV_RUTCLIENTE", DbType = DbType.String)]
        public string CustomerDocumentNumber => Client.CustomerDocumentNumber;
        
        [IgnoreDataMember]
        [DbParameter("@PIV_OBSDESPACHO", DbType = DbType.String)]
        public string DispatchObservation => Client.DispatchObservation;

        [IgnoreDataMember]
        [DbParameter("@PIV_OBSENTREGA", DbType = DbType.String)]
        public string DeliveryObservation => Client.DeliveryObservation;

        [IgnoreDataMember]
        [DbParameter("@PIV_RUC", DbType = DbType.String)]
        public string CompanyIdentity => Client.CompanyIdentity;

        [IgnoreDataMember]
        [DbParameter("@PIV_RAZONSOCIAL", DbType = DbType.String)]
        public string CompanyName => Client.CompanyName;

        [IgnoreDataMember]
        [DbParameter("@PIV_SERVICIO", DbType = DbType.String)]
        public string ServiceType => Client.ServiceType;

        [IgnoreDataMember]
        [DbParameter("@PII_NUMEROLOCAL", DbType = DbType.Int32)]
        public int LocalCode => Client.StoreID;

        [IgnoreDataMember]
        [DbParameter("@PIV_REFERENCIA", DbType = DbType.String)]
        public string DispatchReference => Client.DispatchReference;

        [IgnoreDataMember]
        [DbParameter("@PIV_TIENDADERECOJO", DbType = DbType.String)]
        public string PickUpPoint { get => Client.PickUpPoint; }

        [IgnoreDataMember]
        [DbParameter("@PIV_NOMRECEPTOR", DbType = DbType.String)]
        public string ReceptorFullName { get => Client.ReceptorFullName; }

        [IgnoreDataMember]
        [DbParameter("@PIV_DOCRECEPTOR", DbType = DbType.String)]
        public string ReceptorDocumentNumber { get => Client.ReceptorDocumentNumber; }

        [IgnoreDataMember]
        [DbParameter("@PII_CUOTAS", DbType = DbType.Int32)]
        public int Installments { get => Payment.Installments; }

        [IgnoreDataMember]
        [DbParameter("@PIV_MEDIOPAGO", DbType = DbType.String)]
        public string PaymentType { get => Payment.Payment; }

        [IgnoreDataMember]
        [DbParameter("@PID_FECHACOMPRA", DbType = DbType.DateTime)]
        public DateTime TransactionDate { get => Payment.TransactionDate; }

        [IgnoreDataMember]
        [DbParameter("@PIV_CUPON", DbType = DbType.String)]
        public string DiscountCoupon { get => Client.DiscountCoupon; }

        [IgnoreDataMember]
        [DbParameter("@PIN_FLETE", DbType = DbType.Decimal)]
        public decimal TotalPriceDispatch { get => Payment.TotalPriceDispatch; }

        [IgnoreDataMember]
        [DbParameter("@PIF_MONTOTOTAL", DbType = DbType.Decimal)]
        public decimal TotalAmount { get => Payment.TotalAmount; }

        [IgnoreDataMember]
        [DbParameter("@PIF_MONTODESPACHO", DbType = DbType.Decimal)]
        public decimal DispatchAmount { get => 0; }

        [IgnoreDataMember]
        [DbParameter("@PID_FECHAHORAINIDESP", DbType = DbType.DateTime)]
        public DateTime DeliveryDate => DateTime.Parse($"{Client.DateDispatch.ToString("yyyy-MM-dd")} {Client.HourDispatch.Replace("Hrs", "", StringComparison.InvariantCulture).Split("-")[0]}:00", CultureInfo.InvariantCulture);

        [IgnoreDataMember]
        [DbParameter("@PID_FECHAHORAFINDESP", DbType = DbType.DateTime)]
        public DateTime DispatchDate => DateTime.Parse($"{Client.DateDispatch.ToString("yyyy-MM-dd")} {Client.HourDispatch.Replace("Hrs", "", StringComparison.InvariantCulture).Split("-")[1]}:00", CultureInfo.InvariantCulture);

        [IgnoreDataMember]
        [DbParameter("@PIV_LUGARDESPACHO", DbType = DbType.String)]
        public string DispatchPlace => Client.DispatchAddress;

        [IgnoreDataMember]
        [DbParameter("@PIV_COMUNADESPACHO", DbType = DbType.String)]
        public string DispatchCommunityName => Client.Comuna;

        [IgnoreDataMember]
        [DbParameter("@PIV_CIUDADESPACHO", DbType = DbType.String)]
        public string DispatchCityName => Client.ProvinceName;

        [IgnoreDataMember]
        [DbParameter("@PIV_CODVENDEDOR", DbType = DbType.String)]
        public string SellerCode => Client.SellerCode;

        private ClientRequestModel m_clientRequestModel;
        private PaymentRequestModel m_paymentInternal;
        private InsertOrderQueueRequestModel m_insertOrderQueueInternal;
        private InsertValidateUpdateRequestModel m_insertaValida;
        private InsertOrdenXMLRequestModel m_InsertOrdenXML;
        private SubOrderCreateRequestModel m_subOrden;
        private WS_API_updOCGeoreferenceRequestModel m_WS_API_updOCGeoreferencia;
        private DeriveCDRequestModel m_derivaCD;
        
        [DataMember]
        public ClientRequestModel Client => this.EnsureInstance(ref m_clientRequestModel);

        [DataMember]
        public InsertOrderQueueRequestModel InsertOrderQueue => this.EnsureInstance(ref m_insertOrderQueueInternal);

        [DataMember]
        public PaymentRequestModel Payment => this.EnsureInstance(ref m_paymentInternal);

        [IgnoreDataMember]
        public InsertValidateUpdateRequestModel InsertaValida => this.EnsureInstance(ref m_insertaValida);

        [IgnoreDataMember]
        public InsertOrdenXMLRequestModel InsertOrdenXML => this.EnsureInstance(ref m_InsertOrdenXML);

        [IgnoreDataMember]
        public SubOrderCreateRequestModel SubOrden => this.EnsureInstance(ref m_subOrden);

        [IgnoreDataMember]
        public WS_API_updOCGeoreferenceRequestModel WS_API_updOCGeoreferencia => this.EnsureInstance(ref m_WS_API_updOCGeoreferencia);

        [IgnoreDataMember]
        public DeriveCDRequestModel DerivaCD => this.EnsureInstance(ref m_derivaCD);

        #region Products

        [DataMember]
        public IEnumerable<ProductRequestModel> Products
        {
            get
            {
                ProductsEnsure();

                return ProductsInternal;
            }
        }

        #endregion

        private ModelChildren<OrderRequestModel, ProductRequestModel> ProductsInternal { get; set; }

        #region FolioEstadoOrden

        [IgnoreDataMember]
        public FolioStateOrdenCrearRequestModel FolioEstadoOrden
        {
            get
            {
                FolioEstadoOrdenEnsure();

                return FolioEstadoOrdenInternal;
            }
        }

        #endregion

        private FolioStateOrdenCrearRequestModel FolioEstadoOrdenInternal { get; set; }

        #region Folios

        [DataMember]
        public IEnumerable<FolioGeneraRequestModel> Folios
        {
            get
            {
                FoliosEnsure();

                return FoliosInternal;
            }
        }

        #endregion

        private ModelChildren<OrderRequestModel, FolioGeneraRequestModel> FoliosInternal { get; set; }
        
        [IgnoreDataMember]
        [DbParameter("@POI_IDOC", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int? OrderID { get; set; }

        [DataMember]
        public int OrderTypeID { get; set; }

        #region FolioEstadoOrdenEnsure

        protected void FolioEstadoOrdenEnsure()
        {
            if (object.Equals(FolioEstadoOrdenInternal, null))
            {
                FolioEstadoOrdenInternal = new FolioStateOrdenCrearRequestModel(this);
            }
        }

        #endregion

        #region FoliosEnsure

        protected void FoliosEnsure()
        {
            if (object.Equals(FoliosInternal, null))
            {
                FoliosInternal = new ModelChildren<OrderRequestModel, FolioGeneraRequestModel>(this);
            }
        }

        #endregion
        
        #region ProductsEnsure

        protected void ProductsEnsure()
        {
            if (object.Equals(ProductsInternal, null))
            {
                ProductsInternal = new ModelChildren<OrderRequestModel, ProductRequestModel>(this);
            }
        }

        #endregion
    }
}
