﻿using System;

namespace HT
{
    public static class IStringExtension
    {
        public static string ToCamelCase(this string value)
        {
            if (!string.IsNullOrEmpty(value) && value.Length > 1)
            {
                return Char.ToLowerInvariant(value[0]) + value.Substring(1);
            }
            return value;
        }
    }
}
