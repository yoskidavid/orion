﻿using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    public class MailNotificationResponseModel
    {
        [DbColumn(Order = 0, Name = "ID_Notif")]
        public int IDNotification { get; set; }
        
        [DbColumn(Order = 1, Name = "Codigo")]
        public string Code { get; set; }
        
        [DbColumn(Order = 2, Name = "Aplicacion")]
        public string Application { get; set; }

        [DbColumn(Order = 3, Name = "Descripcion")]
        public string Description { get; set; }

        [DbColumn(Order = 4, Name = "Asunto")]
        public string Subject { get; set; }

        [DbColumn(Order = 5, Name = "Cuerpo")]
        public string Body { get; set; }

        [DbColumn(Order = 6, Name = "MailFrom")]
        public string MailFrom { get; set; }

        [DbColumn(Order = 7, Name = "HTML")]
        public bool HTML { get; set; }

        [DbColumn(Order = 8, Name = "Prioridad")]
        public short Priority { get; set; }

        [DbColumn(Order = 9, Name = "NameFrom")]
        public string NameFrom { get; set; }
    }
}
