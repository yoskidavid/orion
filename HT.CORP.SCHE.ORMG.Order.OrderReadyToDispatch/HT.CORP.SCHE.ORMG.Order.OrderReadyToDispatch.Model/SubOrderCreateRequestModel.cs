﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DbCommand("ORD_INSERTA_SUB_ORDEN")]
    public class SubOrderCreateRequestModel : ModelChild<OrderRequestModel>
    {
        public SubOrderCreateRequestModel()
            : this(null)
        { }

        public SubOrderCreateRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID { get => Parent.OrderID; }
    }
}
