﻿using System.Collections.Generic;

namespace HT.Configuration
{
    public class DbSettings
    {
        public IDictionary<string, DbConnectionSettings> Connections { get; set; }
    }
}
