﻿using HT.Api;
using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;

namespace HT.Order.Api.Controllers
{
    public abstract class ApiControllerBaseType : BaseController
    {
        protected ApiControllerBaseType(IServiceProvider services, ILogger logger, IDatabase<DbDispatch> dbDispatch)
            : base(services, logger)
        {
            DbDispatch = dbDispatch;
        }

        protected IDatabase DbDispatch { get; set; }
    }
}