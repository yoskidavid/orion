﻿using HT.Data.Db;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HT.Shared.Model
{
    [DbCommand("ADM_lisTABLAGENERAL")]
    public class TablaGeneralRequestModel
    {
        public TablaGeneralRequestModel() { }

        public TablaGeneralRequestModel(int flag,string pCOD_TABLA) {
            this.pFLAG = flag;
            this.pCOD_TABLA = pCOD_TABLA;
        }

        [DbParameter("@pFLAG", DbType = DbType.Int32)]
        public int pFLAG { get; set; }

        [DbParameter("@pIDTABLAGENERAL", DbType = DbType.Int32)]
        public int pIDTABLAGENERAL { get => 0; }

        [DbParameter("@pCOD_TABLA", DbType = DbType.String,Size = 20)]
        public string pCOD_TABLA { get; set; }

        [DbParameter("@pCOD_CAMPO", DbType = DbType.String, Size = 20)]
        public string pCOD_CAMPO { get => ""; }

        [DbParameter("@pCOD_INTERNO", DbType = DbType.String, Size = 20)]
        public string pCOD_INTERNO { get => ""; } 

        [DbParameter("@pDESCRIPCION", DbType = DbType.String, Size = 200)]
        public string pDESCRIPCION { get => ""; }

        [DbParameter("@pVALOR1", DbType = DbType.String, Size = 254)]
        public string pVALOR1 { get => ""; }

        [DbParameter("@pVALOR2", DbType = DbType.String, Size = 254)]
        public string pVALOR2 { get => ""; }

        [DbParameter("@pVALOR3", DbType = DbType.String, Size = 254)]
        public string pVALOR3 { get => ""; }

        [DbParameter("@pORDEN", DbType = DbType.Int32)]
        public int pORDEN { get => 0; }

        [DbParameter("@pFLG_SISTEMA", DbType = DbType.Boolean)]
        public bool pFLG_SISTEMA { get => true; }

        [DbParameter("@pHABILITADO", DbType = DbType.Boolean)]
        public bool pHABILITADO { get => true; }

        [DbParameter("@pCREADOPOR", DbType = DbType.String,Size = 25)]
        public string pCREADOPOR { get => ""; }

        [DbParameter("@pFECHACREACION", DbType = DbType.DateTime)]
        public DateTime pFECHACREACION { get => DateTime.Now; }  

        [DbParameter("@pXml", DbType = DbType.String, Size = 8000)]
        public string pXml { get => ""; }
    }
}
