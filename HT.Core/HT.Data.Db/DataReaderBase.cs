﻿using System.Data;

namespace HT.Data.Db
{
    public abstract class DataReaderBase<T>
    {
        protected DataReaderBase (IDataReader reader)
        {
            this.Reader = reader;
        }

        protected IDataReader Reader { get; private set; }
    }
}
