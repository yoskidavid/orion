﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DbCommand("ORD_INSERTA_SUB_ORDEN")]
    public class SubOrderCreaRequestModel : ModelChild<OrderRequestModel>
    {
        public SubOrderCreaRequestModel()
            : this(null)
        { }

        public SubOrderCreaRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;
    }
}
