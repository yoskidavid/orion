﻿using HT.Data.Db;

namespace HT.Shared.Model
{
    public class ParameterResponseModel
    {
        [DbColumn(Order = 0, Name = "CPARAM")]
        public string CPARAM { get; set; }

        [DbColumn(Order = 1, Name = "XPARAM_VALOR")]
        public string XPARAM_VALOR { get; set; }

        [DbColumn(Order = 2, Name = "XPARAM_DESC")]
        public string XPARAM_DESC { get; set; }
    }
}
