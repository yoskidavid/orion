﻿using System;
using System.Runtime.Serialization;

namespace HT.Data.Generic
{
    [DataContract]
    public class Request<T> : IRequest<T>
    {
        public Request()
        { }

        [DataMember]
        public T Body
        {
            get
            {
                EnsureInnerBody();

                return InnerBody;
            }
        }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        private T InnerBody { get; set; }

        private void EnsureInnerBody()
        {
            if (Equals(InnerBody, null))
            {
                InnerBody = Activator.CreateInstance<T>();
            }
        }

        object IRequest.Body =>
            Body;
    }
}
