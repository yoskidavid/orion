﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DbCommand("[PICK_ACTUALIZAR_SUB_ORDEN]")]
    public class PICK_ACTUALIZAR_SUB_ORDEN
    {
        [DbParameter("@pFLAG", DbType = DbType.Int16)]
        public short Flag { get; set; }

        [DbParameter("@pIDSUB_ORDEN", DbType = DbType.Int32)]
        public int SubOrderID { get; set; }

        [DbParameter("@pID_ESTADO", DbType = DbType.Int32)]
        public int Status { get; set; }

        [DbParameter("@pCREADOPOR", DbType = DbType.String, Size = 25)]
        public string UserID   { get; set; }

        [DbParameter("@pXml", DbType = DbType.String)]
        public string Xml => "";

    }
}
