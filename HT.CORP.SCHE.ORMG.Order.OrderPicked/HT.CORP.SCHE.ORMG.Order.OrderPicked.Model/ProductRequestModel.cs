﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract(Name = "Producto")]
    [DbCommand("ORD_INSERTDETALLEORDEN_F")]
    public class ProductRequestModel : ModelChild<OrderRequestModel>
    {
        public ProductRequestModel() : this(null)
        { }

        public ProductRequestModel(OrderRequestModel parent)
            : base(parent)
        { }


        private string skuCode;
        private string eanCode;
        private string subClass;

        [IgnoreDataMember]
        [DbParameter("@PII_ID_OC", DbType = DbType.Int32)]
        public int? OrderID { get => Parent.OrderID; }

        [DataMember(Name = "skuCode")]
        [DbParameter("@PIV_CODIGOSKU", DbType = DbType.String, Size = 10)]
        public string Sku { get { return skuCode.Trim(); } set { skuCode = value.Trim(); } }

        [DataMember(Name = "ean")]
        [DbParameter("@PIV_CODIGOEAN", DbType = DbType.String, Size = 20)]
        public string Ean { get { return eanCode.Trim(); } set { eanCode = value.Trim(); } }

        // VALIDAR
        [IgnoreDataMember]
        [DbParameter("@PIV_TIPOMEDIDA", DbType = DbType.String, Size = 1)]
        public string ProductType { get => "P"; }

        [DataMember]
        [DbParameter("@PIV_NOMBREPRODUCTO", DbType = DbType.String, Size = 150)]
        public string SkuDescription { get; set; }

        [DataMember]
        [DbParameter("@PIV_MARCA", DbType = DbType.String, Size = 20)]
        public string Brand { get; set; }

        [DataMember]
        [DbParameter("@PIV_MODELO", DbType = DbType.String, Size = 25)]
        public string Format { get; set; }

        [DataMember]
        [DbParameter("@PIV_CODIGOLINEA", DbType = DbType.String, Size = 15)]
        public string SubClass { get { return subClass.Trim(); } set { subClass = value.Trim(); } }

        [DataMember]
        [DbParameter("@PIV_DESCLINEA", DbType = DbType.String, Size = 50)]
        public string SubClassDescription { get; set; }

        [DataMember]
        [DbParameter("@PIF_PRECIOLISTA", DbType = DbType.Decimal)]
        public decimal ListPrice { get; set; }

        [DataMember]
        [DbParameter("@PIF_DESCUENTO", DbType = DbType.Decimal)]
        public decimal Discount { get; set; }

        [DataMember]
        [DbParameter("@PIF_PRECIOUNITARIO", DbType = DbType.Decimal)]
        public decimal UnitPrice { get; set; }

        [DataMember]
        [DbParameter("@PIF_CANTIDAD", DbType = DbType.Decimal)]
        public decimal Quantity { get; set; }

        // UN | KG
        [DataMember(Name = "unit")]
        [DbParameter("@PIV_UNIDAD", DbType = DbType.String, Size = 5)]
        public string UnitMeasure { get; set; }

        [DataMember]
        [DbParameter("@PIF_PRECIOTOTAL", DbType = DbType.Decimal)]
        public decimal TotalPrice { get; set; }

        [DataMember(Name = "promotionId")]
        [DbParameter("@PIV_IDPROMOCION", DbType = DbType.String, Size = 50)]
        public string PromotionID { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PIV_COMENTARIO", DbType = DbType.String, Size = 100)]
        public string SubstitutionCriteria => "No reemplazar";

        [IgnoreDataMember]
        [DbParameter("@PIV_NOTAS_PICKING", DbType = DbType.String, Size = 100)]
        public string PickingNote => string.Empty;

        private PickRecordManualDetailPickingIURequestModel m_pickManualRegistrationDetailPickingIU;
        private PickRecordManualResultUIRequestModel m_pickRecordManualResultUI;

        [IgnoreDataMember]
        public PickRecordManualDetailPickingIURequestModel PickManualRegistrationDetailPickingIU => this.EnsureInstance(ref m_pickManualRegistrationDetailPickingIU);

        [IgnoreDataMember]
        public PickRecordManualResultUIRequestModel PickRecordManualResultUI => this.EnsureInstance(ref m_pickRecordManualResultUI);
    }
}
