﻿using HT.Database;
using HT.Database.Generic;
using HT.Order.Model;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace HT.Order.Logic
{
    public class SolicitudOCLogic : OrderLogicBaseType
    {
        public SolicitudOCLogic(IServiceProvider services, ILogger<SolicitudOCLogic> logger, IDatabase<DbDispatch> dbDispatch)
          : base(services, logger, dbDispatch)
        {
        }

        public SolicitudOCActualizarEstadoResponseModel ActualizarEstadoSolicitudOC(SolicitudOCActualizarEstadoRequestModel request)
        {
            string userName = "HT.Order";
            SolicitudOCCrearModel resCrear = CrearSolicitudOC(request.NumeroPedido, userName);
            SolicitudOCActualizarEstadoResponseModel objResponse = new SolicitudOCActualizarEstadoResponseModel();

            if (resCrear.ErrorCodigo == 0)
            {
                SolicitudOCActualizarEstadoInnerRequestModel InnerRequest = new SolicitudOCActualizarEstadoInnerRequestModel();
                InnerRequest = ActualizarEstadoSolicitudOC(request, userName);

                objResponse.NumeroPedido = request.NumeroPedido;
                objResponse.CodigoActEstado = InnerRequest.ErrorCodigo;
                objResponse.Observacion = InnerRequest.ErrorCodigo == "0" ? "Éxito" : InnerRequest.ErrorMensaje;
            }
            else
            {
                objResponse.NumeroPedido = request.NumeroPedido;
                objResponse.CodigoActEstado = resCrear.ErrorCodigo.ToString();
                objResponse.Observacion = resCrear.ErrorMensaje;
            }

            return objResponse;
        }

        private SolicitudOCCrearModel CrearSolicitudOC(string NumeroPedido, string userName)
        {
            SolicitudOCCrearModel objSolicitudOCCrear = new SolicitudOCCrearModel();
            objSolicitudOCCrear.NumeroOrden = NumeroPedido;
            objSolicitudOCCrear.CreadoPor = userName;

            Logger.Log(LogLevel.Information, "******** Inicio de Creación de Solicitud OC ********");
            Logger.Log(LogLevel.Information,"N° Orden = {0}, Creado Por = {1}", objSolicitudOCCrear.NumeroOrden, objSolicitudOCCrear.CreadoPor);

            try
            {
                DbDispatch.ExecuteNonQuery(objSolicitudOCCrear);
                Logger.Log(LogLevel.Information, "IDSOLICITUD_OC = {0}, ERROR_CODIGO = {1}, ERROR_MENSAJE = {2}", objSolicitudOCCrear.IDSolicitudOC, objSolicitudOCCrear.ErrorCodigo, objSolicitudOCCrear.ErrorMensaje);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error,ex, "Creación de Solicitud OC", objSolicitudOCCrear);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de Creación de Solicitud OC ********");
            }

            return objSolicitudOCCrear;
        }
        private SolicitudOCActualizarEstadoInnerRequestModel ActualizarEstadoSolicitudOC(SolicitudOCActualizarEstadoRequestModel request, string userName)
        {
            SolicitudOCActualizarEstadoInnerRequestModel InnerRequest = new SolicitudOCActualizarEstadoInnerRequestModel();
            InnerRequest.NumeroOrden = request.NumeroPedido;
            InnerRequest.NumeroPos = string.Empty;
            InnerRequest.NumeroTienda = string.Empty;
            InnerRequest.IdCajero = string.Empty;
            InnerRequest.IdTGEstado = request.EstadoPedido;
            InnerRequest.IdTGObservacion = request.Observacion;
            InnerRequest.IdTGRequest = GetXMLFromObject(request);
            InnerRequest.ModificadoPor = userName;
            InnerRequest.FlgPickingPos = true;

            Logger.Log(LogLevel.Information, "******** Inicio de Actualización de Estado de Solicitud OC ********");
            Logger.Log(LogLevel.Information, "Nº Orden = {0}, Estado Pedido = {1}, Observación = {2}, XmlRequest = {3}", InnerRequest.NumeroOrden, InnerRequest.IdTGEstado, InnerRequest.IdTGObservacion, InnerRequest.IdTGRequest);

            try
            {
                DbDispatch.ExecuteNonQuery(InnerRequest);
                Logger.Log(LogLevel.Information, "ERROR_CODIGO = {0}, ERROR_MENSAJE = {1}", InnerRequest.ErrorCodigo, InnerRequest.ErrorMensaje);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex, "Actualización de Estado de Solicitud OC", InnerRequest);
            }
            finally
            {
                Logger.Log(LogLevel.Information, "******** Fin de Actualización de Estado de Solicitud OC ********");
            }

            return InnerRequest;
        }
        private string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }
    }
}