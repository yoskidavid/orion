﻿using System;
using System.Runtime.Serialization;

namespace HT.Data.Generic
{
    [DataContract]
    public class ResponseEntity<T> : Response, IResponseEntity<T>
    {
        public ResponseEntity(T body) :
            base(null)
        {
            Body = body;
        }

        public ResponseEntity(Exception exception) :
            base(exception)
        { }

        [DataMember]
        public T Body { get; private set; }

        object IResponseEntity.Body =>
            Body;
    }
}
