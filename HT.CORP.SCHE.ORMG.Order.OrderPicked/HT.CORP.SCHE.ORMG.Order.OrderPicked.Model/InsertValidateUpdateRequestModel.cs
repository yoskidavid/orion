﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[ORD_INSERTORDENCOLAMASIVO]")]
    public class InsertValidateUpdateRequestModel : ModelChild<OrderRequestModel>
    {
        public InsertValidateUpdateRequestModel()
            : this(null)
        { }

        public InsertValidateUpdateRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@PIV_NUMEROORDEN", DbType = DbType.String, Size = 50)]
        public string OrderNumber => Parent.OrderNumber;

        [DbParameter("@PII_ID_USUARIO", DbType = DbType.Int32)]
        public string UserID => Parent.UserID;

        [DbParameter("@PII_IDTG_TIPO", DbType = DbType.Int32)]
        public int OrderTypeID => Parent.OrderTypeID;


        [DbParameter("@VAL", DbType = DbType.Int32)]
        public int ValidaFolio => 1;
    }
}
