﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[PICK_BuscarDetOC_PorIdOC]")]
    public class PickRecordManualFindDetOC_PorIdOC_NewRequestModel : ModelChild<OrderRequestModel>
    {
        public PickRecordManualFindDetOC_PorIdOC_NewRequestModel()
            : this(null)
        { }

        public PickRecordManualFindDetOC_PorIdOC_NewRequestModel(OrderRequestModel parent)
            : base(parent)
        { }


        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;
    }
}
