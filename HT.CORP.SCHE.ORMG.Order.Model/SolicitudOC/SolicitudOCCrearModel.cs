﻿using HT.Data.Db;
using System.Data;
using System.Runtime.Serialization;

namespace HT.Order.Model
{
    [DataContract]
    [DbCommand("WS_SOL_CreateSolicitudOC")]
    public class SolicitudOCCrearModel
    {
        [DataMember(Order = 0)]
        [DbParameter("@NUMERO_ORDEN", DbType = DbType.String)]
        public string NumeroOrden { get; set; }

        [DataMember(Order = 1)]
        [DbParameter("@CREADOPOR", DbType = DbType.String)]
        public string CreadoPor { get; set; }

        [DataMember(Order = 2)]
        [DbParameter("@IDSOLICITUD_OC", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int IDSolicitudOC { get; set; }

        [DataMember(Order = 3)]
        [DbParameter("@ERROR_CODIGO", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int ErrorCodigo { get; set; }

        [DataMember(Order = 4)]
        [DbParameter("@ERROR_MENSAJE", DbType = DbType.String, Direction = ParameterDirection.Output)]
        public string ErrorMensaje { get; set; }
    }
}
