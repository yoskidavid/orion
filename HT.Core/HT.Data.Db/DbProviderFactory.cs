﻿using HT.Configuration;
using System.Data;
using DbCommon = System.Data.Common;

namespace HT.Data.Db
{
    public static class DbProviderFactory
    {
        public static IDbConnection CreateConnection(DbConnectionSettings dbConnectionSettings)
        {
            string
                providerName = dbConnectionSettings.Provider,
                connectionString = dbConnectionSettings.ConnectionString;

            DbCommon.DbProviderFactory dbProviderFactory = DbCommon.DbProviderFactories.GetFactory(providerName);

            IDbConnection dbConnection = dbProviderFactory.CreateConnection();

            dbConnection.ConnectionString = connectionString;

            return dbConnection;
        }
    }
}
