﻿using HT.Data.Db;
using HT.Data.Generic;
using System;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DataContract]
    [DbCommand("FOL_INSERTFOLIOLOGESTADO")]
    public class FolioStateLogGenerateRequestModel : ModelChild<FolioGeneraRequestModel>
    {
        public FolioStateLogGenerateRequestModel()
            : this(null)
        { }

        public FolioStateLogGenerateRequestModel(FolioGeneraRequestModel parent)
            : base(parent)
        { }

        [IgnoreDataMember]
        [DbParameter("@PIV_NUMEROFOLIO", DbType = DbType.String, Size = 50)]
        public string FolioNumber { get => Parent.FolioNumber; }

        [IgnoreDataMember]
        public int Correlativo { get => 1; }

        [IgnoreDataMember]
        [DbParameter("@PIV_ESTADO", DbType = DbType.String, Size = 30)]
        public string Status { get => Parent.Status; }

        [IgnoreDataMember]
        public DateTime FechaRegistro { get => DateTime.Now; }

        [IgnoreDataMember]
        [DbParameter("@PII_IDFOLIOLOGXML", DbType = DbType.Int32)]
        public int IdFolioLogXml=> 0;
    }
}
