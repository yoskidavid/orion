﻿using System.Runtime.Serialization;
using HT.Data.Db;

namespace HT.Dashboard.Model.MonitorATG
{
    [DataContract]
    public class SemaforoResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "SERVICIO")]
        public int idServicio { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "ESTADO")]
        public int idEstado { get; set; }
        [DataMember]
        [DbColumn(Order = 2, Name = "NOMBRE_ESTADO")]
        public string nombreEstado { get; set; }
        [DataMember]
        [DbColumn(Order = 3, Name = "LIMITE_INFERIOR")]
        public double limiteInferior { get; set; }

        [DataMember]
        [DbColumn(Order = 4, Name = "LIMITE_SUPERIOR")]
        public double limiteSuperior { get; set; }
        [DataMember]
        [DbColumn(Order = 5, Name = "DESCRIPCION")]
        public string semaforo { get; set; }
 
    }
}