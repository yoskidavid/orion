﻿using HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Logic;
using HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model;
using HT.Data;
using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Api.Api.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    [ProducesResponseType(500)]
    [EnableCors("AllowSpecificOrigin")]
    [Route("/rtl/tot/corp/SCHE/ORMG/order/orderReadyToDispatch")]
    [ApiController]
    public class OrderReadyToDispatchController : DispacthControllerBaseType
    {
        public IConfiguration _configuration;

        public OrderReadyToDispatchController(IServiceProvider services, ILogger<OrderReadyToDispatchController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Create the Purchase Order
        /// </summary>
        /// <param name="order">Business object that represents the Purchase Order</param>
        /// <returns></returns>
        [HttpPost]
        [ShortCircuitingResourceFilter]
        public async Task<IActionResult> Post([FromBody]OrderRequestModel order)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            Logger.LogInformation(" ".PadLeft(20, '='));
            Logger.LogInformation($"Inicio de Inyección de Orden {order.OrderNumber} ");
            string signature = Guid.NewGuid().ToString().ToLower();
            IResponseNonQuery response;
            string status = "";
            try
            {
                string codigo_local = _configuration.GetSection("codigolocal").Value;

                order.OrderTypeID = Convert.ToInt32(_configuration.GetSection("IdTipoOC").Value);
                order.UserID = _configuration.GetSection("IdUsuario").Value;
                order.Pais = _configuration.GetSection("pais").Value;
                //order.Client.StoreID = _configuration.GetSection("codigo_local").Value == "" ? order.Client.StoreID : Convert.ToInt32(_configuration.GetSection("codigo_local").Value);
                var parameterModel = await Services.GetService<OrderLogic>().GetFlagOrionSucursal();
                
                if (parameterModel.XPARAM_VALOR.ToString() == "0")
                {
                    // do nothing ()
                }
                else if (parameterModel.XPARAM_VALOR.ToString() == "1")
                {
                    order.Client.StoreID = Convert.ToInt32(codigo_local);

                    if (order.Folios != null)
                    {
                        foreach (var folio in order.Folios)
                        {
                            folio.dispatchWh = Convert.ToInt32(codigo_local);
                        }
                    }

                } 

               
                response = await Services.GetService<OrderLogic>().CreateNewOrder(order, signature);

                status = $"{Convert.ToInt32(HttpStatusCode.OK)} {HttpStatusCode.OK.ToString()}";

            }
            catch (System.Exception ex)
            { 
                response = new ResponseNonQuery(ex);
                status = $"{Convert.ToInt32(HttpStatusCode.BadRequest)} {HttpStatusCode.BadRequest.ToString()}";
            }
            
            watch.Stop();
            var tiempoEjecucion = watch.ElapsedMilliseconds.ToString();

            Logger.LogInformation($"Fin de Inyección de Orden {order.OrderNumber} ");
            Logger.LogInformation($"Tiempo de Ejecución {tiempoEjecucion} milisegundos ");
            Logger.LogInformation(" ".PadLeft(20, '=')); 
          
            var apiLog = new LogApiModel(order.UserSystem) { NUMERO_ORDEN = order.OrderNumber, ESTADO_HTTP = status, MENSAJE = response.Message, TIEMPO = tiempoEjecucion, DATA = JsonConvert.SerializeObject(order) ,SIGNATURE = signature };

            var rows = await Services.GetService<OrderLogic>().WriteLog(apiLog);

            var log = $"{order.OrderID} {status} {response.Message} {tiempoEjecucion}";
            Logger.LogInformation(log);

            return Ok(response);
        }
    }
}
