﻿using HT.CORP.SCHE.ORMG.Order.OrderPicked.Logic;
using HT.CORP.SCHE.ORMG.Order.OrderPicked.Model;
using HT.Data;
using HT.Data.Generic;
using HT.Database;
using HT.Database.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Api.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    [ProducesResponseType(500)]
    [EnableCors("AllowSpecificOrigin")]
    [Route("/rtl/tot/corp/SCHE/ORMG/order/")]
    [ApiController]
    public class orderPickedController : ApiControllerBaseType
    {
        public IConfiguration _configuration;

        public orderPickedController(IServiceProvider services, ILogger<orderPickedController> logger, IDatabase<DbDispatch> dbDispatch, IConfiguration configuration)
            : base(services, logger, dbDispatch)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Create the Purchase Order
        /// </summary>
        /// <param name="order">Business object that represents the Purchase Order</param>
        /// <returns></returns>
        [HttpPost("orderPicked")]
        public async  Task<IActionResult> Post([FromBody]RequestModel order)
        {
            string status = "";          
            IResponseNonQuery response;
            var watch = System.Diagnostics.Stopwatch.StartNew();
            string signature = Guid.NewGuid().ToString().ToLower();
            try
            { 
                order.Body.OrderTypeID = Convert.ToInt32(_configuration.GetSection("IdTipoOC").Value);
                order.Body.UserID =  _configuration.GetSection("IdUsuario").Value;
            
             
                response = await Services.GetService<OrderLogic>().Create(order, signature);
                //Envío de notificación
                //response.Code = 0 ok   response.Code != 0 error en validacion, error controlado                
                if (response.Code != 0)
                {
                    string orden = order.Body.OrderNumber.ToString();
                    await envioNotificacionError(orden, "Ocurrio un error al persistir en BD");
                }                
                status = $"{Convert.ToInt32(HttpStatusCode.OK)} {HttpStatusCode.OK.ToString()}";
            }
            catch (System.Exception ex)
            {
                //Envío de notificación
                //excepcion
                string orden = order.Body.OrderNumber.ToString();
                await envioNotificacionError(orden, ex.Message);

                response = new ResponseNonQuery(ex); 
                status = $"{Convert.ToInt32(HttpStatusCode.BadRequest)} {HttpStatusCode.BadRequest.ToString()}";
            }
          
            watch.Stop();
            var tiempoEjecucion = watch.ElapsedMilliseconds.ToString();
            
            var apiLog = new LogApiModel(order.Body.UserSystem) { NUMERO_ORDEN = order.Body.OrderNumber,ESTADO_HTTP = status, MENSAJE = response.Message, TIEMPO = tiempoEjecucion, DATA = JsonConvert.SerializeObject(order),SIGNATURE = signature };

            var rows = await Services.GetService<OrderLogic>().WriteLog(apiLog);

            var log = $"{order.Body.OrderNumber} {status} {response.Message} {tiempoEjecucion}";
            Logger.LogInformation(log);  

            return Ok(response);
        }

        protected async Task envioNotificacionError(string orden, string error)
        {
            string mensaje = string.Empty;

            try
            {
                Logger.LogInformation(DateTime.Now.ToString("HH:mm:ss") + " - INICIO envío de notificación de error: ID Orden = " + orden);
                string url = _configuration.GetSection("RutaApiNotificacion").Value;
                string response = await Services.GetService<OrderLogic>().EnviarCorreoAPINotificacion(orden, error, "ERROR_TOTTUS_YA", url);
                Logger.LogInformation(DateTime.Now.ToString("HH:mm:ss") + "----Se envía la notificación usando API HT.EmailDelivery");
            }
            catch (Exception ex)
            {
                Logger.LogInformation(DateTime.Now.ToString("HH:mm:ss") + " ----Error = " + ex.Message);
            }
            finally
            {
                Logger.LogInformation(DateTime.Now.ToString("HH:mm:ss") + " - FIN envío de correo en despacho: ID Orden = " + orden);
            }

        }

        [HttpPost("health")]
        public async Task<IActionResult> health()
        {
            object response = null;
            try
            {
                  response = new
                {
                    status = $"{Convert.ToInt32(HttpStatusCode.OK)} {HttpStatusCode.OK.ToString()}"
                };
            }
            catch (Exception ex) {
                  response = new
                {
                    status = $"{HttpStatusCode.BadRequest} {HttpStatusCode.BadRequest.ToString()}"
                };
            }

            return Ok(response);
        }
    }
}
