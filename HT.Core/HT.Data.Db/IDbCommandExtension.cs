﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace HT.Data.Db
{
    public static class IDbCommandExtension
    {
        public static IDataParameter CreateParameter(this IDbCommand dbCommand, string parameterName, DbType dbType) =>
            dbCommand.CreateParameter(parameterName, dbType, -1, null);

        public static IDataParameter CreateParameter(this IDbCommand dbCommand, string parameterName, DbType dbType, int size) =>
            CreateParameter(dbCommand, parameterName, dbType, size, null, ParameterDirection.Input);

        public static IDataParameter CreateParameter(this IDbCommand dbCommand, string parameterName, DbType dbType, int size, object value) =>
            CreateParameter(dbCommand, parameterName, dbType, size, value, ParameterDirection.Input);

        public static IDataParameter CreateParameter(this IDbCommand dbCommand, string parameterName, DbType dbType, int size, object value, ParameterDirection direction)
        {
            IDbDataParameter dbParameter = dbCommand.CreateParameter();

            dbParameter.ParameterName = parameterName;
            dbParameter.DbType = dbType;
            dbParameter.Size = (size > 0 ? size : -1);
            dbParameter.Value = (value == null ? DBNull.Value : value);
            dbParameter.Direction = direction;

            return dbParameter;
        }

        public static void CreateParameters<T>(this IDbCommand dbCommand, T instance) // where T : new()
        {
            var type = typeof(T);

            IEnumerable<PropertyInfo> properties = type.GetProperties();

            if (properties != null)
            {
                IDataParameter dbParameter;

                foreach (var property in properties)
                {
                    var member = property.GetCustomAttribute<DbParameterAttribute>();

                    if (member != null)
                    {
                        object value = property.GetValue(instance);

                        EvaluateAllowedValues(property, member, value);

                        dbParameter = dbCommand.CreateParameter(member.ParameterName, member.DbType, member.Size);

                        dbParameter.Direction = member.Direction;

                        if (value == null) value = DBNull.Value;

                        dbParameter.Value = value;

                        dbCommand.Parameters.Add(dbParameter);
                    }
                }
            }
        }

        private static void EvaluateAllowedValues(PropertyInfo property, DbParameterAttribute dbParameterAttribute, object value)
        {
            object[] allowedValues = dbParameterAttribute.AllowedValues;

            bool hasAllowedValues = (allowedValues != null && allowedValues.Length > 0);

            if (hasAllowedValues)
            {
                bool valueIsAllowed = allowedValues.Contains(value);

                if (!valueIsAllowed)
                {
                    string name = property.Name;

                    throw new System.Exception($"Invalid value: {dbParameterAttribute.ParameterName}.\r\n{name} allows the following values: {string.Join<object>(", ", allowedValues)}.");
                }
            }
        }
    }
}
