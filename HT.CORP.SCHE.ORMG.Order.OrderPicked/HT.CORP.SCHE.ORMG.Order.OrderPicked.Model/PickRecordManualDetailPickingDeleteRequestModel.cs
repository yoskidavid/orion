﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("[DBO].[PICK_REGISTROMANUAL_DETALLE_PICKING_DEL_NEW]")]
    public class PickRecordManualDetailPickingDeleteRequestModel : ModelChild<OrderRequestModel>
    {
        public PickRecordManualDetailPickingDeleteRequestModel()
            : this(null)
        { }

        public PickRecordManualDetailPickingDeleteRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [DbParameter("@ID_DET_PICKINGS", DbType = DbType.String)]
        public string ResultIDs => string.Empty;
    }
}
