﻿using System;

namespace HT.Data.Generic
{
    public static class IModelExtension
    {
        public static IModelChildren<TParent, TChild> EnsureInstance<TParent, TChild>(this TParent parent, ref IModelChildren<TParent, TChild> children)
            where TChild : IModelChild<TParent>
        {
            if (children == null)
            {
                children = new ModelChildren<TParent, TChild>(parent);
            }

            return children;
        }

        public static TChild EnsureInstance<TParent, TChild>(this TParent parent, ref TChild child)
            where TChild : IModelChild<TParent>
        {
            if (child == null)
            {
                child = (TChild)Activator.CreateInstance(typeof(TChild), parent);
            }

            return child;
        }

        public static object EnsureInstance<TParent>(this TParent parent, ref object child)
        {
            if (child == null)
            {
                child = Activator.CreateInstance(child.GetType(), parent);
            }

            return child;
        }
    }
}
