﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HT.Order.Logic
{
    public interface IOrderRepository
    {
        Task<TipoOrdenDTO> GetOrderTypeAsync(long idOc);
    }
}
