﻿using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    public class MailRecipientResponseModel
    {
        [DbColumn(Order = 0, Name = "ID_Notif")]
        public int IDNotification { get; set; }

        [DbColumn(Order = 1, Name = "Correlativo")]
        public int Correlative { get; set; }

        [DbColumn(Order = 2, Name = "Correo")]
        public string Email { get; set; }

        [DbColumn(Order = 3, Name = "Nombre")]
        public string Name { get; set; }

        [DbColumn(Order = 4, Name = "Prioridad")]
        public short Priority { get; set; }
    }
}
