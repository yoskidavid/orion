﻿using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HT.Shared.Logic
{
    public class UbigeoLogic : BaseType
    {
        private static IEnumerable<Model.UbigeoResponseModel> UBIGEO;

        public UbigeoLogic(IServiceProvider services, ILogger<UbigeoLogic> logger, IDatabase<DbDispatch> dbDispatch)
            : base(services, logger)
        {
            DbDispatch = dbDispatch;
        }

        protected IDatabase DbDispatch { get; set; }

        private void Ensure()
        {
            if (object.Equals(UBIGEO, null))
            {
                var request = new Model.UbigeoRequestModel();

                UBIGEO = DbDispatch.ExecuteList<Model.UbigeoRequestModel, Model.UbigeoResponseModel>(request);
            }
        }

        public IEnumerable<Model.UbigeoResponseModel> List()
        {
            Ensure();

            return UBIGEO;
        }

        public Model.UbigeoResponseModel GetByNames(string departmentName, string provinceName, string comunaName)
        {
            Ensure();

            var provinces = UBIGEO.Where(x => x.NOMBREDEPARTAMENTO.Equals(departmentName, StringComparison.InvariantCultureIgnoreCase));

            if (Equals(provinces, null) || provinces.Count().Equals(0))
            {
                throw new System.Exception("Departamento no está registrado en Ubigeo.");
            }

            var comunas = provinces.Where(x => x.NOMBREPROVINCIA.Equals(provinceName, StringComparison.InvariantCultureIgnoreCase));

            if (Equals(comunas, null) || comunas.Count().Equals(0))
            {
                throw new System.Exception("Provincia no está registrado en Ubigeo o no corresponde al departamento.");
            }

            var ubigeo = comunas.FirstOrDefault(x => x.NOMBREDISTRITO.Equals(comunaName, StringComparison.InvariantCultureIgnoreCase));

            if (Equals(ubigeo, null))
            {
                throw new System.Exception("Comuna no está registrado en Ubigeo o no corresponde a la provincia.");
            }

            return ubigeo;
        }
    }
}
