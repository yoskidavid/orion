﻿using HT.Configuration;
using HT.CORP.SCHE.ORMG.Order.OrderPicked.Logic;
using HT.Shared.Logic;
using HT.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
//using Swashbuckle.AspNetCore.Swagger;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddOptions();

            services
                .AddCors(options =>
                {
                    options.AddPolicy("AllowSpecificOrigin",
                        builder =>
                            builder.WithOrigins("*")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowAnyOrigin()
                            .AllowCredentials());
                })
                .AddSingleton<ILoggerFactory, LoggerFactory>()
                .AddSingleton(typeof(ILogger<>), typeof(Logger<>))
                .AddTransient<IOrderPickedRepository, OrderPickedRepository>()
                .AddDatabases()
                .AddSharedLogic()
                .AddOrderPickedLogic();

            services.Configure<DbSettings>(Configuration);

            // Register the Swagger generator, defining 1 or more Swagger documents
            /*services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "API Order Picked",
                    Description = "Business object that represents the Order Picked",
                    Contact = new Contact
                    {
                        Name = "NEKSYS",
                        Email = "mreyna@neksysse.com",
                        Url = "http://www.neksysse.com"
                    },
                    License = new License
                    {
                        Name = "Tottus",
                        //Url = "https://--/license"
                    }
                });

                // Configure Swagger to use the xml documentation file
                var xmlFile = Path.ChangeExtension(typeof(Startup).Assembly.Location, ".xml");
                c.IncludeXmlComments(xmlFile);
            });*/
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowSpecificOrigin");

            //app.UseStaticFiles();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            //app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Order Picked");
            //    c.RoutePrefix = string.Empty;
            //});

            app.UseMvc();
        }
    }
}
