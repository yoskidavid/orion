﻿using HT.Data.Db;
using System.Runtime.Serialization;

namespace HT.Dashboard.Model
{
    [DataContract]
    public class SucursalResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "nombre")]
        public string nombre { get; set; }
    }
}