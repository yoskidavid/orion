﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Text;
using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DbCommand("WS_SOL_ELIMINAR_OC")]
    public class OrderDeleteModelRequest
    {
        [DataMember]
        [DbParameter("@NRO_OC", DbType = DbType.String, Size = 25)]
        public string NRO_OC { get; set; }

        [DataMember]
        [DbParameter("@ERROR_CODIGO", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int ERROR_CODIGO { get; set; }
        [DataMember]
        [DbParameter("@ERROR_MENSAJE", DbType = DbType.String, Direction = ParameterDirection.Output, Size = 250)]
        public string ERROR_MENSAJE { get; set; }
    }
}
