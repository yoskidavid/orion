﻿using HT.Data.Db;
using HT.Data.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DataContract]
    [DbCommand("ATG_UpdateValidaFolioInserta")]
    public class InsertValidateUpdateRequestModel : ModelChild<OrderRequestModel>
    {
        public InsertValidateUpdateRequestModel()
            : this(null)
        { }

        public InsertValidateUpdateRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [DbParameter("@PIV_NUMEROORDEN", DbType = DbType.String, Size = 20)]
        public string OrderNumber { get => Parent.OrderNumber; }


        [DbParameter("@PII_VALIDAFOLIO", DbType = DbType.Boolean)]
        public bool ValidaFolio { get; set; }
    }
}
