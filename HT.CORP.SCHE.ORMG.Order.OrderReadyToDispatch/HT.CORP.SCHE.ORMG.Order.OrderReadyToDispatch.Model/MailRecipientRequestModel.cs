﻿using HT.Data.Db;
using System.Data;


namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DbCommand("NTF_GLDESTINANOTIFICACIONBYCODIGO")]
    public class MailRecipientRequestModel
    {
        [DbParameter("@PIV_CODIGO", DbType = DbType.String, Size = 20)]
        public string NotificationCode { get; set; }
    }
}
