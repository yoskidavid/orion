﻿using HT.Collections;
using System.Collections;

namespace HT.Data
{
    public interface IResponsePager : IResponse
    {
        IEnumerable Body { get; }

        Pager Pager { get; }

        IResponsePager ToPager();
    }
}
