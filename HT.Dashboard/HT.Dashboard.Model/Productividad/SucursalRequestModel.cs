﻿using HT.Data.Db;
using System.Data;
using System.Runtime.Serialization;

namespace HT.Dashboard.Model
{
    [DataContract]
    [DbCommand("ADM_getSucursalesNombre")]
    public class SucursalRequestModel
    {
        [DataMember(Name = "branchId")]
        [DbParameter("@ID", DbType = DbType.String)]
        public int ID { get; set; }
    }
}