﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Text;
using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("PICK_Insertar_Transaccion")]
    public class PICK_Insertar_Transaccion
    {
        [DbParameter("@ID_OC", DbType = DbType.Int32)]
        public Int32 IdOC { get; set; }

        [DbParameter("@ID_Estado", DbType = DbType.Int32)]
        public Int32 StateID { get; set; }

        [DbParameter("@descripcion", DbType = DbType.String, Size = 500)]
        public string Descripction { get; set; }

        [DbParameter("@ID_Usuario", DbType = DbType.Int32)]
        public Int32 idUser { get; set; }
    }
}
