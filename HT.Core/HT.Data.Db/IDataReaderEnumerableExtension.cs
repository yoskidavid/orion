﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace HT.Data.Db
{
    public static class IDataReaderEnumerableExtension
    {
        public static IEnumerable<T> ReadEnumerable<T>(this IDataReader reader) =>
            ((DbDataReader)reader).ReadEnumerable<T>();

        public static IEnumerable<T> ReadEnumerable<T>(this DbDataReader reader) =>
            ((DbDataReader)reader).ReadEnumerable<T>(null);

        public static IEnumerable<T> ReadEnumerable<T>(this IDataReader reader, ExecuteReaderDelegate<T> @delegate) =>
            ((DbDataReader)reader).ReadEnumerable<T>(@delegate);

        public static IEnumerable<T> ReadEnumerable<T>(this DbDataReader reader, ExecuteReaderDelegate<T> @delegate)
        {
            if (reader.HasRows)
            {
                T entity;

                while (reader.Read())
                {
                    entity = reader.GetEntity<T>();

                    if (object.Equals(@delegate, null))
                    {
                        yield return entity;
                    }
                    else
                    {
                        yield return @delegate(reader, entity);
                    }
                }
            }
        }
    }
}
