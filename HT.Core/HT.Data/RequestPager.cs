﻿using HT.Collections;
using System;
using System.Runtime.Serialization;

namespace HT.Data
{
    [DataContract]
    public class RequestPager : Request, IRequestPager
    {
        public RequestPager(Type type) :
            base(type)
        {
            AllowPaging = false;
        }

        [DataMember]
        public bool AllowPaging { get; set; }

        [DataMember]
        public PagerSettings PagerSettings
        {
            get
            {
                EnsureInnerPagerSettings();

                return InnerPagerSettings;
            }
        }

        private PagerSettings InnerPagerSettings { get; set; }

        private void EnsureInnerPagerSettings()
        {
            if (Equals(InnerPagerSettings, null))
            {
                InnerPagerSettings = new PagerSettings();
            }
        }
    }
}
