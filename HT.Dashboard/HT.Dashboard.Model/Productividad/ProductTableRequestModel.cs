﻿using HT.Data.Db;
using System.Data;
using System.Runtime.Serialization;

namespace HT.Dashboard.Model
{
    [DataContract]
    [DbCommand("WS_productividad_tabla")]
    public class ProductTableRequestModel
    {
        [DataMember(Name = "branchId")]
        [DbParameter("@ID_Sucursal", DbType = DbType.Int32)]
        public int BranchID { get; set; }
    }
}