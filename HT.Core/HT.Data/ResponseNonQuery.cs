﻿using System.Runtime.Serialization;

namespace HT.Data
{
    [DataContract]
    public class ResponseNonQuery : Response, IResponseNonQuery
    {
        public ResponseNonQuery(int recordsAffected) :
            base(null)
        {
            RecordsAffected = recordsAffected;
        }

        public ResponseNonQuery(System.Exception exception) :
            base(exception)
        { }

        [IgnoreDataMember]
        public int Duration { get; private set; }

        [IgnoreDataMember]
        public int RecordsAffected { get; private set; }
    }
}
