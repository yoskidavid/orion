﻿using System.Runtime.Serialization;
using HT.Data.Db;

namespace HT.Dashboard.Model.MonitorATG
{
    [DataContract]
    public class OrdenATGPendingResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "ORDER_ID")]
        public string nroOC { get; set; }
    }
}