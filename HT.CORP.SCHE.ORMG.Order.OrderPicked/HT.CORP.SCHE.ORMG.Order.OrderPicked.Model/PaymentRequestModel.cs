﻿using HT.Data.Db;
using HT.Data.Generic;
using System;
using System.Data;
using System.Runtime.Serialization;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract(Name = "Payment")]
    [DbCommand("[DBO].[ORD_INSERTBOLETAORDEN_F]")]
    public class PaymentRequestModel : ModelChild<OrderRequestModel>
    {
        public PaymentRequestModel()
            : this(null)
        { }

        public PaymentRequestModel(OrderRequestModel parent)
            : base(parent)
        { }

        [IgnoreDataMember]
        [DbParameter("@PII_ID_OC", DbType = DbType.Int32)]
        public int? OrderID => Parent.OrderID;

        [IgnoreDataMember]
        [DbParameter("@pii_Numero_Boleta", DbType = DbType.Int32)]
        public int ReceiptNumber { get; set; }
        
        [IgnoreDataMember]
        [DbParameter("@PIV_TIPOVENTA", DbType = DbType.String, Size = 20)]
        public string SaleType => "I";

        // B:BOLETA, F:FACTURA
        [DataMember]
        [DbParameter("@PIV_TIPOCOMPROBANTE", DbType = DbType.String, Size = 1)]
        public string ReceiptType { get; set; }

        [DataMember]
        public DateTime TransactionDate { get; set; }

        [DataMember] 
        public decimal OriginalAmount { get; set; }

        [DataMember]
        [DbParameter("@monto_total", DbType = DbType.Decimal)]
        public decimal TotalAmount { get; set; }

        [DataMember]
        public decimal TotalPriceDispatch { get; set; }

        [DataMember]
        [DbParameter("@PIV_TIPOPAGO", DbType = DbType.String, Size = 25)]
        public string PaymentType { get; set; }

        [DataMember]
        [DbParameter("@PIV_TIPOTARJETA", DbType = DbType.String, Size = 25)]
        public string Payment { get; set; }

        [DataMember]
        [DbParameter("@PIV_CONTRAENTREGA", DbType = DbType.String, Size = 30)]
        public string PaymentMethod { get; set; }

        [DataMember]
        [DbParameter("@PIV_NUMTARJETA", DbType = DbType.String, Size = 30)]
        public string CardNumber { get; set; }

        [DataMember]
        [DbParameter("@PIV_ETICKET", DbType = DbType.String, Size = 50)]
        public string Eticket { get; set; }

        [DataMember]
        [DbParameter("@PIV_ORDENVISA", DbType = DbType.String, Size = 30)]
        public string OrderVisa { get; set; }

        [DataMember]
        [DbParameter("@PII_NUMEROCUOTAS", DbType = DbType.Int32)]
        public int Installments { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PIV_DIFERIDO", DbType = DbType.String, Size = 1)]
        public char Deferrer => '0';

        [IgnoreDataMember]
        [DbParameter("@PII_MESDIFERIDO", DbType = DbType.Int32)]
        public int DeferredMonth => '0';

        [DataMember(Name = "operationId")]
        [DbParameter("@PIV_ID_OPERACION", DbType = DbType.String, Size = 20)]
        public string OperationID { get; set; }

        [DataMember(Name = "AuthorizationCod")]
        [DbParameter("@PIV_CODIGOAUTORIZACION", DbType = DbType.String, Size = 50)]
        public string AuthorizeCode { get; set; }

        public string AuthorizeCodeChange
        {
            get
            {
                if (AuthorizeCode == null || string.IsNullOrEmpty(AuthorizeCode.ToString()))
                {
                    AuthorizeCode = Parent.OrderNumber;
                }

                return AuthorizeCode;
            }
        }

        [IgnoreDataMember]
        [DbParameter("@PIV_CONTADO", DbType = DbType.String, Size = 1)]
        public char IsCash => '0';

        [DataMember(Name = "transactionId")]
        [DbParameter("@PIV_TRANSACTIONID", DbType = DbType.String, Size = 50)]
        public string TransactionID { get; set; }

        [DataMember(Name = "preAuthorizationCod")]
        [DbParameter("@PIV_CODPREAUTORIZACION", DbType = DbType.String, Size = 50)]
        public string PreAuthorizationCode { get; set; }

        [DataMember(Name = "quickpayId")]
        [DbParameter("@PIV_QUICKPAYID", DbType = DbType.String, Size = 50)]
        public string QuickpayID { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PII_ANOVENCTARJ", DbType = DbType.Int32)]
        public int ExpirationYear => 1900;

        [IgnoreDataMember]
        [DbParameter("@PII_MESVENCTARJ", DbType = DbType.Int32)]
        public int? ExpirationMonth => 0;

        [DataMember]
        [DbParameter("@PIV_MONEDAPAGO", DbType = DbType.String, Size = 10)]
        public string Currency { get; set; }

        [IgnoreDataMember]
        [DbParameter("@PID_FECHAPAGO", DbType = DbType.Date)]
        public DateTime PaymentDate => DateTime.Now;

        [IgnoreDataMember]
        [DbParameter("@PIV_ESTADOCOMPROBANTE", DbType = DbType.String, Size = 25)]
        public string Status => "01";

        [DataMember(Name = "transactionIdVisanet")]
        [DbParameter("@transactionIdVisanet", DbType = DbType.String, Size = 100)]
        public string transactionIdVisanet { get; set; }

        [DataMember(Name = "signatureVisanet")]
        [DbParameter("@signatureVisanet", DbType = DbType.String, Size = 100)]
        public string signatureVisanet { get; set; }

        [DataMember(Name = "capture_token")]
        [DbParameter("@capture_token", DbType = DbType.String, Size = 200)]
        public string Capture_token { get; set; } 

        [DataMember(Name = "capture_url")]
        [DbParameter("@capture_url", DbType = DbType.String, Size = 200)]
        public string Capture_url { get; set; }
    }
}
