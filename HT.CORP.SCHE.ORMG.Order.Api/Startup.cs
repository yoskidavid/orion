using HT.Configuration;
using HT.Database;
using HT.Order.Logic;
using HT.Order.Logic.Data;
using HT.Shared.Logic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;

namespace HT.Order.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddOptions();
            services
                .AddCors(options =>
                {
                    options.AddPolicy("AllowSpecificOrigin",
                        builder =>
                            builder.WithOrigins("*")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowAnyOrigin()
                            .AllowCredentials());
                })
                .AddSingleton<ILoggerFactory, LoggerFactory>()
                .AddSingleton(typeof(ILogger<>), typeof(Logger<>))
                .AddSingleton<IComprobanteRepository, ComprobanteRepository>()
                .AddSingleton<IEmailQueueRepository, EmailQueueRepository>()
                .AddSingleton<IOrderRepository, OrderRepository>()
                .AddDatabases()
                .AddSharedLogic()
                .AddOrderLogic();

            services.Configure<DbSettings>(Configuration);

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "API Order",
                    Description = "Contiene la inserción de comprobante y el cambio de estado de pedido",
                    Contact = new Contact
                    {
                        Name = "Sebastian Sancho",
                        Email = "ssancho@neksysse.com",
                        Url = "http://www.neksysse.com"
                    },
                    License = new License
                    {
                        Name = "Tottus"
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowSpecificOrigin");

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("swagger/v1/swagger.json", "API Order V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseMvc();
        }
    }
}
