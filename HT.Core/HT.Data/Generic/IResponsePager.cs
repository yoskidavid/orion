﻿using System.Collections.Generic;

namespace HT.Data.Generic
{
    public interface IResponsePager<T> : IResponse, IResponsePager
    {
        new IEnumerable<T> Body { get; }

        new IResponsePager<T> ToPager();
    }
}
