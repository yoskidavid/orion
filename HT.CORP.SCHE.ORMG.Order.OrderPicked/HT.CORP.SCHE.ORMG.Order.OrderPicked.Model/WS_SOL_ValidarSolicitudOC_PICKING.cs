﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Text;
using HT.Data.Db;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Model
{
    [DataContract]
    [DbCommand("WS_SOL_ValidarSolicitudOC_PICKING")]
    public class WS_SOL_ValidarSolicitudOC_PICKING
    {
        [DataMember]
        [DbParameter("@NUMERO_ORDEN", DbType = DbType.String, Size = 25)]
        public string NUMERO_ORDEN { get; set; }
        [DataMember]
        [DbParameter("@ERROR_CODIGO", DbType = DbType.Int32, Direction = ParameterDirection.Output)]
        public int COD_INTERNO { get; set; }
        [DataMember]
        [DbParameter("@ERROR_MENSAJE", DbType = DbType.String,Direction = ParameterDirection.Output, Size = 250)]
        public string VALOR1 { get; set; }
    }
}
