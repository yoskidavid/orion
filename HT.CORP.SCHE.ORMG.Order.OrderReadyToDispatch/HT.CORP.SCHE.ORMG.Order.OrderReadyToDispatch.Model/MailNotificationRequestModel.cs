﻿using HT.Data.Db;
using System.Data;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model
{
    [DbCommand("NTF_GETNOTIFICACIONBYCODIGO")]
    public class MailNotificationRequestModel
    {
        [DbParameter("@PIV_CODIGO", DbType = DbType.String, Size = 20)]
        public string Code { get; set; }
    }
}
