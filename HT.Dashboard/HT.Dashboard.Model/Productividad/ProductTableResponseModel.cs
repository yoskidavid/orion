﻿using HT.Data.Db;
using System.Runtime.Serialization;

namespace HT.Dashboard.Model
{
    [DataContract]
    public class ProductTableResponseModel
    {
        [DataMember]
        [DbColumn(Order = 0, Name = "Rut")]
        public string Rut { get; set; }

        [DataMember]
        [DbColumn(Order = 1, Name = "nombre")]
        public string nombre { get; set; }

        [DataMember]
        [DbColumn(Order = 2, Name = "productos")]
        public double productos { get; set; }

        [DataMember]
        [DbColumn(Order = 3, Name = "inicio")]
        public string inicio { get; set; }

        [DataMember]
        [DbColumn(Order = 4, Name = "bultos")]
        public double bultos { get; set; }

        [DataMember]
        [DbColumn(Order = 5, Name = "jabas")]
        public double jabas { get; set; }

        [DataMember]
        [DbColumn(Order = 6, Name = "color")]
        public string color { get; set; }
    }
}