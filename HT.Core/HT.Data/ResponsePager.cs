﻿using HT.Collections;
using System;
using System.Collections;
using System.Runtime.Serialization;

namespace HT.Data
{
    [DataContract]
    public class ResponsePager : Response, IResponsePager
    {
        public ResponsePager(IPagedEnumerable paged) :
            base(null)
        {
            IsPaged = false;
            Paged = paged;
        }

        public ResponsePager(Exception exception) :
            base(exception)
        { }

        [DataMember]
        public IEnumerable Body
        {
            get
            {
                if (IsPaged.Equals(true))
                {
                    return InnerPaged;
                }
                else if (Paged == null)
                {
                    return null;
                }
                else
                {
                    return Paged;
                }
            }
        }

        [DataMember]
        public Pager Pager
        {
            get
            {
                if (Paged == null)
                {
                    return null;
                }
                else
                {
                    return Paged.Pager;
                }
            }
        }

        private bool IsPaged { get; set; }

        private IList InnerPaged { get; set; }

        private IPagedEnumerable Paged { get; set; }

        public IResponsePager ToPager()
        {
            InnerPaged = new ArrayList();

            foreach (var item in Paged)
            {
                InnerPaged.Add(item);
            }

            return this;
        }
    }
}
