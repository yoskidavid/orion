﻿using HT.Database;
using HT.Database.Generic;
using Microsoft.Extensions.Logging;
using System;

namespace HT.CORP.SCHE.ORMG.Order.OrderPicked.Logic
{
    public abstract class OrderPickerLogicBaseType : BaseType
    {
        protected OrderPickerLogicBaseType(ILogger logger, IDatabase<DbDispatch> dbDispatch) :
            this(null, logger, dbDispatch)
        { }

        protected OrderPickerLogicBaseType(IServiceProvider services, ILogger logger, IDatabase<DbDispatch> dbDispatch) :
            base(services, logger)
        {
            this.DbDispatch = dbDispatch;
        }

        public IDatabase DbDispatch
        {
            get;
            private set;
        }
    }
}
