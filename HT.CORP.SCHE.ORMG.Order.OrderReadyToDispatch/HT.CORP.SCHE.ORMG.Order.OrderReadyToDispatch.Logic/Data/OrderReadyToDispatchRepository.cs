﻿using HT.Shared.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Model;
using HT.Configuration;
using Microsoft.Extensions.Options;

namespace HT.CORP.SCHE.ORMG.Order.OrderReadyToDispatch.Logic
{
    public class OrderReadyToDispatchRepository : IOrderReadyToDispatchRepository
    {
        public IDbConnection Connection
        {            
            get
            {
                return new SqlConnection(Options.Value.Connections["Dispatch"].ConnectionString);
            }
        }

        public IOptions<DbSettings> Options { get; }

        public OrderReadyToDispatchRepository(IOptions<DbSettings> options)
        {
            Options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public async Task<ParameterResponseModel> GetAsync(string code)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var result = 
                    await conn.QueryFirstOrDefaultAsync<ParameterResponseModel>("ADM_GETPARAMETRO", new { PIV_CODIGO = code }, commandType: CommandType.StoredProcedure);

                return result;
            }
        }



        public async Task<ExistsOrderResponseModel> ExistsOrder(string order)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open(); 

                var result =
                        await conn.QueryFirstOrDefaultAsync<ExistsOrderResponseModel>("ATG_GETEXISTEORDEN_F",new { piv_NumeroOrden = order },commandType:CommandType.StoredProcedure);
                
                return result;
            }
        }
        


        public async Task<IEnumerable<UbigeoResponseModel>> GetUbigeos()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var result =
                        await conn.QueryAsync<UbigeoResponseModel>("SP_SEL_UBIGEO", commandType: CommandType.StoredProcedure); 
                return result;
            }
        }

        public async Task<IEnumerable<TablaGeneralResponseModel>> GetDataFromTablaGeneral(int pFLAG,string pCOD_TABLA)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                
                var request = new TablaGeneralRequestModel(pFLAG, pCOD_TABLA);

                var result =
                        await conn.QueryAsync<TablaGeneralResponseModel>("ADM_lisTABLAGENERAL", request , commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<ParameterResponseModel> GetParameterValue(string parametro)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var result =
                        await conn.QueryFirstOrDefaultAsync<ParameterResponseModel>("ADM_GETPARAMETRO", new { PIV_CODIGO = parametro }, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<Int32> SaveInOrderQueue(OrderQueueModel orderQueue)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                 
                var rowsAffected =
                        await conn.ExecuteAsync("InsertOrderQueue", orderQueue, commandType: CommandType.StoredProcedure);
                 
                return rowsAffected;
            }
        }


        public async Task<int> SaveInLog(LogApiModel logApiModel)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var rowsAffected =
                        await conn.ExecuteAsync("WS_SOL_LOG_API", logApiModel, commandType: CommandType.StoredProcedure);

                return rowsAffected;
            }
        }
 
    }
}
