﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HT.Order.Model
{
    public sealed class EmailQueueCode
    {
        public const string EmailTemplateCode = "COMPROBANTE";
        public const string EmailTemplateCode_SD = "COMPROBANTE_SD";
        public const string EmailTemplateFamilyName = "COMPROBANTE";
    }
}
