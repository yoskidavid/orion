﻿using Microsoft.Extensions.DependencyInjection;

namespace HT.Shared.Logic
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddSharedLogic(this IServiceCollection services)
        {
            services.AddTransient<ParameterLogic>();
            services.AddTransient<UbigeoLogic>();
            services.AddTransient<TablaGeneralLogic>();
            return services;
        }
    }
}
